package Repository;

import Common.Utils.Utils;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Vector;

public class EmailMessage implements Serializable {
    public String subject = ""; //тема письма
    public String text = ""; //текст письма
    public Vector<String> targets = new Vector<>(); //адресаты
    public LinkedHashMap<String, byte[]> files = new LinkedHashMap<>(); //вложения.

    public EmailMessage() {
    }

    public EmailMessage(String subject_in, String text_in, Vector<String> targets_in){
        subject = subject_in;
        text = text_in;
        targets.addAll(targets_in);
    }
    public void addAttachement(File f) throws Exception{
        files.put(f.getName(), Utils.packFile(f));
    }
}
