package Repository.BugReport;

import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;
import javafx.util.Pair;

import java.awt.*;
import java.io.Serializable;

public enum BugReportState implements Serializable, StatusEnum {
    active,
    closed,
    draft;

    @Override
    public Font getFont() {
        switch (this) {
            case active:
                return Current.getTheme().Fonts.get(VisualiserFonts.BadState);
            case closed:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            default:
                return StatusEnum.super.getFont();
        }
    }

    public String getDescription() {
        switch (this) {
            case draft:
                return "черновик";
            case active:
                return "открыт";
            case closed:
                return "закрыт";
            default:
                return StatusEnum.super.getDescription();
        }
    }

}
