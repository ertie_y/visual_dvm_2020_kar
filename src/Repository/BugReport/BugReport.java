package Repository.BugReport;

import Common.Current;
import Common.Database.DBObject;
import Common.Global;
import Common.UI.UI;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import GlobalData.Recipient.DBRecipient;
import Repository.Component.ComponentType;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Properties.PropertyName;
import com.sun.org.glassfish.gmbal.Description;
import javafx.util.Pair;

import java.awt.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.Vector;
public class BugReport extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String id = "";
    @Description("DEFAULT 0")
    public long date = 0;
    @Description("DEFAULT 0")
    public long change_date;
    public String sender_name = "";
    public String sender_address = "";
    public String project_version = "";
    public long visualiser_version = -1;
    public long sapfor_version = -1;
    public String sapfor_settings = "";
    public String description = "";
    public String comment = "";
    public String targets = "";
    //-
    @Description("DEFAULT ''")
    public String executor = "";
    //-
    public BugReportState state;
    @Description("DEFAULT 0")
    public int percentage = 0;
    //-
    @Description("IGNORE")
    public String descriptionAdditionDraft = "";
    @Description("IGNORE")
    public String commentAdditionDraft = "";
    //-
    public void Actualize(BugReport last) {
        change_date = last.change_date;
        description = last.description;
        comment = last.comment;
        targets = last.targets;
        state = last.state;
        percentage = last.percentage;
    }
    public void print() {
        System.out.println("id=" + id);
        System.out.println("-----------------------");
        System.out.println("description=\n" + Utils.Brackets(description));
        System.out.println("-----------------------");
        System.out.println("comment=\n" + Utils.Brackets(comment));
        System.out.println("-----------------------");
        System.out.println("percentage=" + percentage);
        System.out.println("-----------------------");
    }
    public BugReport() {
    }
    public static String getPackedTargets() {
        Vector<String> selected = new Vector<>();
        for (DBRecipient recipient : Global.db.recipients.Data.values())
            if (recipient.isSelected()) selected.add(recipient.Email);
        return String.join("\n", selected);
    }
    @Override
    public Object getPK() {
        return id;
    }

    public Date getDate() {
        return new Date(date);
    }
    public Date getChangeDate() {
        return new Date(change_date);
    }
    public String getDescriptionHeader() {
        if (description != null) {
            String[] data = description.split("\n");
            return (data.length > 0) ? data[0] : "";
        } else return "";
    }
    public void CheckRecipients() {
        for (DBRecipient recipient : Global.db.recipients.Data.values())
            recipient.Select(targets.contains(recipient.Email));
    }
    public String getRemoteArchiveAddress() {
        return RepositoryServer.home + "Bugs/" + id;
    }
    @Description("IGNORE")
    public File owner=null;

    public boolean CheckNotDraft(TextLog log) {
        if (state.equals(BugReportState.draft)) {
            log.Writeln_("Отчёт об ошибке является черновиком");
            return false;
        }
        return true;
    }
    public String getMailTitlePrefix() {
        return "Ошибка " + Utils.Brackets(id) + ", автор " + Utils.Brackets(sender_name) + " : ";
    }
    public Vector<String> getRecipients(boolean add_defaults) {
        Vector<String> res = new Vector<>();
        String[] data = targets.split("\n");
        for (String a : data)
            if (a.length() > 0)
                res.add(a);

        if (add_defaults) {
           // res.add(Email.full_address); //служебный ящик
            //добавить админов если их там нет.
            Arrays.stream(Global.admins_mails).filter(adm -> !res.contains(adm)).forEach(res::add);
            // и себя
            if (!res.contains(Current.getAccount().email))
                res.add(Current.getAccount().email);
        }
        return res;
    }
    public Vector<String> getRecipients() {
        return getRecipients(true);
    }
    public File[] getAttachements() {
        File[] project_attachements = Current.getProject().getAttachmentsDirectory().listFiles();
        File[] res = new File[project_attachements.length + 1];
        res[0] = this.getArchiveFile();
        for (int i = 0; i < project_attachements.length; ++i)
            res[i + 1] = project_attachements[i];
        return res;
    }
    public boolean CheckDraft(TextLog log) {
        if (!state.equals(BugReportState.draft)) {
            log.Writeln("Отчёт об ошибке не является черновиком. Он уже опубликован");
            return false;
        }
        return true;
    }
    public String getNewMailText() {
        String res = String.join("\n",
                "Описание:", description,
                getPassport()
        );
        return res;
    }
    public String getPassport() {
        return String.join("\n",
                RepositoryServer.separator,
                "Отправитель: " + sender_name,
                "Исполнитель: "+executor,
                "Проект: " + project_version,
                RepositoryServer.separator,
                getSettingsSummary(),
                RepositoryServer.separator);
    }
    public String getSettingsSummary() {
        return
                (Current.HasAccount() ? (Current.getAccount().isAdmin() ? ("Адрес отправителя: " + sender_address + "\n") : "") : "") +
                        "Версия SAPFOR: " + sapfor_version + "\n" +
                        "Версия визуализатора: " + visualiser_version + "\n" +
                        "----------------------------------\n" +
                        sapfor_settings;
    }
    public void Init(String sender_name_in, String sender_address_in, String description_in, String version_in) {
        id = Utils.getDateName("bug");
        sender_name = sender_name_in;
        sender_address = sender_address_in;
        project_version = version_in;
        visualiser_version = Global.visualiser.version;
        sapfor_version = Global.Components.get(ComponentType.Sapfor_F).version;
        sapfor_settings = Global.db.settings.getSapforSettingsText();
        percentage = 0;
        description = description_in;
        date = new Date().getTime();
        change_date = new Date().getTime();
        state = BugReportState.draft;
    }
    public File getArchiveFile() {
        return Paths.get(System.getProperty("user.dir"), "Bugs", id + ".zip").toFile();
    }
    @Override
    public boolean isVisible() {
        return
                state.equals(BugReportState.draft) ||
                        (
                                id.toUpperCase().contains(filterKey.toUpperCase())
                                        && sender_name.toUpperCase().contains(filterSenderName.toUpperCase())
                                        && description.toUpperCase().contains(filterDescription.toUpperCase())
                                        && comment.toUpperCase().contains(filterComment.toUpperCase())
                                        && executor.toUpperCase().contains(filterExecutor.toUpperCase())
                                        && project_version.toUpperCase().contains(filterVersion.toUpperCase())
                                        && (!filterOpenedOnly || state.equals(BugReportState.active))
                                        && (!filterMyOnly || sender_address.equalsIgnoreCase(Current.getAccount().email))
                        );
    }
    public static String filterKey = "";
    public static String filterSenderName = "";
    public static String filterDescription = "";
    public static String filterComment = "";
    public static String filterExecutor = "";
    public static String filterVersion = "";
    public static boolean filterOpenedOnly = false;
    public static boolean filterMyOnly = false;
}
