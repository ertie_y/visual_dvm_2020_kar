package Repository;
import Common.Database.SQLITE.SQLiteDatabase;
import Common.Global;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportsDBTable;
import Repository.Subscribes.SubsribersDBTable;
import Visual_DVM_2021.Properties.PropertyName;

import java.nio.file.Paths;
import java.util.Vector;

public class BugReportsDatabase extends SQLiteDatabase {
    public BugReportsDBTable bugReports;
    public SubsribersDBTable subscribers;
    public BugReportsDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", Global.properties.get(PropertyName.BugReportsDBName)).toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(bugReports = new BugReportsDBTable());
        addTable(subscribers = new SubsribersDBTable());
    }

    @Override
    public void Init() throws Exception {
       DeleteDrafts();
    }
    public void DeleteDrafts() throws Exception{
        Vector<BugReport> drafts = bugReports.getAllDrafts();
        for (BugReport draft: drafts)
            Delete(draft);
    }
}
