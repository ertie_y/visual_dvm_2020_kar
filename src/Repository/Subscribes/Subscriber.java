package Repository.Subscribes;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class Subscriber extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String address = "";
    public Subscriber() {
    }
    public Subscriber(String address_in) {
        address = address_in;
    }
    @Override
    public Object getPK() {
        return address;
    }
}
