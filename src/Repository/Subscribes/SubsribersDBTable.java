package Repository.Subscribes;
import Common.Database.DBTable;
public class SubsribersDBTable extends DBTable<String, Subscriber> {
    public SubsribersDBTable() {
        super(String.class, Subscriber.class);
    }
    @Override
    public String getDataDescription() {
        return "подписчик";
    }
}
