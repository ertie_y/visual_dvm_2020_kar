package Repository.Component.Sapfor;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Repository.Component.OSDComponent;
import Repository.Component.Visualizer_2;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Vector;

public abstract class Sapfor extends OSDComponent {
    public static final int empty_code = -100;
    public static final int canceled_code = -99;
    public static final int invalid_proj_code = -2;

    //<editor-fold desc="компонент">
    @Override
    public void GetVersionInfo() {
        try {
            RunAnalysis("SPF_GetVersionAndBuildDate", -1, "", "");
            Visualizer_2.UnpackVersionInfo(this, getResult());
        } catch (Exception e) {
            Global.Log.PrintException(e);
            UI.Error("Не удалось получить версию компонента " + Utils.DQuotes(getComponentType().getDescription()));
        }
    }

    public abstract String getUpdateCommand();

    public abstract String getRestartCommand();

    @Override
    public void Update() throws Exception {
        super.Update();
        Global.visualizer_2.Command(getUpdateCommand());
        GetVersionInfo();
        ResetAllAnalyses();
    }

    //</editor-fold>
    //--------
    //<editor-fold desc="функционал">
    public String readStat(String src) throws Exception {
        RunAnalysis(
                "SPF_OpenDvmStatistic",
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                src);
        return result;
    }

    public void Restart() throws Exception {
        ResetAllAnalyses();
        Global.visualizer_2.Command(getRestartCommand());
    }

    public void cd(File directory_in) throws Exception {
        if (RunAnalysis("SPF_ChangeDirectory", -1, directory_in.getAbsolutePath(), "") != 0)
            throw new Exception("Sapfor: Не удалось перейти в папку "
                    + Utils.Brackets(directory_in.getAbsolutePath()) +
                    "\n" + "Код возврата: " + getErrorCode());
    }

    public Vector<String> Intrinsics = new Vector<>();
    private int errorCode;
    private String result;
    private String output;
    private String outputMessage;
    private String predictorStats;
    int size;
    int[] sizes;
    public LinkedHashMap<String, String> ModifiedFiles = new LinkedHashMap<>();
    public LinkedHashMap<String, String> OldFiles = new LinkedHashMap<>();

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public String getOutputMessage() {
        return outputMessage;
    }

    public void setOutputMessage(String outputMessage) {
        this.outputMessage = outputMessage;
    }

    public String getPredictorStats() {
        return predictorStats;
    }

    public void setPredictorStats(String predictorStats) {
        this.predictorStats = predictorStats;
    }

    //-
    public static String pack(String... params) {
        StringBuilder res = new StringBuilder();
        for (String param : params)
            res.append(param.length()).append(" ").append(param);
        return res.toString();
    }

    public void decodeString(String runResult) throws Exception {
        int codeIdx = runResult.indexOf(' ');
        if (codeIdx == -1) throw new Exception("Wrong input parameter");
        setErrorCode(Integer.parseInt(runResult.substring(0, codeIdx)));
        int lastCodeIdx = 0, count = 0;
        // for analysis and transformation
        for (int z = 0; z < 4; ++z) {
            lastCodeIdx = codeIdx;
            codeIdx = runResult.indexOf(' ', codeIdx + 1);
            if (codeIdx == -1) throw new Exception("Wrong input parameter");
            count = Integer.parseInt(runResult.substring(lastCodeIdx + 1, codeIdx));
            String sub = runResult.substring(codeIdx + 1, codeIdx + 1 + count);
            if (z == 0) setResult(sub);
            else if (z == 1) setOutput(sub);
            else if (z == 2) setOutputMessage(sub);
            else if (z == 3) setPredictorStats(sub);
            codeIdx += count;
        }
        // for modification
        String file_text = null;
        if (codeIdx + 1 + count < runResult.length())
            for (int z = 0; z < 3; ++z) {
                lastCodeIdx = codeIdx;
                codeIdx = runResult.indexOf(' ', codeIdx + 1);
                if (codeIdx == -1) throw new Exception("Wrong input parameter");
                count = Integer.parseInt(runResult.substring(lastCodeIdx + 1, codeIdx));
                String sub = runResult.substring(codeIdx + 1, codeIdx + 1 + count);
                if (z == 0) {
                    String[] splited = sub.split("\\|");
                    if (splited.length == 0 || sub.length() == 0)
                        size = 0;
                    else {
                        size = splited.length - 1;
                        sizes = new int[splited.length];
                        for (int k = 0; k < size + 1; ++k)
                            sizes[k] = Integer.parseInt(splited[k]);
                    }
                } else if (z == 1) file_text = sub;
                else if (z == 2) {
                    ModifiedFiles.put(Utils.toW(sub), file_text);
                    file_text = null;
                }
                codeIdx += count;
            }
    }

    //-
    public void Command(String request_in) throws Exception {
        setErrorCode(empty_code);
        outputMessage = output = result = predictorStats = "";
        size = 0;
        sizes = null;
        ModifiedFiles.clear();
        //модификации.-------------------------------------------------------------->>>>
        decodeString(Global.visualizer_2.Command(request_in).replace((char) 1, '\n'));
    }

    //-
    public int RunAnalysis(String analysisName,
                           int winHandler,
                           String options,
                           String projName) throws Exception {
        Command("analysis:" + pack(analysisName, options, projName) + winHandler);
        return getErrorCode();
    }

    public void RunTransformation(String transformName,
                                  int winHandler,
                                  String options,
                                  String projName,
                                  String folderName,
                                  String addOpts) throws Exception {
        Command("transformation:" + pack(transformName, options, projName, folderName, addOpts) + winHandler);
    }

    /*
     Модификации:
    SPF_ModifyArrayDistribution (addOpt1_c -> regId, addOpt2_c-> int64_t arrArrs, '|' as delimiter)
    SPF_InlineProcedure (addOpt1_c -> name | file, addOpt2_c-> line)
    */
    public void RunModification(String modifyName, int winHandler, String options, String projName,
                                String folderName, String addOpt1, String addOpt2) throws Exception {
        Command("modification:" + pack(modifyName, options, projName, folderName, addOpt1, addOpt2) + winHandler);
    }

    public void GetIntrinsics() throws Exception {
        Intrinsics.clear();
        if (RunAnalysis("SPF_GetIntrinsics", -1, "", "") >= 0) {
            String[] data = getResult().split(" ");
            Collections.addAll(Intrinsics, data);
        }
    }

    public boolean isIntrinsic(String func_name) {
        return Intrinsics.contains(func_name.toLowerCase());
    }

    //todo рефакторить. отвязать от текущего проекта.
    public void UpdateProjectFiles(boolean mode) throws Exception {
        ResetAllAnalyses();
        Current.getProject().dropLastModification();
        DBProjectFile cuf = null;
        if (Current.HasFile()) {
            cuf = Current.getFile();
            Pass_2021.passes.get(PassCode_2021.CloseCurrentFile).Do();
        }
        if (mode) //модификация
        {
            OldFiles.clear();
            for (String name : ModifiedFiles.keySet()) {
                if (Current.getProject().db.files.Data.containsKey(name)) {
                    File file = Current.getProject().db.files.Data.get(name).file;
                    OldFiles.put(name, Utils.ReadAllText(file));
                    Utils.WriteToFile(file, ModifiedFiles.get(name));
                }
            }
            ModifiedFiles.clear();
        } else //откат.
        {
            if (OldFiles.size() > 0) {
                for (String name : OldFiles.keySet()) {
                    File file = Current.getProject().db.files.Data.get(name).file;
                    Utils.WriteToFile(file, OldFiles.get(name));
                }
                OldFiles.clear();
            } else UI.Info("Сохранение файлов отсутствует.");
        }
        if (cuf != null)
            Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(cuf);
    }

    //</editor-fold>
    public Visual_DVM_2021.Passes.SapforAnalysis getAnalysisByPhase(String phase) {
        for (PassCode_2021 analysis_code : getAnalysesCodes()) {
            Visual_DVM_2021.Passes.SapforAnalysis analysis = (Visual_DVM_2021.Passes.SapforAnalysis) Pass_2021.passes.get(analysis_code);
            if (analysis.phase().equals(phase)) return analysis;
        }
        return null;
    }

    public static PassCode_2021[] getAnalysesCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_ParseFilesWithOrder,
                PassCode_2021.SPF_GetFileLineInfo,
                PassCode_2021.SPF_GetArrayDistributionOnlyRegions,
                PassCode_2021.SPF_GetIncludeDependencies,
                PassCode_2021.SPF_GetGraphLoops,
                PassCode_2021.SPF_GetGraphFunctions,
                PassCode_2021.SPF_GetAllDeclaratedArrays,
                PassCode_2021.SPF_GetArrayDistributionOnlyAnalysis,
                PassCode_2021.SPF_GetArrayDistribution
        };
    }

    public static PassCode_2021[] getLoopsTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_LoopEndDoConverterPass,
                PassCode_2021.SPF_LoopFission,
                PassCode_2021.SPF_LoopUnion
        };
    }

    public static PassCode_2021[] getPrivatesTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_PrivateShrinking,
                PassCode_2021.SPF_PrivateExpansion
        };
    }

    public static PassCode_2021[] getProceduresTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_InlineProcedures,
                PassCode_2021.SPF_InlineProceduresH,
                PassCode_2021.SPF_DuplicateFunctionChains,
                PassCode_2021.SPF_RemoveUnusedFunctions
        };
    }

    public static PassCode_2021[] getDVMTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_RemoveDvmDirectivesToComments,
                PassCode_2021.SPF_RemoveDvmDirectives
        };
    }

    public static PassCode_2021[] getIntervalsTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_CreateIntervalsTree,
                PassCode_2021.SPF_RemoveDvmIntervals
        };
    }

    public static PassCode_2021[] getRegionsTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_ResolveParallelRegionConflicts
        };
    }

    public static PassCode_2021[] getPreparationTransformationsCodes() {
        return new PassCode_2021[]{
                PassCode_2021.SPF_InsertIncludesPass,
                PassCode_2021.SPF_CorrectCodeStylePass,
                PassCode_2021.SPF_ConvertStructures,
                PassCode_2021.SPF_CreateCheckpoints,
                PassCode_2021.SPF_InitDeclsWithZero,
                PassCode_2021.EraseBadSymbols,
                PassCode_2021.CombineFiles,
                PassCode_2021.CopyProject
        };
    }

    public static Vector<PassCode_2021> getAllTransformationsCodes() {
        Vector<PassCode_2021> res = new Vector<>();
        Collections.addAll(res, getLoopsTransformationsCodes());
        Collections.addAll(res, getPrivatesTransformationsCodes());
        Collections.addAll(res, getProceduresTransformationsCodes());
        Collections.addAll(res, getDVMTransformationsCodes());
        Collections.addAll(res, getIntervalsTransformationsCodes());
        Collections.addAll(res, getRegionsTransformationsCodes());
        Collections.addAll(res, getPreparationTransformationsCodes());
        return res;
    }

    public void ResetAllAnalyses() {
        for (PassCode_2021 code : getAnalysesCodes())
            (Pass_2021.passes.get(code)).Reset();
        Pass_2021.passes.get(PassCode_2021.Precompilation).Reset();
        Pass_2021.passes.get(PassCode_2021.SPF_GetGCovInfo).Reset();
        Global.enable_text_changed = false;
        Global.transformationPermission = TransformationPermission.None;
    }
}
