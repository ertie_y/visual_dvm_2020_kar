package Repository.Component.Sapfor;

public enum TransformationPermission {
    None,
    All,
    VariantsOnly
}
