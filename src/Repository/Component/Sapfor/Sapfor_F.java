package Repository.Component.Sapfor;
import Common.Global;
import GlobalData.Settings.SettingName;
import Repository.Component.ComponentType;

import java.io.File;
import java.nio.file.Paths;
public class Sapfor_F extends Sapfor {
    @Override
    public ComponentType getComponentType() {
        return ComponentType.Sapfor_F;
    }
    @Override
    public long getMinimalVersion() {
        return 1594;
    }
    @Override
    public String getAssemblyCommand() {

        return "cd Repo/sapfor/experts/Sapfor_2017/_bin\n" +
                "cmake ../\n" +
                "make -j "  +Global.db.settings.get(SettingName.Kernels).toString() + "\n";
    }
    @Override
    public File getAssemblyFile() {
        return Paths.get(
                Global.RepoDirectory.getAbsolutePath(),
                "sapfor/experts/Sapfor_2017/_bin/Sapfor_F").toFile();
    }
    @Override
    public String getUpdateCommand() {
        return "update_spf: ";
    }
    @Override
    public String getRestartCommand() {
        return "restart: ";
    }
}
