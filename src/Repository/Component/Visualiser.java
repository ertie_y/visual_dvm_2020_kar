package Repository.Component;
import Common.Global;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
public class Visualiser extends Component {
    //-----------------------------------------
    private static Date getClassBuildTime() {
        Date d = null;
        Class<?> currentClass = new Object() {
        }.getClass().getEnclosingClass();
        URL resource = currentClass.getResource(currentClass.getSimpleName() + ".class");
        if (resource != null) {
            if (resource.getProtocol().equals("file")) {
                try {
                    d = new Date(new File(resource.toURI()).lastModified());
                } catch (URISyntaxException ignored) {
                }
            } else if (resource.getProtocol().equals("jar")) {
                String path = resource.getPath();
                d = new Date(new File(path.substring(5, path.indexOf("!"))).lastModified());
            } else if (resource.getProtocol().equals("zip")) {
                String path = resource.getPath();
                File jarFileOnDisk = new File(path.substring(0, path.indexOf("!")));
                try (JarFile jf = new JarFile(jarFileOnDisk)) {
                    ZipEntry ze = jf.getEntry(path.substring(path.indexOf("!") + 2));
                    long zeTimeLong = ze.getTime();
                    Date zeTimeDate = new Date(zeTimeLong);
                    d = zeTimeDate;
                } catch (IOException | RuntimeException ignored) {
                }
            }
        }
        return d;
    }
    @Override
    public String getFileName() {
        return "VisualSapfor.jar";
    }
    @Override
    public String getNewFileName() {
        return "VisualSapfor_new.jar";
    }
    @Override
    public ComponentType getComponentType() {
        return ComponentType.Visualiser;
    }
    //</editor-fold>
    //<editor-fold desc="Методы">
    //</editor-// fold>
    //http://www.seostella.com/ru/article/2012/02/05/formatirovanie-daty-v-java.html
    @Override
    public void GetVersionInfo() {
        version = 885;
        String pattern = "MMM dd yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern, Locale.ENGLISH);
        date_text = df.format(getClassBuildTime());
    }
    @Override
    public void Update() throws Exception {
        super.Update();
        Global.visualizer_2.SendRequest("update: ");
        System.exit(0);
    }
    public File getWorkspace() {
        if (!Global.db.settings.get(SettingName.Workspace).toString().isEmpty()) {
            File workspace = new File(Global.db.settings.get(SettingName.Workspace).toString());
            if (workspace.exists())
                return workspace;
            else
                Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                        SettingName.Workspace, "");
        }
        return Global.ProjectsDirectory;
    }
}