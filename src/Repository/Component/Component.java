package Repository.Component;

import Common.Database.DBObject;
import Common.Global;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import GlobalData.Settings.DBSetting;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.UI.Interface.Loggable;
import javafx.util.Pair;

import java.awt.*;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.LinkedHashMap;

public abstract class Component extends DBObject implements Loggable {
    public String date_text = Global.dateNaN;
    public long version = Utils.Nan;
    public long actual_version = Utils.Nan;
    //--
    public String code = "";
    public String actual_code = "";
    private ComponentState state;

    public abstract ComponentType getComponentType();

    //--
    public String getVersionText() {
        return String.valueOf(version);
    }

    public void CheckIfNeedsUpdateOrPublish() {
        if (actual_version != Utils.Nan) {
            ComponentState new_state =
                    (actual_version > version) ? ComponentState.Needs_update : (
                            (actual_version < version) ? ComponentState.Needs_publish : ComponentState.Actual);
            setState(new_state);
        }
    }

    public void InitialVersionCheck() {
        setState(ComponentState.Undefined);
        if (getFile().exists()) {
            GetVersionInfo();
            if (version == Utils.Nan)
                setState(ComponentState.Unknown_version);
            else if (version < getMinimalVersion()) setState(ComponentState.Old_version);
        } else setState(ComponentState.Not_found);
    }

    public boolean CanBeUpdated() {
        return state != ComponentState.Not_found && state != ComponentState.Unknown_version && state != ComponentState.Old_version;
    }

    public void GetVersionInfo() {
    }

    public void getActualVersion(String v_string) {
        actual_version = Long.parseLong(v_string);
        if (CanBeUpdated())
            CheckIfNeedsUpdateOrPublish();
    }

    public void ReplaceOldFile() throws Exception {
        Utils.delete_with_check(getFile());
        System.out.println("old file removed");
        //-скопировать файл
        Files.move(getNewFile().toPath(), getFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        //удалить новый файл.
        Utils.forceDeleteWithCheck(getNewFile());
        System.out.println("new file removed");
    }

    public void Update() throws Exception {
        if (!getNewFile().setExecutable(true)) throw new Exception("Не удалось разрешить файл\n" +
                getNewFile() +
                "\nна выполнение");
    }

    @Override
    public boolean isVisible() {
        return true;
    }

    @Override
    public Object getPK() {
        return getComponentType();
    }

    public boolean isValidVersion(TextLog Log, String desc) {
        if (version == Utils.Nan) {
            Log.Writeln_("Не определена версия " + desc + " компонента " + Utils.Brackets(getComponentType().getDescription()));
            return false;
        }
        return true;
    }

    public long getMinimalVersion() {
        return 1;
    }

    public String getRemoteHome() {
        return Global.getRepository() + getClass().getSimpleName() + "/";
    }

    public String getVersionRemotePath() {
        return getRemoteHome() + "version.txt";
    }

    public String getChangesRemotePath() {
        return getRemoteHome() + "changes.txt";
    }

    public String getRemoteFilePath() {
        return getRemoteHome() + getFileName();
    }

    public String getHome() {
        return Global.ComponentsDirectory.getAbsolutePath();
    }

    public String getFileName() {
        return "";
    }

    public String getNewFileName() {
        return "";
    }

    public String getBackUpFileName() {
        return getComponentType().toString() + "_" + version;
    }

    public File getFile() {
        return Paths.get(getHome(), getFileName()).toFile();
    }

    public File getNewFile() {
        return Paths.get(getHome(), getNewFileName()).toFile();
    }

    public File getBackUpFile() {
        return Paths.get(Global.BackUpsDirectory.getAbsolutePath(), getBackUpFileName()).toFile();
    }

    public ComponentState getState() {
        return state;
    }

    public void setState(ComponentState state_in) {
        state = state_in;
    }

    public String getAssemblyCommand() {
        return "";
    }

    public File getAssemblyFile() {
        return null;
    }

    @Override
    public String getLogHomePath() {
        return Global.ComponentsDirectory.getAbsolutePath();
    }

    @Override
    public String getLogName() {
        return getComponentType().toString();
    }
    public boolean isNecessary(){return true;}
}