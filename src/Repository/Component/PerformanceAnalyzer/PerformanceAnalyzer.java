package Repository.Component.PerformanceAnalyzer;

import Common.Current;
import Common.Global;
import Common.MessageJtoJ;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Component.Component;
import Repository.Component.ComponentType;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;

public class PerformanceAnalyzer extends Component {
    @Override
    public ComponentType getComponentType() {
        return ComponentType.PerformanceAnalyzer;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public void Update() throws Exception {
        ReplaceOldFile();
        GetVersionInfo();
    }

    @Override
    public boolean isNecessary() {
        return false;
    }

    @Override
    public String getFileName() {
        return "PerformanceAnalyzer.jar";
    }

    @Override
    public String getNewFileName() {
        return "PerformanceAnalyzer_new.jar";
    }

    //<editor-fold desc="серверная часть">
    private int port;
    ServerSocket serverSocket = null;
    Socket client = null;
    Thread process_thread = null;
    Thread server_thread = null;
    private ObjectInputStream in; // поток чтения из сокета
    private ObjectOutputStream out; // поток записи в сокет
    private MessageJtoJ message_out = null;
    private MessageJtoJ message_in = null;

    public void Shutdown() {
        try {
            if (client != null) {
                client.setSoLinger(true, 500);
                client.close();
            }
            if (serverSocket != null) serverSocket.close();
        } catch (Exception ignored) {
        }
    }

    public void ReadMessageIn() throws Exception {
        message_in = (MessageJtoJ) in.readObject();
        System.out.println("message = " + Utils.DQuotes(message_in.getMessage()));
        System.out.println("command = " + Utils.DQuotes(message_in.getCommand()));
    }

    public void SendMessageOut(MessageJtoJ messageJtoJ) throws Exception {
        message_out = messageJtoJ;
        out.writeObject(message_out);
    }

    public void ConvertStatistic() throws Exception {
        message_in = (MessageJtoJ) in.readObject();
        System.out.println("message = " + Utils.DQuotes(message_in.getMessage()));
        System.out.println("command = " + Utils.DQuotes(message_in.getCommand()));
        String message = Current.getSapfor().readStat(message_in.getMessage());
        message_out = new MessageJtoJ(message, message_in.getCommand());
        out.writeObject(message_out);
    }

    public void StartServer(Callable body) throws Exception {
        serverSocket = new ServerSocket(0, 5, InetAddress.getLoopbackAddress());
        setPort(serverSocket.getLocalPort());
        server_thread = new Thread(() -> {
            try {
                client = serverSocket.accept();
                System.out.println("Основной цикл анализатора статистик начат..");
                out = new ObjectOutputStream(client.getOutputStream());
                in = new ObjectInputStream(client.getInputStream());
                //------------------------------------------------------->>;
                body.call();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        server_thread.start();
        System.out.println("Нить сервера запущена");
    }

    @Override
    public void GetVersionInfo() {
        try {
            StartServer(() -> {
                System.out.println("Запрос версии анализатора..");
                ReadMessageIn(); //на версию.
                version = (long) Double.parseDouble(message_in.getMessage());
                ReadMessageIn(); //на дату.
                date_text = message_in.getMessage();
                System.out.println("Завершено");
                return null;
            });
            Utils.performProcess("java", "-jar", Utils.DQuotes(Global.performanceAnalyzer.getFile()), "--port", String.valueOf(getPort()), "--version");
            server_thread.join();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Shutdown();
            System.out.println("FINALIZE");
        }
    }

    public static boolean isActive = false;
    public static Thread main_thread = null;

    public void ServerBody() {
        //1. нить сервера. слушать.
        try {
            StartServer(() -> {
                SendMessageOut(new MessageJtoJ(
                        Current.HasProject()? Current.getProject().getAnalyzerDirectory().getAbsolutePath():Global.PerformanceAnalyzerDirectory.getAbsolutePath(), "StatDirPath"));
                while (true) ConvertStatistic();
            });
            process_thread = new Thread(() -> {
                try {
                    Utils.performProcess("java", "-jar", Utils.DQuotes(Global.performanceAnalyzer.getFile()), "--port", String.valueOf(getPort()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            process_thread.start();
            isActive = true;
            process_thread.join();
            server_thread.join();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            Shutdown();
            System.out.println("FINALIZE");
            isActive = false;
        }
    }

    public void Start() {
        if (isActive) {
            UI.Info("Анализатор уже запущен");
        } else {
            main_thread = new Thread(this::ServerBody);
            main_thread.start();
        }
    }
    //</editor-fold>
}
