package Repository.Component;
import java.io.Serializable;
public enum ComponentType implements Serializable {
    Undefined,
    //внутренние
    Visualiser,  //возможно к нему привязать VisualSapfor.jar
    Visualizer_2,
    SapforOptions,
    ComparsionOptions,
    SapforWrapper, //todo ЗАЧЕМ???
    //---------------------
    //------------------
    Sapfor_F,
    Sapfor_C,
    Instruction,
    PerformanceAnalyzer
    ;
    //------------------
    public String getDescription() {
        String res = "";
        switch (this) {
            case Visualiser:
                return "Визуализатор";
            case Sapfor_F:
                return "Sapfor (Fortran)";
            case Visualizer_2:
                return "Сервер";
            case Instruction:
                return "Инструкция";
            case PerformanceAnalyzer:
                return "Анализатор DVM статистик";
            default:
                return "?";
        }
    }
}
