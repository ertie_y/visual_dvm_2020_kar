package Repository.Component;
import Common.Global;
import Common.Utils.Utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
public class Instruction extends Component {
    @Override
    public String getFileName() {
        return "Instruction.pdf";
    }
    @Override
    public String getNewFileName() {
        return "Instruction_new.pdf";
    }

    @Override
    public boolean isNecessary() {
        return false;
    }

    @Override
    public ComponentType getComponentType() {
        return ComponentType.Instruction;
    }
    public void Update() throws Exception {
        ReplaceOldFile();
        GetVersionInfo();
        actual_version = 1;
    }
    @Override
    public String getVersionText() {
        return code;
    }
    //вызывается в первую очередь
    @Override
    public void getActualVersion(String v_string) {
        actual_code = v_string;
        actual_version = code.isEmpty() ? 1 : ((code.equals(actual_code)) ? 1 : 2);
        if (CanBeUpdated())
            CheckIfNeedsUpdateOrPublish();
    }
    @Override
    public void GetVersionInfo() {
        try {
            version = 1;
            code = Utils.md5Custom(Utils.ReadAllText(getFile()));
            DateFormat df = new SimpleDateFormat("MMM dd yyyy HH:mm:ss", Locale.ENGLISH);
            date_text = df.format(getFile().lastModified());
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
}
