package Repository.Component;

import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;
import javafx.util.Pair;

import java.awt.*;

public enum ComponentState implements StatusEnum {
    Undefined,
    Actual,
    Needs_update,
    Not_found,
    Old_version,
    Needs_publish,
    Unknown_version;

    @Override
    public Font getFont() {
        switch (this) {
            case Actual:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            case Not_found:
            case Unknown_version:
            case Old_version:
                return Current.getTheme().Fonts.get(VisualiserFonts.BadState);
            case Needs_update:
                return Current.getTheme().Fonts.get(VisualiserFonts.ProgressState);
            case Needs_publish:
                return Current.getTheme().Fonts.get(VisualiserFonts.BlueState);
            default:
                return StatusEnum.super.getFont();
        }
    }
    public String getDescription() {
        switch (this) {
            case Actual:
                return  "актуален";
            case Not_found:
                return  "не найден";
            case Old_version:
                return  "устаревшая версия";
            case Needs_update:
                return  "найдено обновление";
            case Needs_publish:
                return  "ожидает публикации";
            case Unknown_version:
                return  "не удалось определить версию";
            default:
                return StatusEnum.super.getDescription();
        }
    }

}
