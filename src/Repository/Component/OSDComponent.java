package Repository.Component;
import Common.Global;
public abstract class OSDComponent extends Component {
    @Override
    public String getFileName() {
        return getComponentType().toString() + (Global.isWindows ? ".exe" : "");
    }
    @Override
    public String getNewFileName() {
        return getComponentType().toString() + "_new" + (Global.isWindows ? ".exe" : "");
    }
}
