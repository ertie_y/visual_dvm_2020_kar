package Repository.Component;
import Common.Current;
import Common.Database.DataSet;
import Common.UI.DataSetControlForm;

import static Common.UI.Tables.TableRenderers.*;

public class ComponentsSet extends DataSet<ComponentType, Component> {
    public ComponentsSet() {
        super(ComponentType.class, Component.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                columns.get(2).setRenderer(RendererMaskedInt);
                columns.get(3).setRenderer(RendererMaskedInt);
                columns.get(5).setRenderer(RendererStatusEnum);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Компонент", "Текущая версия", "Актуальная версия", "Дата сборки", "Статус"};
    }
    @Override
    public Object getFieldAt(Component object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.getComponentType().getDescription();
            case 2:
                return object.version;
            case 3:
                return object.actual_version;
            case 4:
                return object.date_text;
            case 5:
                return object.getState();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.Component;
    }
}
