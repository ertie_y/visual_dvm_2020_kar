package Repository.Component.UI;
import Common.Current;
import Common.Global;
import Common.Passes.Pass;
import Common.Passes.PassCode;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.Account.AccountRole;
import Repository.EmailMessage;
import Repository.Server.RepositoryServer;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;
import java.util.stream.Collectors;

public class ComponentsFields implements DialogFields {
    public JPanel content;
    private JPanel componentsPanel;
    private JButton bPublishComponent;
    private JButton bUpdatesSearch;
    private JButton bResurrect;
    private JButton bShowLog;
    private JButton bInstallFormFolder;
    private JToolBar componentsTools;
    private JButton bUpdate;

    public ComponentsFields() {
        Global.Components.mountUI(componentsPanel);
        //--
        bPublishComponent.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.PublishComponent).Do();
            }
        });
        bUpdatesSearch.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Pass_2021.passes.get(PassCode_2021.GetComponentsActualVersions).Do())
                    Global.RefreshUpdatesStatus();
            }
        });
        bResurrect.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Pass_2021.passes.get(PassCode_2021.ResurrectComponent).Do();
            }
        });
        bShowLog.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.ShowComponentChangesLog).Do();
            }
        });
        bInstallFormFolder.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.InstallComponentFromFolder).Do();
            }
        });
        bUpdate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.UpdateComponent).Do();
            }
        });
    }
    public void CheckPublishButtons() {
        boolean toShow = Current.HasAccount() && Current.getAccount().role.equals(AccountRole.Admin);
        bPublishComponent.setEnabled(toShow);
    }
    @Override
    public Component getContent() {
        return content;
    }
}
