package Repository.Component.UI;
import Common.UI.Windows.Dialog.Dialog;
public class PublishForm extends Dialog<String, PublishFields> {
    public PublishForm() {
        super(PublishFields.class);
    }
    @Override
    public boolean NeedsScroll() {
        return false;
    }
    @Override
    public void ProcessResult() {
        Result = fields.cbNeedsBroadcast.isSelected() ? fields.taBroadcast.getText() : null;
    }
}
