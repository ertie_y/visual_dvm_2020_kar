package Repository.Component.UI;
import Common.UI.Editor.BaseEditor;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class PublishFields implements DialogFields {
    public JPanel content;
    public JCheckBox cbNeedsBroadcast;
    public JTextArea taBroadcast;
    public JCheckBox cbForceMail;

    public PublishFields() {
        cbNeedsBroadcast.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                taBroadcast.setEnabled(cbNeedsBroadcast.isSelected());
            }
        });
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        taBroadcast = new BaseEditor();
    }
    @Override
    public Component getContent() {
        return content;
    }
}
