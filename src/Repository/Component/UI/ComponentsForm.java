package Repository.Component.UI;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Dialog;
import Visual_DVM_2021.Properties.PropertyName;

import java.awt.*;

public class ComponentsForm extends Dialog<Object, ComponentsFields> {
    public ComponentsForm() {
        super(ComponentsFields.class);
    }
    @Override
    public boolean NeedsScroll() {
        return false;
    }
    @Override
    public int getDefaultWidth() {
        return Integer.parseInt(Global.properties.get(PropertyName.ComponentsWindowWidth));
    }
    @Override
    public int getDefaultHeight() {
        return  Integer.parseInt(Global.properties.get(PropertyName.ComponentsWindowHeight));
    }
    @Override
    public void CreateButtons() {
    }
    @Override
    public void Init(Object... params) {
     //   setAlwaysOnTop(true);
        fields.CheckPublishButtons();
        Global.Components.ShowUI();
    }

    @Override
    public void LoadSize() {
        setMinimumSize(new Dimension(650, 250));
        Dimension dimension = new Dimension(getDefaultWidth(), getDefaultHeight());
        setPreferredSize(dimension);
        setSize(dimension);
    }

    @Override
    public void onClose() {
        Global.properties.replace(PropertyName.ComponentsWindowWidth, String.valueOf(getWidth()));
        Global.properties.replace(PropertyName.ComponentsWindowHeight, String.valueOf(getHeight()));
        Global.UpdateProperties();
    }
}
