package Repository.Server;

public enum ServerCode {
    Undefined,
    ReadFile,
    SendFile,
    ReceiveFile,
    //-
    GetComponentsActualVersions,
    //-
    PublishBugReport,
    ActualizeBugReport,
    UpdateBugReportField,
    DeleteBugReport,
    //-
    AddSubscriber,
    DeleteSubscriber,
    Email,
    ReceiveAllArchives,
    //-
    PublishTest,
    DownloadTest,
    DeleteTest,
    //-
    PublishGroup,
    EditGroup,
    DeleteGroup,
    //-
    OK,
    EditTest,
    CompileTest, //скорее отладочный проход. Работа на примере одного теста.
    ActualizeTestsTasks,
    FAIL,
    OLD
}
