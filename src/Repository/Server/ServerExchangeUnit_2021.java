package Repository.Server;

import Common.Utils.Utils;

import java.io.File;
import java.io.Serializable;

public class ServerExchangeUnit_2021 implements Serializable {
    public String codeName;
    public String arg;
    public Serializable object;

    //--------
    public ServerCode getCode() throws Exception {
        return ServerCode.valueOf(codeName);
    }

    //--------
    public ServerExchangeUnit_2021(ServerCode code_in, String arg_in, Serializable object_in) {
        codeName = code_in.toString();
        arg = arg_in;
        object = object_in;
    }

    public ServerExchangeUnit_2021(ServerCode code_in, String arg_in) {
        codeName = code_in.toString();
        arg = arg_in;
        object = null;
    }

    public ServerExchangeUnit_2021(ServerCode code_in) {
        codeName = code_in.toString();
        arg = null;
        object = null;
    }

    //--------
    public void Unpack() throws Exception {
        Utils.unpackFile((byte[]) object, new File(arg));
    }

    public void Unpack(File file) throws Exception {
        Utils.unpackFile((byte[]) object, file);
    }

    public void Print() {
        System.out.println("codeName=" + Utils.Brackets(codeName));
        System.out.println(arg);
    }
}
