package Repository.Server;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.Component.ComponentType;
import Repository.EmailMessage;
import Repository.Subscribes.Subscriber;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;

import java.io.File;
import java.io.Serializable;
import java.util.LinkedHashMap;
//объект через который идет обмен данных с сервером. почтовая единица.
public class ServerExchangeUnit implements Serializable {
    public ServerCode code;
    public String arg;
    public byte[] bytes;
    public BugReport bugReport = null;
    public Group group=null;
    public Subscriber subscriber = null;
    public Exception exception = null;
    public LinkedHashMap<ComponentType, String> actual_versions = new LinkedHashMap<>();
    public EmailMessage message = null;
    public Test test=null;
    //-->>> оптимизированное обновление одиночного поля бага
    public String fieldName = null;
    public ServerExchangeUnit(BugReport bugReport_in, String fieldName_in, Object fieldValue_in) throws Exception {
        code = ServerCode.UpdateBugReportField;
        fieldName = fieldName_in;
        bugReport = new BugReport();
        //баг репорт с выбранным полем и датой.
        bugReport.id = bugReport_in.id;
        BugReport.class.getField(fieldName).set(bugReport, fieldValue_in);
        bugReport.change_date = bugReport_in.change_date;
    }
    //-->>>
    public ServerExchangeUnit(Exception exception_in) {
        code = ServerCode.FAIL;
        exception = exception_in;
    }
    public ServerExchangeUnit(ServerCode code_in, String arg_in) {
        code = code_in;
        arg = arg_in;
    }
    public ServerExchangeUnit(ServerCode code_in, Test test_in, File project_archive) throws Exception{
        code = code_in;
        test = test_in;
        Pack(project_archive);
    }
    public ServerExchangeUnit(ServerCode code_in, Test test_in) throws Exception{
        code = code_in;
        test = test_in;
    }
    public ServerExchangeUnit(ServerCode code_in, Group group_in) throws Exception{
        code = code_in;
        group = group_in;
    }
    public ServerExchangeUnit(ServerCode code_in) {
        code = code_in;
    }
    public ServerExchangeUnit(ServerCode code_in, String arg_in, File file_in) throws Exception {
        code = code_in;
        arg = arg_in;
        Pack(file_in);
    }
    public ServerExchangeUnit(ServerCode code_in, BugReport bugReport_in) {
        code = code_in;
        bugReport = bugReport_in;
    }
    public ServerExchangeUnit(ServerCode code_in, Subscriber subscriber_in) {
        code = code_in;
        subscriber = subscriber_in;
    }
    public ServerExchangeUnit(ServerCode code_in, EmailMessage message_in) {
        code = code_in;
        message = message_in;
    }
    public void Pack(File file) throws Exception {
        bytes = Utils.packFile(file);
    }
    public void Unpack() throws Exception {
        Utils.unpackFile(bytes, new File(arg));
    }
    public void Unpack(File file) throws Exception {
        Utils.unpackFile(bytes, file);
    }
}
