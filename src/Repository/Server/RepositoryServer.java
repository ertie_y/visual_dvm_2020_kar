package Repository.Server;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachineType;
import GlobalData.Tasks.TaskState;
import GlobalData.User.User;
import ProjectData.Project.db_project_info;
import Repository.BugReport.BugReport;
import Repository.BugReportsDatabase;
import Repository.Component.ComponentType;
import Repository.EmailMessage;
import Repository.Subscribes.Subscriber;
import TestingSystem.Group.Group;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.Tasks.TestTask;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.All.ArchivesBackupPass;
import Visual_DVM_2021.Passes.All.UnzipFolderPass;
import Visual_DVM_2021.Passes.All.ZipFolderPass;
import Visual_DVM_2021.Properties.PropertyName;
import javafx.util.Pair;
import org.apache.commons.io.FileUtils;
import sun.misc.Signal;
import sun.misc.SignalHandler;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.*;

public class RepositoryServer {
    //отладочный 7998
    //резервный  7997
    //основной   7995
    //убираем из пропертиес. тот случай когда проще зашить этот параметр в код.

    //тесты
    /*
    public static final int port = 7998 ; //
    public static final String home =  "/home/testuser/_sapfor_x64/"; //Основное рабочее пространство.
    public static final String home = "/home/testusers/Desktop/server_test/";
    */
    //Основа
    public static final int port = 7997 ; //
    public static final String home =  "/home/testuser/_sapfor_x64/";
    //EMAIL------------------------------------
    public static final String Eliminated = "Eliminated";
    public final static String separator = "----------------------------------";
    //режим для "старых" серверов
    public final static boolean update_only = false;

    public static Vector<Pair<Machine, User>> storages =
            new Vector<>(Arrays.asList(
                    new Pair<>(
                            new Machine("titan", "dvmh.keldysh.ru", 22, MachineType.Server),
                            new User("dvmuser1", "mprit_2011"))
                    ,
                    new Pair<>(
                            new Machine("k100", "k100.kiam.ru", 22, MachineType.Server),
                            new User("dvmuser1", "mprit_2011"))
            )
            );
    public static ZipFolderPass zip = new ZipFolderPass();
    public static ArchivesBackupPass backupSession = new ArchivesBackupPass();
    //-
    public static BugReportsDatabase db = new BugReportsDatabase();
    protected ServerExchangeUnit request_old;
    protected ServerExchangeUnit response_old;
    //-
    protected ServerExchangeUnit_2021 request;
    protected ServerExchangeUnit_2021 response;
    //-
    protected ServerCode code;
    protected long count = 0; //для отладки.
    public static FileWriter Log;
    //https://javarush.ru/groups/posts/654-klassih-socket-i-serversocket-ili-allo-server-tih-menja-slihshishjh
    private Socket clientSocket; //сокет для общения
    private ServerSocket server; // серверсокет
    private ObjectInputStream in; // поток чтения из сокета
    private ObjectOutputStream out; // поток записи в сокет
    //-СИГНАЛЫ--------------------------------------------
    SignalHandler signalHandler = new SignalHandler() {
        @Override
        public void handle(Signal signal) {
        }
    };
    //----------------------------------------------------
    protected Thread backUp = new Thread(() -> {
        while (true) {
            try {
                //-------------------------------------
                Calendar rightNow = Calendar.getInstance();
                int year = rightNow.get(Calendar.YEAR);
                int month = rightNow.get(Calendar.MONTH);
                int day = rightNow.get(Calendar.DAY_OF_MONTH);
                int hour = rightNow.get(Calendar.HOUR_OF_DAY);
                int minute = rightNow.get(Calendar.MINUTE);
                if ((hour == Integer.parseInt(Global.properties.get(PropertyName.BackupHour))
                ) && (minute <= Integer.parseInt(Global.properties.get(PropertyName.BackupMinute)))) {

                    //определить имя папки с багом.
                    String backUpName = year + "_" + (month + 1) + "_" + (day);
                    File todayBackUp = Paths.get(System.getProperty("user.dir"), "DataBackUps", backUpName).toFile();
                    File todayBackUpArchive = Paths.get(System.getProperty("user.dir"), "DataBackUps", backUpName + ".zip").toFile();
                    //-
                    File bugsDBBackUp = Paths.get(todayBackUp.getAbsolutePath(), db.getFile().getName()).toFile();
                    File bugsArchives = Paths.get(todayBackUp.getAbsolutePath(), "Bugs.zip").toFile();
                    //-
                    File testsDBBackUp = Paths.get(todayBackUp.getAbsolutePath(), TestingSystem.db.getFile().getName()).toFile();
                    File testsArchives = Paths.get(todayBackUp.getAbsolutePath(), "Tests.zip").toFile();
                    //-
                    //
                    // Чистка старых бекапов на самом сервере.
                    System.out.println(todayBackUp.getAbsolutePath());
                    File[] old_ = todayBackUp.getParentFile().listFiles();
                    if (old_ != null) {
                        Vector<File> old = new Vector<>();
                        Collections.addAll(old, old_);
                        old.sort((o1, o2) -> (int) (o2.lastModified() - o1.lastModified()));
                        for (int i = 2; i < old.size(); ++i) {
                            File file = old.get(i);
                            System.out.println(file.getName() + ":" + file.lastModified());
                            FileUtils.forceDelete(file);
                        }
                    }
                    if (!todayBackUpArchive.exists()) {
                        FileUtils.forceMkdir(todayBackUp);
                        //-
                        Files.copy(db.getFile().toPath(), bugsDBBackUp.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        Files.copy(TestingSystem.db.getFile().toPath(), testsDBBackUp.toPath(), StandardCopyOption.REPLACE_EXISTING);
                        //-
                        zip.Do("Bugs", bugsArchives.getAbsolutePath());
                        zip.Do("Tests", testsArchives.getAbsolutePath());
                        zip.Do(todayBackUp.getAbsolutePath(), todayBackUpArchive.getAbsolutePath());
                        Utils.forceDeleteWithCheck(todayBackUp);
                        //-
                        for (Pair<Machine, User> cred : storages) {
                            backupSession.Do(cred.getKey(), cred.getValue(),
                                    todayBackUpArchive
                            );
                        }
                        //bonus backup
                        if (rightNow.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {

                            Vector<String> targets = new Vector<>();
                            targets.add(Global.properties.get(PropertyName.MailAddress));
                            targets.addAll(Arrays.asList(Global.admins_mails));
                            EmailMessage message = new EmailMessage(
                                    "db backup",
                                    "копия баз данных журнала ошибок и тестов",
                                    targets
                            );
                            Email(message, db.getFile(),
                                    TestingSystem.db.getFile());
                        }
                    }

                }
                //-------------------------------------
                Thread.sleep(60000);
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    });
    protected Thread testing = new Thread(()->{
        while (true){
            try {
                Thread.sleep(10000);
                Print("Testing system thread started...");
                TestingSystem.PerformTasks();
                Print("Testing system thread done");
            }
            catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    });
    //-
    protected Thread interrupt = new Thread(new Runnable() {
        @Override
        public void run() {
            File interruptFile = new File(db_project_info.interrupt);
            try {
                while (true) {
                    Thread.sleep(10000);
                    if (interruptFile.exists()) {
                        FileUtils.writeStringToFile(new File(Eliminated + " by INTERRUPT file " + new Date()), "");
                        FileUtils.forceDelete(interruptFile);
                        System.exit(0);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    });

    public static void ActivateDB() throws Exception {
        db.Connect();
        db.CreateAllTables();
        db.Synchronize();
    }

    protected static void Print(String message) throws IOException {
        Log = new FileWriter("ServerLog.txt", true);
        String dmessage = Utils.Brackets(new Date()) + " " + message;
        System.out.println(dmessage);
        Log.write(dmessage + "\n");
        Log.close();
    }

    protected void Connect() throws Exception {
        clientSocket = server.accept(); // accept() будет ждать пока
        Print((count++) + " клиент присоединился, IP=" + clientSocket.getInetAddress());
        code = ServerCode.Undefined;
        out = new ObjectOutputStream(clientSocket.getOutputStream());
        in = new ObjectInputStream(clientSocket.getInputStream());
    }

    //сеанс с клиентом.
    protected void Action() throws Exception {
        while (true) {
            Object transport = in.readObject();
            //---------------------------------------------
            if (transport instanceof ServerExchangeUnit_2021) {
                System.out.println("NEW BRAHCH");
                request = (ServerExchangeUnit_2021) transport;
                response = null;
                //-буферные объекты. временные с одинаковым именами нельзя создать в разных ветках.
                BugReport bugReport = null;
                Subscriber subscriber = null;
                Group group = null;
                Test test = null;
                TestCompilationTask compilationTask = null;
                //-
                Print("клиент_2021: <- " + (request.codeName));
                //--
                try {
                    code = request.getCode();
                    System.out.println(code);
                    //базовый функционал.
                    switch (code){
                        //<editor-fold desc="БАЗОВЫЕ">
                        case ReadFile:
                            Print("Отправить клиенту текст файла по пути " + Utils.Brackets(request.arg));
                            response = new ServerExchangeUnit_2021(ServerCode.OK, "", Utils.ReadAllText(new File(request.arg)));
                            break;
                        case SendFile:
                            //нам пришел файл.
                            Print("Получить от клиента файл, и распаковать его по пути " + Utils.Brackets(request.arg));
                            request.Unpack(); //распаковка идет по его аргу-пути назначения
                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                            break;
                        case ReceiveFile:
                            Print("Отправить клиенту файл по пути " + Utils.Brackets(request.arg));
                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                            response.object = Utils.packFile(new File(request.arg));
                            Print("Файл успешно запакован");
                            break;
                        case GetComponentsActualVersions:
                            Print("Запросить актуальные версии компонент");
                            LinkedHashMap<ComponentType, String> request_actual_versions = (LinkedHashMap<ComponentType, String>) request.object;
                            LinkedHashMap<ComponentType, String> response_actual_versions = new LinkedHashMap<>();
                            for (ComponentType componentType : request_actual_versions.keySet()) {

                                File v_file = new File(request_actual_versions.get(componentType));
                                String v_string = Utils.remove(
                                        Utils.ReadAllText(v_file),
                                        "\n", "\r"
                                );
                                System.out.println(v_file.getAbsolutePath());
                                System.out.println(Utils.DQuotes(v_string));
                                response_actual_versions.put(componentType, v_string);

                            }
                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                            response.object = response_actual_versions;
                            break;
                        //</editor-fold>

                        default:
                            if (!update_only){
                                switch (code) {
                                    case Email:
                                        Print("Отправка сообщения электронной почты");
                                        Email((EmailMessage) request.object);
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        break;

                                    //<editor-fold desc="БАГ РЕПОРТЫ">
                                    case ReceiveAllArchives:
                                        Print("Отправить клиенту архив всех архивов баг репортов");
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        //--
                                        ZipFolderPass zip = new ZipFolderPass();
                                        File archives = new File(Utils.getDateName("Bugs"));
                                        if (zip.Do("Bugs", archives.getAbsolutePath())) {
                                            response.object = Utils.packFile(archives);
                                            Print("Архив успешно запакован");
                                        } else throw new Exception("Не удалось запаковать архивы");
                                        //--
                                        break;
                                    case PublishBugReport:
                                        bugReport = (BugReport) request.object;
                                        Print("Получить баг репорт с ключом " + bugReport.id);
                                        if (!db.bugReports.containsKey(bugReport.id)) {
                                            db.Insert(bugReport);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else
                                            throw new Exception("Баг репорт с ключом " + bugReport.id + " уже опубликован.");
                                        break;
                                    case DeleteBugReport:
                                        Print("Удалить баг репорт с ключом " + request.arg);
                                        if (db.bugReports.Data.containsKey(request.arg)) {
                                            bugReport = db.bugReports.Data.get(request.arg);
                                            db.Delete(bugReport);
                                            Utils.delete_with_check(bugReport.getArchiveFile());
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else
                                            throw new Exception("Баг репорт с ключом " + request.arg + " не существует.");
                                        break;
                                    case ActualizeBugReport:
                                        Print("Синхронизировать баг репорт с ключом " + request.arg);
                                        if (db.bugReports.containsKey(request.arg)) {
                                            bugReport = db.bugReports.get(request.arg);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK, "", bugReport);
                                        } else
                                            throw new Exception("Баг репорт с ключом " + request.arg + " не существует.");
                                        break;
                                    case UpdateBugReportField:
                                        //-
                                        BugReport oldBugReport = (BugReport) request.object;
                                        Print("Обновить поле " + request.arg + " баг репорта " + oldBugReport.id);
                                        if (db.bugReports.containsKey(oldBugReport.id)) {
                                            bugReport = db.bugReports.get(oldBugReport.id);
                                            Object newValue = BugReport.class.getField(request.arg).get(oldBugReport);
                                            BugReport.class.getField(request.arg).set(bugReport, newValue);
                                            bugReport.change_date = oldBugReport.change_date;
                                            db.Update(bugReport, request.arg);
                                            db.Update(bugReport, "change_date");
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else
                                            throw new Exception("Баг репорт с ключом " + request_old.bugReport.id + " не существует.");
                                        break;
                                    //</editor-fold>

                                    //<editor-fold desc="ПОДПИСЧИКИ">
                                    case AddSubscriber:
                                        subscriber = (Subscriber) request.object;
                                        Print("Включить в подписку " + subscriber.address);
                                        if (!db.subscribers.containsKey(subscriber.address))
                                            db.Insert(subscriber);
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        break;
                                    case DeleteSubscriber:
                                        Print("Исключить из подписки " + request.arg);
                                        if (db.subscribers.containsKey(request.arg)) {
                                            subscriber = db.subscribers.get(request.arg);
                                            db.Delete(subscriber);
                                        }
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        break;
                                    //</editor-fold>
                                    //<editor-fold desc="ГРУППЫ ТЕСТОВ">
                                    case PublishGroup:
                                        group = (Group) request.object;
                                        Print("Опубликовать группу для тестов " + group.name);
                                        if (!TestingSystem.db.groups.Data.containsKey(group.name)) {
                                            TestingSystem.db.Insert(group);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else throw new Exception("Группа с именем " + group.name + " уже существует");
                                        break;
                                    case EditGroup:
                                        //пока примитивно. кто последний перезаписал, тот и прав.
                                        group = (Group) request.object;
                                        Print("Редактировать группу для тестов " + group.name);
                                        if (TestingSystem.db.groups.Data.containsKey(group.name)) {
                                            TestingSystem.db.Update(group);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else throw new Exception("Не существует группы с именем " + group.name);
                                        break;
                                    case DeleteGroup:
                                        Print("Удалить группу для тестов " + request.arg);
                                        if (TestingSystem.db.groups.Data.containsKey(request.arg)) {
                                            group = TestingSystem.db.groups.Data.get(request.arg);
                                            TestingSystem.db.FullDelete(group);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else throw new Exception("Не существует группы с именем " + request.arg);
                                        break;
                                    //</editor-fold>
                                    //<editor-fold desc="ТЕСТЫ">
                                    case DownloadTest:
                                        Print("Отправить клиенту тест " + request.arg);
                                        if (TestingSystem.db.tests.containsKey(request.arg)) {
                                            test = TestingSystem.db.tests.get(request.arg);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK, "", Utils.packFile(test.getArchive()));
                                        } else throw new Exception("Теста с именем " + request.arg + " не существует");
                                        break;
                                    case PublishTest:
                                        test = (Test) request.object;
                                        Print("Опубликовать тест " + test.name);
                                        if (!TestingSystem.db.tests.containsKey(test.name)) {
                                            TestingSystem.db.Insert(test);
                                            //теперь сохраняем архив.
                                            Utils.unpackFile(test.project_archive_bytes, test.getArchive());
                                            //распаковать архив в папку с тестами. для тестирования удобнее хранить их уже открытыми.
                                            UnzipFolderPass unzipFolderPass = new UnzipFolderPass();
                                            //--
                                            if (!unzipFolderPass.Do(
                                                    test.getArchive().getAbsolutePath(),
                                                    test.getServerPath().getParentFile().getAbsolutePath()))
                                                throw new Exception("Не удалось распаковать Тест с именем " + test.name);
                                            response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        } else throw new Exception("Тест с именем " + test.name + " уже существует");
                                        break;
                                    case DeleteTest:
                                        Print("Удалить тест " + request.arg);
                                        if (TestingSystem.db.tests.containsKey(request.arg)) {
                                            test = TestingSystem.db.tests.get(request.arg);
                                            TestingSystem.db.Delete(test);
                                            Utils.forceDeleteWithCheck(test.getArchive());
                                            Utils.forceDeleteWithCheck(test.getServerPath());
                                        } else throw new Exception("Теста с именем " + request.arg + " не существует");
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        break;
                                    case EditTest:
                                        Test newTest = (Test) request.object;
                                        Print("Редактировать тест " + newTest.name);
                                        if (TestingSystem.db.tests.containsKey(newTest.name)) {
                                            test = TestingSystem.db.tests.get(newTest.name);
                                            test.CopyFields(newTest);
                                            TestingSystem.db.Update(test);
                                        } else throw new Exception("Теста с именем " + newTest.name + " не существует");
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        break;
                                    case CompileTest:
                                        compilationTask = (TestCompilationTask) request.object;
                                        Print("принять задачу на компиляцию");
                                        if (!TestingSystem.db.groups.containsKey(compilationTask.group_name))
                                            throw new Exception("Группы с именем " + compilationTask.group_name + " не существует");
                                        if (!TestingSystem.db.tests.containsKey(compilationTask.test_name))
                                            throw new Exception("Теста с именем " + compilationTask.test_name + " не существует");
                                        //теперь эту задачу надо поставить в очередь. и вернуть пользователю, уже с id
                                        compilationTask.state = TaskState.Waiting;
                                        compilationTask.id = TestingSystem.db.taskCounter.Inc();
                                        TestingSystem.db.Insert(compilationTask);
                                        if (compilationTask.runTasks != null) {
                                            System.out.println("found " + compilationTask.runTasks.size() + " new run tasks");
                                            for (TestRunTask rt : compilationTask.runTasks) {
                                                rt.id = TestingSystem.db.taskCounter.Inc();
                                                rt.testcompilationtask_id = compilationTask.id;
                                                TestingSystem.db.Insert(rt);
                                                System.out.println(rt.id + " : " + rt.matrix);
                                            }
                                            System.out.println("------");
                                        }
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        response.object = compilationTask;
                                        break;
                                    case ActualizeTestsTasks:
                                        Print("Актуализировать задачи тестов");
                                        Vector<Integer> ids = (Vector<Integer>) request.object;
                                        Vector<TestRunTask> tasks = new Vector<>();
                                        //у них общий счетчик. так что пока так.
                                        for (Integer taskId : ids) {
                                            if (TestingSystem.db.testRunTasks.containsKey(taskId)) {
                                                TestRunTask tt = TestingSystem.db.testRunTasks.get(taskId);
                                                tasks.add(tt);
                                                System.out.println("id = " + tt.id + " state=" + tt.state);
                                            }
                                        }
                                        response = new ServerExchangeUnit_2021(ServerCode.OK);
                                        response.object = tasks;
                                        break;
                                    // </editor-fold>
                                    default:
                                        throw new Exception("Неподдерживаемый код: " + code);
                                }
                            }else
                                response = new ServerExchangeUnit_2021(ServerCode.OLD);
                            break;
                    }

                } catch (Exception ex) {
                    response = new ServerExchangeUnit_2021(ServerCode.FAIL, "Исключение сервера", ex);
                } finally {
                    Print("сервер: -> " + response.codeName);
                    out.writeObject(response);
                }
                //--
            }
        }
    }
    public void Start() throws Exception {
        DiagnosticSignalHandler.install("TERM", signalHandler);
        DiagnosticSignalHandler.install("INT", signalHandler);
        DiagnosticSignalHandler.install("ABRT", signalHandler);
        ActivateDB();
        TestingSystem.ActivateDB();
        interrupt.start();
        if (!update_only) {
            backUp.start();
            testing.start();
        }
        server = new ServerSocket(port);
        Print("Сервер запущен!"); // хорошо бы серверу
        while (true) {
            try {
                Connect();
                Action();
            } catch (Exception ex) {
                Print("Соединение с клиентом завершено.");
            } finally {
                clientSocket.close();
                // потоки тоже хорошо бы закрыть
                in.close();
                out.close();
                Print("Сервер ждет следующего клиента.");
            }
        }
    }

    //https://javaee.github.io/javamail/docs/api/com/sun/mail/smtp/package-summary.html
    public static void Email(EmailMessage message_in, File... directAttachements) throws Exception {
        Properties props = new Properties();
        props.put("mail.smtp.host", Global.properties.get(PropertyName.SMTPHost));
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", Global.properties.get(PropertyName.SMTPPort));
        props.put("mail.smtp.socketFactory.port", Global.properties.get(PropertyName.MailSocketPort));
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.connectiontimeout", Global.properties.get(PropertyName.SocketTimeout));
        props.put("mail.smtp.timeout", Global.properties.get(PropertyName.SocketTimeout));
        props.put("mail.smtp.writetimeout", Global.properties.get(PropertyName.SocketTimeout));
        //------------------------------
        LinkedHashMap<String, File> innerFiles = new LinkedHashMap<>();
        for (String aName : message_in.files.keySet()) {
            File f = Utils.getTempFileName(aName);
            Utils.unpackFile(message_in.files.get(aName), f);
            innerFiles.put(aName, f);
        }
        //------------------------------
        for (String target : message_in.targets) {
            try {
                Session session = Session.getDefaultInstance(props,
                        new javax.mail.Authenticator() {
                            @Override
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(
                                        Global.properties.get(PropertyName.MailAddress),
                                        Global.properties.get(PropertyName.MailPassword));
                            }
                        });
                MimeMessage message = new MimeMessage(session);
                message.setFrom(new InternetAddress(Global.properties.get(PropertyName.MailAddress)));
                message.setRecipients(Message.RecipientType.CC, InternetAddress.parse(target));
                message.setSubject(message_in.subject);
                Multipart multipart = new MimeMultipart();
                MimeBodyPart textBodyPart = new MimeBodyPart();
                textBodyPart.setText(message_in.text);
                multipart.addBodyPart(textBodyPart);

                for (String aName : innerFiles.keySet()) {
                    MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(innerFiles.get(aName));
                    attachmentBodyPart.setDataHandler(new DataHandler(source));
                    attachmentBodyPart.setFileName(aName);
                    multipart.addBodyPart(attachmentBodyPart);
                }
                for (File f : directAttachements) {
                    MimeBodyPart attachmentBodyPart = new MimeBodyPart();
                    DataSource source = new FileDataSource(f);
                    attachmentBodyPart.setDataHandler(new DataHandler(source));
                    attachmentBodyPart.setFileName(f.getName());
                    multipart.addBodyPart(attachmentBodyPart);
                }

                message.setContent(multipart);
                Transport.send(message);
            } catch (Exception ex) {
                Print(ex.getMessage());
            }
        }
    }
}