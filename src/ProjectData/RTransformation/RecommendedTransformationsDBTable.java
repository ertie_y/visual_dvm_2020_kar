package ProjectData.RTransformation;
import Common.Database.DBTable;
import Common.Passes.PassCode;
public class RecommendedTransformationsDBTable extends DBTable<PassCode, RTransformation> {
    public RecommendedTransformationsDBTable() {
        super(PassCode.class, RTransformation.class);
    }
    @Override
    public String getDataDescription() {
        return "рекоммендуемое преобразование";
    }
}
