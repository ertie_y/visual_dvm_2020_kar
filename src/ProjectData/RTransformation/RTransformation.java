package ProjectData.RTransformation;
import Common.Database.DBObject;
import Common.Passes.PassCode;
import com.sun.org.glassfish.gmbal.Description;
public class RTransformation extends DBObject {
    @Description("PRIMARY KEY,NOT NULL,UNIQUE")
    public PassCode code = PassCode.Undefined;
    public RTransformation() {
    }
    public RTransformation(PassCode code_in) {
        code = code_in;
    }
    @Override
    public Object getPK() {
        return code;
    }
}
