package ProjectData.DBArray;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
public class ArraysDBTable extends DBTable<String, DBArray> {
    public ArraysDBTable() {
        super(String.class, DBArray.class);
    }
    @Override
    public String getDataDescription() {
        return "массив";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(1).setEditable(false);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Имя"};
    }
    @Override
    public Object getFieldAt(DBArray array, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return array.State;
            case 2:
                return array.shortName;
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.DBArray;
    }
}
