package ProjectData.Project.UI;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.StyledTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class VersionsMenu extends GraphMenu {
    public VersionsMenu(StyledTree tree) {
        super(tree, "подверсии");
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.DeleteVersion).getMenuItem());
    }
}
