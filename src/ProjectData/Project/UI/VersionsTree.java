package ProjectData.Project.UI;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.DataTree;
import Common.UI.Trees.TreeRenderers;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.tree.TreePath;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
public class VersionsTree extends DataTree {
    public VersionsTree() {
        super(Current.getRoot().node);
        SelectCurrentProject();
        ExpandAll();
        this.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()) {
                    case KeyEvent.VK_DELETE:
                        Pass_2021.passes.get(PassCode_2021.DeleteVersion).Do();
                        break;
                    case KeyEvent.VK_ENTER:
                        if (Current.HasVersion())
                            Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(Current.getVersion());
                        break;
                }
            }
        });
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererVersion;
    }
    @Override
    public Current getCurrent() {
        return Current.Version;
    }
    public void SelectCurrentProject() {
        setSelectionPath(new TreePath(Current.getProject().node.getPath()));
    }

    @Override
    protected GraphMenu createMenu() {
        return new VersionsMenu(this);
    }
    @Override
    public void LeftMouseAction2() {
        Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(Current.getVersion());
    }
}