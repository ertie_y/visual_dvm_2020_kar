package ProjectData.Project.UI;
import Common.Current;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import Common.UI.UI;
import ProjectData.LanguageName;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class ProjectLanguageMenu extends StyledPopupMenu {
    public ProjectLanguageMenu() {
        for (LanguageName l : LanguageName.values()) {
            if (l.equals(LanguageName.fortran) || (l.equals(LanguageName.c))) {
                JMenuItem m = new StyledMenuItem(l.getDescription());
                m.addActionListener(
                        new AbstractAction() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (Current.getProject().UpdateLanguage(l)) {
                                    Current.getSapfor().ResetAllAnalyses();
                                    UI.getNewMainWindow().getProjectWindow().ShowProjectLanguage();
                                }
                            }
                        });
                add(m);
            }
        }
    }
}