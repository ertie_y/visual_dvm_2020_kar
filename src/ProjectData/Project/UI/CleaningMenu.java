package ProjectData.Project.UI;
import Common.UI.Menus.StyledPopupMenu;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class CleaningMenu extends StyledPopupMenu {
    public CleaningMenu() {
        add(Pass_2021.passes.get(PassCode_2021.DropAnalyses).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.CleanAnalyses).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DropSavedArrays).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteLonelyM).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteSubversions).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.DeleteDebugResults).getMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.ResetCurrentProject).getMenuItem());
    }
}