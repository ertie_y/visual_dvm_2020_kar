package ProjectData.Project;
import GlobalData.Settings.DBSetting;
import Visual_DVM_2021.Passes.Pass_2021;
public class ChangeSettingPass extends Pass_2021<DBSetting> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target= (DBSetting) args[0];
        return true;
    }
}
