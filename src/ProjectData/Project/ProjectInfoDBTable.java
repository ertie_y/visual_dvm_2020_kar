package ProjectData.Project;
import Common.Database.DBTable;
public class ProjectInfoDBTable extends DBTable<String, db_project_info> {
    public ProjectInfoDBTable() {
        super(String.class, db_project_info.class);
    }
    @Override
    public String getDataDescription() {
        return "проект";
    }
}
