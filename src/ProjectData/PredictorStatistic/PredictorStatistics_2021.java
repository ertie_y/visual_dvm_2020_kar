package ProjectData.PredictorStatistic;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Date;

import static ProjectData.SapforData.Variants.ParallelVariant.rankNaN;
import static ProjectData.SapforData.Variants.ParallelVariant.statNaN;
public class PredictorStatistics_2021 extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String key = "";
    public long PredictionTime = 0;
    //статистика -------------------------------
    public long ParallelCount = statNaN;
    public long RemoteCount = statNaN;
    public long RedistributeCount = statNaN;
    public long IntervalCount = statNaN;
    public long PS_RemoteCount = statNaN;
    public long PS_ShadowCount = statNaN;
    public long PS_ReductionCount = statNaN;
    public long PS_AcrossCount = statNaN;
    public long Rank = rankNaN;
    //-------------------------------------------
    @Description("IGNORE")
    public boolean loaded = true;
    //этот конструктор только для пустой статистики.
    public PredictorStatistics_2021(String key_in) {
        key = key_in;
        loaded = false;
    }
    public PredictorStatistics_2021() {
    }
    public Date getPredictionDate() {
        return new Date(PredictionTime);
    }
    public void Unpack(String packed) {
        String[] splited = packed.split("\\|");
        ParallelCount = Integer.parseInt(splited[0]);
        RemoteCount = Integer.parseInt(splited[1]);
        RedistributeCount = Integer.parseInt(splited[2]);
        IntervalCount = Integer.parseInt(splited[3]);
        PS_RemoteCount = Integer.parseInt(splited[4]);
        PS_ShadowCount = Integer.parseInt(splited[5]);
        PS_ReductionCount = Integer.parseInt(splited[6]);
        PS_AcrossCount = Integer.parseInt(splited[7]);
        try {
            Rank = Integer.parseInt(splited[8]);
        } catch (Exception ex) {
        }
        PredictionTime = new Date().getTime();
    }
    @Override
    public Object getPK() {
        return key;
    }
}
