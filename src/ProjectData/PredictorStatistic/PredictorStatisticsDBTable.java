package ProjectData.PredictorStatistic;
import Common.Database.DBTable;
public class PredictorStatisticsDBTable extends DBTable<String, PredictorStatistics_2021> {
    public PredictorStatisticsDBTable() {
        super(String.class, PredictorStatistics_2021.class);
    }
    @Override
    public String getDataDescription() {
        return "статистика варианта";
    }
}
