package ProjectData.SapforData.Regions;
import Common.Current;
import Common.Database.DataSet;
import Common.UI.DataSetControlForm;

import java.math.BigInteger;

import static Common.UI.Tables.TableRenderers.RendererMultiline;
import static Common.UI.Tables.TableRenderers.RendererTopLeft;
public class RegionsSet extends DataSet<BigInteger, ParallelRegion> {
    @Override
    public Current CurrentName() {
        return Current.ParallelRegionInfo;
    }
    //суррогат. нужен только для сохры столбцов. во всяком случае пока.

    public RegionsSet() {
        super(BigInteger.class, ParallelRegion.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                for (int i = 1; i < 7; ++i)
                    columns.get(i).setRenderer(RendererTopLeft);
                columns.get(7).setRenderer(RendererMultiline);
            }
        };
    }
    @Override
    public Object getFieldAt(ParallelRegion object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.originalName;
            case 2:
                return object.lines_count;
            case 3:
                return object.arrays_count;
            case 4:
                return object.loops_count;
            case 5:
                return object.fd_count;
            case 6:
                return object.fc_count;
            case 7:
                return object.fragments;
            default:
                return null;
        }
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Имя", "Строк кода", "Массивов", "Циклов", "Объявлений процедур", "Вызовов процедур", "Фрагменты"};
    }
}
