package ProjectData.SapforData.Regions.UI;

import javax.swing.*;
import java.awt.*;

public class DimensionSpinner extends JSpinner {
    public DimensionSpinner(int value_in){
        java.awt.Dimension p = new Dimension(40, 26);
        setMinimumSize(p);
        setMaximumSize(p);
        setPreferredSize(p);
        setModel(new SpinnerNumberModel(
               value_in,
                -65535, 65535, 1));
    }
}
