package ProjectData.SapforData.Regions.UI;

import Common.Current;
import Common.UI.Tables.StyledCellLabel;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.util.LinkedHashMap;
import java.util.Vector;

public class ArrayAlignmentBar extends JToolBar {
    public ProjectArray array;
    public LinkedHashMap<Integer, Pair<JSpinner, JSpinner>> dimensions = new LinkedHashMap<>();

    public ArrayAlignmentBar(ProjectArray array_in) {
        array = array_in;
        array.bar = this;
        array.init_new_ac(); //набор коефициентов.
        this.setFloatable(false);
        //-
        StyledCellLabel label = new StyledCellLabel();
        label.setOpaque(false);
        label.setText(array.GetShortNameWithDimLetters() + " → " + array.align_template.shortName + " [ ");
        add(label);
        //---
        int i = 0;
        for (int dim : array.ac_current.keySet()) {
            if (array.ac_current.get(dim).active) {
                //просто идут подряд. четные - K, нечетные B
                JSpinner K = new DimensionSpinner(array.ac_current.get(dim).K);
                JSpinner B = new DimensionSpinner(array.ac_current.get(dim).B);
                dimensions.put(dim, new Pair<>(K, B));
                add(K);
                label = new StyledCellLabel();
                label.setOpaque(false);
                label.setText(" * " + array.ac_current.get(dim).getLetter() + " + ");
                add(label);
                add(B);
                label = new StyledCellLabel();
                label.setOpaque(false);
                label.setText((i < array.ac_current.keySet().size() - 1) ? " , " : " ]");
                add(label);
                ChangeListener changeListener = e -> {
                    //инфа о связях. из списка всех объявленных массивов.
                    ProjectArray d_array = Current.getProject().declaratedArrays.get(array.id);
                    System.out.println("current region = " + Current.getParallelRegion().regionId);
                    System.out.println(array.id + " links count= " + d_array.links.size()+":"+d_array.printLinks());
                    //опять же, берем инфу из объявлений!
                    for (ProjectArray d_link : d_array.links.values()) {
                        //это массивы из объявлений. прежде чем их трогать проверим а есть ли они в текущей области.
                        // по адресам принадлежность смотреть нельзя.
                        // на момент поиска массивов у них их еще нет. а вот ид совпадают.
                        if (Current.getParallelRegion().ArrayBelongsToRegion(d_link.id)) {
                            System.out.println(d_link.id + " belongs to current region, apply link!");
                            //инфа о массиве уже из области. имеющая адрес и бар.
                            ProjectArray r_link = Current.getParallelRegion().getArrayById(d_link.id);
                            Pair<JSpinner, JSpinner> pair = r_link.bar.dimensions.get(dim);
                            pair.getKey().setValue(K.getValue());
                            pair.getValue().setValue(B.getValue());
                        }
                    }

                };
                K.addChangeListener(changeListener);
                B.addChangeListener(changeListener);
            }
            ++i;
        }
    }

    public void apply_changes() {
        System.out.println(array);
        for (int dim : dimensions.keySet()) {
            array.ac_new.get(dim).K = (int) dimensions.get(dim).getKey().getValue();
            array.ac_new.get(dim).B = (int) dimensions.get(dim).getValue().getValue();
            //-
            System.out.println("dim=" + dim +
                    " k=" + array.ac_new.get(dim).K +
                    " b=" + array.ac_new.get(dim).B
            );
            //-        }
            System.out.println("-----");
        }
    }
}
