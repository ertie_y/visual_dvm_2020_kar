package ProjectData.SapforData.Arrays.Distribution;
import ProjectData.LanguageName;
public class Directive {
    public LanguageName langType = LanguageName.fortran;
    public String file = "";
    public int line = -1;
    public int col = -1;
}
