package ProjectData.SapforData.Arrays.Distribution;

import Common.Database.DBObject;
import ProjectData.SapforData.Arrays.ProjectArray;
import sun.security.krb5.internal.tools.Klist;

public class Dimension extends DBObject {
    //--------------------------------------
    public int num; //номер измерения.
    public int K = 0;
    public int B = 0;
    public boolean active = false;

    //-------------------------------------
    public Dimension(int num_in) {
        num = num_in;
    }

    public Dimension(int num_in, int K_in, int B_in) {
        num = num_in;
        K = K_in;
        B = B_in;
        active = true;
    }
    public Dimension clone_() {
        Dimension res = new Dimension(num);
        res.active = active;
        res.K = K;
        res.B = B;
        return res;
    }

    @Override
    public Object getPK() {
        return num;
    }

    public String getLetter() {
        return ProjectArray.alignNames[num];
    } //для отображения букв

    public int getS() { return active ? num : -1; } //для триплета. шифр.

}
