package ProjectData.SapforData.Arrays.UI;

import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Tables.StyledCellLabel;
import Common.UI.Trees.StyledTree;
import Common.UI.Trees.TreeRenderers;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachineType;
import GlobalData.Machine.UI.MachineFields;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
import ProjectData.SapforData.Regions.UI.ArrayAlignmentBar;
import ProjectData.SapforData.Regions.UI.ParallelRegionFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;

import java.awt.*;
import java.math.BigInteger;
import java.util.LinkedHashMap;

import static Common.UI.Trees.TreeRenderers.RendererRule;

public class RulesTree extends StyledTree {
    public RulesTree() {
        super(Current.getProject().align_rules_root);
        setRootVisible(false);
        expandRow(0);
        ExpandAll();
        Current.set(Current.ParallelRegion,null);
    }

    @Override
    protected GraphMenu createMenu() {
        return new DistributionMenu(this);
    }

    @Override
    public TreeRenderers getRenderer() {
        return RendererRule;
    }

    @Override
    public void SelectionAction(TreePath e) {
        ParallelRegion region = null;
        if (e != null) {
            DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getLastPathComponent();
            Object o = node.getUserObject();
            if (o instanceof String) {
                region = (ParallelRegion) ((DefaultMutableTreeNode) node.getParent()).getUserObject();
            }
            if (o instanceof ParallelRegion) {
                region = (ParallelRegion) o;
            }
        }
        Current.set(Current.ParallelRegion, region);
    }
}
