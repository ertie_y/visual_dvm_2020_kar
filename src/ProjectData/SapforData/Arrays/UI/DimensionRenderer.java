package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import Common.UI.Tables.RendererCell;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.SapforData.Arrays.Templates.TemplateDimension;

import javax.swing.*;
public class DimensionRenderer extends RendererCell<TemplateDimension> {
    @Override
    public TemplateDimension Init(JTable table, Object value_in, int row, int column) {
        return (TemplateDimension) value_in;
    }
    @Override
    public void Display() {
        if (value != null) {
            setText(value.state.getMaskDescription());
            setFont(value.isBlocked() ? Current.getTheme().Fonts.get(VisualiserFonts.Disabled) : Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
        }
    }
}
