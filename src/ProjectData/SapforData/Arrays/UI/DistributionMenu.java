package ProjectData.SapforData.Arrays.UI;

import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.StyledTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;

public class DistributionMenu extends GraphMenu {
    JMenuItem mChangeDistribution;
    public DistributionMenu(StyledTree tree) {
        super(tree, "области распараллеливания");
        mChangeDistribution = Pass_2021.passes.get(PassCode_2021.SPF_ModifyArrayDistribution).getMenuItem();
        add(mChangeDistribution);
    }

    @Override
    public void CheckElementsVisibility() {
        super.CheckElementsVisibility();
        mChangeDistribution.setVisible(Pass_2021.passes.get(PassCode_2021.SPF_GetArrayDistribution).isDone());
    }
}
