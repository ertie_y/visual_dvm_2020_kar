package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import Common.UI.Tables.DBObjectEditor;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import ProjectData.SapforData.Arrays.Templates.TemplateDimension;

import javax.swing.*;
public class DimensionStateChanger extends DBObjectEditor<TemplateDimension> {
    @Override
    public Object getCellEditorValue() {
        return value;
    }
    @Override
    public void InitValue(JTable table, Object value_in, int row, int column) {
        value = (TemplateDimension) value_in;
    }
    @Override
    public void Action() {
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
        value.SwitchState();
        setText(value.state.getMaskDescription());
        UI.getNewMainWindow().getProjectWindow().ShowFilteredVariantsCount();
    }
}
