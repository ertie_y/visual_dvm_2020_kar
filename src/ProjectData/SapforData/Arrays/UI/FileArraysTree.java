package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import ProjectData.Files.UI.FileGraphTree;
public class FileArraysTree extends FileGraphTree {
    public FileArraysTree() {
        super(Current.getFile().getArraysTree());
    }
    @Override
    public String getBranchesName() {
        return "объявления массивов";
    }
}
