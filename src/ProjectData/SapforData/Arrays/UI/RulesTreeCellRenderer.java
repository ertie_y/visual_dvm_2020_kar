package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.Trees.StyledTreeCellRenderer;
import ProjectData.SapforData.Regions.ParallelRegion;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
public class RulesTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof String) {
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.Distribution));
        } else if (o instanceof ParallelRegion)
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
        setForeground(tree.getForeground());
        return this;
    }
}
