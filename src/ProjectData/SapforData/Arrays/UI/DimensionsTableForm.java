package ProjectData.SapforData.Arrays.UI;
import Common.Current;
import Common.UI.ControlForm;
import Common.UI.Tables.Grid.GridAnchestor;
import Common.UI.Tables.StyledTable;
import Common.UI.UI;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Arrays.Templates.TemplateDimension;

import java.util.Vector;

import static Common.UI.Tables.TableEditors.EditorDimension;
import static Common.UI.Tables.TableRenderers.RendererDimension;
public class DimensionsTableForm extends ControlForm<StyledTable> {
    public DimensionsTableForm() {
        super(StyledTable.class);
    }
    @Override
    public void CreateControl() {
        Vector<String> columns = new Vector<>();
        columns.add("шаблон");
        for (int i = 0; i < Current.getProject().maxdim; ++i)
            columns.add(ProjectArray.alignNames[i]);
        Vector<Object> dimensions = new Vector<>();
        Current.getProject().templates.values().forEach(t -> dimensions.add(t.CreateTemplateCells()));
        control = new StyledTable(new GridAnchestor(columns, dimensions) {
            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                return ((Object[]) data.get(rowIndex))[columnIndex];
            }
            @Override
            public boolean isCellEditable(int row, int col) {
                return (col > 0) && !((TemplateDimension) getValueAt(row, col)).isBlocked();
            }
        }) {
            @Override
            public void Init() {
                setDefaultRenderer(TemplateDimension.class, UI.TableRenderers.get(RendererDimension));
                setDefaultEditor(TemplateDimension.class, UI.TableEditors.get(EditorDimension));
            }
        };
    }
}
