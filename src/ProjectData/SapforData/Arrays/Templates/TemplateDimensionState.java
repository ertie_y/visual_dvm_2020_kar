package ProjectData.SapforData.Arrays.Templates;
import Common.Current;
public enum TemplateDimensionState {
    multiplied, //0
    distributed; //1
    public String getMaskDescription() {
        switch (this) {
            case distributed:
                if (Current.getProject().f_distributed()) {
                    return "BLOCK";
                } else {
                    if (Current.getProject().f_multiplied())
                        return "  X  ";
                    else return "BLOCK";
                }
            case multiplied:
                if (Current.getProject().f_multiplied()) {
                    return "  *  ";
                } else {
                    if (Current.getProject().f_distributed())
                        return "  X  ";
                    else return "  *  ";
                }
        }
        return "?";
    }
    public String getDescription() {
        switch (this) {
            case distributed:
                return "BLOCK";
            case multiplied:
                return "  *  ";
        }
        return "??";
    }
}
