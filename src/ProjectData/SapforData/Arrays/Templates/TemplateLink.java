package ProjectData.SapforData.Arrays.Templates;
import Common.Utils.Index;
import javafx.util.Pair;

import java.util.Vector;
public class TemplateLink {
    Vector<Integer> linkWithTemplate = new Vector<>();
    Vector<Pair<Integer, Integer>> alignRuleWithTemplate = new Vector<>();
    long templateArray;
    public TemplateLink(String[] splited, Index idx) {
        int linkWithTemplate_size = Integer.parseInt(splited[idx.Inc()]);
        for (int i = 0; i < linkWithTemplate_size; ++i) {
            int link_ = Integer.parseInt(splited[idx.Inc()]);
            linkWithTemplate.add(link_);
        }
        int alignRuleWithTemplate_size = Integer.parseInt(splited[idx.Inc()]);
        for (int i = 0; i < alignRuleWithTemplate_size; ++i) {
            int f = Integer.parseInt(splited[idx.Inc()]);
            int s = Integer.parseInt(splited[idx.Inc()]);
            alignRuleWithTemplate.add(new Pair<>(f, s));
        }
        templateArray = Long.parseLong(splited[idx.Inc()]);
    }
}