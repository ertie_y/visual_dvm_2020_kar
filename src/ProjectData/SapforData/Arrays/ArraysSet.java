package ProjectData.SapforData.Arrays;
import Common.Current;
import Common.Database.DataSet;
import Common.Global;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.UI.Interface.FilterWindow;
import Visual_DVM_2021.UI.Main.ArraysFilterForm;

import static Common.UI.Tables.TableEditors.EditorHyperlinks;
import static Common.UI.Tables.TableRenderers.RendererHiddenList;
import static Common.UI.Tables.TableRenderers.RendererHyperlinks;
public class ArraysSet extends DataSet<Long, ProjectArray> {
    public ArraysSet() {
        super(Long.class, ProjectArray.class);
    }

    @Override
    public Current CurrentName() {
        return Current.ProjectArray;
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                if (Global.db.settings.get(SettingName.ShowFullArraysDeclarations).toBoolean()) {
                    columns.get(4).setRenderer(RendererHyperlinks);
                    columns.get(4).setEditor(EditorHyperlinks);
                } else {
                    columns.get(4).setRenderer(RendererHiddenList);
                    columns.get(4).setMaxWidth(200);
                }
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Имя", "Область описания", "Файлы объявления", "Размерность", "Размер элемента(байт)", "Область распараллеливания"
        };
    }
    @Override
    public Object getFieldAt(ProjectArray object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.State;
            case 2:
                return object.GetShortNameWithDim();
            case 3:
                return object.locName + " : " + object.location;
            case 4:
                return object.GetDeclPlacesList();
            case 5:
                return object.dimSize;
            case 6:
                return object.typeSize;
            case 7:
                return object.GetRegionsText();
            default:
                return null;
        }
    }

    @Override
    public FilterWindow CreateFilterUI() {
        return new ArraysFilterForm();
    }
}
