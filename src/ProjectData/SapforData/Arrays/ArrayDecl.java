package ProjectData.SapforData.Arrays;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
// это то что отображается в боковом графе файла. не путать с сапфоровским ProjectArray
public class ArrayDecl extends FileObjectWithMessages {
    public String array_name;
    public ArrayLocation array_loc;
    public ArrayDecl(String array_name_in, ArrayLocation array_loc_in, DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
        array_name = array_name_in;
        array_loc = array_loc_in;
    }
    @Override
    public String Description() {
        return array_loc.getDescription() + " массив " + Utils.Brackets(array_name);
    }
}
