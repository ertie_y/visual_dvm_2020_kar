package ProjectData.SapforData;
import Common.Current;
import Common.Database.iDBObject;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import com.sun.org.glassfish.gmbal.Description;
//объект принадлежащий файлу и относящийся к его строке.
public abstract class FileObject extends iDBObject {
    @Description("DEFAULT ''")
    public String file = "";
    @Description("DEFAULT 1")
    public int line = 1;
    public FileObject() {
    }
    public FileObject(String file_in) {
        file = file_in;
    }
    @Override
    public String getSelectionText() {
        return "файл " + Utils.Brackets(file) + " строка: " + line;
    }
    public DBProjectFile getFather() {
        return Current.getProject().db.files.Data.get(file);
    }
    public void Show(boolean focus) {
        UI.getNewMainWindow().getProjectWindow().GotoFile(file, line, focus);
    }
}
