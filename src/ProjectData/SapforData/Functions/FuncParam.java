package ProjectData.SapforData.Functions;
import java.util.Vector;
public class FuncParam {
    static final int IN_BIT = 16;
    static final int OUT_BIT = 256;
    Vector<Integer> inout_types;
    Vector<String> identificators;
    Vector<String> parametersT;
    int countOfPars;
    public FuncParam() {
        countOfPars = 0;
    }
    public void Init(int numPar) {
        countOfPars = numPar;
        if (numPar != 0) {
            parametersT = new Vector<>(numPar);
            inout_types = new Vector<>(numPar);
            identificators = new Vector<>(numPar);
            for (int z = 0; z < numPar; ++z) {
                parametersT.add("NONE_T");
                inout_types.add(0);
                identificators.add("");
            }
        }
    }
    boolean IsArgIn(int num) {
        if (num >= countOfPars)
            return false;
        else
            return (inout_types.get(num) & IN_BIT) != 0;
    }
    boolean IsArgOut(int num) {
        if (num >= countOfPars)
            return false;
        else
            return (inout_types.get(num) & OUT_BIT) != 0;
    }
    boolean IsArgInOut(int num) {
        if (num >= countOfPars)
            return false;
        else
            return IsArgIn(num) && IsArgOut(num);
    }
    public void FillParam(int num, String type, String ident, int inout) {
        parametersT.set(num, type);
        inout_types.set(num, inout);
        identificators.set(num, ident);
    }
    @Override
    public String toString() {
        Vector<String> res = new Vector<>();
        for (int i = 0; i < countOfPars; ++i) {
            String ps = IsArgInOut(i) ? "inout" :
                    (IsArgOut(i) ? "out" : (IsArgIn(i) ? "in" : "?!"));
            ps += " " + inout_types.get(i) + " " + identificators.get(i) + " " + parametersT.get(i);
            res.add(ps);
        }
        return String.join(",", res);
    }
}
