package ProjectData.SapforData.Functions;
import Common.UI.UI;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;
public class FuncCallH extends FuncCall {
    //--
    public Vector<FuncCallH> calls = new Vector<>();
    //--
    //сообщения не проверяем.
    public FuncCallH(FuncCall funcCall_in) {
        file = funcCall_in.file;
        line = funcCall_in.line;
        funcName = funcCall_in.funcName;
        canBeInlined = funcCall_in.canBeInlined;
    }
    //особый случай. только для мейна
    //его объявление - его же и вызов.
    //сообщения не проверяем.
    public FuncCallH(FuncInfo funcInfo) {
        file = funcInfo.file;
        line = funcInfo.line;
        funcName = funcInfo.funcName;
        canBeInlined = true;
    }
    @Override
    public String getSelectionText() {
        return "вызов " + Utils.Brackets(funcName) + " в строке " + line;
    }
    @Override
    public void SelectAllChildren(boolean select) {
        for (FuncCallH callH : calls)
            callH.Select(select);
    }
    @Override
    public void Select(boolean flag) {
        if (Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctions).isDone()) {
            super.Select(flag);
        } else {
            UI.Info("Для подстановки функций требуется выполнить проход " + Utils.DQuotes(PassCode_2021.SPF_GetGraphFunctions.getDescription()));
        }
    }
}
