package ProjectData.SapforData.Functions;

import Common.Utils.Index;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Vector;

public class FuncInfo extends FileObjectWithMessages {
    //----------------------------------------------------------
    public int lineEnd;
    public String funcName;
    public FunctionType type;
    public boolean doNotInline;
    public boolean doNotAnalyze;
    public boolean needToInline;
    public FuncParam parameters;
    //вызовы функций в теле этой функции
    public Vector<FuncCall> calls = new Vector<>();
    //вызовы этой самой функции, рассортированные по файлам.
    public LinkedHashMap<String, Vector<FuncCall>> own_calls = new LinkedHashMap<>();

    //это конструктор при пофайловой распаковке. в него попадают только объявленные в проекте функции.
    public FuncInfo(String f_name, DBProjectFile father_in, String[] splited, Index calls_number) {
        super(father_in, Integer.parseInt(splited[0]));
        funcName = f_name;
        lineEnd = Integer.parseInt(splited[1]);
        calls_number.Set(Integer.parseInt(splited[2]));
        needToInline = (Integer.parseInt(splited[3])) != 0;
        doNotInline = (Integer.parseInt(splited[4])) != 0;
        doNotAnalyze = (Integer.parseInt(splited[5])) != 0;
        //в си это поле isMain
        type = ((Integer.parseInt(splited[6])) != 0) ? FunctionType.Main : FunctionType.Default;
        parameters = new FuncParam();
        if (splited.length > 6) {
            int countP = Integer.parseInt(splited[7]);
            parameters.Init(countP);
            for (int z = 0; z < countP * 3; z += 3)
                parameters.FillParam(z / 3, splited[8 + z + 2], splited[8 + z], Integer.parseInt(splited[8 + z + 1]));
        }
    }

    //функция без объявления. у нее есть только имя и тип.
    public FuncInfo(String f_name, FunctionType type_in) {
        funcName = f_name;
        type = type_in;
        doNotInline = true;
    }

    //--
    @Override
    public String getSelectionText() {
        return type.getDescription() + " " + Utils.Brackets(funcName);
    }

    public boolean isMain() {
        return type.equals(FunctionType.Main);
    }

    //------------------------------------------------
    @Override
    public boolean isSelectionEnabled() {
        return false;
    }

    @Override
    public ImageIcon GetDisabledIcon() {
        return Utils.getIcon("/icons/Function.png");
    }

    @Override
    public void SelectAllChildren(boolean select) {
        for (String file_name : own_calls.keySet())
            for (FuncCall fc : own_calls.get(file_name))
                fc.Select(select);
    }

    public boolean isDeclared() {
        return type.equals(FunctionType.Main) || type.equals(FunctionType.Default);
    }

    @Override
    public String Description() {
        return type.getDescription() + " " + Utils.Brackets(funcName);
    }
}
