package ProjectData.SapforData.Functions;

import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

public class FuncCall extends FileObjectWithMessages {
    public String funcName = "";  //не нужны дополнительные поля.имя функции это уже ключ.
    public boolean canBeInlined = false;

    public FuncCall(DBProjectFile father_in, String funcName_in, int line_in) {
        super(father_in, line_in);
        funcName = funcName_in;
    }

    public FuncCall() {
    }

    @Override
    public String getSelectionText() {
        return "вызов в строке " + line;
    }

    @Override
    public boolean isSelectionEnabled() {
        return canBeInlined;
    }

    @Override
    public String Description() {
        return "вызов " + Utils.Brackets(funcName);
    }

    @Override
    public void Select(boolean flag) {
        if (Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctions).isDone()) {
            super.Select(flag);
        } else {
            UI.Info("Для подстановки функций требуется выполнить проход " + Utils.DQuotes(PassCode_2021.SPF_GetGraphFunctions.getDescription()));
        }
    }
}
