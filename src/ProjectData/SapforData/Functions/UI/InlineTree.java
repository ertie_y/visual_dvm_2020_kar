package ProjectData.SapforData.Functions.UI;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.SelectableTree;
import Common.UI.Trees.TreeRenderers;
import ProjectData.SapforData.FileObject;
public class InlineTree extends SelectableTree {
    public InlineTree() {
        super(Current.getProject().inline_root);
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererSelection;
    }
    @Override
    public Current getCurrent() {
        return Current.InlineGraphElement;
    }
    @Override
    public void ShowCurrentObject() {
        Object o = Current.get(getCurrent());
        if (o instanceof FileObject) {
            ((FileObject) o).Show(false);
        }
    }
    @Override
    protected GraphMenu createMenu() {
        return new InlineMenu(this);
    }

    @Override
    protected int getStartLine() {
        return 1;
    }
}
