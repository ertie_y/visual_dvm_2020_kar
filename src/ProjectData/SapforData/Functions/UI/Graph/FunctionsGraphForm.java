package ProjectData.SapforData.Functions.UI.Graph;

import Common.Current;
import Common.UI.ControlForm;
import com.mxgraph.swing.mxGraphComponent;
public class FunctionsGraphForm extends ControlForm<mxGraphComponent> {

    public FunctionsGraphForm() {
        super(mxGraphComponent.class);
    }

    @Override
    public void CreateControl() {
        control = Current.getProject().DrawFunctionsGraph();
    }

}
