package ProjectData.SapforData.Functions.UI.Graph;

import Common.Utils.Utils;
import com.mxgraph.swing.mxGraphComponent;
import javafx.util.Pair;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Vector;

//только инфа о вершинах и ребрах. без отрисовки.
public class GraphInfo {
    //вершины графа и ребра
    public LinkedHashMap<String, Vector<String>> vertexMap = new LinkedHashMap<>();
    //координаты для отрисовки. раз это теперь полноценная инфа, которая передается извне.
    public LinkedHashMap<String, Pair<Double, Double>> vertexCoordinates = new LinkedHashMap<>();

    //----------------
    public void addVertex(String vertexName) {
        if (!hasVertex(vertexName)) {
            vertexMap.put(vertexName, new Vector<>());
        }
    }

    public boolean hasVertex(String vertexName) {
        return vertexMap.containsKey(vertexName);
    }

    //-----------------
    public boolean hasEdge(String vertexName1, String vertexName2) {
        if (!hasVertex(vertexName1)) return false;
        Vector<String> edges = vertexMap.get(vertexName1);
        for (String end : edges)
            if (end.equals(vertexName2)) return true;
        return false;
    }

    public void addEdge(String vertexName1, String vertexName2) {
        if (!hasVertex(vertexName1)) addVertex(vertexName1);
        if (!hasVertex(vertexName2)) addVertex(vertexName2);
        if (!hasEdge(vertexName1, vertexName2)) {
            //находим список смежности, и добавляем новую вершину.
            Vector<String> edges1 = vertexMap.get(vertexName1);
            edges1.add(vertexName2);
            Collections.sort(edges1);
        }
    }

    //-
    public void Print() {
        Vector<String> edges = new Vector<>();
        for (String name : vertexMap.keySet()) {
            for (String neighbor : vertexMap.get(name)) {
                edges.add(Utils.Brackets(name + "," + neighbor));
            }
        }
    }

    public boolean isEmpty() {
        return vertexMap.isEmpty();
    }

    public void Clear() {
        vertexMap.clear();
        vertexCoordinates.clear();
    }
/*
    public void ClearCoords() {
        vertexCoordinates.clear();
    }
 */

    public mxGraphComponent Draw() {
        // return new FunctionsGraphUIGreed(this).Draw();
        return new FunctionsGraphUI(this).Draw();
    }
}
