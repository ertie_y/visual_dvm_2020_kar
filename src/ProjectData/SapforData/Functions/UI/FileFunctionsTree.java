package ProjectData.SapforData.Functions.UI;
import Common.Current;
import ProjectData.Files.UI.FileGraphTree;
public class FileFunctionsTree extends FileGraphTree {
    public FileFunctionsTree() {
        super(Current.getFile().getFunctionsTree());
    }
    @Override
    public String getBranchesName() {
        return "объявления процедур";
    }
}
