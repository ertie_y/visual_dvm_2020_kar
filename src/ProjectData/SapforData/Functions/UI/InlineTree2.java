package ProjectData.SapforData.Functions.UI;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.SelectableTree;
import Common.UI.Trees.TreeRenderers;
import ProjectData.SapforData.FileObject;
public class InlineTree2 extends SelectableTree {
    public InlineTree2() {
        super(Current.getProject().inline_root2);
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererSelection;
    }
    @Override
    public Current getCurrent() {
        return Current.InlineGraphElement2;
    }
    @Override
    protected GraphMenu createMenu() {
        return new InlineMenu2(this);
    }
    @Override
    public void ShowCurrentObject() {
        Object o = Current.get(getCurrent());
        if (o instanceof FileObject) {
            ((FileObject) o).Show(false);
        }
    }

    @Override
    protected int getStartLine() {
        return 1;
    }
}