package ProjectData.SapforData.Functions.UI;
import Common.UI.Menus.SelectionTreeMenu;
import Common.UI.Trees.SelectableTree;
import ProjectData.SapforData.Functions.FuncCallH;

import javax.swing.tree.DefaultMutableTreeNode;
public class InlineMenu2 extends SelectionTreeMenu {
    public InlineMenu2(SelectableTree tree_in) {
        super(tree_in);
    }
    @Override
    public Class getTargetClass() {
        return FuncCallH.class;
    }
    @Override
    public void SelectAll(boolean select) {
        if (tree.root.getChildCount() == 1)
            SelectR(
                    (FuncCallH) ((DefaultMutableTreeNode) (tree.root.getChildAt(0))).getUserObject(),
                    select);
    }
    public void SelectR(FuncCallH callH, boolean select) {
        callH.Select(select);
        for (FuncCallH child : callH.calls)
            SelectR(child, select);
    }
}
