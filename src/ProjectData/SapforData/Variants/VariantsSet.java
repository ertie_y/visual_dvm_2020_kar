package ProjectData.SapforData.Variants;
import Common.Current;
import Common.Database.DataSet;
import Common.UI.DataSetControlForm;
import Common.UI.UI;

import static Common.UI.Tables.TableRenderers.*;
public class VariantsSet extends DataSet<String, ParallelVariant> {
    public VariantsSet() {super(String.class, ParallelVariant.class); }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(0).setVisible(false);
                columns.get(2).setRenderer(RendererMultiline);
                for (int i = 3; i < columns.size() - 2; ++i)
                    columns.get(i).setRenderer(RendererMaskedInt);
                columns.get(columns.size() - 2).setRenderer(RendererVariantRank);
                columns.get(columns.size() - 1).setRenderer(RendererDate);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Распределение", "PARALLEL", "REMOTE", "REDISTRIBUTE", "INTERVALS",
                "PS.REMOTE", "PS.SHADOW", "PS.REDUCTION", "PS.ACROSS", "Оценка", "Дата оценки"};
    }
    @Override
    public Object getFieldAt(ParallelVariant object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.templates_description;
            case 3:
                return object.stats.ParallelCount;
            case 4:
                return object.stats.RemoteCount;
            case 5:
                return object.stats.RedistributeCount;
            case 6:
                return object.stats.IntervalCount;
            case 7:
                return object.stats.PS_RemoteCount;
            case 8:
                return object.stats.PS_ShadowCount;
            case 9:
                return object.stats.PS_ReductionCount;
            case 10:
                return object.stats.PS_AcrossCount;
            case 11:
                return object.stats.Rank;
            case 12:
                return object.stats.getPredictionDate();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.ParallelVariant;
    }
}
