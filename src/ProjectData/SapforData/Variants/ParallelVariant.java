package ProjectData.SapforData.Variants;
import Common.Database.DBObject;
import Common.UI.UI;
import Common.Utils.Index;
import ProjectData.PredictorStatistic.PredictorStatistics_2021;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
import ProjectData.Project.db_project_info;
import javafx.util.Pair;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Vector;
public class ParallelVariant extends DBObject {
    public static final long statNaN = -1;
    public static final long rankNaN = 1;
    public String UniqKey = "";
    public int dc = 0;
    public db_project_info father = null;
    public PredictorStatistics_2021 stats = null;
    public Vector<String> templates_description = new Vector<>();
    public LinkedHashMap<Pair<BigInteger, BigInteger>, Long> key = new LinkedHashMap<>();
    public db_project_info last_built = null; //? не факт что нужна.
    public ParallelVariant(db_project_info father_in, Vector<BigInteger> vector) {
        father = father_in;
        UniqKey = "";
        dc = 0;
        for (int i = 0; i < vector.size(); i += 3) {
            ProjectArray template = father.templates.get(vector.get(i));
            long varID = vector.get(i + 1).longValue(); //тут вар ид. он обычный лонг.
            ParallelRegion region = father.parallelRegions.get(vector.get(i + 2));
            if (varID != 0)
                dc++;
            templates_description.add(template.GetShortNameForVariant(varID));
            UniqKey += template.name + "_" + varID + "_" + region.originalName;
            key.put(new Pair<>(template.address, region.regionId), varID);
        }
        load_stats();
    }
    public void load_stats() {
        stats = father.db.predictorStatistics.Data.containsKey(UniqKey) ?
                father.db.predictorStatistics.Data.get(UniqKey) : new PredictorStatistics_2021(UniqKey);
    }
    public void UpdateStats(String packed) throws Exception {
        stats.Unpack(packed);
        if (stats.loaded) {
            father.db.Update(stats);
        } else {
            father.db.Insert(stats);
            stats.loaded = true;
        }
    }
    public boolean Match(boolean distributed) {
        for (Pair<BigInteger, BigInteger> p : key.keySet()) {
            ProjectArray template = father.templates.get(p.getKey());
            if (!template.CheckFilterMask(key.get(p), distributed)) return false;
        }
        return true;
    }
    public String getVector() {
        String res = "";
        int i = 0;
        for (Pair<BigInteger, BigInteger> p : key.keySet()) {
            res += p.getKey() + "|" + key.get(p) + "|" + p.getValue() + ((i < key.size() - 1) ? "|" : "");
            ++i;
        }
        res = i + "|" + res;
        return res;
    }
    public String getDescription() {
        return String.join(";", templates_description);
    }
    @Override
    public boolean isVisible() {
        boolean res = true;
        if (father.f_distributed())
            res = Match(true);
        if (res && father.f_multiplied()) {
            res = Match(false);
        }
        if (!res) Select(false);
        return res;
    }
    @Override
    public Object getPK() {
        return UniqKey;
    }
    @Override
    public Index getSelectionCounter() {
        return father.CheckedVariantsCounter;
    }
    @Override
    public void Select(boolean flag) {
        super.Select(flag);
        UI.getNewMainWindow().getProjectWindow().ShowCheckedVariantsCount();
    }
}
