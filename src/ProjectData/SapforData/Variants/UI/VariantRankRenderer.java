package ProjectData.SapforData.Variants.UI;
import Common.UI.Tables.MaskedIntegerValueRenderer;
public class VariantRankRenderer extends MaskedIntegerValueRenderer {
    @Override
    public long getMask() {
        return 1;
    }
}
