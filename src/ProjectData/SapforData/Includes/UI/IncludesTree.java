package ProjectData.SapforData.Includes.UI;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.SelectableTree;
import Common.UI.Trees.TreeRenderers;
public class IncludesTree extends SelectableTree {
    public IncludesTree() {
        super(Current.getProject().includes_root);
    }
    @Override
    public Current getCurrent() {
        return Current.IncludeGraphElement;
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererSelection;
    }
    @Override
    protected GraphMenu createMenu() {
        return new IncludesMenu(this);
    }
}
