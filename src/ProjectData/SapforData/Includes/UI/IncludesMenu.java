package ProjectData.SapforData.Includes.UI;
import Common.Current;
import Common.UI.Menus.SelectionTreeMenu;
import Common.UI.Trees.SelectableTree;
import ProjectData.SapforData.Includes.FileInfo;
public class IncludesMenu extends SelectionTreeMenu {
    public IncludesMenu(SelectableTree tree_in) {
        super(tree_in);
    }
    @Override
    public Class getTargetClass() {
        return FileInfo.class;
    }
    @Override
    public void SelectAll(boolean select) {
        for (FileInfo fi : Current.getProject().addictedFiles.values())
            fi.SelectAllChildren(select);
    }
}
