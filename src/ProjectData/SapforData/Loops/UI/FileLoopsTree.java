package ProjectData.SapforData.Loops.UI;
import Common.Current;
import ProjectData.Files.UI.FileGraphTree;
public class FileLoopsTree extends FileGraphTree {
    public FileLoopsTree() {
        super(Current.getFile().getLoopsTree());
    }
    @Override
    public String getBranchesName() {
        return "гнёзда циклов";
    }
}
