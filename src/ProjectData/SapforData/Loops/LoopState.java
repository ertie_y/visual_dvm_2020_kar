package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;

import javax.swing.text.Highlighter;
public enum LoopState {
    Loop, GoodLoop, BadLoop;
    public String getDescription() {
        switch (this) {
            case GoodLoop:
                return "цикл поддается распарллеливанию";
            case BadLoop:
                return "в цикле обнаружены проблемы";
            case Loop:
                return "нет данных по циклу";
        }
        return "";
    }
    public VisualiserFonts getFont() {
        switch (this) {
            case GoodLoop:
                return VisualiserFonts.GoodState;
            case BadLoop:
                return VisualiserFonts.BadState;
            default:
                return VisualiserFonts.UnknownState;
        }
    }
    public Highlighter.HighlightPainter getPainter() {
        switch (this) {
            case GoodLoop:
                return UI.GoodLoopPainter;
            case BadLoop:
                return UI.BadLoopPainter;
            default:
                return null;
        }
    }
}
