package ProjectData.SapforData.Loops;
import Common.UI.Themes.VisualiserFonts;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.FileObjectWithMessages;
public class Stop extends FileObjectWithMessages {
    public Stop(DBProjectFile father_in, int lineNum_in) {
        super(father_in, lineNum_in);
    }
    @Override
    public String Description() {
        return "оператор останова";
    }
    @Override
    public VisualiserFonts getFont() {
        return VisualiserFonts.BadState;
    }
}
