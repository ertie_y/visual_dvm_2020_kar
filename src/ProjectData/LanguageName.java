package ProjectData;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
public enum LanguageName {
    n,
    fortran,
    c,
    cpp;
    public String getDescription() {
        switch (this) {
            case fortran:
                return "Fortran";
            case c:
                return "С";
            case cpp:
                return "С++";
            case n:
                return "нет";
        }
        return "";
    }
    public String getDVMLink() {
        switch (this) {
            case fortran:
                return "flink";
            case c:
                return "clink";
            default:
                return "";
        }
    }
    public String getDVMCompile() {
        switch (this) {
            case fortran:
                return "f";
            case c:
                return "c";
            default:
                return "";
        }
    }
    public String getStyleKey() {
        switch (this) {
            case fortran:
                return //SyntaxConstants.SYNTAX_STYLE_FORTRAN;
                        "text/FortranSPF";
            case c:
                return SyntaxConstants.SYNTAX_STYLE_C;
            case cpp:
                return SyntaxConstants.SYNTAX_STYLE_CPLUSPLUS;
            default:
                return SyntaxConstants.SYNTAX_STYLE_NONE;
        }
    }
}
