package ProjectData.RSetting;
import Common.Database.DBTable;
import GlobalData.Settings.SettingName;
public class RecommendedSettingsDBTable extends DBTable<SettingName, RSetting> {
    public RecommendedSettingsDBTable() {
        super(SettingName.class, RSetting.class);
    }
    @Override
    public String getDataDescription() {
        return "рекомендуемая настройка";
    }
}
