package ProjectData.RSetting;
import Common.Database.DBObject;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import com.sun.org.glassfish.gmbal.Description;
public class RSetting extends DBObject {
    @Description("PRIMARY KEY,NOT NULL,UNIQUE")
    public SettingName name = SettingName.Undefined;
    public String value = "";
    public RSetting() {
    }
    public RSetting(SettingName name_in, Object value_in) {
        name = name_in;
        value = value_in.toString();
    }
    @Override
    public Object getPK() {
        return name;
    }
    @Override
    public String toString() {
        return name.getDescription() + "=" + Utils.DQuotes(value);
    }
}
