package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import Common.UI.UI;
import ProjectData.LanguageName;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class FileLanguageMenu extends StyledPopupMenu {
    public FileLanguageMenu() {
        for (LanguageName l : LanguageName.values()) {
            JMenuItem m = new StyledMenuItem(l.getDescription());
            m.addActionListener(
                    new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (Current.getFile().UpdateLanguage(l)) {
                                Current.getSapfor().ResetAllAnalyses();
                                Current.getFile().form.ShowLanguage();
                                UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
                            }
                        }
                    });
            add(m);
        }
    }
}