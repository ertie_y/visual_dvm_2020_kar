package ProjectData.Files.UI;

import Common.Current;
import Common.Global;
import Common.UI.Editor.CaretInfo;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.TextEditorMenu;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.UI.Editor.SPFEditor;
import ProjectData.SapforData.Functions.FuncCall;
import ProjectData.SapforData.Functions.FuncInfo;
import ProjectData.SapforData.Functions.FunctionType;
import ProjectData.SapforData.Loops.Loop;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MainEditorMenu extends TextEditorMenu {
    FuncCall call = null;
    FuncInfo decl = null;
    Loop loop = null;
    DBProjectFile header = null;
    //------------------
    JMenuItem m_comment;
    JMenuItem m_uncomment;
    JMenuItem m_inline;
    JMenuItem m_add_lines_to_region;
    JMenuItem m_remove_lines_from_region;
    JMenuItem m_loop_union;
    JMenuItem m_undo;
    JMenuItem m_gotoFunction;
    JMenuItem m_gotoHeader;

    //-----------------
    public MainEditorMenu(RSyntaxTextArea editor_in) {
        super(editor_in);
        addSeparator();
        m_gotoHeader = new StyledMenuItem("Перейти к заголовочному файлу", "/icons/Transformations/SPF_InsertIncludesPass.png");
        m_gotoHeader.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(header);
            }
        });
        add(m_gotoHeader);
        addSeparator();
        m_inline = new StyledMenuItem("Подставить вызов процедуры", "/icons/Transformations/SPF_InlineProcedures.png");
        m_inline.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Pass_2021.passes.get(PassCode_2021.SPF_InlineProcedure).Do(call);
                    }
                });
        add(m_inline);
        addSeparator();
        m_gotoFunction = new StyledMenuItem("Перейти к объявлению процедуры", "/icons/versions/currentVersion.png");
        m_gotoFunction.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        decl.Show(true);
                    }
                });
        add(m_gotoFunction);
        m_loop_union = new StyledMenuItem("Объединить цикл со следующим", "/icons/Transformations/SPF_LoopUnion.png");
        m_loop_union.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Pass_2021.passes.get(PassCode_2021.SPF_LoopUnionCurrent).Do();
                    }
                });
        add(m_loop_union);
        m_add_lines_to_region = new StyledMenuItem("Добавить строки в область", "/icons/Menu/AddLines.png");
        m_add_lines_to_region.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Pass_2021.passes.get(PassCode_2021.SPF_ChangeSpfIntervals).Do(
                                    ((RSyntaxTextArea) editor).getLineOfOffset(editor.getSelectionStart()) + 1,
                                    ((RSyntaxTextArea) editor).getLineOfOffset(editor.getSelectionEnd()) + 1,
                                    1
                            );
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(m_add_lines_to_region);
        m_remove_lines_from_region = new StyledMenuItem("Удалить строки из области", "/icons/Menu/RemoveLines.png");
        m_remove_lines_from_region.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Pass_2021.passes.get(PassCode_2021.SPF_ChangeSpfIntervals).Do(
                                    ((RSyntaxTextArea) editor).getLineOfOffset(editor.getSelectionStart()) + 1,
                                    ((RSyntaxTextArea) editor).getLineOfOffset(editor.getSelectionEnd()) + 1,
                                    0
                            );
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(m_remove_lines_from_region);
        addSeparator();
        m_comment = new StyledMenuItem("Закомментировать блок", "/icons/Editor/Comment.png");
        m_comment.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String new_ = "";
                        String[] data = selectedText.split("\n");
                        int i = 0;
                        switch (Current.getFile().languageName) {
                            case fortran:
                                for (String line : data) {
                                    if (!line.startsWith("!")) {
                                        new_ += "!" + line;
                                    } else new_ += line;
                                    if (i < data.length - 1) new_ += "\n";
                                    ++i;
                                }
                                break;
                            case c:
                            case cpp:
                                for (String line : data) {
                                    if (!line.startsWith("//")) {
                                        new_ += "//" + line;
                                    } else new_ += line;
                                    if (i < data.length - 1) new_ += "\n";
                                    ++i;
                                }
                                break;
                            default:
                                new_ = selectedText;
                                break;
                        }
                        editor.replaceSelection(new_);
                    }
                });
        add(m_comment);
        m_uncomment = new StyledMenuItem("Раскомментировать блок", "/icons/Editor/Uncomment.png");
        m_uncomment.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String new_ = "";
                        String[] data = selectedText.split("\n");
                        int i = 0;
                        switch (Current.getFile().languageName) {
                            case fortran:
                                for (String line : data) {
                                    if (line.startsWith("!")) {
                                        new_ += line.substring(1);
                                    } else new_ += line;
                                    if (i < data.length - 1) new_ += "\n";
                                    ++i;
                                }
                                break;
                            case c:
                            case cpp:
                                for (String line : data) {
                                    if (line.startsWith("//")) {
                                        new_ += line.substring(2);
                                    } else new_ += line;
                                    if (i < data.length - 1) new_ += "\n";
                                    ++i;
                                }
                                break;
                            default:
                                new_ = selectedText;
                                break;
                        }
                        //todo. возможно, изменить концепцию на выделенные строки?
                        editor.replaceSelection(new_);
                    }
                });
        add(m_uncomment);
        addSeparator();
        m_undo = new StyledMenuItem("Отменить последнюю модификацию", "/icons/Menu/Undo.png");
        m_undo.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Current.getSapfor().UpdateProjectFiles(false);
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(m_undo);
    }

    @Override
    public void CheckElementsVisibility() {
        super.CheckElementsVisibility();
        //-----------------------------------------------
        call = null;
        decl = null;
        header = null;

        if (selectedText != null) {
            call = Current.getFile().find_func_call(selectedText);
            if (call != null) {
                FuncInfo decl_ = Current.getProject().allFunctions.get(call.funcName);
                decl = decl_.type.equals(FunctionType.Default) ? decl_ : null;
            }
        }
        CaretInfo caretInfo = ((SPFEditor)editor).getCaretInfo();
        if (caretInfo!=null){
            String header_ = Utils.extractHeaderName(caretInfo.current_line);
            if (header_ != null) {
                if (Current.getFile().relativeHeaders.containsKey(header_))
                    header = Current.getFile().relativeHeaders.get(header_);
            }
        }
        //---
        if (call != null)
            System.out.println("call:" + call.funcName + " " + call.canBeInlined + " ------>>>");
        else
            System.out.println("null");
        if (decl != null)
            System.out.println("decl=" + decl.funcName + " ------>>>");
        else
            System.out.println("decl=null");
        if (header!=null){
            System.out.println("header = "+header.name+" ------->>>");
        } else
            System.out.println("header=null");

        //---
        loop = Current.getFile().find_current_loop();
        //-------------------------------------------------------------
        if (Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctions).isDone()) {
            if ((call != null) && (call.canBeInlined)) {
                m_inline.setVisible(true);
                m_inline.setText("Подставить вызов процедуры " + Utils.Brackets(call.funcName));
                m_gotoFunction.setVisible(true);
            } else m_inline.setVisible(false);
            if (decl != null) {
                m_gotoFunction.setVisible(true);
                m_gotoFunction.setText("Перейти к объявлению процедуры " + Utils.Brackets(decl.funcName));
            } else m_gotoFunction.setVisible(false);
        } else {
            m_inline.setVisible(false);
            m_gotoFunction.setVisible(false);
        }
        //пока пусть вся пдстановка идет через преобразования.
        if (Pass_2021.passes.get(PassCode_2021.SPF_GetGraphLoops).isDone() && (loop != null)) {
            m_loop_union.setVisible(true);
            m_loop_union.setText("Объединить цикл в строке " + Utils.Brackets(loop.line) + " со следующим");
        } else m_loop_union.setVisible(false);
        //-
        if (Pass_2021.passes.get(PassCode_2021.SPF_GetIncludeDependencies).isDone() && (header != null)) {
            m_gotoHeader.setVisible(true);
            m_gotoHeader.setText("Перейти к заголовочному файлу " + Utils.Brackets(header.name));
        } else m_gotoHeader.setVisible(false);

        m_comment.setVisible(selectedText != null);
        m_uncomment.setVisible(selectedText != null);
        m_add_lines_to_region.setVisible(selectedText != null);
        m_remove_lines_from_region.setVisible(selectedText != null);
        m_undo.setVisible(!Current.getSapfor().OldFiles.isEmpty());
    }
}
