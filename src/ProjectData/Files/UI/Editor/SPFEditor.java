package ProjectData.Files.UI.Editor;

import Common.Current;
import Common.Global;
import Common.UI.Editor.BaseEditor;
import Common.UI.Editor.CaretInfo;
import Common.UI.Menus.StyledPopupMenu;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileState;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
import ProjectData.Files.UI.MainEditorMenu;
import ProjectData.SapforData.Loops.Loop;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.SPFEditorInterface;
import javafx.util.Pair;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextAreaHighlighter;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.util.Vector;

public class SPFEditor extends BaseEditor implements SPFEditorInterface {
    public DBProjectFile file = null; //файл связанный с редактором
    //----
    public RSyntaxTextAreaHighlighter highlighter = null;
    Vector<Object> loopsHighlights = new Vector<>();
    Vector<Object> gcovHighlights = new Vector<>();
    //----
    public SapforAutoComplete autoComplete = null;
    public boolean switching_language = false;
    //-включение /отключение событий --
    private CaretInfo caretInfo = new CaretInfo();

    public SPFEditor(DBProjectFile file_in) {
        file = file_in;
        highlighter = (RSyntaxTextAreaHighlighter) getHighlighter();
        autoComplete = new SapforAutoComplete(this);
        //-------------------------
        float font_size = (float) Global.db.settings.get(SettingName.EditorFontSize).toInt32();
        setFont(getFont().deriveFont(font_size));
        //-------------------------
        setText(Utils.ReadAllText(file.file).replace("\r", " "));
        gotoLine(file.lastLine);
        //-------------------------
        addCaretListener(сe -> UpdateCaretInfo());
        getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
            }

            @Override
            public void changedUpdate(DocumentEvent arg0) {
                if (!switching_language) {
                    file.NeedsSave = true;
                    file.father.updateGCOV_status(0);
                    if (Global.enable_text_changed && file.state != FileState.Excluded) {
                        Current.getSapfor().ResetAllAnalyses();
                        //текст изменился, значит M ка более не актуальна.
                        file.father.dropLastModification();
                    }
                    Pass_2021.passes.get(PassCode_2021.Save).Reset();
                }
            }
        });
        //----
    }

    public void ScrollChanged() {
        UI.Info("scroll changed");
    }

    //---------------------------------------------------->>
    public static String getLastWord(String text, char... separators) {
        Vector<Character> separators_ = new Vector<>();
        for (char s : separators)
            separators_.add(s);
        String res = "";
        char[] letters = text.toCharArray();
        for (int i = letters.length - 1; i >= 0; --i) {
            char c = letters[i];
            if (separators_.contains(c)) break;
            else res = c + res;
        }
        return res;
    }

    public static String getFirstWord(String text, char... separators) {
        Vector<Character> separators_ = new Vector<>();
        for (char s : separators)
            separators_.add(s);
        String res = "";
        char[] letters = text.toCharArray();
        for (int i = 0; i < letters.length; ++i) {
            char c = letters[i];
            if (separators_.contains(c)) break;
            else res += c;
        }
        return res;
    }

    public CaretInfo getCaretInfo() {
        return caretInfo;
    }

    public void UpdateCaretInfo() {
        caretInfo = new CaretInfo(this);
        autoComplete.Parse();
    }

    @Override
    public void ClearHighlights() {
        highlighter.removeAllHighlights();
    }

    @Override
    public void ClearLoopsHighLights() {
        for (Object loop : loopsHighlights)
            highlighter.removeHighlight(loop);
    }

    @Override
    public void ClearGOCVHighlights() {
        for (Object line : gcovHighlights)
            highlighter.removeHighlight(line);
    }

    @Override
    public void HighlightLoops() {
        ClearLoopsHighLights();
        for (Loop loop : file.AllLoops.values()) {
            Highlighter.HighlightPainter painter = loop.loopState.getPainter();
            if (painter != null) {
                try {
                    loopsHighlights.add(highlighter.addHighlight(
                            getLineStartOffset(loop.line - 1),
                            getLineEndOffset(loop.line - 1),
                            painter
                    ));

                } catch (BadLocationException ex) {
                    Global.Log.PrintException(ex);
                }
            }
        }
    }

    //нумерация с нуля!
    boolean lineNumberIsValid(int num) {
        return (num >= 0) && (num < this.getLineCount());
    }

    public static final Color never = new Color(229, 228, 226, 90);

    @Override
    public void HighlightGCOV() {
        ClearGOCVHighlights();
        for (int lineNum : file.gcov_info.line_info.keySet()) {
            Pair<Long, Integer> p = file.gcov_info.line_info.get(lineNum);
            //System.out.println((lineNum + 1) + ": " + p.getKey() + " " + p.getValue() + "%");
            Color color = never;
            if (p.getKey() > 0) {
                color = (p.getValue() >= Global.db.settings.get(SettingName.GCOVLimit).toInt32()) ?
                        new Color(255, 255, (100 - p.getValue()), 90) : null;
            }
            if (color != null) {
                Highlighter.HighlightPainter painter = new DefaultHighlighter.DefaultHighlightPainter(color);
                try {
                    gcovHighlights.add(highlighter.addHighlight(
                            getLineStartOffset(lineNum),
                            getLineEndOffset(lineNum),
                            painter
                    ));
                } catch (Exception ignored) {
                }
            }
        }
    }

    @Override
    protected void saveFont() {
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                SettingName.EditorFontSize,
                getFont().getSize());
    }

    @Override
    protected void saveText() {
        Pass_2021.passes.get(PassCode_2021.Save).Do();
    }

    @Override
    protected StyledPopupMenu createMenu() {
        return new MainEditorMenu(this);
    }

    public int getCurrentLine() {
        return getCaretLineNumber() + 1;
    }

    public int getCurrentSymbol() {
        return getCaretOffsetFromLineStart() + 1;
    }

    public void Test() {
        try {


            //  HighlightFullLine(0, new Color(255, 0, 0, 80));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void HighlightFullLine(int lineNum, Color lineColor) {
        Rectangle Rect = this.getVisibleRect();
        Graphics g = this.getGraphics();
        g.setPaintMode();
        int height = getLineHeight();
        g.setColor(lineColor);

        try {
            g.fillRect(Rect.x, this.yForLine(lineNum), Rect.width, height);
        } catch (BadLocationException e) {
            e.printStackTrace();
        }
    }
}
