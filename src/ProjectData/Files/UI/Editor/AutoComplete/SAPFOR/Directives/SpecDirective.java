package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import Common.Utils.Utils;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
//промежуточный тип. относится ко всем директивам со скобками и требующим запятых.
public class SpecDirective extends BaseDirective {
    protected String prefix;
    protected String word;
    protected String suffix;
    public SpecDirective(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    public String get_word() {
        boolean isNoinline = name.equals(DirectiveName.NOINLINE);
        return name.getText() + (isNoinline ? "" : "()");
    }
    @Override
    public String getReplacementText() {
        prefix = Utils.pack(getCaretInfo().before.substring(0, getCaretInfo().before.length() - getCaretInfo().prefix_word.length()));
        word = get_word();
        suffix = Utils.pack(getCaretInfo().after.substring(getCaretInfo().suffix_word.length()));
        String pp = prefix.replace(" ", "");
        String ps = suffix.replace(" ", "");
        if (!pp.isEmpty() && (pp.charAt(pp.length() - 1) != ',') && (pp.charAt(pp.length() - 1) != '('))
            word = ("," + word);
        if ((!ps.isEmpty()) && (ps.charAt(0) != ',') && (ps.charAt(0) != ')'))
            word = (word + ",");
        if ((ps.isEmpty()) || ps.charAt(ps.length() - 1) != ')')
            word += ")";
        return word;
    }
}
