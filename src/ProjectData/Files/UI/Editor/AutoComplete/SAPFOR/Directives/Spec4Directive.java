package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class Spec4Directive extends SpecDirective {
    public Spec4Directive(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public boolean Check() {
        return (getOwner().flags.size() > 1)
                && getOwner().flags.get(0).equals(DirectiveName.SPF)
                && getOwner().flags.get(1).equals(DirectiveName.CHECKPOINT)
                && getOwner().nearestGroup.equals(DirectiveName.CHECKPOINT);
    }
}
