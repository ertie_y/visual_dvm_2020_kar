package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class TypeDirective extends RegionDirective {
    public TypeDirective(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public String getReplacementText() {
        return super.getReplacementText() + "()";
    }
}
