package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers.BaseProvider;
public class Spec3Directive extends SpecDirective {
    public Spec3Directive(BaseProvider provider_in, DirectiveName name_in) {
        super(provider_in, name_in);
    }
    @Override
    public boolean Check() {
        return (getOwner().flags.size() > 1)
                && getOwner().flags.get(0).equals(DirectiveName.SPF)
                && getOwner().flags.get(1).equals(DirectiveName.TRANSFORM)
                && getOwner().nearestGroup.equals(DirectiveName.TRANSFORM)
                && !getOwner().flags.contains(DirectiveName.NOINLINE)
                && !getOwner().flags.contains(DirectiveName.FISSION)
                && !getOwner().flags.contains(DirectiveName.SHRINK)
                && !getOwner().flags.contains(DirectiveName.EXPAND);
    }
}
