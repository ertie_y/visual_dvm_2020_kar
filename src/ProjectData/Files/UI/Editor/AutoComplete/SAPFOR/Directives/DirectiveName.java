package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives;
public enum DirectiveName {
    UNDEFINED,
    //--------------
    START,
    SPF,
    ANALYSIS,
    PARALLEL,
    TRANSFORM,
    PARALLEL_REG,
    END_PARALLEL_REG,
    REDUCTION,
    MAX,
    MIN,
    SUM,
    PROD,
    AND,
    OR,
    EQV,
    NEQV,
    MAXLOC,
    MINLOC,
    PRIVATE,
    SHADOW,
    ACROSS,
    REMOTE_ACCESS,
    NOINLINE,
    FISSION,
    EXPAND,
    SHRINK,
    CHECKPOINT,
    INTERVAL,
    FILES_COUNT,
    TIME,
    ITER,
    VARLIST,
    EXCEPT,
    TYPE,
    ASYNC,
    FLEXIBLE,
    PARAMETER;
    public String getDescription() {
        switch (this) {
            case SPF:
                return "SPF директива";
            case ANALYSIS:
                return "Директива анализа";
            case PARAMETER:
                return "Директива параметра задачи";
            case PARALLEL:
                return "Директива распараллеливания";
            case TRANSFORM:
                return "Директива трансформации";
            case PARALLEL_REG:
                return "Директива начала области распараллеливания";
            case END_PARALLEL_REG:
                return "Директива конца области распараллеливания";
            case REDUCTION:
                return "Клауза редукции";
            case MAX:
                return "Операция MAX";
            case MIN:
                return "Операция MIN";
            case SUM:
                return "Операция SUM";
            case PROD:
                return "Операция PROD";
            case AND:
                return "Операция AND";
            case OR:
                return "Операция OR";
            case EQV:
                return "Операция EQV";
            case NEQV:
                return "Операция NEQV";
            case MAXLOC:
                return "Операция MAXLOC";
            case MINLOC:
                return "Операция MINLOC";
            case PRIVATE:
                return "Клауза приватных переменных";
            case SHADOW:
                return "Клауза теневых граней";
            case ACROSS:
                return "Клауза регулярных зависимостей";
            case REMOTE_ACCESS:
                return "Клауза удаленных ссылок";
            case NOINLINE:
                return "Клауза для отмены подстановки процедуры";
            case FISSION:
                return "Клауза для разделения циклов";
            case EXPAND:
                return "Клауза для расширения приватных переменных";
            case SHRINK:
                return "Клауза для сужения приватных переменных";
            case CHECKPOINT:
                return "Директива контрольной точки";
            case INTERVAL:
                return "Клауза интервала";
            case FILES_COUNT:
                return "Клауза количества файлов";
            case VARLIST:
                return "Клауза переменных";
            case EXCEPT:
                return "Клауза исключённых переменных";
            case TYPE:
                return "Клауза типа";
            case ITER:
                return "По достижении итерации";
            case TIME:
                return "По истечении времени (сек)";
            case ASYNC:
                return "Асинхронная";
            case FLEXIBLE:
                return "Гибкая";
            default:
                return "?";
        }
    }
    public String getText() {
        switch (this) {
            case SPF:
                return "!$SPF ";
            case END_PARALLEL_REG:
                return "END PARALLEL_REG";
            default:
                return toString();
        }
    }
}
