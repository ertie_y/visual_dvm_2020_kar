package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.Spec4Directive;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class Spec4Provider extends PrefixWordProvider {
    public Spec4Provider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new Spec4Directive(this, DirectiveName.INTERVAL));
        addDirective(new Spec4Directive(this, DirectiveName.FILES_COUNT));
        addDirective(new Spec4Directive(this, DirectiveName.VARLIST));
        addDirective(new Spec4Directive(this, DirectiveName.EXCEPT));
        addDirective(new Spec4Directive(this, DirectiveName.TYPE));
    }
}
