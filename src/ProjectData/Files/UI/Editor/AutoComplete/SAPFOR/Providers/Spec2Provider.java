package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.Spec2Directive;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class Spec2Provider extends PrefixWordProvider {
    public Spec2Provider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new Spec2Directive(this, DirectiveName.SHADOW));
        addDirective(new Spec2Directive(this, DirectiveName.ACROSS));
        addDirective(new Spec2Directive(this, DirectiveName.REMOTE_ACCESS));
    }
}
