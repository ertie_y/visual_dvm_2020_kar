package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.Spec1Directive;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class Spec1Provider extends PrefixWordProvider {
    public Spec1Provider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new Spec1Directive(this, DirectiveName.REDUCTION));
        addDirective(new Spec1Directive(this, DirectiveName.PRIVATE));
        addDirective(new Spec1Directive(this, DirectiveName.PARAMETER));
    }
}
