package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.RedListDirective;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class ReductionProvider extends PrefixWordProvider {
    public ReductionProvider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new RedListDirective(this, DirectiveName.MAX));
        addDirective(new RedListDirective(this, DirectiveName.MIN));
        addDirective(new RedListDirective(this, DirectiveName.MAXLOC));
        addDirective(new RedListDirective(this, DirectiveName.MINLOC));
        addDirective(new RedListDirective(this, DirectiveName.AND));
        addDirective(new RedListDirective(this, DirectiveName.OR));
        addDirective(new RedListDirective(this, DirectiveName.EQV));
        addDirective(new RedListDirective(this, DirectiveName.NEQV));
        addDirective(new RedListDirective(this, DirectiveName.SUM));
        addDirective(new RedListDirective(this, DirectiveName.PROD));
    }
}
