package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.DirectiveName;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Directives.Spec3Directive;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
public class Spec3Provider extends PrefixWordProvider {
    public Spec3Provider(SapforAutoComplete owner_in) {
        super(owner_in);
        addDirective(new Spec3Directive(this, DirectiveName.NOINLINE));
        addDirective(new Spec3Directive(this, DirectiveName.FISSION));
        addDirective(new Spec3Directive(this, DirectiveName.SHRINK));
        addDirective(new Spec3Directive(this, DirectiveName.EXPAND));
    }
}
