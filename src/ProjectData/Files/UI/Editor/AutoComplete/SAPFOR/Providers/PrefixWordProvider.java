package ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.Providers;
import ProjectData.Files.UI.Editor.AutoComplete.SAPFOR.SapforAutoComplete;
import ProjectData.Files.UI.Editor.SPFEditor;

import javax.swing.text.JTextComponent;
//https://github.com/bobbylight/AutoComplete/blob/master/AutoCompleteDemo/src/main/java/org/fife/ui/autocomplete/demo/DemoRootPane.java
//ОБРАЗЕЦ
//отличается тем, что принимает на вход дополнительные символы
// и берет не всю строку перед кареткой а только последнее слово
public class PrefixWordProvider extends BaseProvider {
    public PrefixWordProvider(SapforAutoComplete owner_in) {
        super(owner_in);
    }
    @Override
    public String getAlreadyEnteredText(JTextComponent comp) {
        return ((SPFEditor) comp).getCaretInfo().prefix_word;
    }
}


