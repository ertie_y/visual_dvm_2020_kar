package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;
//https://javaswing.wordpress.com/2009/10/10/jlist_using/
//https://qarchive.ru/14623936_kak_poluchit__vydelennyi__tekst_elementa_iz_jlist_s_izobrazheniem_
public class FilesHyperlinksPanel extends JPanel {
    public Vector<String> links = null;
    public JList<String> Hyperlinks = null;
    MouseAdapter adapter = null;
    public FilesHyperlinksPanel() {
        super(new BorderLayout());
        //о курсорах https://javaswing.wordpress.com/2010/07/13/cursor/
        setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    }
    public void UpdateByCell(Vector<String> links_in) {
        links = links_in;
        removeAll();
        Hyperlinks = new JList<>(links);
        Hyperlinks.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        if (adapter != null) {
            removeMouseListener(adapter);
            adapter = null;
        }
        Hyperlinks.addMouseListener(adapter = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = Hyperlinks.locationToIndex(e.getPoint());
                if (index >= 0) {
                    String[] data = links.get(index).split(":");
                    String file = data[0];
                    int line = Integer.parseInt(data[1]);
                    UI.getNewMainWindow().getProjectWindow().GotoFile(file, line, true);
                }
            }
        });
        Hyperlinks.setLayoutOrientation(JList.VERTICAL);
        Hyperlinks.setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
        add(Hyperlinks, BorderLayout.CENTER);
    }
}




