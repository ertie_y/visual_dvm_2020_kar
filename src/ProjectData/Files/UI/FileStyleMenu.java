package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import ProjectData.Files.LanguageStyle;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class FileStyleMenu extends StyledPopupMenu {
    public FileStyleMenu() {
        for (LanguageStyle s : LanguageStyle.values()) {
            JMenuItem m = new StyledMenuItem(s.getDescription());
            m.addActionListener(
                    new AbstractAction() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            if (Current.getFile().UpdateStyle(s)) {
                                Current.getSapfor().ResetAllAnalyses();
                                Current.getFile().form.ShowStyle();
                            }
                        }
                    });
            add(m);
        }
    }
}
