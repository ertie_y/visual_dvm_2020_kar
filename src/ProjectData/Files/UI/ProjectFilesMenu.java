package ProjectData.Files.UI;
import Common.Current;
import Common.Global;
import Common.UI.Menus.GraphMenu;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Trees.StyledTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.UI.PassesSubMenu;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
public class ProjectFilesMenu extends GraphMenu {
    public ProjectFilesMenu(StyledTree tree) {
        super(tree, "подпапки");
        addSeparator();
        JMenuItem m = new StyledMenuItem("Открыть в проводнике...", "/icons/Explorer.png");
        m.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Desktop.getDesktop().open(Current.getProject().Home);
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(m);
        addSeparator();
        add(new PassesSubMenu("Добавить", "/icons/RedAdd.png",
                PassCode_2021.AddFile,
                PassCode_2021.CreateEmptyDirectory,
                PassCode_2021.ImportFiles));
        addSeparator();
        add(new PassesSubMenu("Переименовать", "/icons/Menu/Rename.png",
                PassCode_2021.RenameFile,
                PassCode_2021.RenameDirectory));
        addSeparator();
        add(new PassesSubMenu("Удалить", "/icons/Delete.png",
                PassCode_2021.DeleteFile,
                PassCode_2021.DeleteDirectory));
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.ExcludeFile).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.IncludeFile).getMenuItem());
    }
}
