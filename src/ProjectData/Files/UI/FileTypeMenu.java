package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import Common.UI.UI;
import ProjectData.Files.FileType;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class FileTypeMenu extends StyledPopupMenu {
    public FileTypeMenu() {
        for (FileType f : FileType.values()) {
            if (f != FileType.forbidden) {
                JMenuItem m = new StyledMenuItem(f.getDescription());
                m.addActionListener(
                        new AbstractAction() {
                            @Override
                            public void actionPerformed(ActionEvent e) {
                                if (Current.getFile().UpdateType(f)) {
                                    Current.getSapfor().ResetAllAnalyses();
                                    Current.getFile().form.ShowType();
                                    UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
                                }
                            }
                        });
                add(m);
            }
        }
    }
}