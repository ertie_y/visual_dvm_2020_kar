package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.Trees.StyledTreeCellRenderer;
import ProjectData.Files.DBProjectFile;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
//https://kodejava.org/how-do-i-create-jtree-with-different-icons-for-each-node/
//https://ru.coredump.biz/questions/14968005/dynamically-change-icon-of-specific-nodes-in-jtree
public class FilesTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof File) {
            //это папка.
            File dir = (File) o;
            setIcon(new ImageIcon(getClass().getResource("/icons/Folder.png")));
            setText(dir.getName());
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
        } else if (o instanceof DBProjectFile) {
            DBProjectFile file = (DBProjectFile) o;
            setIcon(file.GetIcon());
            if (file.IsMain())
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeBold));
            else
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
            setText(file.file.getName());
        }
        setForeground(tree.getForeground());
        return this;
    }
}
