package ProjectData.Files.UI;
import Common.Current;
import Common.UI.Trees.DataTree;
import Common.UI.Trees.TreeRenderers;
import Common.UI.UI;
import ProjectData.SapforData.FileObject;
import ProjectData.SapforData.FileObjectWithMessages;
import com.sun.org.apache.xml.internal.resolver.readers.ExtendedXMLCatalogReader;

import javax.swing.tree.DefaultMutableTreeNode;
public class FileGraphTree extends DataTree {
    public FileGraphTree(DefaultMutableTreeNode root) {
        super(root);
        ExpandAll();
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererGraph;
    }
    @Override
    public Current getCurrent() {
        return Current.FileGraphElement;
    }
    @Override
    public void ShowCurrentObject() throws Exception {
        Current.getFile().form.EventsOff();
        Object o = Current.get(getCurrent());
        Current.getFile().form.getEditor().gotoLine((o instanceof FileObjectWithMessages) ? (((FileObjectWithMessages) o).line) : 1);
        Current.getFile().form.ShowMessages();
        Current.getFile().form.EventsOn();
    }
    @Override
    protected boolean findNode(Object userObject, Object criteria) {
        return (userObject instanceof FileObject) && (
                ((FileObject) (userObject)).line == (Integer) criteria);
    }

    @Override
    protected int getStartLine() {
        return 1;
    }
}