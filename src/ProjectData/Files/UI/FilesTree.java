package ProjectData.Files.UI;
import Common.Current;
import Common.UI.DragDrop.FileDrop;
import Common.UI.Menus.GraphMenu;
import Common.UI.Trees.StyledTree;
import Common.UI.Trees.TreeRenderers;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
public class FilesTree extends StyledTree {
    private static void forkFD(PassCode_2021 file_pass, PassCode_2021 folder_pass) {
        DefaultMutableTreeNode node = Current.getProjectNode();
        if (node != null)
            Pass_2021.passes.get((node.getUserObject() instanceof DBProjectFile) ?
                    file_pass : folder_pass).Do();
    }
    public FilesTree() {
        super(Current.getProject().filesTreeRoot);
        this.addKeyListener(new KeyAdapter() {
                                @Override
                                public void keyPressed(KeyEvent e) {
                                    switch (e.getKeyCode()) {
                                        case KeyEvent.VK_DELETE:
                                            forkFD(PassCode_2021.DeleteFile, PassCode_2021.DeleteDirectory);
                                            break;
                                        case KeyEvent.VK_ADD: //num lock  +
                                        case KeyEvent.VK_EQUALS:    //+
                                            Pass_2021.passes.get(PassCode_2021.IncludeFile).Do();
                                            break;
                                        case KeyEvent.VK_SUBTRACT:  //num lock -
                                        case KeyEvent.VK_MINUS: //-
                                            Pass_2021.passes.get(PassCode_2021.ExcludeFile).Do();
                                            break;
                                        case KeyEvent.VK_F2:
                                            forkFD(PassCode_2021.RenameFile, PassCode_2021.RenameDirectory);
                                            break;
                                        case KeyEvent.VK_ENTER:
                                            LeftMouseAction2();
                                            break;
                                    }
                                }
                            }
        );
        new FileDrop(System.out, this, files -> {
            Pass_2021.passes.get(PassCode_2021.ImportFiles).Do(files);
        });
        Current.set(Current.File, null);
    }
    @Override
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererFile;
    }
    @Override
    protected GraphMenu createMenu() {
        return new ProjectFilesMenu(this);
    }
    @Override
    public void SelectionAction(TreePath e) {
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) e.getLastPathComponent();
        Current.set(Current.ProjectNode, node);
        Object o = node.getUserObject();
        if (o instanceof File) {
            Current.set(Current.SelectedDirectory, o);
            Current.set(Current.SelectedFile, null);
            UI.getNewMainWindow().getProjectWindow().ShowNoSelectedFile();
        } else if (o instanceof DBProjectFile) {
            Current.set(Current.SelectedFile, o);
            File file = ((DBProjectFile) o).file;
            Current.set(Current.SelectedDirectory, file.getParentFile());
            UI.getNewMainWindow().getProjectWindow().ShowSelectedFile();
        }
        UI.getNewMainWindow().getProjectWindow().ShowSelectedDirectory();
    }
    @Override
    public void LeftMouseAction2() {
        if (Current.getProjectNode() != null) {
            Object o = Current.getProjectNode().getUserObject();
            if (o instanceof DBProjectFile) {
                //очень важно. иначе по открытии файла дерево остается в фокусе.
                //и не происходит прокрутки скролла к строке!!
                UI.getNewMainWindow().getProjectWindow().FocusFileTabs();
                Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(o);
            }
        }
    }
}
