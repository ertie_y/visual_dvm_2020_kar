package ProjectData.Files;
public enum LanguageStyle {
    none,
    fixed,
    extended,
    free;
    public String getDescription() {
        switch (this) {
            case fixed:
                return "Фиксированный";
            case extended:
                return "Расширенный";
            case free:
                return "Свободный";
            case none:
                return "нет";
        }
        return "";
    }
    public int getMargin() {
        switch (this) {
            case fixed:
                return 72;
            case extended:
                return 132;
            case free:
                return 160;
            default:
                return 0;
        }
    }
}
