package ProjectData.Files;

import Common.Current;
import Common.Database.DBObject;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.GCOV.GCOV_info;
import ProjectData.LanguageName;
import ProjectData.Messages.Errors.MessageError;
import ProjectData.Messages.Message;
import ProjectData.Messages.Notes.MessageNote;
import ProjectData.Messages.Warnings.MessageWarning;
import ProjectData.SapforData.Arrays.ArrayDecl;
import ProjectData.SapforData.FileObjectWithMessages;
import ProjectData.SapforData.Functions.FuncCall;
import ProjectData.SapforData.Functions.FuncInfo;
import ProjectData.SapforData.Loops.Loop;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.UI.Main.FileForm;
import com.sun.org.glassfish.gmbal.Description;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Vector;

//по файлам нет смысла делать совместимость.
//так что переименую
public class DBProjectFile extends DBObject {
    public static final String no_data = "Нет данных";
    public static final String dep = ".dep";
    public static final String out = ".out";
    public static final String err = ".err";
    //</editor-fold>
    //<editor-fold desc="хранимые в бд поля">
    @Description("PRIMARY KEY, UNIQUE")
    public String name;  //имя относительно корневой папки проекта. нужно только как ключ для бд!!
    @Description("IGNORE")
    public String last_assembly_name = ""; //имя объектника.
    //--------------------------------------
    //в сотальных случаях используем file.
    public FileType fileType = FileType.none;
    public LanguageName languageName = LanguageName.n;
    public LanguageStyle style = LanguageStyle.none;
    public String options = ""; //пользовательские опции для парсера.
    // public int caretPosition = 0;
    //---
    @Description("DEFAULT 0")
    public int lastLine = 0;
    @Description("DEFAULT ''")
    public String GCOVLog = "";

    public void UpdateLastLine(int line_in) {
        if (lastLine != line_in) {
            try {
                lastLine = line_in;
                father.db.Update(this, "lastLine");
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }

        }
    }

    //---
    public FileState state = FileState.Undefined; //состояние файла.
    public int isMain = 0; //содержит ли ГПЕ
    @Description("IGNORE")
    public int lines_count = 0;
    @Description("IGNORE")
    public boolean NeedsSave = false;
    @Description("IGNORE")
    public String LoopGraphTitle = no_data;
    @Description("IGNORE")
    public String CallGraphTitle = no_data;
    @Description("IGNORE")
    public String ArrayGraphTitle = no_data;
    public db_project_info father = null;
    public File file = null;
    public DefaultMutableTreeNode node = null; //узел файла в дереве
    public FileForm form = null; //отображение.
    //сообщения.
    //----------------
    //анализы
    public Vector<Loop> LoopNests = new Vector<>();
    public LinkedHashMap<Integer, Loop> AllLoops = new LinkedHashMap<>();
    public LinkedHashMap<String, FuncInfo> function_decls = new LinkedHashMap<>(); //объявления
    public Vector<ArrayDecl> array_decls = new Vector<>();
    public LinkedHashMap<String, DBProjectFile> relativeHeaders = new LinkedHashMap<>();
    public GCOV_info gcov_info = new GCOV_info();


    public DBProjectFile() {
    }

    public DBProjectFile(File file_, db_project_info father_) {
        Init(file_, father_);
        //имя относительно папки проекта.
        RefreshName();
        AutoDetectProperties();
    }

    public static Pair<Integer, String> decodeParserMessage(String S, String file_in) {
        Integer line = 1;
        String file = file_in;
        String[] data = S.split("on line ");
        if (data.length > 1) {
            String[] data1 = data[1].split(" ");
            if (data1.length > 0)
                line = Integer.parseInt(data1[0]);
        }
        data = S.split("of");
        if (data.length > 1) {
            String[] data1 = data[1].split(":");
            if (data1.length > 0) {
                file = Utils.toW(data1[0].substring(1)); //первый символ тут всегда пробел. слеши всегда виндовые.
            }
        }
        return new Pair<>(line, file);
    }

    //наследуем от объекта, чтобы использовать интерфейс выбора.
    @Override
    public Object getPK() {
        return name;
    }

    @Description("IGNORE")
    public boolean IsMain() {
        return isMain != 0;
    }

    /*
     * получить имя относительно домашней папки проекта, в рамках текущей ОС    *
     */
    public String getLocalName() {
        return file.getAbsolutePath().substring(father.Home.getAbsolutePath().length());
    }

    public void Init(File file_, db_project_info father_) {
        father = father_;
        file = file_;
    }

    public void RefreshName() {
        String path = file.getAbsolutePath();
        //для совместимости пусть палки будут от винды всегда.
        name = path.substring(father.Home.getAbsolutePath().length() + 1).replace('/', '\\');
    }

    public void AutoDetectProperties() {
        switch (Utils.getExtension(file)) {
            case "f":
            case "fdv":
            case "for":
            case "f77":
                fileType = FileType.program;
                languageName = LanguageName.fortran;
                style = LanguageStyle.fixed;
                break;
            case "f90":
                fileType = FileType.program;
                languageName = LanguageName.fortran;
                style = LanguageStyle.free;
                break;
            case "c":
            case "cdv":
                fileType = FileType.program;
                languageName = LanguageName.c;
                break;
            case "cpp":
                fileType = FileType.program;
                languageName = LanguageName.cpp;
                break;
            case "h":
                fileType = FileType.header;
                break;
            case "fh":
                fileType = FileType.header;
                languageName = LanguageName.fortran;
                break;
            case "o":
            case "gcda":
            case "gcno":
            case "gcov":
            case "exe":
            case "pdf":
                fileType = FileType.forbidden;
                break;
            case "":
                if (Utils.isDigit(file.getName())) {
                    fileType = FileType.forbidden;
                } else {
                    state = FileState.Excluded;
                    fileType = FileType.none;
                    languageName = LanguageName.n;
                    if (file.isFile())
                        UI.Info("файлы с именем [makefile] будет игнорироваться при сборке проекта.\n" +
                                "Пользовательские мейкфайлы запрещены.");
                }
                break;
            default:
                //все остальное считаем исключенными из рассмотрения.
                //если юзеру надо сам их разблочит.
                state = FileState.Excluded;
                fileType = FileType.none;
                languageName = LanguageName.n;
                break;
        }
    }

    public boolean isMakefile() {
        return file.getName().equalsIgnoreCase("makefile");
    }

    public String ImageKey() {
        //icons/files/Excludeddata.png
        String pref = "/icons/files/";
        String body;
        switch (fileType) {
            case program:
                body = state.toString() + languageName.toString();
                break;
            default:
                body = state.toString();
                break;
        }
        return pref + body + fileType + ".png";
    }

    public ImageIcon GetIcon() {
        URL imageUrl = getClass().getResource(ImageKey());
        if (imageUrl == null) {
            System.out.println(ImageKey() + "not found");
        }
        return new ImageIcon(imageUrl);
    }

    public boolean isActiveProgram() {
        return fileType.equals(FileType.program)
                && languageName.equals(father.languageName) && !state.equals(FileState.Excluded);
    }

    //у хедера язык может быть неизвестен. потому что зависимости еще не искались.
    public boolean isActiveHeader() {
        return fileType.equals(FileType.header) && !state.equals(FileState.Excluded);
    }

    public boolean isActive() {
        return isActiveProgram() || isActiveHeader();
    }

    public File getDepFile() {
        return Paths.get(father.getOptionsDirectory().getAbsolutePath(), getLocalName() + dep).toFile();
    }

    public File getParserOutFile() {
        return Paths.get(father.getOptionsDirectory().getAbsolutePath(), getLocalName() + out).toFile();
    }

    public File getParserErrFile() {
        return Paths.get(father.getOptionsDirectory().getAbsolutePath(), getLocalName() + err).toFile();
    }

    public File getOptionsFile() {
        String path = file.getAbsolutePath().substring(father.Home.getAbsolutePath().length());
        return Paths.get(father.getOptionsDirectory().getAbsolutePath(), path + ".opt").toFile();
    }

    public void CreateParserOptions() throws IOException {
        String default_options = "";
        switch (languageName) {
            case fortran:
                default_options += " -spf -noProject -o " + Utils.DQuotes(getDepFile().getAbsolutePath());
                switch (style) {
                    case free:
                        default_options += " -f90";
                        break;
                    case fixed:
                        default_options += " -FI";
                        break;
                    case extended:
                        default_options += " -extend_source";
                        break;
                }
                break;
            case c:
                //TODO
                break;
        }
        Utils.WriteToFile(getOptionsFile(), (default_options + " " + options + "\n"));
    }

    public void ReadMessages(String text) throws Exception {
        String[] nw = text.split("\n");
        for (String S : nw) {
            Pair<Integer, String> p = decodeParserMessage(S, name);
            if (S.toLowerCase().startsWith("note")) {
                father.db.files.Data.get(p.getValue()).CreateAndAddNewMessage(2, S, p.getKey(), Message.parser_group);
            } else if (S.toLowerCase().startsWith("warning")) {
                father.db.files.Data.get(p.getValue()).CreateAndAddNewMessage(0, S, p.getKey(), Message.parser_group);
            } else if (S.toLowerCase().startsWith("error"))
                father.db.files.Data.get(p.getValue()).CreateAndAddNewMessage(1, S, p.getKey(), Message.parser_group);
        }
    }

    public void ReadParseMessages() throws Exception {
        if (getParserOutFile().exists())
            ReadMessages(Utils.ReadAllText(getParserOutFile()));
        if (getParserErrFile().exists())
            ReadMessages(Utils.ReadAllText(getParserErrFile()));
    }


    //важно. тут транзакция своя, оборачивать ее нельзя!
    public void CleanMessages() throws Exception {
        if (!state.equals(FileState.Excluded)) state = FileState.Undefined;
        father.db.BeginTransaction();
        father.db.Update(this);
        Vector<Message> to_delete = new Vector<>();
        for (Message message : father.db.notes.Data.values()) {
            if (message.file.equalsIgnoreCase(name))
                to_delete.add(message);
        }
        for (Message message : father.db.warnings.Data.values()) {
            if (message.file.equalsIgnoreCase(name))
                to_delete.add(message);
        }
        for (Message message : father.db.errors.Data.values()) {
            if (message.file.equalsIgnoreCase(name))
                to_delete.add(message);
        }
        for (Message message : to_delete)
            father.db.Delete(message);
        father.db.Commit();
    }

    public void CleanAll() throws Exception {
        lines_count = 0;
        isMain = 0;
        CleanMessages();
        LoopGraphTitle = no_data;
        LoopNests.clear();
        AllLoops.clear();
        CallGraphTitle = no_data;
        function_decls.clear();
        relativeHeaders.clear();
        ArrayGraphTitle = no_data;
        array_decls.clear();
        gcov_info.clear();
    }

    public void CreateAndAddNewMessage(int m_type, String m_value, int m_line, int m_group) throws Exception {
        switch (m_type) {
            case 0:
                MessageWarning warning = new MessageWarning(name, m_line, m_value, m_group);
                father.db.Insert(warning);
                father.checkMessageRecommendations(warning);
                if (state != FileState.HasErrors)
                    state = FileState.HasWarnings;
                break;
            case 1:
                MessageError error = new MessageError(name, m_line, m_value, m_group);
                father.db.Insert(error);
                father.checkMessageRecommendations(error);
                state = FileState.HasErrors;
                break;
            case 2:
                MessageNote note = new MessageNote(name, m_line, m_value, m_group);
                father.db.Insert(note);
                father.checkMessageRecommendations(note);
                if (state != FileState.HasWarnings && state != FileState.HasErrors)
                    state = FileState.HasNotes;
                break;
        }
    }

    public DefaultMutableTreeNode show_loop_graph_r(FileObjectWithMessages element) {
        DefaultMutableTreeNode res = new DefaultMutableTreeNode(element);
        if (element instanceof Loop) {
            for (FileObjectWithMessages child : ((Loop) element).children)
                res.add(show_loop_graph_r(child));
        }
        return res;
    }

    public DefaultMutableTreeNode getLoopsTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(LoopGraphTitle);
        for (Loop nest : LoopNests)
            root.add(show_loop_graph_r(nest));
        return root;
    }

    public DefaultMutableTreeNode getFunctionsTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(CallGraphTitle);
        for (FuncInfo fi : function_decls.values()) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(fi);
            for (FuncCall fc : fi.calls)
                node.add(new DefaultMutableTreeNode(fc));
            root.add(node);
        }
        return root;
    }

    public DefaultMutableTreeNode getArraysTree() {
        DefaultMutableTreeNode root = new DefaultMutableTreeNode(ArrayGraphTitle);
        for (ArrayDecl a : array_decls)
            root.add(new DefaultMutableTreeNode(a));
        return root;
    }

    public boolean UpdateType(FileType type_in) {
        if (!fileType.equals(type_in)) {
            fileType = type_in;
            try {
                father.db.Update(this, "fileType");
                return true;
            } catch (Exception e) {
                Global.Log.PrintException(e);
            }
        }
        return false;
    }

    public boolean UpdateLanguage(LanguageName lang_in) {
        if (!languageName.equals(lang_in)) {
            languageName = lang_in;
            try {
                father.db.Update(this, "languageName");
                return true;
            } catch (Exception e) {
                Global.Log.PrintException(e);
            }
        }
        return false;
    }

    public boolean UpdateStyle(LanguageStyle s_in) {
        if (!style.equals(s_in)) {
            style = s_in;
            try {
                father.db.Update(this, "style");
                return true;
            } catch (Exception e) {
                Global.Log.PrintException(e);
            }
        }
        return false;
    }

    public int getFirstBadLine() {
        for (MessageError error : father.db.errors.Data.values())
            if (error.file.equals(this.name)) return error.line;
        return -1;
    }

    public int FragmentLoopCount(int first, int second) {
        int res = 0;
        for (Loop l : AllLoops.values()) {
            if ((l.line >= first) && (l.line <= second))
                res++;
        }
        return res;
    }

    public int FragmentFunctionDeclsCount(int first, int second) {
        int res = 0;
        for (FuncInfo fi : function_decls.values()) {
            if ((fi.line >= first) && (fi.line <= second))
                res++;
        }
        return res;
    }

    public int FragmentFunctionCallsCount(int first, int second) {
        int res = 0;
        for (FuncInfo fi : function_decls.values())
            for (FuncCall fc : fi.calls) {
                if ((fc.line >= first) && (fc.line <= second))
                    res++;
            }
        return res;
    }

    public FuncCall find_current_func_call() {

        //-------------
        for (FuncInfo fi : function_decls.values()) {
            for (FuncCall fc : fi.calls) {
                if ((fc.line == form.getEditor().getCurrentLine()) &&
                        !Current.getSapfor().isIntrinsic(fc.funcName)
                ) {
                    return fc;
                }
            }
        }
        //-------------
        return null;
    }

    public FuncCall find_func_call(String funcName) {
        for (FuncInfo fi : function_decls.values()) {
            for (FuncCall fc : fi.calls) {
                if (fc.funcName.equalsIgnoreCase(funcName) &&
                        (fc.line == form.getEditor().getCurrentLine() &&
                                !Current.getSapfor().isIntrinsic(fc.funcName)
                        )) {
                    return fc;
                }
            }
        }
        return null;
    }


    public Loop find_current_loop() {
        int line = form.getEditor().getCurrentLine();
        return AllLoops.getOrDefault(line, null);
    }

    public void Exclude() throws Exception {
        state = FileState.Excluded;
        father.db.Update(this, "state");
    }

    public void Include() throws Exception {
        state = FileState.Undefined;
        father.db.Update(this, "state");
    }

    public String getUnixName() {
        return Utils.toU(name);
    }

    public String getQObjectName() {
        return Utils.DQuotes(getUnixName() + ".o");
    }

    public String getQSourceName() {
        return Utils.DQuotes(getUnixName());
    }

    @Override
    public String toString() {
        return name;
    }

    public String getProjectNameWithoutExtension() {
        String extension = Utils.getExtension(file);
        return name.substring(0, name.length() - (extension.length() + 1));
    }

    public void importSettings(DBProjectFile parent) {
        fileType = parent.fileType;
        switch (parent.state) {
            case Undefined:
            case Excluded:
                state = parent.state;
                break;
            default:
                state = FileState.Undefined;
                break;
        }
        languageName = parent.languageName;
        style = parent.style;
    }

    public void importSourceCodeSettings(DBProjectFile parent, boolean sapforStyle) {
        switch (parent.state) {
            case Undefined:
            case Excluded:
                state = parent.state;
                break;
            default:
                state = FileState.Undefined;
                break;
        }
        languageName = parent.languageName;
        style = (sapforStyle && Global.db.settings.get(SettingName.FREE_FORM).toBoolean()) ? LanguageStyle.free : LanguageStyle.fixed;
    }
}