package ProjectData.Files;
public enum FileType {
    none,
    program,
    header,
    forbidden,
    data;
    public String getDescription() {
        switch (this) {
            case program:
                return "Программа";
            case header:
                return "Заголовочный";
            case none:
                return "нет";
            case forbidden:
                return "не поддерживается";
            case data:
                return "Данные";
        }
        return "";
    }
}
