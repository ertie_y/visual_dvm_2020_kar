package ProjectData;
import Common.Database.SQLITE.SQLiteDatabase;
import ProjectData.DBArray.ArraysDBTable;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import ProjectData.Files.FilesDBTable;
import ProjectData.Messages.Errors.ErrorsDBTable;
import ProjectData.Messages.Notes.NotesDBTable;
import ProjectData.Messages.Warnings.WarningsDBTable;
import ProjectData.PredictorStatistic.PredictorStatisticsDBTable;
import ProjectData.Project.ProjectInfoDBTable;
import ProjectData.Project.db_project_info;
import ProjectData.RSetting.RecommendedSettingsDBTable;
import ProjectData.RTransformation.RecommendedTransformationsDBTable;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.util.Vector;
public class ProjectDatabase extends SQLiteDatabase {
    public db_project_info owner;
    public ProjectInfoDBTable projectInfo;
    public FilesDBTable files;
    public ArraysDBTable savedArrays; //мб в дальнейшем как то объединить эти объекты с сапфоровскими? хз.
    public PredictorStatisticsDBTable predictorStatistics;
    public RecommendedTransformationsDBTable recommendedTransformations;
    public RecommendedSettingsDBTable recommendedSettings;
    public NotesDBTable notes;
    public WarningsDBTable warnings;
    public ErrorsDBTable errors;
    public ProjectDatabase(db_project_info owner_in) {
        super(db_project_info.get_db_file((owner_in).Home));
        owner = owner_in;
    }
    //----------------------------------------------------
    @Override
    protected void initAllTables() throws Exception {
        addTable(projectInfo = new ProjectInfoDBTable());
        addTable(files = new FilesDBTable());
        addTable(savedArrays = new ArraysDBTable());
        addTable(predictorStatistics = new PredictorStatisticsDBTable());
        addTable(recommendedTransformations = new RecommendedTransformationsDBTable());
        addTable(recommendedSettings = new RecommendedSettingsDBTable());
        addTable(notes = new NotesDBTable());
        addTable(warnings = new WarningsDBTable());
        addTable(errors = new ErrorsDBTable());
    }
    //Делать это только после пострения дерева версий. чтобы файлы версий не учитывались!!
    public DefaultMutableTreeNode get_files_r(File file) throws Exception {
        DefaultMutableTreeNode res = null;
        if (owner.isProjectDirectory(file)) {
            res = new DefaultMutableTreeNode(file);
            File[] files_ = file.listFiles();
            if (files_!=null) {
                for (File f : files_) {
                    DefaultMutableTreeNode node = get_files_r(f);
                    //Null может быть если подпапки оказались от версий
                    if (node != null)
                        res.add(node);
                }
            }
        } else if (file.isFile()) {
            DBProjectFile pf = null;
            String name = owner.getInnerName(file);
            if (files.Data.containsKey(name)) {
                pf = files.Data.get(name);
                pf.Init(file, owner);
            } else {
                pf = new DBProjectFile(file, owner);
                if (pf.fileType != FileType.forbidden)
                    Insert(pf);
                else return null;
            }
            res = pf.node = new DefaultMutableTreeNode(pf); //узел файла тут же надо запомнить.
        }
        return res;
    }
    @Override
    public void Init() throws Exception {
        owner.DeleteCrushedVersionIfNeed();
        owner.filesTreeRoot = get_files_r(owner.Home);
        Vector<DBProjectFile> inexisting_files = new Vector<>();
        for (DBProjectFile f : files.Data.values())
            if (f.father == null) inexisting_files.add(f);
        for (DBProjectFile f : inexisting_files)
            Delete(f);
    }
    //особый проход. нужен при первичной загрузке проекта.
    public db_project_info LoadOnlyProjectInfo() throws Exception {
        LoadAll(projectInfo);
        return projectInfo.getFirstRecord();
    }

}
