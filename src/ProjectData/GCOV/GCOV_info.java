package ProjectData.GCOV;

import javafx.util.Pair;

import java.util.LinkedHashMap;

public class GCOV_info {

    public long max;
    public LinkedHashMap<Integer, Pair<Long, Integer>> line_info = new LinkedHashMap<>();

    //номер строки / число ее выполнений / масштаб
    public void clear()
    {
        max = 0;
        line_info.clear();
    }

    public void add_line(int num, long count)
    {
        line_info.put(num, new Pair<>(count, 0));
        if (max < count) max = count;
    }

    //пределим минимум и максимум. оба должны быть >0. + определим интервал
    public void setup()
    {
        //определям процент по отношению к максимуму
        for (int i: line_info.keySet()){
            Pair<Long, Integer> p = line_info.get(i);
            int proportion = (max>0)? ((int)(((double)p.getKey() / max) * 100)):0;
            if (proportion<1) proportion=1;
            line_info.replace(i, new Pair<>(p.getKey(), proportion));
        }
    }
}

