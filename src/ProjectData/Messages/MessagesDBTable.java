package ProjectData.Messages;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableRenderers;
import Common.UI.Themes.VisualiserFonts;

import java.util.Comparator;
public class MessagesDBTable<M extends Message> extends iDBTable<M> {
    public MessagesDBTable(Class<M> d_in) {
        super(d_in);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                Current.getFile().form.getEditor().gotoLine(getCurrent().line);
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(3).setMinWidth(700);
                columns.get(3).setRenderer(TableRenderers.RendererWrapText);
            }
            @Override
            public void MouseAction2() throws Exception {
                ShowCurrentObject();
            }
        };
    }
    @Override
    public Object getFieldAt(M object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.group_s;
            case 2:
                return object.line;
            case 3:
                return object.value;
            default:
                return null;
        }
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"группа", "строка", "текст"};
    }
    @Override
    public Comparator<M> getComparator() {
        return Comparator.comparingInt(o -> o.line);
    }
}
