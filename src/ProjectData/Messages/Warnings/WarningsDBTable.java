package ProjectData.Messages.Warnings;
import Common.Current;
import Common.UI.UI;
import ProjectData.Messages.MessagesDBTable;
public class WarningsDBTable extends MessagesDBTable<MessageWarning> {
    public WarningsDBTable() {
        super(MessageWarning.class);
      //  setUIContent(UI.getMainWindow().warningsPanel);
    }
    @Override
    public String getDataDescription() {
        return "предупреждение";
    }
    @Override
    public Current CurrentName() {
        return Current.Warnings;
    }
}
