package ProjectData.Messages.Warnings;
import ProjectData.Messages.Message;
public class MessageWarning extends Message {
    public MessageWarning(String file_in, int line_in, String value_in, int group_in) throws Exception {
        super(file_in, line_in, value_in, group_in);
    }
    public MessageWarning() {
    }
}
