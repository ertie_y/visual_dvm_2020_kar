package ProjectData.Messages.Errors;
import ProjectData.Messages.Message;
public class MessageError extends Message {
    public MessageError(String file_in, int line_in, String value_in, int group_in) throws Exception {
        super(file_in, line_in, value_in, group_in);
    }
    public MessageError() {
    }
}
