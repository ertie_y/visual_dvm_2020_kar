package ProjectData.Messages.Errors;
import Common.Current;
import Common.UI.UI;
import ProjectData.Messages.MessagesDBTable;
public class ErrorsDBTable extends MessagesDBTable<MessageError> {
    public ErrorsDBTable() {
        super(MessageError.class);
     //   setUIContent(UI.getMainWindow().errorsPanel);
    }
    @Override
    public String getDataDescription() {
        return "сообщение об ошибке";
    }
    @Override
    public Current CurrentName() {
        return Current.Errors;
    }
}
