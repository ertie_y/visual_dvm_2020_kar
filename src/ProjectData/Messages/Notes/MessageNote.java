package ProjectData.Messages.Notes;
import ProjectData.Messages.Message;
public class MessageNote extends Message {
    public MessageNote(String file_in, int line_in, String value_in, int group_in) throws Exception {
        super(file_in, line_in, value_in, group_in);
    }
    public MessageNote() {
    }
}
