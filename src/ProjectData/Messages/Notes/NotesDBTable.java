package ProjectData.Messages.Notes;
import Common.Current;
import Common.UI.UI;
import ProjectData.Messages.MessagesDBTable;
public class NotesDBTable extends MessagesDBTable<MessageNote> {
    public NotesDBTable() {
        super(MessageNote.class);
       // setUIContent(UI.getMainWindow().notesPanel);
    }
    @Override
    public String getDataDescription() {
        return "примечание";
    }
    @Override
    public Current CurrentName() {
        return Current.Notes;
    }
}
