package Visual_DVM_2021.UI.Main;

import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import Repository.BugReport.BugReport;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.FilterWindow;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BugReportsFilterForm implements FilterWindow {
    public JPanel content;
    private JTextField BugReportsKeyFilter;
    private JTextField BugReportsSenderNameFilter;
    private JLabel matchesLabel;
    private JTextField BugReportsDescriptionFilter;
    private JTextField BugReportsCommentFilter;
    private JTextField BugReportsExecutorFilter;
    private JTextField BugReportsVersionFilter;
    private JToolBar bugsTools;
    private JCheckBox BugReportsMyOnlyFilter;
    private JCheckBox BugReportsOpenedOnly;

    public BugReportsFilterForm() {
        BugReportsKeyFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterKey = BugReportsKeyFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterKey = BugReportsKeyFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        BugReportsSenderNameFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterSenderName = BugReportsSenderNameFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterSenderName = BugReportsSenderNameFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });

        BugReportsDescriptionFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterDescription = BugReportsDescriptionFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterDescription = BugReportsDescriptionFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //------->>>
        BugReportsCommentFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterComment = BugReportsCommentFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterComment = BugReportsCommentFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        BugReportsExecutorFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterExecutor = BugReportsExecutorFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterExecutor = BugReportsExecutorFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        BugReportsVersionFilter.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                BugReport.filterVersion = BugReportsVersionFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                BugReport.filterVersion = BugReportsVersionFilter.getText();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //--------
        BugReportsMyOnlyFilter.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BugReport.filterMyOnly = BugReportsMyOnlyFilter.isSelected();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }
        });
        BugReportsOpenedOnly.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BugReport.filterOpenedOnly = BugReportsOpenedOnly.isSelected();
                UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
            }
        });
        //-
        UI.add(bugsTools,
                PassCode_2021.SynchronizeBugReports,
                PassCode_2021.DownloadAllBugReportsArchives,
                PassCode_2021.AddBugReport,
                PassCode_2021.PublishBugReport,
                PassCode_2021.OpenBugReportTestProject,
                PassCode_2021.OpenBugReport,
                PassCode_2021.UpdateBugReportProgress,
                PassCode_2021.CloseBugReport,
                PassCode_2021.DeleteBugReport
        );
    }

    @Override
    public void ShowMatchesCount(int count) {
        matchesLabel.setText(String.valueOf(count));
    }

    @Override
    public JPanel getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        BugReportsKeyFilter = new StyledTextField();
        BugReportsSenderNameFilter = new StyledTextField();
        BugReportsDescriptionFilter = new StyledTextField();
        BugReportsCommentFilter = new StyledTextField();
        BugReportsExecutorFilter = new StyledTextField();
        BugReportsVersionFilter = new StyledTextField();
    }
}
