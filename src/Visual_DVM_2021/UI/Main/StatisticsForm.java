package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.Global;
import Common.UI.TextArea.StyledTextArea;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import GlobalData.RunStsRecord.RunStsRecord;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.StatisticsWindow;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;

public class StatisticsForm implements StatisticsWindow, FormWithSplitters {
    public JPanel content;
    //-
    public JSplitPane SC23;
    //-
    private JToolBar runStsTools;
    private JPanel runStsPanel;
    private JPanel filtersPanel;
    private JPanel flags_filters_panel;
    private JTextArea filter_link_flags;
    private JTextArea filter_fortran_flags;
    private JTextArea filter_c_flags;
    private JTextArea filter_cpp_flags;
    private JTextArea filter_environments;
    private JSpinner filter_proc;
    private JTextField filter_project_path;
    private JTextField filter_machine;
    private JTextField filter_matrix;
    private JCheckBox filter_current_project_only;
    private JCheckBox filter_dvm_sts_only;

    public StatisticsForm() {
        LoadSplitters();
        Global.db.runStsRecords.mountUI(runStsPanel);
        filter_proc.setModel(new SpinnerNumberModel(0, 0, 512, 1));
        init_filter_field(filter_project_path);
        init_filter_field(filter_machine);
        init_filter_field(filter_link_flags);
        init_filter_field(filter_fortran_flags);
        init_filter_field(filter_c_flags);
        init_filter_field(filter_cpp_flags);
        init_filter_field(filter_environments);
        init_filter_field(filter_matrix);
        filter_current_project_only.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ShowAll();
            }
        });
        filter_proc.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                ShowAll();
            }
        });
        filter_dvm_sts_only.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ShowAll();
            }
        });
        //-
        UI.add(runStsTools,
                PassCode_2021.DeleteRunStsRecord,
                PassCode_2021.ClearRunSts);
    }

    private void parse_filter_list(JTextComponent text, Collection<String> filter) {
        filter.clear();
        String[] data = text.getText().split("\n");
        for (String f : data)
            if (!f.isEmpty())
                filter.add(f);
    }

    public void init_filter_field(JTextComponent field) {
        field.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ShowAll();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ShowAll();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
    }

    private void RefreshFilters() {
        RunStsRecord.filter_path = filter_project_path.getText();
        RunStsRecord.filter_machine = filter_machine.getText();
        RunStsRecord.only_current_project = filter_current_project_only.isSelected() && Current.HasProject();
        RunStsRecord.filter_matrix = filter_matrix.getText();
        RunStsRecord.filter_proc = (int) filter_proc.getValue();
        RunStsRecord.only_dvm_sts = filter_dvm_sts_only.isSelected();
        parse_filter_list(filter_link_flags, RunStsRecord.filter_link_flags);
        parse_filter_list(filter_fortran_flags, RunStsRecord.filter_fortran_flags);
        parse_filter_list(filter_c_flags, RunStsRecord.filter_c_flags);
        parse_filter_list(filter_cpp_flags, RunStsRecord.filter_cpp_flags);
        parse_filter_list(filter_environments, RunStsRecord.filter_environments);
    }

    @Override
    public void ShowAll() {
        RefreshFilters();
        Global.db.runStsRecords.ShowUI();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        filter_project_path = new StyledTextField();
        filter_machine = new StyledTextField();
        filter_matrix = new StyledTextField();
        filter_link_flags = new StyledTextArea();
        filter_fortran_flags = new StyledTextArea();
        filter_c_flags = new StyledTextArea();
        filter_cpp_flags=new StyledTextArea();
        filter_environments=new StyledTextArea();
    }
}
