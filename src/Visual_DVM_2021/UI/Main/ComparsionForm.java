package Visual_DVM_2021.UI.Main;
import Common.Current;
import Common.Global;
import Common.UI.Editor.BaseEditor;
import Common.UI.Label.ShortLabel;
import Common.UI.UI;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import GlobalData.Settings.UI.GlobalMenu;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.All.UpdateSetting;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.UI.Interface.ComparsionWindow;
import javafx.util.Pair;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextAreaHighlighter;
import org.fife.ui.rtextarea.RTextScrollPane;
import org.omg.PortableInterceptor.SYSTEM_EXCEPTION;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedHashMap;
import java.util.Vector;
public class ComparsionForm implements ComparsionWindow {
    //---------------------
    static boolean registerOn;
    static boolean spacesOn;
    static boolean emptyLinesOn;
    static boolean fortranWrapsOn;
    public db_project_info version = null;
    public JPanel content;
    public RSyntaxTextAreaHighlighter slave_highlighter = null; //погонщик рабов
    //---------------------
    public Vector<String> lines = new Vector<>(); //строки с учетом/неучетом пробелов. для сравнения
    public Vector<String> visible_lines = new Vector<>(); //строки с нетронутыми пробелами. для отображения
    public LinkedHashMap<Integer, Pair<Integer, Boolean>> colors = new LinkedHashMap<>();
    ComparsionForm slave = null;
    ComparsionForm master = null;
    DBProjectFile File = null;
    boolean events_on = false;//относится только к мастеру.
    int current_diff_line = -1;
    private BaseEditor Body;
    private RTextScrollPane Scroll;
    private JLabel lVersion;
    private JButton bApplyVersion;
    private JComboBox cbFile;
    private JButton bMerge;
    private JButton mSettings;
    private JToolBar tools;
    private JToolBar version_bar;
    private JPanel editor_panel;
    private JButton bPrevious;
    private JButton bNext;
    private JButton button1;

    //----------------------
    public ComparsionForm(ComparsionForm slave_in) {
        slave = slave_in;
        mSettings.setVisible(isMaster());
        bPrevious.setVisible(isMaster());
        bNext.setVisible(isMaster());
        Scroll.setLineNumbersEnabled(true);
        bApplyVersion.addActionListener(e -> {
            ClearVersion();
            TextLog log = new TextLog();
            if (Current.Check(log, Current.Version)) {
                version = Current.getVersion();
                lVersion.setText(version.getTitle());
                cbFile.removeAllItems();
                Vector<DBProjectFile> files = version.getFilesForComparsion();
                for (DBProjectFile file : files)
                    cbFile.addItem(file);
            } else
                UI.Info(log.toString());
        });
        cbFile.addActionListener(e -> {
            DBProjectFile File1 = null;
            DBProjectFile File2 = null;
            ClearFileText();
            File = (cbFile.getSelectedItem() instanceof DBProjectFile) ?
                    ((DBProjectFile) cbFile.getSelectedItem()) : null;
            if (isMaster()) {
                System.out.print("%master file = ");
                System.out.print(File);
                System.out.print(";slave file = ");
                System.out.println(slave.File);
                File1 = File;
                File2 = slave.File;
            } else {
                System.out.print("$master file = ");
                System.out.print(master.File);
                System.out.print(";slave file = ");
                System.out.println(File);
                File1 = master.File;
                File2 = File;
            }
            if ((File1 != null) && (File2 != null)) {
                boolean ExtensionsOn = Global.db.settings.get(SettingName.ExtensionsOn).toBoolean();
                String name1 = ExtensionsOn ? File1.file.getName() : Utils.getNameWithoutExtension(File1.file);
                String name2 = ExtensionsOn ? File2.file.getName() : Utils.getNameWithoutExtension(File2.file);
                System.out.println(name1 + "/" + name2);
                if (name1.equalsIgnoreCase(name2)) {
                    Pass_2021.passes.get(PassCode_2021.CompareFiles).Do();
                }
            }
        });
        Body.setWhitespaceVisible(true);
        Body.setEditable(false);
        if (isMaster()) {
            //синхронизация скроллов
            slave.Scroll.getVerticalScrollBar().setModel(Scroll.getVerticalScrollBar().getModel());
            slave_highlighter = (RSyntaxTextAreaHighlighter) slave.Body.getHighlighter();
            //бяк быть не должно при условии что строк одинаковое количество. а это должно выполяться.
            Body.addCaretListener(e -> {
                if (events_on) {
                    try {
                        int master_lineNum = Body.getCaretLineNumber();
                        slave.Body.setCaretPosition(slave.Body.getLineStartOffset(master_lineNum));
                    } catch (Exception ex) {
                        Global.Log.PrintException(ex);
                    }
                }
            });
            slave.master = this;
            tools.add(Pass_2021.passes.get(PassCode_2021.CompareFiles).getButton());
            mSettings.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    ShowSettings();
                }
            });
            bPrevious.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (current_diff_line != Utils.Nan) {
                        if (current_diff_line > 0)
                            current_diff_line--;
                        else
                            current_diff_line = colors.size() - 1;
                        showCurrentDiff();
                    }
                }
            });
            bNext.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if (current_diff_line != Utils.Nan) {
                        if (current_diff_line < colors.size() - 1)
                            current_diff_line++;
                        else
                            current_diff_line = 0;
                        showCurrentDiff();
                    }
                }
            });
        }
    }
    //https://pandia.ru/text/80/144/22177-2.php
    public static boolean CompareLines(String line1_raw, String line2_raw) {
        String line1 = line1_raw;
        String line2 = line2_raw;
        if (!registerOn) {
            line1 = line1.toUpperCase();
            line2 = line2.toUpperCase();
        }
        if (!spacesOn) {
            line1 = Utils.remove(line1, " ", "\t");
            line2 = Utils.remove(line2, " ", "\t");
        }
        return line1.equals(line2);
    }
    public static boolean Contains(Vector<String> list, String line, int max_index) {
        int last_index = -1;
        for (int i = 0; i < list.size(); ++i)
            if (CompareLines(list.get(i), line)) last_index = i;
        return (last_index >= max_index);
    }
    public boolean isMaster() {
        return slave != null;
    }
    public boolean isSlave() {
        return master != null;
    }
    @Override
    public void ShowSettings() {
        UpdateSetting.current_menu = GlobalMenu.ComparsionSettings;
        UI.ShowDropDownUnder(mSettings, UI.menus.get(GlobalMenu.ComparsionSettings));
    }
    public void showCurrentDiff() {
        Body.gotoLine_(colors.get(current_diff_line).getKey());
    }
    public boolean HasFile() {
        return File != null;
    }
    public void ClearVersion() {
        version = null;
        lVersion.setText("?");
        cbFile.removeAllItems();
        File = null;
        ClearFileText();
        if (isMaster())
            slave.ClearFileText();
        else if (isSlave())
            master.ClearFileText();
    }
    public void ClearFileText() {
        events_on = false;
        Body.setText("");
    }
    public void getLines() {
        lines.clear();
        visible_lines.clear();
        //1.прочитать весь текст.
        char[] chars = Utils.ReadAllText(File.file).toCharArray();
        //по символам получить строки.
        char c = 0; //текущий символ
        int i = 0; //индекс текущего символа.
        StringBuilder line = new StringBuilder(); //текущая строка
        StringBuilder v_line = new StringBuilder(); //текущая строка
        while (i < chars.length) {
            c = chars[i];
          //  System.out.print("`"+c+"`");
            ++i;
            switch (c) {
                case '\r': //возврат каретки, игнор
                    break;
                case ' ':
                case '\t':
                    if (spacesOn) line.append(c);
                    v_line.append(c);
                    break;
                case '\n': //конец строки
                    if (!fortranWrapsOn) {
                        //оракул. лезем в начало следующей строки
                        //и анализируем первые 5 символов
                        boolean hasWrap =false;
                        int wi;
                        //------
                       // System.out.println("checking wrap...");

                        //с нуля потому что и уже увеличено.
                        for (int j=0; (j<6)&&((wi=i+j)<chars.length); ++j){
                            char s = chars[wi];
                           // System.out.print(s);
                            if ((j==0)&&((s=='c')||(s=='C')||(s=='!'))) {
                               // System.out.println("next line is FL comment");
                                break;
                            }
                            if ((j>0)&&(j<5)&&(s=='!')) {
                               // System.out.println("next line is comment");
                                break;
                            }
                            if ((j==5)&&(s!=' ')){
                                hasWrap=true;
                                i=wi+1;
                               // System.out.println("next line is WRAP");
                                break;
                            }
                        }
                       // System.out.println();
                        //-----
                        if (hasWrap)
                            break;
                    }
                   // System.out.println();
                    //добавление строки в результат.
                    if ((line.length() > 0) || emptyLinesOn && spacesOn) {
                        lines.add(line.toString());
                        visible_lines.add(v_line.toString());
                    }
                    //сброс
                    line = new StringBuilder();
                    v_line = new StringBuilder();
                    break;
                default:
                    line.append(c);
                    v_line.append(c);
                    break;
            }
        }
        if ((i > 0) && (c != '\n')) {
            //строка оборвалась на EOF
            //добавление строки в результат.
            if ((line.length() > 0) || emptyLinesOn && spacesOn) {
                lines.add(line.toString());
                visible_lines.add(v_line.toString());
            }
        }
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        lVersion = new ShortLabel(40);
        editor_panel = new JPanel(new BorderLayout());
        Body = new BaseEditor();
        Scroll = new RTextScrollPane(Body);
        editor_panel.add(Scroll);
    }
    //-
    @Override
    public void Compare() throws Exception {
        events_on = false;
        current_diff_line = Utils.Nan;
        colors.clear();
        //----------------------------------------------------------------------------------------------
        registerOn = Global.db.settings.get(SettingName.RegisterOn).toBoolean();
        spacesOn = Global.db.settings.get(SettingName.SpacesOn).toBoolean();
        emptyLinesOn = Global.db.settings.get(SettingName.EmptyLinesOn).toBoolean();
        fortranWrapsOn = Global.db.settings.get(SettingName.FortranWrapsOn).toBoolean();
        //-----------------------------------------------------------------------------------------------
        Body.setText("");
        slave.Body.setText("");
        int d = 0;
        if (HasFile() && slave.HasFile()) {
            getLines();
            slave.getLines();
            //--------------------------------------------------------------------
            Vector<String> t1 = new Vector<>();
            Vector<String> t2 = new Vector<>();
            int old_j = 0;
            int j = 0;
            for (int i = 0; i < lines.size(); ++i) {
                if (Contains(slave.lines, lines.get(i), old_j)) {
                    for (int k = old_j; k < slave.lines.size(); ++k) {
                        j = k;
                        if (CompareLines(lines.get(i), slave.lines.get(k))) {
                            j++;
                            t1.add(visible_lines.get(i));
                            t2.add(slave.visible_lines.get(k));
                            break;
                        } else {
                            t1.add("+");
                            t2.add("+ " + slave.visible_lines.get(k));
                            colors.put(d, new Pair(t2.size() - 1, true));
                            ++d;
                        }
                    }
                    old_j = j;
                } else {
                    //строки гарантированно нет.
                    t1.add("- " + visible_lines.get(i));
                    t2.add("- " + visible_lines.get(i));
                    colors.put(d, new Pair(t2.size() - 1, false));
                    ++d;
                }
            }
            //теперь граничное условие. если первый файл кончился а второй нет, его остаток это добавление.
            for (int i = j; i < slave.lines.size(); ++i) {
                t1.add("+");
                t2.add("+ " + slave.visible_lines.get(i));
                colors.put(d, new Pair(t2.size() - 1, true));
                ++d;
            }
            Body.setText(String.join("\n", t1));
            slave.Body.setText(String.join("\n", t2));
            Body.setCaretPosition(0);
            slave.Body.setCaretPosition(0);
            //теперь покрас.
            for (Integer diff_num : colors.keySet()) {
                slave_highlighter.addHighlight(
                        slave.Body.getLineStartOffset(colors.get(diff_num).getKey()),
                        slave.Body.getLineEndOffset(colors.get(diff_num).getKey()),
                        colors.get(diff_num).getValue() ? UI.GoodLoopPainter : UI.BadLoopPainter
                );
            }
            if (colors.size() > 0) current_diff_line = 0;
        }
        events_on = true;
    }
    @Override
    public void CheckVersion(db_project_info version_in) {
        if ((version != null) && version_in.Home.equals(version.Home)) {
            ClearVersion();
        }
    }
    @Override
    public void MasterCheckVersion(db_project_info version_in) {
        CheckVersion(version_in);
        slave.CheckVersion(version_in);
    }
}
