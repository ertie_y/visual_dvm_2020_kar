package Visual_DVM_2021.UI.Main;

import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import ProjectData.LanguageName;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.UI.Interface.FilterWindow;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TestsFilterForm implements FilterWindow {
    public JPanel Content;
    private JTextField filterName;
    private JLabel matchesLabel;
    private JCheckBox filterMyOnly;
    private JTextField filterSenderName;

    public TestsFilterForm() {
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                Test.filterName = filterName.getText();
                TestingSystem.db.tests.ShowUI();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                Test.filterName = filterName.getText();
                TestingSystem.db.tests.ShowUI();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterSenderName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                Test.filterSenderName = filterSenderName.getText();
                TestingSystem.db.tests.ShowUI();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                Test.filterSenderName = filterSenderName.getText();
                TestingSystem.db.tests.ShowUI();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterMyOnly.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Test.filterMyOnly = filterMyOnly.isSelected();
                TestingSystem.db.tests.ShowUI();
            }
        });
    }

    @Override
    public void ShowMatchesCount(int count) {
        matchesLabel.setText(String.valueOf(count));
    }

    @Override
    public JPanel getContent() {
        return Content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        filterName = new StyledTextField();
        filterSenderName = new StyledTextField();
    }
}
