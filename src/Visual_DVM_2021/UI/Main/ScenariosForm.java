package Visual_DVM_2021.UI.Main;
import Common.Global;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.ScenariosWindow;

import javax.swing.*;
public class ScenariosForm  implements FormWithSplitters, ScenariosWindow {
    public JPanel content;
    //-
    public JSplitPane SC24;
    //-
    private JToolBar scenariosTools;
    private JPanel scenariosPanel;
    private JPanel scenarioCommandsPanel;
    private JToolBar scenarioCommandsTools;
    //-
    public ScenariosForm(){
        LoadSplitters();
        Global.db.scenarios.mountUI(scenariosPanel);
        Global.db.scenarioCommands.mountUI(scenarioCommandsPanel);
        //-
        UI.add(scenariosTools,
                PassCode_2021.AddScenario,
                PassCode_2021.EditScenario,
                PassCode_2021.DeleteScenario,
                PassCode_2021.RunScenario);
        UI.add(scenarioCommandsTools,
                PassCode_2021.AddScenarioCommand,
                PassCode_2021.EditScenarioCommand,
                PassCode_2021.DeleteScenarioCommand);
    }
    @Override
    public void ShowAll() {
        Global.db.scenarios.ShowUI();
    }
}
