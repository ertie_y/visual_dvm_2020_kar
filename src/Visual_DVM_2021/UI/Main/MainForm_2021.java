package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Form;
import Common.UI.Windows.FormType;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import GlobalData.Settings.UI.GlobalMenu;
import Repository.Component.ComponentType;
import Repository.Component.PerformanceAnalyzer.PerformanceAnalyzer;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.All.UpdateSetting;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.*;

import javax.swing.*;
import java.awt.event.*;

public class MainForm_2021 extends Form implements MainWindow {
    //<editor-fold desc="поля">
    private final WelcomeForm welcomeForm = new WelcomeForm(); //заглушка когда проекта нет.
    private final VersionsWelcomeForm versionsWelcomeForm = new VersionsWelcomeForm(); //заглушка когда версий нет.
    private ProjectForm projectForm;
    private VersionsForm versionsForm;
    private CallbackForm callbackForm;
    private StatisticsForm statisticsForm;
    private TestingForm testingForm;
    private JPanel Content;
    private JToolBar globalTools;
    private JTabbedPane globalTabs;
    private JButton bOpenProject;
    private JButton bCloseProject;
    private JButton bCreateProject;
    private JButton mVisualiser;
    private JButton mLastProjects;
    private JToolBar projectTools;
    private JButton mAnalyses;
    private JButton mTransformations;
    private JButton mSapfor;
    private JButton mCleaning;
    private JScrollPane FastAccessScroll;
    private JToolBar fastAccessBar;
    private JButton bUpdates;
    private JButton mDebug;
    private JButton mVariants;
    private JButton bHelp;
    private JButton mAttachements;
    private JPanel mainPanel;
    private JButton bStartPerformanceAnalyzer;

    //</editor-fold>
    public MainForm_2021() {
        InstallWelcomePanel();
        InstallEmptyVersionsPanel(); //сразу отображаем пустой корень.
        InstallCallbackPanel();
        InstallStatisticsPanel();
        InstallTestingPanel();
        RefreshFastAccess();
        ShowUpdatesIcon();
        mLastProjects.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseEntered(MouseEvent e) {
                UI.ShowDropDownLeft(mLastProjects, UI.menus.get(GlobalMenu.LastProjects));
            }
        });
        bOpenProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do();
            }
        });
        bCloseProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.CloseCurrentProject).Do();
            }
        });
        bCreateProject.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.CreateEmptyProject).Do();
            }
        });
        mVisualiser.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ShowGlobalSettings();
            }
        });
        //----------------------
        mAnalyses.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mAnalyses, UI.menus.get(GlobalMenu.Analyses));
            }
        });
        mTransformations.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mTransformations, UI.menus.get(GlobalMenu.Transformations));
            }
        });
        mDebug.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mDebug, UI.menus.get(GlobalMenu.Debug));
            }
        });
        mSapfor.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ShowSapforSettings();
            }
        });
        //-
        mCleaning.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mCleaning, UI.menus.get(GlobalMenu.Cleaning));
            }
        });
        bUpdates.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (PerformanceAnalyzer.isActive) {
                    UI.Info("Перед работой с компонентами закройте анализатор производительности!");
                } else {
                    Pass_2021.passes.get(PassCode_2021.GetComponentsActualVersions).Do();
                    Global.RefreshUpdatesStatus();
                    UI.ShowComponentsWindow();
                }
            }
        });
        mVariants.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.GenerateParallelVariants).Do();
            }
        });
        //-
        projectTools.setVisible(false);
        FastAccessScroll.setVisible(false);
        bHelp.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.ShowInstruction).Do();
            }
        });
        mAttachements.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Current.HasProject())
                    UI.ShowDropDownUnder(mAttachements, UI.menus.get(GlobalMenu.Attachments));
            }
        });

        bStartPerformanceAnalyzer.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Global.performanceAnalyzer.Start();
            }
        });
    }

    //<editor-fold desc="функционал окна">
    @Override
    protected JPanel getMainPanel() {
        return Content;
    }

    @Override
    protected FormType getFormType() {
        return FormType.Main;
    }

    @Override
    public String getIconName() {
        return "Sapfor.png";
    }

    @Override
    public String getWTitleText() {
        return "Visual Sapfor 3.0";
    }

    @Override
    public String getUTitleText() {
        return "Visual Sapfor 3.0";
    }

    @Override
    public void AfterClose() {
        Pass_2021.passes.get(PassCode_2021.CloseCurrentProject).Do();
        RemoveVersionsPanel();
        SaveCallbackPanel();
        SaveStatisticsPanel();
        SaveTestingPanel();
        Global.FinishApplication();
    }

    //</editor-fold>
    //<editor-fold desc="служебные методы">
    private void InstallWelcomePanel() {
        globalTabs.insertTab("Начало работы",
                null,
                welcomeForm.content,
                "Для начала работы откройте проект с помощью правой панели инструментов или перетащите его сюда", 0
        );
    }

    private void InstallProjectPanel() {
        globalTabs.insertTab("Проект: " + Current.getProject().name,
                Utils.getIcon("/icons/Common.png"),
                (projectForm = new ProjectForm(Current.getProject())).content, Current.getProject().description, 0);
    }

    private void RemoveProjectPanel() {
        globalTabs.removeTabAt(0);
        if (projectForm != null) {
            projectForm.SaveSplitters();
            projectForm = null;
        }
    }

    private void InstallVersionsPanel() {
        globalTabs.insertTab("Версии " + Current.getRoot().name,
                Utils.getIcon("/icons/VersionsTree.png"),
                (versionsForm = new VersionsForm(Current.getRoot())).content,
                "Текущее дерево версий", 1);
    }

    private void RemoveVersionsPanel() {
        globalTabs.removeTabAt(1);
        if (versionsForm != null) {
            versionsForm.SaveSplitters();
            versionsForm = null;
        }
    }

    private void InstallEmptyVersionsPanel() {
        globalTabs.insertTab("Версии",
                Utils.getIcon("/icons/VersionsTree.png"),
                versionsWelcomeForm.content,
                "Текущее дерево версий", 1);
    }

    private void InstallCallbackPanel() {
        globalTabs.insertTab("Обратная связь",
                Utils.getIcon("/icons/Bug.png"),
                (callbackForm = new CallbackForm()).content,
                "Журнал ошибок и связь с разработчиками", 2);
    }

    private void InstallStatisticsPanel() {
        globalTabs.insertTab("Архив запусков",
                Utils.getIcon("/icons/GlobalStatistic.png"),
                (statisticsForm = new StatisticsForm()).content,
                "Сводная статистика по запускам всех проектов", 3);
    }

    private void SaveCallbackPanel() {
        if (callbackForm != null) {
            callbackForm.SaveSplitters();
            callbackForm = null;
        }
    }

    private void SaveStatisticsPanel() {
        if (statisticsForm != null) {
            statisticsForm.SaveSplitters();
            statisticsForm = null;
        }
    }

    private void InstallTestingPanel() {
        globalTabs.insertTab("Тестирование",
                Utils.getIcon("/icons/Session.png"),
                (testingForm = new TestingForm()).Content,
                "Система тестирования", 4);
    }

    private void SaveTestingPanel() {
        if (testingForm != null) {
            testingForm.SaveSplitters();
            testingForm = null;
        }
    }

    int global_index = 0;

    private void saveGlobalTab() {
        global_index = globalTabs.getSelectedIndex();
    }

    private void restoreGlobalTab() {
        globalTabs.setSelectedIndex(global_index);
    }

    //</editor-fold>
    //<editor-fold desc="младшие формы">
    @Override
    public ProjectWindow getProjectWindow() {
        return projectForm;
    }

    @Override
    public VersionsWindow getVersionsWindow() {
        return versionsForm;
    }

    @Override
    public CallbackWindow getCallbackWindow() {
        return callbackForm;
    }

    @Override
    public StatisticsWindow getStatisticWindow() {
        return statisticsForm;
    }

    @Override
    public boolean isProjectFocused() {
        return Current.HasProject()&&globalTabs.getSelectedIndex()==0;
    }

    @Override
    public void ShowUpdatesIcon() {
        bUpdates.setIcon(Utils.getIcon(
                (Global.need_update > 0) || (Global.bad_state > 0)
                        ? "/icons/ComponentsNeedUpdate.gif"
                        : (Global.need_publish > 0 ? "/icons/ComponentsNeedPublish.png" : "/icons/ComponentsActual.png")));
    }

    @Override
    public void FocusProject() {
        globalTabs.setSelectedIndex(0);
    }

    @Override
    public void FocusVersions() {
        globalTabs.setSelectedIndex(1);
    }

    @Override
    public void FocusCallback() {
        globalTabs.setSelectedIndex(2);
    }

    @Override
    public void FocusTesting() {
        globalTabs.setSelectedIndex(4);
    }

    @Override
    public TestingWindow getTestingWindow() {
        return testingForm;
    }

    @Override
    public void SwitchScreen(boolean small) {
        FastAccessScroll.setVisible(!small);
        mainPanel.revalidate();
        mainPanel.repaint();
    }

    @Override
    public void Show() {
        //приходится идти на это только ПОСЛЕ создания главного окна.
        // иначе ссылка на главное окно в методах пустая.
        getCallbackWindow().ShowAll();
        getStatisticWindow().ShowAll();
        getTestingWindow().ShowAll();
        super.Show();
    }

    //</editor-fold>
    //<editor-fold desc="Интерфейс">
    @Override
    public void ShowProject() {
        projectTools.setVisible(true);
        FastAccessScroll.setVisible(true);
        getTestingWindow().ShowProject();
        //-
        saveGlobalTab();
        RemoveProjectPanel();
        InstallProjectPanel();
        restoreGlobalTab();
        SwitchScreen(Global.db.settings.get(SettingName.SmallScreen).toBoolean());
    }

    @Override
    public void ShowNoProject() {
        projectTools.setVisible(false);
        FastAccessScroll.setVisible(false);
        getTestingWindow().ShowNoProject();
        //-
        saveGlobalTab();
        RemoveProjectPanel();
        InstallWelcomePanel();
        if (Current.HasVersion())
            getVersionsWindow().RefreshVersionsTree();
        restoreGlobalTab();
    }

    @Override
    public void ShowRoot() {
        saveGlobalTab();
        RemoveVersionsPanel();
        InstallVersionsPanel();
        restoreGlobalTab();
    }

    @Override
    public void ShowNoRoot() {
        saveGlobalTab();
        RemoveVersionsPanel();
        InstallEmptyVersionsPanel();
        restoreGlobalTab();
    }

    @Override
    public void ShowGlobalSettings() {
        UpdateSetting.current_menu = GlobalMenu.VisualiserSettings;
        UI.ShowDropDownLeft(mVisualiser, UI.menus.get(GlobalMenu.VisualiserSettings));
    }

    @Override
    public void ShowSapforSettings() {
        UpdateSetting.current_menu = GlobalMenu.SapforSettings;
        UI.ShowDropDownUnder(mSapfor, UI.menus.get(GlobalMenu.SapforSettings));
    }

    @Override
    public void RefreshFastAccess() {
        UI.Clear(fastAccessBar);
        int i = 1;
        for (Pass_2021 pass : Pass_2021.FAPasses) {
            if (pass.stats.HasUsages()) {
                fastAccessBar.add(pass.getButton());
                ++i;
                if (i > (Global.db.settings.get(SettingName.FastAccessPassesCount).toInt32())) break;
            }
        }
        fastAccessBar.add(Pass_2021.passes.get(PassCode_2021.DropFastAccess).getButton());
    }

    @Override
    public void ShowNoFastAccess() {
        UI.Clear(fastAccessBar);
        fastAccessBar.add(Pass_2021.passes.get(PassCode_2021.DropFastAccess).getButton());
    }
    //</editor-fold>
}
