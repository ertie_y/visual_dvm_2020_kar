package Visual_DVM_2021.UI.Main;

import Common.Global;
import Common.UI.UI;
import GlobalData.Tasks.CompilationTask.UI.CompilationTaskComparsionForm;
import GlobalData.Tasks.RunTask.UI.RunTaskComparsionForm;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.TasksWindow;

import javax.swing.*;

public class BigScreenTasksForm implements TasksWindow {
    //-
    public JSplitPane SC30;
    public JSplitPane SC31;
    public JSplitPane SC33;
    public JSplitPane SC32;
    public JSplitPane SC34;
    //-
    public JPanel content;
    //-
    //-

    public BigScreenTasksForm(){
        LoadSplitters();

        //todo сделать через ПКМ, панели реально не нужны.
      //  UI.add(compilationTasksTools, PassCode_2021.DeleteSelectedCompilationTasks);
      //  UI.add(runTasksTools, PassCode_2021.DeleteSelectedRunTasks);
    }

    @Override
    public JPanel getContent() {
        return content;
    }

    @Override
    public void init(JPanel compilationTasksPanel,
                     JPanel runTasksPanel,
                     CompilationTaskComparsionForm compilationMaster,
                     CompilationTaskComparsionForm compilationSlave,
                     RunTaskComparsionForm runMaster,
                     RunTaskComparsionForm runSlave) {
        //-------------------------------------------------------
        SC31.setLeftComponent(compilationTasksPanel);
        SC32.setLeftComponent(runTasksPanel);
        //-------------------------------------------------------
        SC33.setLeftComponent(compilationMaster.content);
        SC33.setRightComponent(compilationSlave.content);
        //-------------------------------------------------------
        SC34.setLeftComponent(runMaster.content);
        SC34.setRightComponent(runSlave.content);
        //-------------------------------------------------------
    }
}
