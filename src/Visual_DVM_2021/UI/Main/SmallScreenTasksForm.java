package Visual_DVM_2021.UI.Main;

import Common.Global;
import Common.UI.UI;
import GlobalData.Tasks.CompilationTask.UI.CompilationTaskComparsionForm;
import GlobalData.Tasks.RunTask.UI.RunTaskComparsionForm;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.TasksWindow;

import javax.swing.*;

public class SmallScreenTasksForm implements TasksWindow {
    //--
    public JSplitPane SC40;
    public JSplitPane SC41;
    public JSplitPane SC42;
    public JSplitPane SC43;
    //-
    private JPanel content;
    private JTabbedPane tasksTabs;
    private JToolBar compilationTasksTools;
    private JToolBar runTasksTools;
    @Override
    public JPanel getContent() {
        return content;
    }


    public SmallScreenTasksForm(){
        LoadSplitters();
    }

    @Override
    public void init(JPanel compilationTasksPanel, JPanel runTasksPanel, CompilationTaskComparsionForm compilationMaster, CompilationTaskComparsionForm compilationSlave, RunTaskComparsionForm runMaster, RunTaskComparsionForm runSlave) {
        SC40.setLeftComponent(compilationTasksPanel);
        SC42.setLeftComponent(runTasksPanel);
        //-------------------------------------------------------
        SC41.setLeftComponent(compilationMaster.content);
        SC41.setRightComponent(compilationSlave.content);
        //-------------------------------------------------------
        SC43.setLeftComponent(runMaster.content);
        SC43.setRightComponent(runSlave.content);
    }

    @Override
    public void FocusCompilationTasks() {
        tasksTabs.setSelectedIndex(0);
    }

    @Override
    public void FocusRunTasks() {
        tasksTabs.setSelectedIndex(1);
    }
}
