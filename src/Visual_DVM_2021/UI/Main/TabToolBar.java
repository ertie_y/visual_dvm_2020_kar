package Visual_DVM_2021.UI.Main;

import Common.UI.UI;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TabToolBar extends JToolBar {

    public TabToolBar(String titleText, PassCode_2021... passes) {
        setFloatable(false);
        setOpaque(false);
        //-
        JLabel title = new JLabel(titleText);
        title.setOpaque(false);
        add(title);
        addSeparator();
        //-
        for (PassCode_2021 code : passes) {
            add(Pass_2021.passes.get(code).getTabButton());
        }
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                leftMouseAction();
            }
        });

    }
    public void leftMouseAction(){}
}
