package Visual_DVM_2021.UI.Main;

import Common.UI.Editor.BaseEditor;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.DescriptionInterface;

import javax.swing.*;

public class DescriptionFields implements DescriptionInterface {
    private JPanel content;
    public JSplitPane SC25;
    private JToolBar descriptionTools;
    private JToolBar descriptionAdditionTools;
    private JPanel editorPanel;
    private JPanel additionPanel;
    //---------
    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }
    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }
    @Override
    public JPanel getContent() {
        return content;
    }
    public DescriptionFields(){
        LoadSplitters();
        UI.add(descriptionAdditionTools, PassCode_2021.AppendBugReportDescription);
        UI.add(descriptionTools, PassCode_2021.SaveBugReportDescription);
    }
}
