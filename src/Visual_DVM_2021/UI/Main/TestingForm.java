package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.Settings.SettingName;
import GlobalData.Tasks.CompilationTask.UI.CompilationTaskComparsionForm;
import GlobalData.Tasks.RunTask.UI.RunTaskComparsionForm;
import ProjectData.LanguageName;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.TasksWindow;
import Visual_DVM_2021.UI.Interface.TestingWindow;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;

public class TestingForm implements FormWithSplitters, TestingWindow {
    public JPanel Content;
    public JSplitPane SC37;
    public JSplitPane SC18;
    public JSplitPane SC19;
    public JSplitPane SC38;
    public JSplitPane SC20;
    public JSplitPane SC21;
    public JSplitPane SC35;
    public JSplitPane SC44;
    public JSplitPane SC45;
    //-
    private JToolBar machinesTools;
    private JPanel machinesPanel;
    private JToolBar usersTools;
    private JPanel usersPanel;
    private JToolBar compilersTools;
    private JPanel compilersPanel;
    private JTabbedPane testingTabs;
    private JTabbedPane debugTabs;
    private JButton mProjectMachine;
    private JButton mProjectUser;
    private JLabel lMakefile;
    private JButton mProjectMakefile;
    private JButton lFortran;
    private JButton lC;
    private JButton lCpp;
    private JButton mCompilationMaxtime;
    private JLabel lRunConfiguration;
    private JButton mProjectRunConfigurationDescription;
    private JButton mMatrixes;
    private JButton mRunMaxtime;
    private JToolBar makefilesTools;
    private JPanel makefilesPanel;
    private JToolBar modulesTools;
    private JPanel modulesPanel;

    private JToolBar runConfigurationsTools;
    private JPanel runConfigurationsPanel;

    private JToolBar environmentsTools;
    private JPanel environmentsPanel;
    private JToolBar parametersTools;
    private JPanel dvmParametersPanel;

    private JPanel projectDebugPanel;
    private JScrollPane FastAccessScroll;
    private JToolBar fastAccessBar;
    private JPanel tasksPanel;
    private JToolBar groupsTools;
    private JPanel groupsPanel;
    private JPanel testsPanel;
    private JToolBar testsTools;
    private JButton bMakefilePreview;
    private JButton bTestPreview;
    private JPanel testsRunTasksPanel;
    private JToolBar testsResultsTools;
    private JButton bTest;
    private JButton bDownloadTestProject;
    //-
    public TasksWindow tasksForm = null;

    private JToolBar compilationTasksTools;
    private final JPanel compilationTasksPanel;
    private JToolBar runTasksTools;
    private final JPanel runTasksPanel;
    //-
    public RunTaskComparsionForm runMaster;
    public RunTaskComparsionForm runSlave;
    //
    public CompilationTaskComparsionForm compilationMaster;
    public CompilationTaskComparsionForm compilationSlave;
    //-
    //-


    @Override
    public void SwitchScreen(boolean small) {
        UI.Clear(tasksPanel);
        tasksForm = small ? new SmallScreenTasksForm() : new BigScreenTasksForm();
        tasksForm.init(
                compilationTasksPanel, runTasksPanel,
                compilationMaster, compilationSlave,
                runMaster, runSlave
        );
        tasksPanel.add(tasksForm.getContent());
    }

    //-
    public TestingForm() {
        LoadSplitters();
        //-
        compilationTasksPanel = new JPanel(new BorderLayout());
        runTasksPanel = new JPanel(new BorderLayout());
        compilationMaster = new CompilationTaskComparsionForm(compilationSlave = new CompilationTaskComparsionForm(null));
        runMaster = new RunTaskComparsionForm(runSlave = new RunTaskComparsionForm(null));
        //-
        Global.db.machines.mountUI(machinesPanel);
        Global.db.users.mountUI(usersPanel);
        Global.db.compilers.mountUI(compilersPanel);
        Global.db.makefiles.mountUI(makefilesPanel);
        //-----------------------------------------------
        Global.db.modules.mountUI(modulesPanel);
        Global.db.runConfigurations.mountUI(runConfigurationsPanel);
        Global.db.environmentValues.mountUI(environmentsPanel);
        Global.db.dvmParameters.mountUI(dvmParametersPanel);
        //-
        Global.db.compilationTasks.mountUI(compilationTasksPanel);
        Global.db.runTasks.mountUI(runTasksPanel);
        //-
        UI.add(machinesTools, PassCode_2021.AddMachine,
                PassCode_2021.ApplyMachine,
                PassCode_2021.EditMachine,
                PassCode_2021.DeleteMachine);
        UI.add(usersTools, PassCode_2021.AddUser,
                PassCode_2021.ApplyUser,
                PassCode_2021.EditUser,
                PassCode_2021.InitialiseUser,
                PassCode_2021.DeleteUser);
        UI.add(compilersTools, PassCode_2021.AddCompiler,
                PassCode_2021.EditCompiler,
                PassCode_2021.DeleteCompiler,
                PassCode_2021.ShowCompilerHelp);
        UI.add(makefilesTools, PassCode_2021.AddMakefile,
                PassCode_2021.ApplyMakefile,
                PassCode_2021.EditMakefile,
                PassCode_2021.DeleteMakefile
        );
        UI.add(modulesTools, PassCode_2021.EditModule);
        UI.add(runConfigurationsTools,
                PassCode_2021.AddRunConfiguration,
                PassCode_2021.ApplyRunConfiguration,
                PassCode_2021.EditRunConfiguration,
                PassCode_2021.DeleteRunConfiguration);
        UI.add(environmentsTools,
                PassCode_2021.AddEnvironmentValue,
                PassCode_2021.EditEnvironmentValue,
                PassCode_2021.DeleteEnvironmentValue,
                PassCode_2021.PickCompilerEnvironments);
        UI.add(parametersTools, PassCode_2021.AddDVMParameter, PassCode_2021.EditDVMParameter, PassCode_2021.DeleteDVMParameter);
        UI.add(groupsTools, PassCode_2021.SynchronizeTests, PassCode_2021.AddGroup, PassCode_2021.EditGroup, PassCode_2021.DeleteGroup);
        UI.add(testsTools,
                PassCode_2021.CompileCurrentTest,
                PassCode_2021.DownloadTest,
                PassCode_2021.AddTest,
                PassCode_2021.EditTest,
                PassCode_2021.DeleteTest
        );
        UI.add(testsResultsTools, PassCode_2021.ActualizeTestsTasks);
        //-
        mCompilationMaxtime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.EditProjectCompilationMaxtime).Do();
            }
        });
        mRunMaxtime.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.EditProjectRunMaxtime).Do();
            }
        });
        mProjectMachine.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelectProjectData(true);
                if (!Current.HasMachine())
                    UI.Info("Машина для текущего проекта не назначена.\n" +
                            "Используйте кнопку 'Назначить' на панели машин.");
            }
        });
        mProjectUser.addActionListener(e -> {
            SelectProjectData(true);
            if (!Current.HasUser())
                UI.Info("Пользователь для текущего проекта не назначен.\n" +
                        "Используйте кнопку 'Назначить' на панели пользователей.");
        });
        mProjectMakefile.addActionListener(e -> {
            SelectProjectData(true);
            if (Current.HasMakefile())
                Pass_2021.passes.get(PassCode_2021.EditMakefile).Do();
            else UI.Info("Мейкфайл для текущего проекта не назначен.\n" +
                    "Используйте кнопку 'Назначить' на панели мейкфайлов.");
        });
        mProjectRunConfigurationDescription.addActionListener(e -> {
            SelectProjectData(true);
            if (Current.HasRunConfiguration())
                Pass_2021.passes.get(PassCode_2021.EditRunConfiguration).Do();
            else UI.Info("Конфигурация запуска для текущего проекта не назначена.\n" +
                    "Используйте кнопку 'Назначить' на панели конфигураций запуска.");
        });
        mMatrixes.addActionListener(e -> {
            SelectProjectData(true);
            if (Current.HasRunConfiguration())
                Pass_2021.passes.get(PassCode_2021.EditRunConfiguration).Do();
            else UI.Info("Конфигурация запуска для текущего проекта не назначена.\n" +
                    "Используйте кнопку 'Назначить' на панели конфигураций запуска.");
        });
        lFortran.addActionListener(e -> TryEditModule(LanguageName.fortran));
        lC.addActionListener(e -> TryEditModule(LanguageName.c));
        lCpp.addActionListener(e -> TryEditModule(LanguageName.cpp));
        //-
        RefreshTabsNames();
        //-
        SwitchScreen(Global.db.settings.get(SettingName.SmallScreen).toBoolean());
        ShowNoProject();
        //-
        TestingSystem.db.groups.mountUI(groupsPanel);
        TestingSystem.db.tests.mountUI(testsPanel);
        TestingSystem.db.testRunTasks.mountUI(testsRunTasksPanel);
        bMakefilePreview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.ShowMakefilePreview).Do();
            }
        });
        bTestPreview.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.ShowTestMakefilePreview).Do();
            }
        });
    }

    @Override
    public void ShowAll() {
        Global.db.machines.ShowUI();
        TestingSystem.db.groups.ShowUI();
        TestingSystem.db.testRunTasks.ShowUI();
    }

    //-
    @Override
    public void ShowProjectMaxCompilationTime() {
        mCompilationMaxtime.setText(Current.getProject().compilation_maxtime + " сек.");
    }

    @Override
    public void ShowProjectMaxRunTime() {
        mRunMaxtime.setText(Current.getProject().run_maxtime + " сек.");
    }

    @Override
    public void ShowProjectMachine() {
        if (Current.getProject().hasMachine())
            mProjectMachine.setText(Current.getProject().getMachine().getURL());
        else
            mProjectMachine.setText("?");
    }

    @Override
    public void ShowProjectUser() {
        if (Current.getProject().hasUser())
            mProjectUser.setText(Current.getProject().getUser().login);
        else
            mProjectUser.setText("?");
    }

    @Override
    public void ShowProjectMakefile() {
        if (Current.getProject().HasMakefile()) {
            Makefile makefile = Current.getProject().getMakefile();
            lMakefile.setText(String.valueOf(makefile.id));
            LinkedHashMap<LanguageName, Module> modules = makefile.getModules();
            //-----------------------
            lFortran.setText(modules.get(LanguageName.fortran).getDescription());
            lC.setText(modules.get(LanguageName.c).getDescription());
            lCpp.setText(modules.get(LanguageName.cpp).getDescription());
            //------------------
            mProjectMakefile.setText(makefile.getDescription());
        } else {
            lMakefile.setText("?");
            mProjectMakefile.setText("");
            lFortran.setText("");
            lC.setText("");
            lCpp.setText("");
        }
    }

    @Override
    public void ShowProjectRunConfiguration() {
        if (Current.getProject().hasRunConfiguration()) {
            RunConfiguration runConfiguration = Current.getProject().getRunConfiguration();
            lRunConfiguration.setText(String.valueOf(runConfiguration.id));
            mProjectRunConfigurationDescription.setText(runConfiguration.getLaunchShortDescription());
            mMatrixes.setText(runConfiguration.minMatrix + " - " + runConfiguration.maxMatrix);
        } else {
            lRunConfiguration.setText("?");
            mProjectRunConfigurationDescription.setText("?");
        }
    }

    @Override
    public void SelectProjectData(boolean needs_focus) {
        if (needs_focus) debugTabs.setSelectedIndex(0);
        if (Current.getProject().hasMachine()) {
            Global.db.machines.SetCurrentObjectUI(Current.getProject().getMachine().id);
            if (Current.getProject().hasUser())
                Global.db.users.SetCurrentObjectUI(Current.getProject().getUser().id);
            else Global.db.users.ClearSelectionUI();
            if (Current.getProject().HasMakefile())
                Global.db.makefiles.SetCurrentObjectUI(Current.getProject().getMakefile().id);
            else Global.db.makefiles.ClearSelectionUI();
            if (Current.getProject().hasRunConfiguration())
                Global.db.runConfigurations.SetCurrentObjectUI(Current.getProject().getRunConfiguration().id);
            else Global.db.runConfigurations.ClearSelectionUI();
            if (Current.getProject().hasCompilationTask())
                Global.db.compilationTasks.SetCurrentObjectUI(Current.getProject().getCompilationTask().id);
            else
                Global.db.compilationTasks.ClearSelectionUI();
        } else Global.db.machines.ClearSelectionUI();
    }

    public void TryEditModule(LanguageName languageName) {
        SelectProjectData(true);
        if (Current.HasMakefile()) {
            LinkedHashMap<LanguageName, Module> modules = Current.getMakefile().getModules();
            Global.db.modules.SetCurrentObjectUI(modules.get(languageName).id);
            Pass_2021.passes.get(PassCode_2021.EditModule).Do();
        } else UI.Info("Редактировать языковой модуль невозможно:\nМейкфайл для текущего проекта не назначен.\n" +
                "Используйте кнопку 'Назначить' на панели мейкфайлов.");
    }

    @Override
    public void FocusCredentials() {
        testingTabs.setSelectedIndex(0);
        debugTabs.setSelectedIndex(0);
    }

    @Override
    public void FocusCompilationTasks() {
        testingTabs.setSelectedIndex(0);
        debugTabs.setSelectedIndex(1);
        tasksForm.FocusCompilationTasks();
    }

    @Override
    public void FocusRunTasks() {
        testingTabs.setSelectedIndex(0);
        debugTabs.setSelectedIndex(1);
        tasksForm.FocusRunTasks();
    }

    @Override
    public void ShowProject() {
        testingTabs.setSelectedIndex(0);
        testingTabs.setEnabledAt(0, true);
        //-
        ShowSession();
        SelectProjectData(false);
        //-
        Global.db.compilationTasks.ShowUI(); //их видимость зависит от текущего проекта.
        Global.db.runTasks.ShowUI();
    }

    @Override
    public void ShowNoProject() {
        Global.db.compilationTasks.ClearUI();
        Global.db.runTasks.ClearUI();
        //-
        testingTabs.setSelectedIndex(1);
        testingTabs.setEnabledAt(0, false);
    }

    @Override
    public TasksWindow getTasksWindow() {
        return tasksForm;
    }


    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        tasksForm.SaveSplitters();
    }

    //-
    @Override
    public void ShowCurrentCompilationTask() {
        if (compilationMaster.isActive())
            compilationMaster.ShowTask();
        else compilationSlave.ShowTask();
    }

    @Override
    public void ShowNoCurrentCompilationTask() {
        if (compilationMaster.isActive())
            compilationMaster.Clear();
        else compilationSlave.Clear();
    }

    //------------------------------------------------------>>
    @Override
    public void ShowCurrentRunTask() {
        if (runMaster.isActive())
            runMaster.ShowTask();
        else
            runSlave.ShowTask();
    }

    @Override
    public void ShowNoCurrentRunTask() {
        if (runMaster.isActive())
            runMaster.Clear();
        else
            runSlave.Clear();
    }

    @Override
    public void RefreshTabsNames() {
        UI.ShowTabsNames(testingTabs);
        UI.ShowTabsNames(debugTabs);
    }

}
