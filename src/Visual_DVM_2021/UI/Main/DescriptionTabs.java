package Visual_DVM_2021.UI.Main;

import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.DescriptionInterface;

import javax.swing.*;

public class DescriptionTabs implements DescriptionInterface {
    private JPanel content;
    private JTabbedPane tabs;
    private JPanel editorPanel;
    private JPanel additionPanel;

    public DescriptionTabs() {
        tabs.setTabComponentAt(0, new TabToolBar("Описание", PassCode_2021.SaveBugReportDescription) {
            @Override
            public void leftMouseAction() {
               tabs.setSelectedIndex(0);
            }
        });
        tabs.setTabComponentAt(1, new TabToolBar("Дополнение", PassCode_2021.AppendBugReportDescription) {
            @Override
            public void leftMouseAction() {
               tabs.setSelectedIndex(1);
            }
        });
    }

    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }

    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }

    @Override
    public JPanel getContent() {
        return content;
    }
}
