package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.Global;
import Common.UI.Editor.Viewer;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import GlobalData.Settings.UI.GlobalMenu;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.UI.Editor.SPFEditor;
import ProjectData.LanguageName;
import ProjectData.SapforData.Arrays.UI.FileArraysTree;
import ProjectData.SapforData.Functions.UI.FileFunctionsTree;
import ProjectData.SapforData.Loops.UI.FileLoopsTree;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.FileWindow;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.SPFEditorInterface;
import com.sun.org.glassfish.gmbal.Description;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class FileForm implements FileWindow, FormWithSplitters {
    //todo _ временно
    public static int current_file_line = Utils.Nan;
    @Description("IGNORE")
    protected boolean events_on = true;
    //-
    public JSplitPane SC1;
    public JSplitPane SC12;
    //-
    public JPanel content;
    private JButton bClose;
    private JToolBar Status;
    private JLabel LineCountLabel;
    private JLabel CurrentSymbolLabel;
    private JCheckBox cbShowSpec;
    private JToolBar Tools;
    private JButton bSave;
    private JButton bSearchAndReplace;
    private JButton bFontUp;
    private JButton bFontDown;
    private JButton mFileType;
    private JButton mFileLanguage;
    private JButton mFileStyle;
    private JPanel editorPanel;
    private JTabbedPane messagesTabs;
    private JPanel notesPanel;
    private JPanel warningsPanel;
    private JPanel errorsPanel;
    private JTabbedPane graphsTabs;
    private JPanel loopsGraphPanel;
    private JPanel functionsGraphPanel;
    private JPanel arraysGraphPanel;
    private JSpinner sToGo;
    private JPanel gcov_log_panel;
    private JPanel compilation_out_panel;
    private JPanel run_out_panel;
    private final Viewer compilationOutput;
    private final Viewer runOutput;
    private final Viewer gcovLog;
    private final DBProjectFile file;
    //-
    private TreeForm loopsForm; //рудиментарно. следует сделать интерфейс такой же как у таблиц
    private TreeForm functionsForm;
    private TreeForm arraysForm;
    //-
    private SPFEditor Body = null; //времянка? не очень красиво.

    @Override
    public SPFEditorInterface getEditor() {
        return Body;
    }

    @Override
    public void FocusLoops() {
        graphsTabs.setSelectedIndex(0);
    }

    @Override
    public void FocusFunctions() {
        graphsTabs.setSelectedIndex(1);
    }

    @Override
    public void FocusArrays() {
        graphsTabs.setSelectedIndex(2);
    }

    @Override
    public void RefreshTabsNames() {
        UI.ShowTabsNames(graphsTabs);
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        messagesTabs.setTitleAt(0, (flag ? "Примечания: " : "") + file.father.db.notes.getRowCountUI());
        messagesTabs.setTitleAt(1, (flag ? "Предупреждения: " : "") + file.father.db.warnings.getRowCountUI());
        messagesTabs.setTitleAt(2, (flag ? "Ошибки: " : "") + file.father.db.errors.getRowCountUI());
        messagesTabs.setTitleAt(3, (flag ? "Компиляция" : ""));
        messagesTabs.setTitleAt(4, (flag ? "Запуск" : ""));
        messagesTabs.setTitleAt(5, (flag ? "Журнал GCOV" : ""));
    }

    //-
    public FileForm(DBProjectFile file_in) {
        LoadSplitters();
        file = file_in;
        file.father.db.notes.mountUI(notesPanel);
        file.father.db.warnings.mountUI(warningsPanel);
        file.father.db.errors.mountUI(errorsPanel);
        mFileType.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mFileType, UI.menus.get(GlobalMenu.FileType));
            }
        });
        mFileLanguage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mFileLanguage,
                        UI.menus.get(GlobalMenu.FileLanguage));
            }
        });
        mFileStyle.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(mFileStyle,
                        UI.menus.get(GlobalMenu.FileStyle));
            }
        });
        bSearchAndReplace.addActionListener(e -> UI.ShowSearchForm(Body));
        bFontUp.addActionListener(e -> Body.FontUp());
        bFontDown.addActionListener(e -> Body.FontDown());
        cbShowSpec.addActionListener(e -> {
            Body.setWhitespaceVisible(cbShowSpec.isSelected());
            Body.setEOLMarkersVisible(cbShowSpec.isSelected());
        });
        //---------------------------------------------------------------------------
        compilation_out_panel.add(new JScrollPane(compilationOutput = new Viewer()));
        run_out_panel.add(new JScrollPane(runOutput = new Viewer()));
        gcov_log_panel.add(new JScrollPane(gcovLog = new Viewer()));
        //----------------------------------------------------------------------------
        ShowText();
        ShowType();
        ShowStyle();
        ShowLanguage();
        ShowMessages();
        ShowGCOVLog();
        ShowAllAnalyses();
        Body.addCaretListener(ce -> ShowCaretInfo());
        ShowCaretInfo();
        Body.requestFocus();
        Pass_2021.passes.get(PassCode_2021.Save).setButton(bSave);
        //-
        RefreshTabsNames();
        sToGo.addChangeListener(e -> Body.gotoLine((Integer) sToGo.getValue()));
        UI.add(Tools, PassCode_2021.CloseCurrentFile);
    }

    @Override
    public void ShowText() {
        UI.Clear(editorPanel);
        editorPanel.add(new RTextScrollPane(Body = new SPFEditor(file)));
    }

    @Override
    public void ShowLanguage() {
        mFileLanguage.setText(file.languageName.getDescription());
        Body.switching_language = true;
        if (file.languageName.equals(LanguageName.fortran)) {
            switch (file.style) {
                case fixed:
                case extended:
                case none:
                    Body.setSyntaxEditingStyle("text/FortranSPF");
                    break;
                case free:
                    Body.setSyntaxEditingStyle("text/FreeFortranSPF");
                    break;
            }
        } else
            Body.setSyntaxEditingStyle(file.languageName.getStyleKey());
        Body.setCodeFoldingEnabled(true);
        Body.switching_language = false;
    }

    @Override
    public void ShowType() {
        mFileType.setText(file.fileType.getDescription());
    }

    @Override
    public void ShowStyle() {
        Body.switching_language = true;
        mFileStyle.setText(file.style.getDescription());
        int m = file.style.getMargin();
        Body.setMarginLineEnabled(true);
        Body.setMarginLinePosition(m);
        if (m <= 0)
            Body.setMarginLineEnabled(false);

        if (file.languageName.equals(LanguageName.fortran)) {
            switch (file.style) {
                case fixed:
                case extended:
                case none:
                    Body.setSyntaxEditingStyle("text/FortranSPF");
                    break;
                case free:
                    Body.setSyntaxEditingStyle("text/FreeFortranSPF");
                    break;

            }
        }
        Body.switching_language = false;
    }

    @Override
    public void ShowMessages() {
        file.father.db.notes.ShowUI();
        file.father.db.warnings.ShowUI();
        file.father.db.errors.ShowUI();
        boolean full_tiles = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();

        messagesTabs.setTitleAt(0, (full_tiles ? "Примечания: " : "") + file.father.db.notes.getRowCountUI());
        messagesTabs.setTitleAt(1, (full_tiles ? "Предупреждения: " : "") + file.father.db.warnings.getRowCountUI());
        messagesTabs.setTitleAt(2, (full_tiles ? "Ошибки: " : "") + file.father.db.errors.getRowCountUI());
        FocusMessagesPriority();
    }

    @Override
    public void ShowNoMessages() {
        boolean full_tiles = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        file.father.db.notes.ClearUI();
        file.father.db.warnings.ClearUI();
        file.father.db.errors.ClearUI();
        messagesTabs.setTitleAt(0, (full_tiles ? "Примечания: " : "") + "-");
        messagesTabs.setTitleAt(1, (full_tiles ? "Предупреждения: " : "") + "-");
        messagesTabs.setTitleAt(2, (full_tiles ? "Ошибки: " : "") + "-");
    }

    @Override
    public void ShowLoops() {
        loopsForm.Show();
        Body.HighlightLoops();
    }

    @Override
    public void ShowNoLoops() {
        loopsForm.Clear();
        Body.ClearLoopsHighLights();
    }

    @Override
    public void ShowGCOVLog() {
        gcovLog.setText(file.GCOVLog);
    }

    @Override
    public void ShowGCOV() {
        Body.HighlightGCOV();
    }

    @Override
    public void ShowNoGCOVLog() {
        gcovLog.setText("");
    }

    @Override
    public void FocusGCOVLog() {
        messagesTabs.setSelectedIndex(5);
    }


    @Override
    public void ShowNoGCOV() {
        Body.ClearGOCVHighlights();

    }

    @Override
    public void ShowFunctions() {
        functionsForm.Show();
    }

    @Override
    public void ShowNoFunctions() {
        functionsForm.Clear();
    }

    @Override
    public void ShowArrays() {
        arraysForm.Show();
    }

    @Override
    public void ShowNoArrays() {
        arraysForm.Clear();
    }

    public void EventsOn() {
        events_on = true;
    }

    public void EventsOff() {
        events_on = false;
    }

    @Override
    public void ShowCaretInfo() {
        int new_current_line = Body.getCurrentLine();
        boolean line_changed = (new_current_line != current_file_line);
        current_file_line = new_current_line;
        sToGo.setModel(new SpinnerNumberModel(current_file_line,
                1, Body.getLineCount(), 1
        ));
        //-
        LineCountLabel.setText(String.valueOf(Body.getLineCount()));
        CurrentSymbolLabel.setText(String.valueOf(Body.getCurrentSymbol()));
        //если выделяем строку из графа, то тоже надо отключиьт события.
        //чтобы по ней не начался поиск.
        if (events_on && line_changed) {
            // System.out.println("Search in graphs by line...");
            if (loopsForm.getTree() != null)
                loopsForm.getTree().SelectNodeByCriteria(current_file_line);
            if (functionsForm.getTree() != null)
                functionsForm.getTree().SelectNodeByCriteria(current_file_line);
            if (arraysForm.getTree() != null)
                arraysForm.getTree().SelectNodeByCriteria(current_file_line);
            //  System.out.println("DONE");
        }
    }

    @Override
    public void FocusMessagesPriority() {
        switch (file.state) {
            case HasErrors:
                messagesTabs.setSelectedIndex(2);
                break;
            case HasWarnings:
                messagesTabs.setSelectedIndex(1);
                break;
            default:
                messagesTabs.setSelectedIndex(0);
                break;
        }
    }

    @Override
    public void ShowFirstError() {
        if (file.father.db.errors.size() > 0)
            file.father.db.errors.ui.control.SelectRow(0);
    }

    @Override
    public void ShowCompilationOutput() {
        compilationOutput.setText(file.father.compilation_output);
    }

    @Override
    public void ShowNoCompilationOutput() {
        compilationOutput.setText("");
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
        loopsGraphPanel = (loopsForm = new TreeForm(FileLoopsTree.class)).getContent();
        functionsGraphPanel = (functionsForm = new TreeForm(FileFunctionsTree.class)).getContent();
        arraysGraphPanel = (arraysForm = new TreeForm(FileArraysTree.class)).getContent();
    }
}
