package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.Global;
import Common.UI.Editor.BaseEditor;
import Common.UI.Editor.Viewer;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportState;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.CallbackWindow;
import Visual_DVM_2021.UI.Interface.CommentInterface;
import Visual_DVM_2021.UI.Interface.DescriptionInterface;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class CallbackForm implements FormWithSplitters, CallbackWindow {
    public JPanel content;
    //-
    public JSplitPane SC6;
    public JSplitPane SC10;
    public JSplitPane SC11;
    //-
    private JToolBar accountTools;
    private JLabel lAccountName;
    private JLabel lAccountMail;
    private JLabel lAccountRole;
    private JPanel bugReportsPanel;
    private JPanel filterFormPanel;

    private JPanel recipientsPanel;
    private JTabbedPane currentBugReportTabs;
    private final JScrollPane bugCommentScroll;
    private JScrollPane bugSettingsScroll;
    private JToolBar recipientsTools;
    private final JScrollPane bugReportCommentAdditionScroll;
    private JTextField BugReportExecutor;
    private JCheckBox BugReportsMyOnlyFilter;
    private JCheckBox BugReportsOpenedOnly;
    private JToolBar bugsTools;
    private JToolBar settingsTools;
    private JToolBar descriptionTools;
    private JToolBar commentTools;
    private JToolBar descriptionAdditionTools;
    private JToolBar commentAdditionTools;
    private JPanel descriptionPanel;
    private JPanel commentPanel;
    private final BaseEditor BugReportComment;
    private final BaseEditor BugReportCommentAddition;
    private Viewer BugReportSettings;
    public DescriptionInterface descriptionInterface;
    public CommentInterface commentInterface;
    //-------------------------------------------
    private final JScrollPane bugDescriptionScroll;
    private final JScrollPane bugReportDescriptionAdditionScroll;
    private final BaseEditor BugReportDescription;
    private final BaseEditor BugReportDescriptionAddition;
    //-------------------------------------------

    @Override
    public void SwitchScreen(boolean small) {
        UI.Clear(descriptionPanel);
        //------------------------------------------------------------------
        descriptionInterface = small ? new DescriptionTabs() : new DescriptionFields();
        descriptionInterface.setEditorScroll(bugDescriptionScroll);
        descriptionInterface.setAdditionScroll(bugReportDescriptionAdditionScroll);
        descriptionPanel.add(descriptionInterface.getContent());
        //----------
        descriptionPanel.revalidate();
        descriptionPanel.repaint();
        //---------
        UI.Clear(commentPanel);
        //------------------------------------------------------------------
        commentInterface = small ? new CommentTabs() : new CommentFields();
        commentInterface.setEditorScroll(bugCommentScroll);
        commentInterface.setAdditionScroll(bugReportCommentAdditionScroll);
        commentPanel.add(commentInterface.getContent());
        //----------
        commentPanel.revalidate();
        commentPanel.repaint();
    }

    public CallbackForm() {
        LoadSplitters();
        //---------------------------------
        //чтобы не было индусятины, лучше создать контролы здесь
        BugReportDescription = new BaseEditor();
        BugReportDescription.setLineWrap(true);
        bugDescriptionScroll = new JScrollPane(BugReportDescription);
        //----------------------------------------------
        BugReportDescriptionAddition = new BaseEditor();
        BugReportDescriptionAddition.setLineWrap(true);
        bugReportDescriptionAdditionScroll = new JScrollPane(BugReportDescriptionAddition);
        BugReportDescription.setWrapStyleWord(true);
        BugReportDescriptionAddition.setWrapStyleWord(true);
        //---------------------------------------------
        //-
        BugReportComment = new BaseEditor();
        BugReportComment.setLineWrap(true);
        bugCommentScroll = new JScrollPane(BugReportComment);
        //-
        BugReportCommentAddition = new BaseEditor();
        BugReportCommentAddition.setLineWrap(true);
        bugReportCommentAdditionScroll = new JScrollPane(BugReportCommentAddition);
        BugReportComment.setWrapStyleWord(true);
        BugReportCommentAddition.setWrapStyleWord(true);
        //----------------------------------------------
        //тут развилка на то вкладки или поля.
        SwitchScreen(Global.db.settings.get(SettingName.SmallScreen).toBoolean());
        //-
        RepositoryServer.db.bugReports.mountUI(bugReportsPanel);
        Global.db.recipients.mountUI(recipientsPanel);
        UI.add(recipientsTools, PassCode_2021.AddRecipient, PassCode_2021.EditRecipient, PassCode_2021.DeleteRecipient);
        UI.add(settingsTools, PassCode_2021.ApplyBugReportSettings);
        UI.add(accountTools, PassCode_2021.RegisterAccount, PassCode_2021.Subscribe);
        UI.add(recipientsTools, PassCode_2021.ExtractRecipients, PassCode_2021.SaveBugReportRecipients);
        recipientsTools.add(Pass_2021.getButton(PassCode_2021.SaveBugReportExecutor), 2);
        //-
        filterFormPanel.add(RepositoryServer.db.bugReports.getFilterUI().getContent());
    }

    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        descriptionInterface.SaveSplitters();
        commentInterface.SaveSplitters();
    }

    @Override
    public String getBugReportDescriptionText() {
        return BugReportDescription.getText();
    }

    @Override
    public String getBugReportDescriptionAdditionText() {
        return BugReportDescriptionAddition.getText();
    }

    @Override
    public String getBugReportCommentText() {
        return BugReportComment.getText();
    }

    @Override
    public String getBugReportCommentAdditionText() {
        return BugReportCommentAddition.getText();
    }

    @Override
    public void ClearBugReportDescriptionAdditionText() {
        BugReportDescriptionAddition.setText("");
    }

    @Override
    public void ClearBugReportCommentAdditionText() {
        BugReportCommentAddition.setText("");
    }

    @Override
    public String getBugReportExecutorText() {
        return BugReportExecutor.getText();
    }

    @Override
    public void FocusRecipients() {
        currentBugReportTabs.setSelectedIndex(2);
    }


    DocumentListener descriptionAdditionListener = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (!Current.getBugReport().state.equals(BugReportState.draft))
                Current.getBugReport().descriptionAdditionDraft =
                        BugReportDescriptionAddition.getText();
        }
    };
    DocumentListener commentAdditionListener = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (!Current.getBugReport().state.equals(BugReportState.draft))
                Current.getBugReport().commentAdditionDraft = BugReportCommentAddition.getText();
        }
    };
    DocumentListener descriptionListener = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (Current.getBugReport().state.equals(BugReportState.draft))
                Current.getBugReport().description =
                        BugReportDescription.getText();
            RepositoryServer.db.bugReports.RefreshUI();
        }
    };
    DocumentListener commentListener = new DocumentListener() {
        @Override
        public void removeUpdate(DocumentEvent e) {
        }

        @Override
        public void insertUpdate(DocumentEvent e) {
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            if (Current.getBugReport().state.equals(BugReportState.draft))
                Current.getBugReport().comment = BugReportComment.getText();
        }
    };

    public void SwitchListeners(boolean on) {
        //System.out.println("switching listeners " + on);
        if (on) {
            BugReportDescriptionAddition.getDocument().addDocumentListener(descriptionAdditionListener);
            BugReportCommentAddition.getDocument().addDocumentListener(commentAdditionListener);
            BugReportDescription.getDocument().addDocumentListener(descriptionListener);
            BugReportComment.getDocument().addDocumentListener(commentListener);
        } else {
            BugReportDescriptionAddition.getDocument().removeDocumentListener(descriptionAdditionListener);
            BugReportCommentAddition.getDocument().removeDocumentListener(commentAdditionListener);
            BugReportDescription.getDocument().removeDocumentListener(descriptionListener);
            BugReportComment.getDocument().removeDocumentListener(commentListener);
        }
    }

    private void createUIComponents() {
        BugReportSettings = new Viewer();
        bugSettingsScroll = new JScrollPane(BugReportSettings);
        BugReportExecutor = new StyledTextField();
    }

    @Override
    public void ShowAccount() {
        lAccountName.setText(Utils.Brackets(Current.getAccount().name));
        lAccountMail.setText(Utils.Brackets(Current.getAccount().email));
        lAccountRole.setText(Utils.Brackets(Current.getAccount().role.getDescription()));
    }

    @Override
    public void ShowBugReports() {
        RepositoryServer.db.bugReports.ShowUI();
    }

    @Override
    public void ShowNoBugReports() {
        RepositoryServer.db.bugReports.ClearUI();
    }

    @Override
    public void ShowRecipients() {
        Global.db.recipients.ShowUI();
    }

    @Override
    public void ShowNoRecipients() {
        Global.db.recipients.ClearUI();
    }

    @Override
    public void ShowCurrentBugReport() {
        SwitchListeners(false);
        currentBugReportTabs.setSelectedIndex(0);
        BugReport target = Current.getBugReport();
        target.CheckRecipients();
        Global.db.recipients.ShowUI();
        //-
        BugReportDescription.setText(target.description);
        BugReportDescription.ShowBegin();
        BugReportDescriptionAddition.setText(target.descriptionAdditionDraft);
        BugReportExecutor.setText(target.executor);
        //-
        BugReportComment.setText(target.comment);
        BugReportComment.ShowBegin();
        BugReportCommentAddition.setText(target.commentAdditionDraft);
        BugReportSettings.setText(
                String.join("\n",
                        ("Версия SAPFOR: " + target.sapfor_version),
                        ("Версия визуализатора: " + target.visualiser_version),
                        target.sapfor_settings
                )
        );
        Pass_2021.ShowButton(PassCode_2021.ApplyBugReportSettings, PassCode_2021.DeleteBugReport);
        //
        if (target.state.equals(BugReportState.draft)) {
            //-
            Pass_2021.HideButton(PassCode_2021.AppendBugReportDescription,
                    PassCode_2021.AppendBugReportComment,
                    PassCode_2021.SaveBugReportDescription,
                    PassCode_2021.SaveBugReportComment,
                    PassCode_2021.OpenBugReport,
                    PassCode_2021.CloseBugReport,
                    PassCode_2021.OpenBugReportTestProject,
                    PassCode_2021.UpdateBugReportProgress
            );
            Pass_2021.ShowButton(
                    PassCode_2021.PublishBugReport,
                    PassCode_2021.SaveBugReportRecipients,
                    PassCode_2021.SaveBugReportExecutor
            );
        } else {
            boolean canSave =
                    Current.getAccount().isAdmin() ||
                            Current.getAccount().email.equals(target.sender_address);
            if (canSave) {
                Pass_2021.ShowButton(PassCode_2021.SaveBugReportDescription,
                        PassCode_2021.SaveBugReportComment,
                        PassCode_2021.SaveBugReportExecutor
                );
            } else {
                Pass_2021.HideButton(PassCode_2021.SaveBugReportDescription,
                        PassCode_2021.SaveBugReportComment,
                        PassCode_2021.SaveBugReportExecutor
                );
            }
            Pass_2021.ShowButton(
                    PassCode_2021.SaveBugReportRecipients,
                    PassCode_2021.AppendBugReportDescription,
                    PassCode_2021.AppendBugReportComment,
                    PassCode_2021.OpenBugReport,
                    PassCode_2021.CloseBugReport,
                    PassCode_2021.OpenBugReportTestProject,
                    PassCode_2021.UpdateBugReportProgress
            );
            Pass_2021.HideButton(PassCode_2021.PublishBugReport);
        }
        SwitchListeners(true);
    }

    @Override
    public void ShowNoCurrentBugReport() {
        SwitchListeners(false);
        Global.db.recipients.ShowUI();
        //-
        BugReportDescription.setText("");
        BugReportComment.setText("");
        BugReportDescriptionAddition.setText("");
        BugReportCommentAddition.setText("");
        BugReportSettings.setText("");
        BugReportExecutor.setText("");
        //-
        Pass_2021.HideButton(PassCode_2021.SaveBugReportExecutor,
                PassCode_2021.SaveBugReportRecipients,
                PassCode_2021.AppendBugReportDescription,
                PassCode_2021.SaveBugReportDescription,
                PassCode_2021.SaveBugReportComment,
                PassCode_2021.AppendBugReportComment,
                PassCode_2021.ApplyBugReportSettings,
                PassCode_2021.PublishBugReport,
                PassCode_2021.OpenBugReport,
                PassCode_2021.CloseBugReport,
                PassCode_2021.OpenBugReportTestProject,
                PassCode_2021.UpdateBugReportProgress,
                PassCode_2021.DeleteBugReport
        );
    }

    @Override
    public void ShowSubscription() {
        Pass_2021.getButton(PassCode_2021.Subscribe).setText(Current.getAccount().subscription.getDescription());
    }

}
