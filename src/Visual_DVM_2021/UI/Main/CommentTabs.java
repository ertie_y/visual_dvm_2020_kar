package Visual_DVM_2021.UI.Main;

import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.CommentInterface;

import javax.swing.*;

public class CommentTabs implements CommentInterface {
    private JPanel comment;
    private JTabbedPane tabs;
    private JPanel editorPanel;
    private JPanel additionPanel;
    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }
    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }
    @Override
    public JPanel getContent() {
        return comment;
    }

    public CommentTabs(){
        tabs.setTabComponentAt(0, new TabToolBar("Комментарий", PassCode_2021.SaveBugReportComment) {
            @Override
            public void leftMouseAction() {
               tabs.setSelectedIndex(0);
            }
        });
        tabs.setTabComponentAt(1, new TabToolBar("Дополнение", PassCode_2021.AppendBugReportComment) {
            @Override
            public void leftMouseAction() {
               tabs.setSelectedIndex(1);
            }
        });
    }
}
