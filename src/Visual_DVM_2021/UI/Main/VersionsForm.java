package Visual_DVM_2021.UI.Main;
import Common.UI.Trees.TreeForm;
import ProjectData.Project.UI.VersionsTree;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.UI.Interface.ComparsionWindow;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.VersionsWindow;

import javax.swing.*;
public class VersionsForm implements FormWithSplitters, VersionsWindow {
    public JPanel content;
    private JPanel versionsPanel;
    private TreeForm versionsTreeForm;
    private final db_project_info root;
    //-
    public JSplitPane SC2;
    public JSplitPane SC9;
    //-
    public ComparsionForm MasterComparsionForm;
    public ComparsionForm SlaveComparsionForm;
    //-
    public VersionsForm(db_project_info root_in) {
        LoadSplitters();
        root = root_in;
        MasterComparsionForm = new ComparsionForm(SlaveComparsionForm = new ComparsionForm(null));
        SC9.setLeftComponent(MasterComparsionForm.content);
        SC9.setRightComponent(SlaveComparsionForm.content);
        versionsTreeForm.Show();
        //-
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        versionsPanel = (versionsTreeForm = new TreeForm(VersionsTree.class)).getContent();
    }
    @Override
    public void RefreshVersionsTree() {
        versionsTreeForm.Refresh();
    }
    @Override
    public void SelectVersion(db_project_info version) {
        versionsTreeForm.getTree().SelectNode(version.node);
        versionsTreeForm.getTree().ExpandNode(version.node);
    }
    @Override
    public ComparsionWindow getMasterComparsionWindow() {
        return MasterComparsionForm;
    }
}
