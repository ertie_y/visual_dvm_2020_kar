package Visual_DVM_2021.UI.Main;

import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import ProjectData.SapforData.Arrays.ProjectArray;
import Visual_DVM_2021.UI.Interface.FilterWindow;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ArraysFilterForm implements FilterWindow {
    public JPanel content;
    private JPanel filter_panel;
    public JTextField filterName;
    public JTextField filterLocationName;
    public JTextField filterFile;
    public JTextField filterRegion;
    public JTextField filterLocation;
    private JLabel matchesLabel;

    public ArraysFilterForm() {
        filterName.setText(ProjectArray.filterName);
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterName = filterName.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterName = filterName.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterLocation.setText(ProjectArray.filterLocation);
        filterLocation.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterLocation = filterLocation.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterLocation = filterLocation.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterLocationName.setText(ProjectArray.filterLocationName);
        filterLocationName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterLocationName = filterLocationName.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterLocationName = filterLocationName.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterFile.setText(ProjectArray.filterFile);
        filterFile.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterFile = filterFile.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterFile = filterFile.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        //-
        filterRegion.setText(ProjectArray.filterRegion);
        filterRegion.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                ProjectArray.filterRegion = filterRegion.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                ProjectArray.filterRegion = filterRegion.getText();
                UI.getNewMainWindow().getProjectWindow().ShowArrays();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
    }

    @Override
    public void ShowMatchesCount(int count) {
        matchesLabel.setText(String.valueOf(count));
    }
    @Override
    public JPanel getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        filterName=new StyledTextField();
        filterLocationName=new StyledTextField();
        filterLocation=new StyledTextField();
        filterFile=new StyledTextField();
        filterRegion=new StyledTextField();
    }
}
