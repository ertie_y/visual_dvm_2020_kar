package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.UI.Editor.Viewer;
import Common.UI.UI;
import Common.Utils.TextLog;
import GlobalData.Tasks.Task;
import org.fife.ui.rtextarea.RTextScrollPane;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public abstract class TaskComparsionForm<T extends Task> {
    public JPanel content;
    private JToolBar tools;
    private JButton bApplyVersion;
    protected JTabbedPane outputTabs;
    private JPanel out_panel;
    private JLabel lTask;
    private JButton mTime;
    private JPanel err_panel;
    //-
    protected JButton mCleanTime;
    protected JButton mMatrix;
    private JPanel sts_panel;
    public JCheckBox active;
    private JButton button1;
    //-
    TaskComparsionForm<T> slave = null;
    TaskComparsionForm<T> master = null;

    //-
    public boolean isMaster() {
        return slave != null;
    }

    public boolean isSlave() {
        return master != null;
    }

    //-
    protected T task = null;

    public boolean isActive() {
        return active.isSelected();
    }

    protected abstract Current getDefaultTaskType();

    //-
    protected Viewer outBody = null;
    public RTextScrollPane outScroll;
    //-
    protected Viewer errBody = null;
    public RTextScrollPane errScroll;
    //-
    protected Viewer stsBody = null;
    public RTextScrollPane stsScroll;

    //-
    public TaskComparsionForm(TaskComparsionForm<T> slave_in) {
        slave = slave_in;
        //-
        outBody = new Viewer();
        outScroll = new RTextScrollPane(outBody);
        out_panel.add(outScroll);
        //-
        errBody = new Viewer();
        errScroll = new RTextScrollPane(errBody);
        err_panel.add(errScroll);
        //-
        stsBody = new Viewer();
        stsScroll = new RTextScrollPane(stsBody);
        sts_panel.add(stsScroll);
        //-
        setIcons();
        if (isMaster()) {
          //  slave.outScroll.getVerticalScrollBar().setModel(outScroll.getVerticalScrollBar().getModel());
            active.setSelected(true);
            slave.master = this;
        }
        active.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TaskComparsionForm<T> partner = isMaster()? slave: master;
                partner.active.setSelected(!active.isSelected());

                if (isActive()){
                    //переключение фокуса с партнера на себя если оно есть.
                    if (partner.task!=null)
                        showTask(partner.task);
                }else {
                    if (task!=null) {
                        partner.showTask(task);
                    }
                }
            }
        });
    }

    public abstract void setIcons();

    protected void showTask(T task_in) {
        task = task_in;
        lTask.setText(String.valueOf(task.id));
        mTime.setText(String.valueOf(task.Time));
        outBody.setText(task.getOutput());
        errBody.setText(task.getErrors());
    }

    public void ShowTask() {
        TextLog log = new TextLog();
        if (Current.Check(log, getDefaultTaskType())) {
            showTask((T) Current.get(getDefaultTaskType()));
            switch (task.state) {
                case FailedToQueue:
                case Crushed:
                case DoneWithErrors:
                    outputTabs.setSelectedIndex(1);
                    break;
                default:
                    outputTabs.setSelectedIndex(0);
                    break;
            }
        } else UI.Info(log.toString());
    }

    public void Clear() {
        lTask.setText("?");
        mTime.setText("?");
        outBody.setText("");
        errBody.setText("");
    }
}
