package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.UI.Editor.Viewer;
import Common.UI.TextField.StyledTextField;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.UI.GlobalMenu;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.UI.FilesTree;
import ProjectData.Project.db_project_info;
import ProjectData.SapforData.Arrays.UI.DimensionsTableForm;
import ProjectData.SapforData.Arrays.UI.RulesTree;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphForm;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphUI;
import ProjectData.SapforData.Functions.UI.InlineTree;
import ProjectData.SapforData.Functions.UI.InlineTree2;
import ProjectData.SapforData.Includes.UI.IncludesTree;
import Visual_DVM_2021.Passes.All.SPF_GetGraphFunctionPositions;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.UI.Interface.FormWithSplitters;
import Visual_DVM_2021.UI.Interface.ProjectWindow;
import javafx.util.Pair;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ProjectForm implements FormWithSplitters, ProjectWindow {
    public JSplitPane SC3;
    public JSplitPane SC4;
    public JSplitPane SC5;
    public JSplitPane SC7;
    public JSplitPane SC8;
    public JSplitPane SC15;
    //</editor-fold>
    public JPanel content;
    private JTabbedPane projectTabs;
    private JPanel filesTreePanel;
    private db_project_info project = null;
    private TreeForm filesForm;
    public TreeForm distributionForm1;
    public TreeForm includesForm;
    private DimensionsTableForm dimensionsForm;
    private final ScenariosForm scenariosForm;
    private Viewer projectDescription = null;
    private JScrollPane projectDescriptionScroll;
    private JScrollPane logScroll;
    private JPanel distributionPanel;
    private JButton bZoomIn;
    private JButton bZoomOut;
    private JButton bSaveGraphAsImage;
    private JPanel functionsGraphPanel;
    private JButton BDistributed;
    private JButton bMultiplied;
    private JButton bGenerateVariants;
    private JPanel dimensionsPanel;
    private JButton bPredictVariants;
    private JButton bCreateVariants;
    private JLabel SelectedVariantsCount;
    private JLabel VisibleVariantsCount;
    private JLabel TotalVariantsCount;
    private JPanel variantsPanel;
    private JPanel inlinePanel2;
    private JPanel inlinePanel;
    private JPanel arraysPanel;
    private JPanel savedArraysPanel;
    private JPanel arraysFilterPanel;
    private JToolBar variantsFilterTools1;
    private JToolBar variantsTools;
    private TreeForm inlineForm;
    private TreeForm inlineForm2;
    private FunctionsGraphForm functionsGraphForm;
    private Viewer logText;
    private JPanel includesPanel;
    private JLabel l_lines;
    private JLabel l_loops;
    private JLabel l_arrays;
    private JLabel l_functions;
    private JLabel l_spf_dirs;
    private JLabel l_dvm_dirs;
    private JPanel regionsPanel;
    private JTabbedPane projectTreesTabs;
    private JLabel lProjectLanguage;
    private JSpinner sIterations;
    private JSpinner sResistance;
    private JSpinner sScreen;
    private JLabel l_maxdim;
    private JPanel filePanel;
    private JScrollPane fgToolsScroll;
    private JToolBar fastAccessBar;
    private JCheckBox ShowStandardFilter;
    private JTextField filterName;
    private JCheckBox ShowExternalFilter;
    private JCheckBox ShowUnreachableFilter;
    private JCheckBox FunctionsFilterMode;
    private JCheckBox ShowIn;
    private JCheckBox ShowOut;
    private JSpinner sDepth;
    private JLabel depthLabel;
    private JButton bFitToScreen;

    //-
    public ProjectForm(db_project_info project_in) {
        LoadSplitters();
        project = project_in;
        //отличие от старой схемы. мы создаем интерфейс только когда создается окно.
        project.parallelVariants.mountUI(variantsPanel);
        project.parallelRegions.mountUI(regionsPanel);
        project.declaratedArrays.mountUI(arraysPanel);
        arraysFilterPanel.add(project.declaratedArrays.getFilterUI().getContent());
        project.db.savedArrays.mountUI(savedArraysPanel);
        //------
        projectTabs.insertTab("",
                Utils.getIcon("/icons/Arrays/SpfPrivate.png"),
                (scenariosForm = new ScenariosForm()).content,
                "Сценарии", 7);
        //----
        lProjectLanguage.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                UI.ShowDropDownUnder(lProjectLanguage, UI.menus.get(GlobalMenu.ProjectLanguage));
            }
        });
        bZoomIn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                functionsGraphForm.control.zoomIn();
            }
        });
        bZoomOut.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                functionsGraphForm.control.zoomOut();
            }
        });
        bSaveGraphAsImage.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.SaveGraph).Do();
            }
        });
        BDistributed.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                project.SwitchFilterDistributed();
                BDistributed.setIcon(Utils.getIcon(project.f_distributed() ? "/icons/Pick.png" : "/icons/NotPick.png"));
                ShowVariantsFilter();
                ShowFilteredVariantsCount();
            }
        });
        bMultiplied.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                project.SwitchFilterMultiplied();
                bMultiplied.setIcon(Utils.getIcon(project.f_multiplied() ? "/icons/Pick.png" : "/icons/NotPick.png"));
                ShowVariantsFilter();
                ShowFilteredVariantsCount();
            }
        });
        //-
        filesForm.Show();
        scenariosForm.ShowAll();
        ShowProjectDescription();
        ShowProjectLanguage();
        ShowAllAnalyses();

        //-
        bGenerateVariants.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.GenerateParallelVariants).Do();
            }
        });
        bPredictVariants.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.PredictParallelVariants).Do();
            }
        });
        bCreateVariants.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.CreateParallelVariants).Do();
            }
        });
        //--

        RefreshTabsNames();
        //---
        sIterations.setModel(new SpinnerNumberModel(project.fgIterations,
                100, 5000, 100
        ));
        sResistance.setModel(new SpinnerNumberModel(project.fgResistance,
                10, 5000, 50
        ));
        sScreen.setModel(new SpinnerNumberModel(project.fgScreen,
                0.25, 2.0, 0.05
        ));
        sDepth.setModel(new SpinnerNumberModel(1,
                0, 64, 1
        ));
        UI.MakeSpinnerRapid(sIterations, e -> {
            project.UpdatefgIterations((int) sIterations.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sResistance, e -> {
            project.UpdatefgResistance((int) sResistance.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sScreen, e -> {
            project.UpdatefgScreen((double) sScreen.getValue());
            FunctionsGraphUI.ffTimer.restart();
        });
        UI.MakeSpinnerRapid(sDepth, e-> {
            SPF_GetGraphFunctionPositions.depth = (int) sDepth.getValue();
            FunctionsGraphUI.ffTimer.restart();
        });
        //----фильтрация функций
        //<editor-fold desc="Фильтрация функций">
        FunctionsGraphUI.ffTimer.setRepeats(false);
        filterName.setText(SPF_GetGraphFunctionPositions.filterName);
        filterName.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                if (!SPF_GetGraphFunctionPositions.showByCurrentFunction) {
                    SPF_GetGraphFunctionPositions.filterName = filterName.getText();
                    FunctionsGraphUI.ffTimer.restart();
                }
            }
            @Override
            public void removeUpdate(DocumentEvent e) {
                if (!SPF_GetGraphFunctionPositions.showByCurrentFunction) {
                    SPF_GetGraphFunctionPositions.filterName = filterName.getText();
                    FunctionsGraphUI.ffTimer.restart();
                }
            }
            @Override
            public void changedUpdate(DocumentEvent e) {
            }
        });
        ShowFunctionGraphSettings();
        //-
        ShowStandardFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showStandardFunctions = ShowStandardFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowExternalFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showExternalFunctions = ShowExternalFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowUnreachableFilter.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showUnreachableFunctions = ShowUnreachableFilter.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        FunctionsFilterMode.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showByCurrentFunction = FunctionsFilterMode.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            if (SPF_GetGraphFunctionPositions.showByCurrentFunction)
                showCurrentFunctionFilterMode();
            else
                showNormalFunctionsFilterMode();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowIn.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showIn = ShowIn.isSelected();
            FunctionsGraphUI.ffTimer.stop();
                Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        ShowOut.addActionListener(e -> {
            SPF_GetGraphFunctionPositions.showOut = ShowOut.isSelected();
            FunctionsGraphUI.ffTimer.stop();
            Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
        });
        //-->>
        if (SPF_GetGraphFunctionPositions.showByCurrentFunction)
            showCurrentFunctionFilterMode();
        else
            showNormalFunctionsFilterMode();
        //</editor-fold>

        //---------------------------------------------------------
        bFitToScreen.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021.passes.get(PassCode_2021.SPF_GetGraphFunctionPositions).Do();
            }
        });
    }

    public void showNormalFunctionsFilterMode(){
        fastAccessBar.remove(depthLabel);
        fastAccessBar.remove(sDepth);
        fastAccessBar.remove(ShowIn);
        fastAccessBar.remove(ShowOut);
        fastAccessBar.revalidate();
        fastAccessBar.repaint();
        //-
        FunctionsFilterMode.setText("имя процедуры");
        filterName.setText("");
        filterName.setEnabled(true);
        fastAccessBar.add(ShowStandardFilter);
        fastAccessBar.add(ShowExternalFilter);
        fastAccessBar.add(ShowUnreachableFilter);
        fastAccessBar.revalidate();
        fastAccessBar.repaint();
    }
    public void showCurrentFunctionFilterMode(){
        fastAccessBar.remove(ShowStandardFilter);
        fastAccessBar.remove(ShowExternalFilter);
        fastAccessBar.remove(ShowUnreachableFilter);
        fastAccessBar.revalidate();
        fastAccessBar.repaint();
        //-
        FunctionsFilterMode.setText("текущая процедура");
        if (Current.HasFunction()) ShowCurrentFunction(); else ShowNoCurrentFunction();
        filterName.setEnabled(false);
        fastAccessBar.add(depthLabel);
        fastAccessBar.add(sDepth);
        fastAccessBar.add(ShowIn);
        fastAccessBar.add(ShowOut);
        fastAccessBar.revalidate();
        fastAccessBar.repaint();
    }

    @Override
    public void SaveSplitters() {
        FormWithSplitters.super.SaveSplitters();
        scenariosForm.SaveSplitters();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        filesTreePanel = (filesForm = new TreeForm(FilesTree.class)).getContent();
        projectDescription = new Viewer();
        projectDescriptionScroll = new JScrollPane(projectDescription);
        functionsGraphPanel = (functionsGraphForm = new FunctionsGraphForm()).getContent();
        logText = new Viewer();
        logScroll = new JScrollPane(logText);
        distributionPanel = (distributionForm1 = new TreeForm(RulesTree.class)).getContent();
        dimensionsPanel = (dimensionsForm = new DimensionsTableForm()).getContent();
        includesPanel = (includesForm = new TreeForm(IncludesTree.class)).getContent();
        inlinePanel = (inlineForm = new TreeForm(InlineTree.class)).getContent();
        inlinePanel2 = (inlineForm2 = new TreeForm(InlineTree2.class)).getContent();
        filterName = new StyledTextField();
    }

    @Override
    public void ShowProjectFiles() {
        filesForm.Show();
    }

    @Override
    public void RefreshProjectFiles() {
        filesForm.Refresh();
    }

    @Override
    public void ShowSelectedDirectory() {
        System.out.println(Current.getSelectedDirectory().getAbsolutePath());
    }

    @Override
    public void ShowNoSelectedDirectory() {
        System.out.println("?");
    }

    @Override
    public void ShowSelectedFile() {
        System.out.println(Current.getSelectedFile().file.getAbsolutePath());
    }

    @Override
    public void ShowNoSelectedFile() {
        System.out.println("?");
    }

    @Override
    public void ShowProjectDescription() {
        projectDescription.setText(project.description);
    }

    @Override
    public void ShowProjectLanguage() {
        lProjectLanguage.setText("Язык: " + project.languageName.getDescription());
    }

    @Override
    public void ShowProjectMaxDim() {
        l_maxdim.setText("Наибольшая размерность DVM-шаблона: " + project.maxdim);
    }

    @Override
    public void ShowNoProjectMaxDim() {
        l_maxdim.setText("Наибольшая размерность DVM-шаблона: ?");
    }

    @Override
    public void ShowNoProjectFiles() {
        filesForm.Clear();
    }

    @Override
    public void ShowCurrentFunction() {
        filterName.setText(Current.getFunction().funcName);
    }

    @Override
    public void ShowNoCurrentFunction() {
        filterName.setText("");
    }

    @Override
    public void ShowProjectSapforLog() {
        logText.setText(project.Log);
        logText.setCaretPosition(0);
    }

    @Override
    public void ShowFunctions() {
        functionsGraphForm.Show();
        inlineForm.Show();
        inlineForm2.Show();
    }

    @Override
    public void ShowNoFunctions() {
        functionsGraphForm.Clear();
        inlineForm.Clear();
        inlineForm2.Clear();
    }

    @Override
    public void ShowArrays() {
        project.declaratedArrays.ShowUI();
        project.db.savedArrays.ShowUI();
    }

    @Override
    public void ShowNoArrays() {
        project.declaratedArrays.ClearUI();
        project.db.savedArrays.ClearUI();
    }

    @Override
    public void ShowVariantsFilterButtons() {
        BDistributed.setIcon(Utils.getIcon(project.f_distributed() ? "/icons/Pick.png" : "/icons/NotPick.png"));
        bMultiplied.setIcon(Utils.getIcon(project.f_multiplied() ? "/icons/Pick.png" : "/icons/NotPick.png"));
    }

    @Override
    public void ShowProjectDistribution() {
        distributionForm1.Show();
    }

    @Override
    public void ShowNoProjectDistribution() {
        distributionForm1.Clear();
    }

    @Override
    public void ShowIncludes() {
        includesForm.Show();
    }

    @Override
    public void ShowNoIncludes() {
        includesForm.Clear();
    }

    @Override
    public void ShowVariantsFilter() {
        dimensionsForm.Show();
    }

    @Override
    public void ShowNoVariantsFilter() {
        dimensionsForm.Clear();
    }

    @Override
    public void ShowFilteredVariantsCount() {
        VisibleVariantsCount.setText(String.valueOf(project.getFilteredVariantsCount()));
    }

    @Override
    public void ShowTotalVariantsCount() {
        TotalVariantsCount.setText(String.valueOf(project.getTotalVariantsCount()));
    }

    @Override
    public void ShowCheckedVariantsCount() {
        SelectedVariantsCount.setText(project.CheckedVariantsCounter.toString());
    }

    @Override
    public void ShowVariants() {
        project.parallelVariants.ShowUI();
    }

    @Override
    public void RefreshVariants() {
        project.parallelVariants.RefreshUI();
    }

    @Override
    public void ShowNoVariants() {
        project.parallelVariants.ClearUI();
    }

    @Override
    public void ShowMetrics() {
        l_lines.setText("Общее количество строк кода: " + Current.getProject().LinesCount());
        l_spf_dirs.setText("Всего объявлено SPF директив: " + Current.getProject().SPFCount());
        l_dvm_dirs.setText("Всего объявлено DVM директив: " + Current.getProject().DVMCount());
    }

    @Override
    public void ShowNoMetrics() {
        l_lines.setText("Общее количество строк кода: ?");
        l_spf_dirs.setText("Всего объявлено SPF директив: ?");
        l_dvm_dirs.setText("Всего объявлено DVM директив: ?");
    }

    @Override
    public void ShowRegions() {
        project.parallelRegions.ShowUI();
    }

    @Override
    public void ShowNoRegions() {
        project.parallelRegions.ClearUI();
    }

    @Override
    public void ShowLoopsCount() {
        l_loops.setText("Общее количество циклов: " + Current.getProject().LoopsCount());
    }

    @Override
    public void ShowFunctionsCount() {
        l_functions.setText("Всего объявлено процедур: " + project.FunctionsCount());
    }

    @Override
    public void ShowArraysCount() {
        l_arrays.setText("  Всего объявлено массивов: " + Current.getProject().ArraysCount());
    }

    @Override
    public boolean isFunctionsGraphFocused() {
        return projectTabs.getSelectedIndex()==2;
    }

    @Override
    public void ShowNoCheckedVariantsCount() {
        SelectedVariantsCount.setText("0");
    }

    @Override
    public void ShowNoFilteredVariantsCount() {
        VisibleVariantsCount.setText("0");
    }

    @Override
    public void ShowNoTotalVariantsCount() {
        TotalVariantsCount.setText("0");
    }

    @Override
    public void ShowFile() {
        //пока текущий файл только один.
        //filesTabs.insertTab(null, null, (Current.getFile().form = new FileForm(Current.getFile())).content,
        //        "текущий файл проекта", 0);
        //filesTabs.setTabComponentAt(0,
        //        new TabToolBar(Current.getFile().name, PassCode_2021.CloseCurrentFile));
        projectTabs.setTitleAt(0, "Файл: " + Current.getFile().name);
        filePanel.add((Current.getFile().form = new FileForm(Current.getFile())).content);
    }

    @Override
    public void ShowNoFile() {
        projectTabs.setTitleAt(0, "Файл: -");
        UI.Clear(filePanel);
        //filesTabs.removeAll();
    }

    @Override
    public void GotoFile(String fileName, int line, boolean focus) {
        if (focus) FocusFile();
        if (Current.HasFile() && Current.getFile().name.equals(fileName))
            Current.getFile().form.getEditor().gotoLine(line);
        else if (project.db.files.containsKey(fileName)) {
            DBProjectFile targetFile = project.db.files.get(fileName);
            targetFile.UpdateLastLine(line);
            Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(targetFile);
        }
    }

    @Override
    public void FocusFile() {
        projectTabs.setSelectedIndex(0);
    }

    @Override
    public void FocusFileTabs() {
        projectTabs.requestFocus();
    }

    @Override
    public FunctionsGraphForm getFunctionsGraphWindow() {
        return functionsGraphForm;
    }

    @Override
    public void FocusDependencies() {
        FocusFile();
        projectTreesTabs.setSelectedIndex(3);
    }

    //-
    @Override
    public void FocusArrays() {
        projectTabs.setSelectedIndex(1);
    }

    @Override
    public void FocusFunctions() {
        projectTabs.setSelectedIndex(2);
        projectTreesTabs.setSelectedIndex(1);
    }

    @Override
    public void FocusDistribution() {
        projectTabs.setSelectedIndex(3);
    }

    @Override
    public void FocusAnalysis() {
        projectTabs.setSelectedIndex(4);
    }


    @Override
    public void FocusHierarchy() {
        FocusFile();
        projectTreesTabs.setSelectedIndex(1);
    }

    @Override
    public void FocusPoints() {
        FocusFile();
        projectTreesTabs.setSelectedIndex(2);
    }

    @Override
    public void FocusLog() {
        projectTabs.setSelectedIndex(6);
    }

    @Override
    public void RefreshTabsNames() {
        UI.ShowTabsNames(projectTabs, 1);
        UI.ShowTabsNames(projectTreesTabs);
    }

    @Override
    public Pair<Integer, Integer> getFunctionsGraphPanelSizes() {
        return new Pair<>(functionsGraphPanel.getWidth(), functionsGraphPanel.getHeight());
    }

    @Override
    public void ShowFunctionGraphSettings() {
        ShowStandardFilter.setSelected(SPF_GetGraphFunctionPositions.showStandardFunctions);
        ShowExternalFilter.setSelected(SPF_GetGraphFunctionPositions.showExternalFunctions);
        ShowUnreachableFilter.setSelected(SPF_GetGraphFunctionPositions.showUnreachableFunctions);
        FunctionsFilterMode.setSelected(SPF_GetGraphFunctionPositions.showByCurrentFunction);
        //-
        sIterations.setValue(project.fgIterations);
        sResistance.setValue(project.fgResistance);
        sScreen.setValue(project.fgScreen);
        //-
        ShowIn.setSelected(SPF_GetGraphFunctionPositions.showIn);
        ShowOut.setSelected(SPF_GetGraphFunctionPositions.showOut);
        sDepth.setValue(SPF_GetGraphFunctionPositions.depth);
    }

    @Override
    public void RefreshProjectTreeAndMessages() {
        RefreshProjectFiles();
        DBProjectFile badFile = Current.getProject().getFirstBadFile();
        if (badFile != null) {
            Pass_2021.passes.get(PassCode_2021.OpenCurrentFile).Do(badFile);
            UI.getNewMainWindow().FocusProject();
            UI.getNewMainWindow().getProjectWindow().FocusFile();
        }
        //-
        if (Current.HasFile()) {
            Current.getFile().form.ShowMessages();
            if (badFile != null && badFile.equals(Current.getFile()))
                Current.getFile().form.ShowFirstError();
        }

    }
}