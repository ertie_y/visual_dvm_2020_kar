package Visual_DVM_2021.UI.Main;

import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Interface.CommentInterface;

import javax.swing.*;

public class CommentFields implements CommentInterface {
    private JPanel content;
    public JSplitPane SC26;
    private JToolBar commentTools;
    private JToolBar commentAdditionTools;
    private JPanel editorPanel;
    private JPanel additionPanel;

    @Override
    public void setEditorScroll(JScrollPane scroll_in) {
        editorPanel.add(scroll_in);
    }
    @Override
    public void setAdditionScroll(JScrollPane scroll_in) {
        additionPanel.add(scroll_in);
    }

    @Override
    public JPanel getContent() {
        return content;
    }

    public CommentFields(){
        LoadSplitters();
        UI.add(commentTools, PassCode_2021.SaveBugReportComment);
        UI.add(commentAdditionTools, PassCode_2021.AppendBugReportComment);
    }
}
