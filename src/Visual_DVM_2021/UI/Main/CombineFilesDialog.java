package Visual_DVM_2021.UI.Main;

import Common.Current;
import Common.UI.Windows.Dialog.Text.ComboTextDialog;
import Common.Utils.Utils;

import java.util.Vector;

public class CombineFilesDialog extends ComboTextDialog {
    public CombineFilesDialog(){
        fields.setEditable(true);
    }
    @Override
    public void validateFields() {
        super.validateFields();
        if (Log.isEmpty()){
            String fileName = (String) fields.getSelectedItem();
            // тут 2 случая. если имя файла совпадает с 1 из существующих, то все хорошо.
            //если нет. тогда проверяем отсуствие слешей и полная валидация имени одиночного файла.
            Vector<String> files_order = Current.getProject().files_order;
            if (!files_order.contains(fileName))
                Utils.validateFileShortNewName(fileName, Log);

        }
    }
}
