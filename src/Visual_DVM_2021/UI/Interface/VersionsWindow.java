package Visual_DVM_2021.UI.Interface;
import ProjectData.Project.db_project_info;
public interface VersionsWindow {
    void RefreshVersionsTree();
    void SelectVersion(db_project_info version);
    ComparsionWindow getMasterComparsionWindow();
}
