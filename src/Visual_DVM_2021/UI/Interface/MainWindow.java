package Visual_DVM_2021.UI.Interface;
public interface MainWindow {
    void Show();
    void ShowProject();
    void ShowNoProject();
    void ShowRoot();
    void ShowNoRoot();
    void ShowGlobalSettings();
    void ShowSapforSettings();
    void RefreshFastAccess();
    void ShowNoFastAccess();
    //-
    ProjectWindow getProjectWindow();
    VersionsWindow getVersionsWindow();
    CallbackWindow getCallbackWindow();
    StatisticsWindow getStatisticWindow();
    boolean isProjectFocused();
    //-
    void ShowUpdatesIcon();
    void FocusProject();
    void FocusVersions();
    void FocusCallback();
    void FocusTesting();
    TestingWindow getTestingWindow();
    void SwitchScreen(boolean small);
}
