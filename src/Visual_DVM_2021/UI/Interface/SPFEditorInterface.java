package Visual_DVM_2021.UI.Interface;
import Common.UI.Editor.CaretInfo;
public interface SPFEditorInterface {
    //-----------------------------------
    void ClearHighlights();
    //---------------->>
    void ClearLoopsHighLights();
    void ClearGOCVHighlights();
    //----------------->>
    void HighlightLoops();
    void HighlightGCOV();
    //------------------------------------
    void gotoLine(int num);
    int getCurrentLine();
    int getCaretPosition();
    String getText();
}
