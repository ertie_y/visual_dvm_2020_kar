package Visual_DVM_2021.UI.Interface;

public interface CallbackWindow {
    void ShowAccount();

    void ShowBugReports();

    void ShowNoBugReports();

    void ShowRecipients();

    void ShowNoRecipients();

    void ShowCurrentBugReport();

    void ShowNoCurrentBugReport();

    void ShowSubscription();
    //-
    default void ShowAll() {
        ShowAccount();
        ShowSubscription();
        ShowBugReports();
        ShowRecipients();
    }

    String getBugReportDescriptionText();

    String getBugReportDescriptionAdditionText();

    String getBugReportCommentText();

    String getBugReportCommentAdditionText();

    void ClearBugReportDescriptionAdditionText();
    void ClearBugReportCommentAdditionText();

    String getBugReportExecutorText();
    void FocusRecipients();
    void SwitchScreen(boolean small);
}
