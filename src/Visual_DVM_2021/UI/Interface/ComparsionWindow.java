package Visual_DVM_2021.UI.Interface;
import ProjectData.Project.db_project_info;
public interface ComparsionWindow {
    void Compare() throws Exception;
    void ShowSettings();
    void CheckVersion(db_project_info version_in);
    void MasterCheckVersion(db_project_info version_in);
}
