package Visual_DVM_2021.UI.Interface;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphForm;
import javafx.util.Pair;

public interface ProjectWindow {
    void ShowProjectFiles();
    void RefreshProjectFiles();
    void ShowSelectedDirectory();
    void ShowNoSelectedDirectory();
    void ShowSelectedFile();
    void ShowNoSelectedFile();
    void ShowProjectDescription();
    void ShowProjectLanguage();
    void ShowProjectMaxDim();
    void ShowNoProjectMaxDim();
    void ShowNoProjectFiles();
    void ShowCurrentFunction();
    void ShowNoCurrentFunction();
    void ShowProjectSapforLog();
    void ShowProjectDistribution();
    void ShowNoProjectDistribution();
    void ShowIncludes();
    void ShowNoIncludes();
    void ShowFunctions();
    void ShowNoFunctions();
    void ShowArrays();
    void ShowNoArrays();
    void ShowVariantsFilterButtons();
    void ShowVariantsFilter();
    void ShowNoVariantsFilter();
    void ShowTotalVariantsCount();
    void ShowFilteredVariantsCount();
    void ShowCheckedVariantsCount();
    void ShowNoCheckedVariantsCount();
    void ShowNoFilteredVariantsCount();
    void ShowNoTotalVariantsCount();
    void ShowVariants();
    void RefreshVariants();
    void ShowNoVariants();
    void ShowMetrics();
    void ShowNoMetrics();
    void ShowRegions();
    void ShowNoRegions();
    void ShowLoopsCount();
    void ShowFunctionsCount();
    void ShowArraysCount();
    void RefreshProjectTreeAndMessages();
    boolean isFunctionsGraphFocused();

    //-
    default void ShowAllAnalyses() {
        ShowProjectSapforLog();
        ShowIncludes();
        ShowFunctions();
        ShowArrays();
        ShowVariantsFilterButtons();
        ShowProjectDistribution();
        ShowVariantsFilter();
        ShowTotalVariantsCount();
        ShowFilteredVariantsCount();
        ShowCheckedVariantsCount();
        ShowMetrics();
        ShowLoopsCount();
        ShowFunctionsCount();
        ShowArraysCount();
        ShowRegions();
        ShowProjectMaxDim();
    }
    default void ShowNoAnalyses() {
        ShowNoIncludes();
        ShowNoFunctions();
        ShowNoCurrentFunction();
        ShowNoArrays();
        ShowNoProjectDistribution();
        ShowNoVariants();
        ShowNoVariantsFilter();
        ShowNoTotalVariantsCount();
        ShowNoFilteredVariantsCount();
        ShowNoCheckedVariantsCount();
        ShowNoMetrics();
        ShowNoRegions();
        ShowLoopsCount();
        ShowFunctionsCount();
        ShowArraysCount();
        ShowNoProjectMaxDim();
    }
    //---
    void ShowFile();
    void ShowNoFile();
    //---
    void GotoFile(String fileName, int line, boolean focus);
    void FocusFile();
    void FocusFileTabs();
    FunctionsGraphForm getFunctionsGraphWindow();
    //-
    void FocusDependencies();
    void FocusArrays();
    void FocusFunctions();
    void FocusDistribution();
    void FocusAnalysis();
    void FocusHierarchy();
    void FocusPoints();
    void FocusLog();
    //-
    void RefreshTabsNames();
    Pair<Integer,Integer> getFunctionsGraphPanelSizes();
    void ShowFunctionGraphSettings();
}
