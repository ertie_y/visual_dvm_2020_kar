package Visual_DVM_2021.UI.Interface;

import javax.swing.*;

public interface FilterWindow {
    void ShowMatchesCount(int count);
    JPanel getContent();
    default void ShowNoMatches(){
        ShowMatchesCount(0);
    }
}
