package Visual_DVM_2021.UI.Interface;

import javax.swing.*;

public interface CommentInterface extends FormWithSplitters{
    //---
    void setEditorScroll(JScrollPane scroll_in);
    void setAdditionScroll(JScrollPane scroll_in);
    //---
    JPanel getContent();
}
