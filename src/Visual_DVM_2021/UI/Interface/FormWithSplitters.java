package Visual_DVM_2021.UI.Interface;
import Common.Global;
public interface FormWithSplitters {
    default void LoadSplitters() {
        Global.db.splitters.Load(this);
    }
    default void SaveSplitters() {
        Global.db.splitters.Save(this);
    }
}
