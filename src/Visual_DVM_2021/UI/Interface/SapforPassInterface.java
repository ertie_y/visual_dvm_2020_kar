package Visual_DVM_2021.UI.Interface;
public interface SapforPassInterface extends PassInterface {
    default String getSapforPassName() { return code().toString(); }
}
