package Visual_DVM_2021.UI.Interface;
import Visual_DVM_2021.Passes.PassCode_2021;
public interface PassInterface {
    default PassCode_2021 code() {
        return PassCode_2021.valueOf(getClass().getSimpleName());
    }
}
