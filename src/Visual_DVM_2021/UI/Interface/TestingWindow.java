package Visual_DVM_2021.UI.Interface;
public interface TestingWindow {
    void ShowAll();

    void ShowProjectMaxCompilationTime();
    void ShowProjectMaxRunTime();
    void ShowProjectMachine();
    void ShowProjectUser();
    void ShowProjectMakefile();
    void ShowProjectRunConfiguration();
    //-
    void SelectProjectData(boolean needs_focus);

    default void ShowSession(){
        ShowProjectMaxCompilationTime();
        ShowProjectMaxRunTime();
        ShowProjectMachine();
        ShowProjectUser();
        ShowProjectMakefile();
        ShowProjectRunConfiguration();
    }
    void FocusCredentials();
    void FocusCompilationTasks();
    void FocusRunTasks();
    void ShowProject();
    void ShowNoProject();
    //-
    TasksWindow getTasksWindow();
    //-
    void SwitchScreen(boolean small);
    //-
    void ShowCurrentCompilationTask();
    void ShowNoCurrentCompilationTask();
    //-
    void ShowCurrentRunTask();
    void ShowNoCurrentRunTask();
    //-
    void RefreshTabsNames();
}
