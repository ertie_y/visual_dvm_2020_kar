package Visual_DVM_2021.UI.Interface;

import GlobalData.Tasks.CompilationTask.UI.CompilationTaskComparsionForm;
import GlobalData.Tasks.RunTask.UI.RunTaskComparsionForm;

import javax.swing.*;

public interface TasksWindow extends FormWithSplitters{
    JPanel getContent();
    void init(
            JPanel compilationTasksPanel,
            JPanel runTasksPanel,
            CompilationTaskComparsionForm compilationMaster,
            CompilationTaskComparsionForm compilationSlave,
            RunTaskComparsionForm runMaster,
            RunTaskComparsionForm runSlave
    );
    default void FocusCompilationTasks(){}
    default void FocusRunTasks(){}
}
