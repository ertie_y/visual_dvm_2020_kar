package Visual_DVM_2021.Properties;
public enum PropertyName {
    ServerHome,

    ServerAddress,
    ServerPort,

    SMTPHost,
    SMTPPort,
    MailSocketPort,
    MailAddress,
    MailPassword,
    SocketTimeout,

    GlobalDBName,
    BugReportsDBName,
    ProjectDBName,

    Testing,
    AutoUpdateSearch,
    AutoBugsSynchronize,
    TestsDBName,
    //-
    BackupWorkspace,
    BackupHour,
    BackupMinute,
    AutoTestsSynchronize,

    ComponentsWindowWidth,
    ComponentsWindowHeight
}
