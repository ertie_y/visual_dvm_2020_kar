package Visual_DVM_2021.PassStats;
import Common.Database.DBTable;
import Visual_DVM_2021.Passes.PassCode_2021;
public class PassStatsDBTable extends DBTable<PassCode_2021, PassStats> {
    public PassStatsDBTable() {
        super(PassCode_2021.class, PassStats.class);
    }
    @Override
    public String getDataDescription() {
        return "статистика выполнения прохода";
    }
}
