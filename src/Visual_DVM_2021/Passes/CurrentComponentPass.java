package Visual_DVM_2021.Passes;
import Common.Current;
import Repository.Component.Component;
public abstract class CurrentComponentPass extends Pass_2021<Component> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Component)) {
            target = Current.getComponent();
            return true;
        }
        return false;
    }
}
