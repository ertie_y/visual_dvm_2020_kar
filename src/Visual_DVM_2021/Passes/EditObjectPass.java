package Visual_DVM_2021.Passes;
import Common.Database.DBObject;
public abstract class EditObjectPass<D extends DBObject> extends ObjectPass<D> {
    public EditObjectPass(Class<D> d_in) {
        super(d_in);
    }
    @Override
    protected String getIconPath() {
        return "/icons/Edit.png";
    }
    @Override
    protected boolean canStart(Object ... args) throws Exception {
        target = (D) getTable().getCurrent();
        return getTable().CheckCurrent(Log) && getTable().ShowEditObjectDialog(target);
    }
    @Override
    protected void body() throws Exception {
        getDb().Update(target);
    }
    @Override
    protected void showFinish() throws Exception {
        getTable().RefreshUI();
        for (Class dep : getTable().getFKDependencies().keySet())
            getDb().tables.get(dep).RefreshUI();
    }
}
