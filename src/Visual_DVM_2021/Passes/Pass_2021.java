package Visual_DVM_2021.Passes;

import Common.Current;
import Common.Global;
import Common.Passes.PassState;
import Common.Passes.UI.PassForm;
import Common.UI.Button.StyledButton;
import Common.UI.DebugPrintLevel;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.UI;
import Common.Utils.Stopwatch;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.PassStats.PassStats;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.LinkedHashMap;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.Semaphore;

public class Pass_2021<T> {
    public static final Dimension buttonTabDimension = new Dimension(18, 18);

    //<editor-fold desc="Интерфейс">
    //https://www.delftstack.com/ru/howto/java/java-resize-image/
    protected String getIconPath() {
        return null;
    }

    protected AbstractAction getButtonAction() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UpdateStatsIfNeed();
                Do();
            }
        };
    }

    protected JMenuItem menuItem = null;
    protected JButton button = null;
    protected JButton tab_button = null;

    protected String getButtonText() {
        return getDescription();
    }

    public JMenuItem getMenuItem() {
        if (menuItem == null) {
            menuItem = new StyledMenuItem(getDescription(), getIconPath());
            menuItem.setToolTipText(getDescription());
            menuItem.addActionListener(getButtonAction());
        }
        return menuItem;
    }

    public JButton getButton() {
        if (button == null) {
            button = new StyledButton(getButtonText(), getIconPath());
            button.setToolTipText(getDescription());
            button.addActionListener(getButtonAction());
        }
        return button;
    }

    public JButton getTabButton() {
        if (tab_button == null) {
            tab_button = new StyledButton(getButtonText(), getIconPath(), true);
            tab_button.setToolTipText(getDescription());
            tab_button.addActionListener(getButtonAction());
            //-
            tab_button.setMaximumSize(buttonTabDimension);
            tab_button.setPreferredSize(buttonTabDimension);
            tab_button.setMinimumSize(buttonTabDimension);
            //-
            tab_button.setOpaque(false);
            tab_button.setBorderPainted(false);
        }
        return tab_button;
    }


    public void setButton(JButton button_in) {
        button = button_in;
        //тут иконка и надпись нас не волнуют.
        button.setToolTipText(getDescription());
        button.addActionListener(getButtonAction());
    }

    public void hideButton() {

        if (button != null) button.setVisible(false);
        if (tab_button != null) tab_button.setVisible(false);
    }

    public void showButton() {

        if (button != null) button.setVisible(true);
        if (tab_button != null) tab_button.setVisible(true);
    }

    public static void ShowButton(PassCode_2021... codes_in) {
        for (PassCode_2021 code_in : codes_in)
            passes.get(code_in).showButton();
    }

    public static void HideButton(PassCode_2021... codes_in) {
        for (PassCode_2021 code_in : codes_in)
            passes.get(code_in).hideButton();
    }

    public static JButton getButton(PassCode_2021 code_in) {
        return passes.get(code_in).getButton();
    }

    protected void FocusResult() {
    }

    protected boolean hasStats() {
        return false;
    }

    public PassStats stats = null;
    public static Vector<Pass_2021> FAPasses = new Vector<>();

    //важно. вызывать только если есть интерфейс.
    public static void CheckAllStats() throws Exception {
        for (Pass_2021 pass : FAPasses) {
            if (!Global.db.passStats.Data.containsKey(pass.code()))
                Global.db.Insert(pass.stats = new PassStats(pass));
            else pass.stats = Global.db.passStats.Data.get(pass.code());
        }
        FAPasses.sort(new SortPassesByStats());
    }

    public int callsCount = 0; //число вызовов прохода за текущий сеанс визуализатора.

    public void UpdateStatsIfNeed() {
        if (hasStats()) {
            stats.Inc();
            try {
                Global.db.Update(stats);
                FAPasses.sort(new SortPassesByStats());
                UI.getNewMainWindow().RefreshFastAccess();
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    }

    //</editor-fold>
    //-
    public static LinkedHashMap<PassCode_2021, Pass_2021> passes = new LinkedHashMap<>();

    public static void CreateAll() {
        for (PassCode_2021 code : PassCode_2021.values()) {
            if (code != PassCode_2021.Undefined) {
                try {
                    Class<?> clazz = Class.forName("Visual_DVM_2021.Passes.All." + code.toString());
                    Pass_2021 pass = ((Pass_2021) clazz.newInstance());
                    passes.put(code, pass);
                    if (pass.hasStats())
                        FAPasses.add(pass);
                } catch (Exception ex) {
                    Global.Log.PrintException(ex);
                }
            }
        }
    }

    public PassCode_2021 code() {
        return PassCode_2021.valueOf(getClass().getSimpleName());
    }

    public T target; //главный аргумент.
    public PassState state = PassState.Inactive; //текущее состояние прохода.
    protected TextLog Log; //внутренний журнал прохода.
    private Exception last_error; //последнее пойманное исключение выполнения.

    public boolean isDone() {
        return state == PassState.Done;
    }

    public void setDone() {
        state = PassState.Done;
    }

    public String getDescription() {
        return code().getDescription();
    }

    protected void FocusBeforeStart(){
        //отображение вкладок.
    }
    //-
    //объединение старых методов InitArgs и CanStart. зачастую семантика у них пересекалась.
    protected boolean canStart(Object... args) throws Exception {
        return true;
    }

    //-
    protected void body() throws Exception {
    }

    protected boolean validate() {
        return true;
    }

    //-
    protected void performCanNotStart() throws Exception {
    }

    protected void showCanNotStart() throws Exception {
    }

    protected void performPreparation() throws Exception {
    }

    protected void showPreparation() throws Exception {
    }

    protected void performFinish() throws Exception {
    }

    protected void showFinish() throws Exception {
    }

    protected void performDone() throws Exception {
    }

    protected void showDone() throws Exception {
    }

    protected void performFail() throws Exception {
    }

    protected void showFail() throws Exception {
    }

    //-
    private void PerformCanNotStart() throws Exception {
        performCanNotStart();
        if (Current.hasUI())
            showCanNotStart();
    }

    protected void PerformPreparation() throws Exception {
        performPreparation();
        if (Current.hasUI())
            showPreparation();
    }

    private void PerformFinish() throws Exception {
        performFinish();
        //-
        if (Current.hasUI())
            showFinish();
    }

    private void PerformDone() throws Exception {
        performDone();
        //-
        if (Current.hasUI())
            showDone();
    }

    private void PerformFail() throws Exception {
        performFail();
        //-
        if (Current.hasUI())
            showFail();
    }

    protected void interrupt() throws Exception {
    }

    //------
    protected PassCode_2021 necessary() {
        return null;
    }

    protected boolean resetsNecessary() {
        return false;
    }

    public void Reset() {
        if (state == PassState.Done)
            state = PassState.Inactive;
    }

    //------
    protected void createStack_r(Stack<Pass_2021> ToDo, Vector<String> ToPrint) {
        ToDo.push(this);
        if (needsConfirmations() &&
                Current.hasUI() &&
                (Global.db != null) &&
                Global.db.settings.get(SettingName.ConfirmPassesStart).toBoolean()
        ) {
            ToPrint.add(this.getDescription());
        }
        if (necessary() != null) {
            Pass_2021 next = passes.get(necessary());
            if (resetsNecessary() || !next.isDone())
                next.createStack_r(ToDo, ToPrint);
        }
    }

    protected boolean interrupt;

    public boolean Do(Object... args) {
        Stack<Pass_2021> ToDo = new Stack<>();
        Vector<String> ToPrint = new Vector<>();
        createStack_r(ToDo, ToPrint);
        if (!ToPrint.isEmpty() &&
                !UI.Question("Выполнить проход(ы)\n\n" + String.join("\n", ToPrint))
        ) return false;
        while (ToDo.size() > 1) {
            if (!ToDo.pop().start()) return false;
        }
        if (start(args)) {
            if (Current.hasUI() &&
                    (Global.db != null) &&
                    Global.db.settings.get(SettingName.FocusPassesResult).toBoolean())
                FocusResult();
            //-
            if (!ToPrint.isEmpty() &&
                    Current.hasUI() &&
                    (Global.db != null) &&
                    Global.db.settings.get(SettingName.ShowPassesDone).toBoolean()
            ) {
                UI.Info("Проход(ы)\n\n" + String.join("\n", ToPrint) +
                        "\nуспешно выполнен(ы)!");
            }
            //-
            return true;
        }
        return false;
    }

    //-
    //<editor-fold desc="Анимация">
    protected boolean needsAnimation() {
        return false;
    }

    protected SwingWorker dispatcher = null;
    public PassForm form = null;
    public Semaphore animation_sem;

    public void Interrupt() throws Exception {
        UI.Info("Проход " + Utils.Brackets(getDescription()) + " не разрешено прерывать.");
    }

    public boolean needsConfirmations() {
        return false;
    }

    //</editor-fold>
    //-
    protected void CheckException(Exception ex) {
        Global.Log.PrintException(ex);
    }

    public boolean start(Object... args) {
        //------------------------------->
        interrupt = false;
        callsCount++;
        Stopwatch timer = new Stopwatch();
        state = PassState.Inactive;
        last_error = null;
        target = null;
        Log = new TextLog();
        Global.Log.Print(DebugPrintLevel.Passes, getDescription() + " стартует..");
        timer.Start();
        //------------------------------->
        try {
            if (Current.hasUI()) FocusBeforeStart();
            if (canStart(args)) {
                PerformPreparation();
                //todo тут должна быть вилка на анимацию?
                if (Current.hasUI() && needsAnimation()) {
                    animation_sem = new Semaphore(1);
                    animation_sem.acquire();
                    //---
                    form = null;
                    Current.set(Current.PassForm, null);
                    System.gc();
                    //--
                    Current.set(Current.PassForm, form = new PassForm(this));
                    dispatcher = new SwingWorker() {
                        @Override
                        protected Object doInBackground() {
                            //  UI.Print(DebugPrintLevel.Passes, "фоновое выполнение началось");
                            try {
                                body();
                            } catch (Exception ex) {
                                last_error = ex;
                            }
                            //  UI.Print(DebugPrintLevel.Passes, "Ожидание активации окна анимации ");
                            //ждем пока окно откроется и освободит его.
                            try {
                                animation_sem.acquire();
                            } catch (InterruptedException e) {
                                Global.Log.PrintException(e);
                            }
                            //и уничтожаем его.
                            //  UI.Print(DebugPrintLevel.Passes, "Окно анимации активировано.");
                            form.dispose();
                            form = null;
                            //   UI.Print(DebugPrintLevel.Passes, "Окно анимации закрыто.");
                            animation_sem.release();
                            return null;
                        }

                        @Override
                        protected void done() {
                            //тут ничего не анализируем. события - отдельный поток. тут это не нужно
                        }
                    };
                    dispatcher.execute();
                    form.ShowDialog();
                    //  UI.Print(DebugPrintLevel.Passes, "фоновое выполнение закончено, обработка резульатов");
                    if (last_error != null) {
                        state = PassState.Crushed;
                        CheckException(last_error);
                    }
                    //  UI.Print(DebugPrintLevel.Passes, "OK");
                    dispatcher = null;
                } else {
                    //исключения во время выполнения возможны в штатном режиме.
                    try {
                        body();
                    } catch (Exception ex) {
                        state = PassState.Crushed;//упал при выполнении.
                        CheckException(ex);
                    }
                }
                //---
                PerformFinish();
                //---
                if ((state != PassState.Crushed) && validate()) {
                    state = PassState.Done;
                    PerformDone();
                } else {
                    state = PassState.Failed;
                    PerformFail();
                }
            } else {
                state = PassState.CantStart;
                PerformCanNotStart();
            }
        } catch (Exception ex) {
            state = PassState.ExternalCrushed;
            Global.Log.PrintException(ex);
        }
        //------------------------------->
        timer.Stop();
        Global.Log.Print(DebugPrintLevel.Passes,
                getDescription() +
                        " окончен за " + timer.Print() +
                        " состояние " + Utils.Brackets(state.getDescription())
        );
        if (!Log.isEmpty() && Current.hasUI())
            UI.Error(
                    "проход " + Utils.Brackets(getDescription()) + "\n" + state.getDescription() + "\n\n" +
                            Log.toString());
        //------------------------------->
        return isDone();
    }
}
