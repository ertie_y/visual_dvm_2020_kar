package Visual_DVM_2021.Passes.OLD;
import Common.Current;
import Common.Passes.ToolBarPass;
import Common.UI.UI;
import ProjectData.RSetting.RSetting;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class ApplyRecommendedSettings extends ToolBarPass {
    @Override
    protected boolean CanStart() {
        if (Current.getProject().db.recommendedSettings.Data.isEmpty()) {
            Log.Writeln("Рекомендованные настройки отсутствуют.");
            return false;
        }
        return UI.Question(
                "Установить рекомендованные значения настроек SAPFOR\n" + Current.getProject().getRecommendedSettingsText()
        );
    }
    @Override
    protected String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    protected void Action() throws Exception {
        for (RSetting rSetting : Current.getProject().db.recommendedSettings.Data.values()) {
            Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(rSetting.name, rSetting.value);
        }
    }
    @Override
    protected void PerformDone() throws Exception {
        Current.getProject().db.DeleteAll(RSetting.class);
       // UI.getMainWindow().ShowProjectRecommendations();
    }
}
