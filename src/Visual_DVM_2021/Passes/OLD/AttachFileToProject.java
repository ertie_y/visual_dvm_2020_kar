package Visual_DVM_2021.Passes.OLD;
import Common.Current;
import Common.Passes.ToolBarPass;
import Common.UI.UI;
import Common.Utils.Files.VFileChooser;
import Common.Utils.Utils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
public class AttachFileToProject extends ToolBarPass<File> {
    @Override
    protected String getIconPath() {
        return "/icons/LastOpened.png";
    }
    VFileChooser fileChooser = new VFileChooser(
            "Выбор вложения",
            "png",
            "jpg",
            "bmp",
            "zip",
            "7z",
            "rar"
    );
    @Override
    protected boolean CanStart() {
        return Current.Check(Log, Current.Project) &&
                ((target = fileChooser.ShowDialog()) != null) &&
                Current.getProject().CheckAttachmentFile(target, Log);
    }
    @Override
    protected void Action() throws Exception {
        Utils.CheckDirectory(Current.getProject().getAttachmentsDirectory());
        Path src = target.toPath();
        Path dst = Paths.get(Current.getProject().getAttachmentsDirectory().getAbsolutePath(), target.getName());
        Files.copy(src, dst, StandardCopyOption.REPLACE_EXISTING);
    }
    @Override
    protected void PerformDone() throws Exception {
      //  UI.getMainWindow().ShowAttachments();
    }
}
