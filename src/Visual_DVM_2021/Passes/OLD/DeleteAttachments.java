package Visual_DVM_2021.Passes.OLD;
import Common.Current;
import Common.Passes.ToolBarPass;
import Common.UI.UI;
import Common.Utils.Utils;
public class DeleteAttachments extends ToolBarPass {
    @Override
    protected String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean CanStart() {
        return Current.Check(Log, Current.Project) && UI.Warning("Удалить все вложения текущего проекта");
    }
    @Override
    protected void Action() throws Exception {
        Utils.CleanDirectory(Current.getProject().getAttachmentsDirectory());
    }
    @Override
    protected void PerformFinish() {
       // UI.getMainWindow().ShowAttachments();
    }
}
