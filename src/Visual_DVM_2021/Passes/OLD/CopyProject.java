package Visual_DVM_2021.Passes.OLD;
import Common.Current;
import Common.UI.UI;
import ProjectData.Project.ProjectPass;
public class CopyProject extends ProjectPass {
    @Override
    protected void InitArgs(Object... args) throws Exception {
        target =Current.getProject();
    }
    @Override
    protected boolean isSilent() {
        return false;
    }
    @Override
    protected void FocusResult() {
        //UI.getMainWindow().FocusVersions();
    }
    @Override
    public boolean HasMenuItem() {
        return true;
    }
    @Override
    protected String getIconPath() {
        return "/icons/Transformations/" + code().toString() + ".png";
    }
    @Override
    protected void Action() throws Exception {
       // target.checkLastModification();
    }
}
