package Visual_DVM_2021.Passes.OLD;
import Common.Current;
import Common.Passes.PassCode;
import Common.Passes.ToolBarPass;
import Common.UI.UI;

//устарело скорее всего. механизм закрытых преобразований будет частью тестирования.
//-
public class ApplyRecommendedTransformations extends ToolBarPass {
    @Override
    protected String getIconPath() {
        return "/icons/Start.png";
    }
    @Override
    protected boolean CanStart() {
        if (Current.getProject().db.recommendedTransformations.Data.isEmpty()) {
            Log.Writeln("Рекомендованные преобразования отсутствуют.");
            return false;
        }
        return UI.Question(
                "Выполнить преобразования кода:\n" + Current.getProject().getRecommendedTransformationsText()
                        + "\n(переход к промежуточным версиям будет осуществлён автоматически)");
    }
    @Override
    protected void Action() throws Exception {
        /*
        ActiveScenario = true;
        for (PassCode code_ : Current.getProject().db.recommendedTransformations.Data.keySet()) {
            if (Passes.get(code_).Do())
                Passes.get(PassCode.OpenVersion).Do();//версия уже выбрана.
            else break;
        }
         */
    }
    @Override
    protected void PerformFinish() throws Exception {
        ActiveScenario = false;
    }
}
