package Visual_DVM_2021.Passes.UI;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
public class PassesSubMenu extends JMenu {
    public PassesSubMenu(String title, String icon, PassCode_2021... passes) {
        super(title);
        setIcon(Utils.getIcon(icon));
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        for (PassCode_2021 code : passes) {
            add(Pass_2021.passes.get(code).getMenuItem());
        }
    }
}
