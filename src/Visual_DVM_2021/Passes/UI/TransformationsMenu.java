package Visual_DVM_2021.Passes.UI;
import Common.UI.Menus.StyledPopupMenu;
import Repository.Component.Sapfor.Sapfor;
public class TransformationsMenu extends StyledPopupMenu {
    public TransformationsMenu() {
        add(new PassesSubMenu("Циклы", "/icons/Menu/Loops.png",
                Sapfor.getLoopsTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Приватные переменные", "/icons/Menu/Privates.png",
               Sapfor.getPrivatesTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Процедуры", "/icons/Menu/Functions.png",
              Sapfor.getProceduresTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("DVM директивы", "/icons/Menu/Dvm.png",
               Sapfor.getDVMTransformationsCodes()));
        addSeparator();
        add(new PassesSubMenu("Интервалы", "/icons/Menu/Intervals.png",
               Sapfor.getIntervalsTransformationsCodes()
        ));
        addSeparator();
        add(new PassesSubMenu("Области распараллеливания", "/icons/Menu/Regions.png",
                Sapfor.getRegionsTransformationsCodes()
        ));
        addSeparator();
        add(new PassesSubMenu("Предобработка проекта", "/icons/Menu/Preprocessing.png",
                       Sapfor.getPreparationTransformationsCodes()
                )
        );
    }
}