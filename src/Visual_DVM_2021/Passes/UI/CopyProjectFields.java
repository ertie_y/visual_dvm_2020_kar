package Visual_DVM_2021.Passes.UI;

import Common.Current;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import Common.Utils.Files.VDirectoryChooser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class CopyProjectFields implements DialogFields {
    private JPanel content;
    public JTextField tfParent;
    public JTextField tfName;
    private JButton bBrowse;
    public JCheckBox MigrateData;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("Выбор целевой папки для копии проекта");

    public CopyProjectFields() {
        tfName.setText(Current.getProject().name+"_copy");
        bBrowse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = directoryChooser.ShowDialog();
                if (file != null)
                    tfParent.setText(file.getAbsolutePath());
            }
        });
    }

    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfParent = new StyledTextField();
        tfName = new StyledTextField();
    }
}
