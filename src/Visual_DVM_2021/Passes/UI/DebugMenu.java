package Visual_DVM_2021.Passes.UI;
import Common.UI.Menus.StyledPopupMenu;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class DebugMenu extends StyledPopupMenu {
    public DebugMenu() {
        add(Pass_2021.passes.get(PassCode_2021.Compile).getMenuItem());
        add(Pass_2021.passes.get(PassCode_2021.Run).getMenuItem());
    }
}
