package Visual_DVM_2021.Passes.UI;
import Common.UI.Menus.StyledPopupMenu;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class AnalysesMenu extends StyledPopupMenu {
    public AnalysesMenu() {
        for (PassCode_2021 code : Sapfor.getAnalysesCodes())
            add(Pass_2021.passes.get(code).getMenuItem());
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.SPF_GetGCovInfo).getMenuItem());
    }
}
