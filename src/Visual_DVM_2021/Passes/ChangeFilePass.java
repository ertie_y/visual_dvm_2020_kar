package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import ProjectData.Project.db_project_info;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
public abstract class ChangeFilePass<T> extends Pass_2021<T> {
    protected File target_dir;
    protected File dst;
    protected String fileName;
    protected FileNameForm ff;
    protected DefaultMutableTreeNode parent_node;
    protected DefaultMutableTreeNode dst_node;
    protected db_project_info project;
    protected void resetArgs() {
        target_dir = null;
        dst = null;
        fileName = null;
        ff = null;
        parent_node = null;
        dst_node = null;
        project = Current.getProject();
    }
}
