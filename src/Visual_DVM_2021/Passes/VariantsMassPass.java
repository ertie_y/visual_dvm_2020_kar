package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.Variants.ParallelVariant;
import Repository.Component.Sapfor.Sapfor;

public abstract class VariantsMassPass extends CurrentProjectPass {
    public abstract PassCode_2021 getSubPassCode();
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public void Interrupt() throws Exception {
        target.CreateInterruptFile();
    }
    @Override
    protected void performPreparation() throws Exception {
        target.CleanInterruptFile();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            if (target.CheckedVariantsCounter.getValue() == 0) {
                Log.Writeln_("Не выбрано ни одного параллельного варианта");
                return false;
            } else if (!passes.get(PassCode_2021.SPF_GetArrayDistribution).isDone()) {
                Log.Writeln_("Варианты неактуальны. Сначала перестройте распределение данных.");
                return false;
            }
            return true;
        }
        return false;
    }
    protected void body() throws Exception {
        form.ShowProgress(target.CheckedVariantsCounter.getValue(), 0, true);
        int i = 0;
        for (ParallelVariant p : target.parallelVariants.Data.values())
            if (p.isSelected()) {
                if (!passes.get(getSubPassCode()).Do(p)) break;
                form.ShowProgress(target.CheckedVariantsCounter.getValue(), ++i, true);
            }
    }
    @Override
    protected void performFinish() throws Exception {
        //распаковку соо делаем только 1 раз. после всей массы вариантов.
        Sapfor sapfor = Current.getSapfor();
        target.unpackMessagesAndLog(sapfor.getOutputMessage(), sapfor.getOutput());
    }

    @Override
    protected void showFinish() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshVariants();
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
        UI.getNewMainWindow().getProjectWindow().ShowProjectSapforLog();
        //-
        DBProjectFile badFile = target.getFirstBadFile();
        if (badFile != null) {
            passes.get(PassCode_2021.OpenCurrentFile).Do(badFile);
            UI.getNewMainWindow().FocusProject();
            UI.getNewMainWindow().getProjectWindow().FocusFile();
        }
        //-
        if (Current.HasFile())
            Current.getFile().form.ShowMessages();
    }
}
