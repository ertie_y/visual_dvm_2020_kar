package Visual_DVM_2021.Passes;

import Common.Current;
import Common.Global;
import Common.Passes.PassState;
import Common.UI.DebugPrintLevel;
import Common.UI.UI;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Vector;

public class SapforAnalysis extends SapforPass {
    boolean asDonePhase = false;

    protected Vector<PassCode_2021> getForbiddenPhases() {
        return new Vector<>();
    }

    @Override
    protected boolean needsAnimation() {
        return super.needsAnimation() && !asDonePhase;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        asDonePhase = false;
        if (super.canStart(args)) {
            asDonePhase = ((args.length >= 1) && ((boolean) args[0]));
            return true;
        }
        return false;
    }

    public String phase() {
        return "";
    }

    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }

    @Override
    protected String getIconPath() {
        return "/icons/NotPick.png";
    }

    protected String getDoneIconPath() {
        return "/icons/Pick.png";
    }

    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() > 0;
    }

    @Override
    protected void PerformPreparation() throws Exception {
        //-
        if (Current.hasUI())
            showPreparation(); // отображение пустых данных
        //-
        performPreparation(); //в данном случае удаление данных
        //-
        //особенность отрисовки местных контролов.
        //если сначала удалить данные, то контролы могут обновиться и сослаться на несуществующие
        //поэтому сначала блокируем контролы, потом удаляем.
    }

    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                target.getProjFile().getAbsolutePath());
    }

    protected void unpack(String packed) throws Exception {
    }

    //---------------------------------------------
    //всегда обновлять фазу, даже если она уже выполнена.
    protected boolean alwaysCheck() {
        return false;
    }

    protected boolean isAtomic() {
        return true;
    }

    //атомарность означает что анализ не ищет фаз.
    @Override
    protected void performDone() throws Exception {
        if (!sapfor.getResult().isEmpty())
            unpack(sapfor.getResult());
        if (!isAtomic() && !asDonePhase) {
            Global.Log.Print(DebugPrintLevel.Passes, "Поиск выполненных фаз..");
            sapfor.RunAnalysis(
                    "SPF_GetPassesStateStr",
                    -Global.messagesServer.getPort(),
                    "",
                    "");
            if (sapfor.getErrorCode() >= 0) {
                String[] arrayPasses = sapfor.getResult().split("\\|");
                for (String p : arrayPasses) {
                    if (!p.isEmpty() && !phase().equals(p)) {
                        SapforAnalysis analysis =
                                sapfor.getAnalysisByPhase(p);
                        if ((analysis != null) && (analysis.code() != code()) && (analysis.alwaysCheck() || !analysis.isDone()))
                        {
                            if (getForbiddenPhases().contains(analysis.code())) {
                                analysis.setDone();
                                analysis.MarkAsDone();
                            }
                             else
                                analysis.start(true);
                        }
                    }
                }
            }
            Global.Log.Print(DebugPrintLevel.Passes, "Фазы получены.");
        }
    }

    //---------------------------------------------
    @Override
    public void Reset() {
        if (state == PassState.Done) {
            state = PassState.Inactive;
            if (menuItem != null)
                menuItem.setIcon(Utils.getIcon(getIconPath()));
            if (button != null)
                button.setIcon(Utils.getIcon(getIconPath()));
        }
    }

    public void MarkAsDone() {
        if (menuItem != null)
            menuItem.setIcon(Utils.getIcon(getDoneIconPath()));
        if (button != null)
            button.setIcon(Utils.getIcon(getDoneIconPath()));
    }

    @Override
    protected void showDone() throws Exception {
        MarkAsDone();
    }

    @Override
    protected AbstractAction getButtonAction() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (isDone()) FocusResult();
                else {
                    UpdateStatsIfNeed();
                    Do();
                }
            }
        };
    }

    @Override
    protected boolean hasStats() {
        return true;
    }

    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusProject();
    }

    @Override
    protected void unpackMessages() throws Exception {
        if (!asDonePhase) super.unpackMessages();

    }

    @Override
    protected void showFinish() throws Exception {
        if (!asDonePhase)
            super.showFinish();
    }
}
