package Visual_DVM_2021.Passes;
import Common.Database.DBObject;
public abstract class AddObjectPass<D extends DBObject> extends ObjectPass<D> {
    public AddObjectPass(Class<D> d_in) {
        super(d_in);
    }
    public Class<? extends DBObject> getOwner() {
        return null;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = d.newInstance();
        return
                ((getOwner() == null) || (getDb().tables.get(getOwner()).CheckCurrent(Log))) &&
                        fillObjectFields();
    }
    protected boolean fillObjectFields() throws Exception {
        return getTable().ShowAddObjectDialog(target);
    }
    @Override
    protected String getIconPath() {
        return "/icons/RedAdd.png";
    }
    @Override
    protected void body() throws Exception {
        getDb().Insert(target);
    }
    @Override
    protected void showDone() throws Exception {
        getTable().ShowUI(target.getPK());
    }
}
