package Visual_DVM_2021.Passes;
public enum PassCode_2021 {
    Undefined,
    DropFastAccess,
    DropLastProjects,
    UpdateSetting,
    //-
    OpenCurrentProject,
    CloseCurrentProject,
    DropAnalyses,
    CleanAnalyses,
    DeleteLonelyM,
    DeleteSubversions,
    DeleteDebugResults,
    ResetCurrentProject,
    CreateEmptyProject,
    DeleteVersion,
    //-
    OpenCurrentFile,
    CloseCurrentFile,
    Save,
    //-
    AddFile,
    RenameFile,
    IncludeFile,
    ExcludeFile,
    DeleteFile,
    ImportFiles,
    //-
    CreateEmptyDirectory,
    RenameDirectory,
    DeleteDirectory,
    //-
    SPF_ParseFilesWithOrder,
    SPF_GetIncludeDependencies,
    SPF_GetFileLineInfo,
    SPF_GetGraphLoops,
    SPF_GetGraphFunctions,
    SPF_GetGraphFunctionPositions,
    SPF_GetAllDeclaratedArrays,
    SPF_GetArrayDistributionOnlyAnalysis,
    SPF_GetArrayDistribution,
    //-
    SPF_CorrectCodeStylePass,
    SPF_CreateIntervalsTree,
    SPF_RemoveDvmDirectives,
    SPF_RemoveDvmDirectivesToComments,
    SPF_RemoveUnusedFunctions,
    SPF_ResolveParallelRegionConflicts,
    SPF_LoopEndDoConverterPass,
    SPF_LoopFission,
    SPF_LoopUnion,
    SPF_PrivateShrinking,
    SPF_PrivateExpansion,
    SPF_RemoveDvmIntervals,
    SPF_DuplicateFunctionChains,
    SPF_ConvertStructures,
    SPF_CreateCheckpoints,
    SPF_InitDeclsWithZero,
    //-
    SPF_LoopUnionCurrent,
    SPF_ChangeSpfIntervals,
    SPF_StatisticAnalyzer,
    //-
    CombineFiles,
    EraseBadSymbols,
    CopyProject,
    //-
    GenerateParallelVariants,
    PredictParallelVariants,
    SPF_PredictParallelVariant,
    CreateParallelVariants,
    SPF_CreateParallelVariant,
    SPF_InlineProceduresH,
    SPF_InlineProcedures,
    SPF_InsertIncludesPass,
    SPF_InlineProcedure,
    SPF_ModifyArrayDistribution,
    SPF_GetGCovInfo,
    SPF_GetArrayDistributionOnlyRegions,
    RestoreSavedArrays,
    ChangeArrayDistributionState,
    DropSavedArrays,
    CompareFiles,
    //-
    SynchronizeBugReports,
    ZipFolderPass,
    UnzipFolderPass,
    AddBugReport,
    DeleteBugReport,
    DeleteBugReportFromServer,
    PublishBugReport,
    SendBugReport,
    ExtractRecipients,
    Email,
    RegisterAccount,
    Subscribe,
    //-
    GetComponentsActualVersions,
    PublishComponent,
    CreateComponentBackUp,
    InstallComponentFromFolder,
    ResurrectComponent,
    DownloadComponent,
    ShowComponentChangesLog,
    UpdateComponent,
    BuildComponent,
    DownloadRepository,
    SaveGraph,
    ApplyBugReportSettings,
    DownloadBugReport,
    UpdateBugReportField,
    AppendBugReportField,
    //-
    AddRecipient,
    EditRecipient,
    DeleteRecipient,
    //-
    AddMachine,
    EditMachine,
    DeleteMachine,
    //-
    AddUser,
    EditUser,
    DeleteUser,
    //-
    AddCompiler,
    EditCompiler,
    DeleteCompiler,
    ShowCompilerHelp,
    //-
    AddMakefile,
    EditMakefile,
    DeleteMakefile,
    //-
    EditModule,
    //-
    AddRunConfiguration,
    EditRunConfiguration,
    DeleteRunConfiguration,
    //-
    AddEnvironmentValue,
    EditEnvironmentValue,
    DeleteEnvironmentValue,
    //-
    ShowMakefilePreview,
    //-
    InitialiseUser,
    DownloadProject,
    //-
    SelectRemoteFile,
    Compile,
    Run,
    RemoteCompilation,
    ServerRun,
    MVSRun,
    //-
    LinuxLocalCompilation,
    LinuxLocalRun,
    DeleteRunStsRecord,
    //-
    ApplyMachine,
    ApplyUser,
    ApplyMakefile,
    ApplyRunConfiguration,
    //-
    EditProjectCompilationMaxtime,
    EditProjectRunMaxtime,
    //-
    AddScenario,
    EditScenario,
    DeleteScenario,
    //-
    AddScenarioCommand,
    EditScenarioCommand,
    DeleteScenarioCommand,
    //-
    RunScenario,
    //-
    OpenBugReportTestProject,
    OpenBugReport,
    CloseBugReport,
    UpdateBugReportProgress,
    SaveBugReportDescription,
    SaveBugReportComment,
    AppendBugReportDescription,
    AppendBugReportComment,
    SaveBugReportRecipients,
    SaveBugReportExecutor,
    //-
    AddTest,
    EditTest,
    DeleteTest,
    //-
    AddGroup,
    EditGroup,
    PublishGroup,
    EditGroupOnServer,
    DeleteGroup,
    DeleteGroupFromServer,
    //-
    DownloadAllBugReportsArchives,
    ShowInstruction,
    //-
    ArchivesBackupPass,
    PublishTest,
    DownloadTest,
    EditTestOnServer,
    DeleteTestFromServer,
    SynchronizeTests,
    //-
    MakeScreenShot,
    RemoteInitialiseUser,
    LocalInitaliseUser,
    RemoteGetCompilerHelp,
    LocalGetCompilerHelp,
    //-
    WindowsLocalCompilation,
    WindowsLocalRun,
    ClearRunSts,
    PickCompilerOptions,
    PickCompilerEnvironments,
    //-
    DeleteSelectedCompilationTasks,
    DeleteSelectedRunTasks,
    AddDVMParameter,
    EditDVMParameter,
    DeleteDVMParameter,
    //-
    CompileCurrentTest, //на самом деле отладочный проход, ибо запуск будет массовым или по галочкам.
    ActualizeTestsTasks,
    ShowTestMakefilePreview,
    SPF_GetArrayLinks,
    Precompilation,
    GCOV,
    //-
    TestPass;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "?";
            case SPF_GetArrayDistributionOnlyRegions:
                return "Анализ областей распараллеливания";
            case SPF_GetGCovInfo:
                return "Показать покрытие GCOV";
            case GCOV:
                return "Обновить покрытие GCOV";
            case Precompilation:
                return "Предварительная компиляция на локальной машине";
            case SPF_GetArrayLinks:
                return "Получить связи между массивами";
            case ShowTestMakefilePreview:
                return "Показать текст мейкфайла для текущего теста";
            case ActualizeTestsTasks:
                return "Актуализировать состояния задач.";
            case CompileCurrentTest:
                return "Компилировать текущий тест";
            case DeleteGroup:
                return "Удаление группы";
          //  case DeleteGroupFromServer:
          //      return "Удаление группы с сервера";
            case EditGroupOnServer:
                return "Обновить группу на сервере";
            case SaveBugReportExecutor:
                return "Назначить исполнителя отчёта об ошибке";
            case SaveBugReportRecipients:
                return "Сохранить адресатов отчёта об ошибке";
            case AppendBugReportComment:
                return "Дополнить комментария отчёта об ошибке";
            case AppendBugReportDescription:
                return "Дополнить описание отчёта об ошибке";
            case SaveBugReportDescription:
                return "Сохранить описание отчёта об ошибке";
            case SaveBugReportComment:
                return "Сохранить комментарий отчёта об ошибке";
            case UpdateBugReportProgress:
                return "Обновить степень завершенности отчёта об ошибке";
            case OpenBugReport:
                return "Открытие отчёта об ошибке";
            case CloseBugReport:
                return "Закрытие отчёта об ошибке";
            case CopyProject:
                return "Копирование проекта";
            case AddDVMParameter:
                return "Добавление параметра DVM системы";
            case EditDVMParameter:
                return "Редактирование параметра DVM системы";
            case DeleteDVMParameter:
                return "Удаление параметра DVM системы";
            case AddGroup:
                return "Добавление группы";
            case EditGroup:
                return "Редактирование группы";
            case PublishGroup:
                return "Публикация группы";
            case DeleteSelectedCompilationTasks:
                return "Удалить выделенные задачи на компиляцию";
            case DeleteSelectedRunTasks:
                return "Удалить выделенные задачи на запуск";
            case PickCompilerEnvironments:
                return "Выбор переменных окружения DVM компилятора";
            case PickCompilerOptions:
                return "Выбор опций DVM компилятора";
            case ClearRunSts:
                return "Полная очистка запусков";
            case WindowsLocalCompilation:
                return "Компиляция на локальной машине (Windows)";
            case WindowsLocalRun:
                return "Запуск на локальной машине (Windows)";
            case LocalGetCompilerHelp:
                return "Получение справки компилятора на локальной машине";
            case RemoteGetCompilerHelp:
                return "Получение справки компилятора на удаленной машине";
            case LocalInitaliseUser:
                return "Инициализация пользователя на локальной машине";
            case RemoteInitialiseUser:
                return "Инициализация пользователя на удаленной машине";
            case MakeScreenShot:
                return "Сделать скриншот";
            case SynchronizeTests:
                return "Синхронизация тестов";
            case DownloadTest:
                return "Загрузить тест";
            case DeleteTestFromServer:
                return "Удалить тест с сервера";
            case PublishTest:
                return "Опубликовать тест";
            case EditTestOnServer:
                return "Редактировать тест на сервере";
            case ArchivesBackupPass:
                return "Бэкап архивов журнала ошибок";
            case ShowInstruction:
                return "Показать инструкцию";
            case DownloadAllBugReportsArchives:
                return "Скачать все архивы";
            case CleanAnalyses:
                return "Очистка анализов";
            case AddTest:
                return "Добавление теста";
            case EditTest:
                return "Редактирование теста";
            case DeleteTest:
                return "Удаление теста";
            case DropFastAccess:
                return "Сброс панели быстрого доступа";
            case DropLastProjects:
                return "Сброс списка последних открытых проектов";
            case UpdateSetting:
                return "Обновить настройку";
            case OpenCurrentProject:
                return "Открытие проекта";
            case CloseCurrentProject:
                return "Закрытие проекта";
            case DropAnalyses:
                return "Сброс актуальности анализов";
            case DeleteLonelyM:
                return "Удаление бесхозных копий";
            case DeleteSubversions:
                return "Удаление подверсий";
            case DeleteDebugResults:
                return "Очистка результатов компиляции и запуска";
            case ResetCurrentProject:
                return "Полная очистка проекта";
            case CreateEmptyProject:
                return "Создание проекта";
            case DeleteVersion:
                return "Удаление версии";
            case OpenCurrentFile:
                return "Открытие файла";
            case CloseCurrentFile:
                return "Закрытие файла";
            case Save:
                return "Сохранение файла";
            case AddFile:
                return "Добавление файла";
            case RenameFile:
                return "Переименование выделенного файла";
            case IncludeFile:
                return "Включение в рассмотрение выделенного файла";
            case ExcludeFile:
                return "Исключение из рассмотрения выделенного файла";
            case DeleteFile:
                return "Удаление выделенного файла";
            case ImportFiles:
                return "Добавление внешних файлов";
            case CreateEmptyDirectory:
                return "Создание пустой папки";
            case RenameDirectory:
                return "Переименование выделенной папки";
            case DeleteDirectory:
                return "Удаление выделенной папки";
            case SPF_ParseFilesWithOrder:
                return "Синтаксический анализ";
            case SPF_GetFileLineInfo:
                return "Метрика кода";
            case SPF_GetIncludeDependencies:
                return "Поиск зависимостей по включению";
            case SPF_GetGraphLoops:
                return "Граф циклов";
            case SPF_GetGraphFunctions:
                return "Граф процедур";
            case SPF_GetGraphFunctionPositions:
                return "Координаты графа процедур";
            case SPF_GetAllDeclaratedArrays:
                return "Объявления массивов";
            case SPF_GetArrayDistributionOnlyAnalysis:
                return "Анализ кода";
            case SPF_GetArrayDistribution:
                return "Распределение данных";
            case SPF_CorrectCodeStylePass:
                return "Коррекция стиля кода";
            case SPF_CreateIntervalsTree:
                return "Построение полной системы интервалов";
            case SPF_RemoveDvmDirectives:
                return "Очистка DVM директив";
            case SPF_RemoveDvmDirectivesToComments:
                return "Комментирование DVM директив";
            case SPF_ResolveParallelRegionConflicts:
                return "Разрешение конфликтов областей";
            case SPF_LoopEndDoConverterPass:
                return "Преобразование циклов в DO-ENDDO";
            case SPF_LoopFission:
                return "Разделение циклов";
            case SPF_LoopUnion:
                return "Слияние циклов";
            case SPF_PrivateShrinking:
                return "Сужение приватных переменных";
            case SPF_PrivateExpansion:
                return "Расширение приватных переменных";
            case SPF_RemoveDvmIntervals:
                return "Очистка DVM интервалов";
            case SPF_DuplicateFunctionChains:
                return "Дублирование цепочек вызовов";
            case SPF_InitDeclsWithZero:
                return "Дополнение инициализации переменных";
            case SPF_RemoveUnusedFunctions:
                return "Удаление неиспользуемых процедур";
            case SPF_CreateCheckpoints:
                return "Построение контрольных точек";
            case SPF_ConvertStructures:
                return "Конвертация производных типов данных";
            case CombineFiles:
                return "Слияние файлов";
            case EraseBadSymbols:
                return "Очистка некорректных символов";
            case GenerateParallelVariants:
                return "Генерация параллельных вариантов";
            case PredictParallelVariants:
                return "Оценка выбранных параллельных вариантов";
            case SPF_PredictParallelVariant:
                return "Оценка параллельного варианта";
            case CreateParallelVariants:
                return "Построение выбранных параллельных вариантов";
            case SPF_CreateParallelVariant:
                return "Построение параллельного варианта";
            case SPF_InlineProceduresH:
                return "Иерархическая подстановка процедур";
            case SPF_InlineProcedures:
                return "Точечная подстановка процедур";
            case SPF_InsertIncludesPass:
                return "Подстановка заголовочных файлов";
            case SPF_LoopUnionCurrent:
                return "Объединение текущего цикла";
            case SPF_ChangeSpfIntervals:
                return "Изменение области распараллеливания";
            case SPF_InlineProcedure:
                return "Выборочная подстановка процедуры";
            case SPF_ModifyArrayDistribution:
                return "Изменение распределения данных";
            case SPF_StatisticAnalyzer:
                return "Распаковка DVM статистики";
            case RestoreSavedArrays:
                return "Восстановление сохраненного распределения массивов";
            case ChangeArrayDistributionState:
                return "Изменение распределения массива";
            case DropSavedArrays:
                return "Сброс сохраненных массивов";
            case CompareFiles:
                return "Сравнение файлов";
            case SynchronizeBugReports:
                return "Синхронизация журнала ошибок";
            case ZipFolderPass:
                return "Архивация папки";
            case UnzipFolderPass:
                return "Распаковка папки";
            case AddBugReport:
                return "Создание отчёта об ошибке";
            case DeleteBugReport:
                return "Удаление отчёта об ошибке";
            case DeleteBugReportFromServer:
                return "Удаление отчёта об ошибке с сервера";
            case PublishBugReport:
                return "Публикация отчёта об ошибке";
            case SendBugReport:
                return "Отправка отчёта об ошибке на сервер";
            case ExtractRecipients:
                return "Извлечение адресатов";
            case Email:
                return "Отправка по email";
            case Subscribe:
                return "Подписка";
            case RegisterAccount:
                return "Регистрация";
            case GetComponentsActualVersions:
                return "Получение актуальных версий компонент";
            case PublishComponent:
                return "Публикация компонента";
            case CreateComponentBackUp:
                return "Создание копии предыдущей версии компонента";
            case InstallComponentFromFolder:
                return "Установка компонента из целевой папки";
            case ResurrectComponent:
                return "Восстановление предыдущей версии компонента";
            case DownloadComponent:
                return "Скачивание компонента с сервера";
            case ShowComponentChangesLog:
                return "Отображение журнала изменений компонента";
            case UpdateComponent:
                return "Обновление компонента";
            case BuildComponent:
                return "Сборка компонента";
            case DownloadRepository:
                return "Загрузка репозитория";
            case SaveGraph:
                return "Сделать скриншот графа функций";
            case ApplyBugReportSettings:
                return "Применение настроек отчёта об ошибке";
            case DownloadBugReport:
                return "Загрузка тестового проекта с сервера";
            case UpdateBugReportField:
                return "Обновление поля отчёта об ошибке";
            case AppendBugReportField:
                return "Дополнение поля отчёта об ошибке";
            //-----
            case AddRecipient:
                return "Добавление адресата";
            case EditRecipient:
                return "Редактирование адресата";
            case DeleteRecipient:
                return "Удаление адресата";
            //-----
            case AddMachine:
                return "Добавление машины";
            case EditMachine:
                return "Редактирование машины";
            case DeleteMachine:
                return "Удаление машины";
            //-----
            case AddUser:
                return "Добавление пользователя";
            case EditUser:
                return "Редактирование пользователя";
            case DeleteUser:
                return "Удаление пользователя";
            //-----
            case AddCompiler:
                return "Добавление компилятора";
            case EditCompiler:
                return "Редактирование компилятора";
            case DeleteCompiler:
                return "Удаление компилятора";
            //-
            case AddMakefile:
                return "Добавление мейкфайла";
            case EditMakefile:
                return "Редактирование мейкфайла";
            case DeleteMakefile:
                return "Удаление мейкфайла";
            case ShowCompilerHelp:
                return "Показать справочную информацию компилятора";
            //-
            case EditModule:
                return "Редактирование модуля";
            //-
            case AddRunConfiguration:
                return "Добавление конфигурации запуска";
            case EditRunConfiguration:
                return "Редактирование конфигурации запуска";
            case DeleteRunConfiguration:
                return "Удаление конфигурации запуска";
            //-
            case DeleteEnvironmentValue:
                return "Удалить переменную окружения";
            case EditEnvironmentValue:
                return "Изменить переменную окружения";
            case AddEnvironmentValue:
                return "Добавить переменную окружения";
            //-
            case ShowMakefilePreview:
                return "Предпросмотр мейкфайла";
            case DownloadProject:
                return "Скачивание проекта";
            case InitialiseUser:
                return "Инициализация пользователя";
            case TestPass:
                return "Отладка";
            case Run:
                return "Запуск";
            case Compile:
                return "Компиляция";
            case SelectRemoteFile:
                return "Выбор удалённого файла/папки";
            case MVSRun:
                return "Запуск на MVS кластере";
            case ServerRun:
                return "Запуск на удалённом сервере";
            case RemoteCompilation:
                return "Компиляция на удалённой машине";
            //----------------------------------------------------
            case LinuxLocalCompilation:
                return "Компиляция на локальной машине (Linux)";
            case LinuxLocalRun:
                return "Запуск на локальной машине (Linux)";
            case DeleteRunStsRecord:
                return "Удалить запись";
            //-
            case ApplyMachine:
                return "Назначить машину";
            case ApplyUser:
                return "Назначить пользователя";
            case ApplyMakefile:
                return "Назначить мейкфайл";
            case ApplyRunConfiguration:
                return "Назначить конфигурацию запуска для текущего проекта";
            case EditProjectCompilationMaxtime:
                return "Изменить максимальное допустимое время компиляции";
            case EditProjectRunMaxtime:
                return "Изменить максимальное допустимое время запуска";
            //-
            case AddScenario:
                return "Добавить сценарий";
            case EditScenario:
                return "Редактировать сценарий";
            case DeleteScenario:
                return "Удалить сценарий";
            //-
            case AddScenarioCommand:
                return "Добавить проход";
            case EditScenarioCommand:
                return "Редактировать проход";
            case DeleteScenarioCommand:
                return "Удалить проход";
            case RunScenario:
                return "Выполнить сценарий";
            case OpenBugReportTestProject:
                return "Открыть тестовый проект";
            //-
            default:
                return toString();
        }
    }
}
