package Visual_DVM_2021.Passes;
import Common.Current;
import Common.UI.UI;

public class SapforFilesModification extends SapforModification {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().UpdateProjectFiles(true);
    }
    //распаковать журнал
    @Override
    protected void performFinish() throws Exception {
        target.updateLog(sapfor.getOutput());
    }
    //показать журнал.
    @Override
    protected void showFinish() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowProjectSapforLog();
    }
}
