package Visual_DVM_2021.Passes;
import Common.Database.DBObject;
public abstract class DeleteObjectPass<D extends DBObject> extends ObjectPass<D> {
    public DeleteObjectPass(Class<D> d_in) {
        super(d_in);
    }
    @Override
    protected String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception{
        target = (D) getTable().getCurrent();
        return getTable().CheckCurrent(Log) && getTable().ShowDeleteObjectDialog(target);
    }
    //Очищаем все связанные таблицы, чтобы не допустить перерисовки во время удаления объекта.
    @Override
    protected void showPreparation() throws Exception {
        getTable().ClearUI();
        for (Class dep : getTable().getFKDependencies().keySet()) {
            switch (getTable().getFKDependencies().get(dep).data) {
                case NONE:
                case DROP:
                    break;
                case DELETE:
                    getDb().tables.get(dep).ClearUI();
                    break;
            }
        }
    }
    @Override
    protected void body() throws Exception {
        getDb().Delete(target);
        for (Class dep : getTable().getFKDependencies().keySet()) {
            switch (getTable().getFKDependencies().get(dep).data) {
                case NONE:
                    break;
                case DROP:
                    getDb().DropByFK(target, dep);
                    break;
                case DELETE:
                    getDb().DeleteByFK(target, dep);
                    break;
            }
        }
    }
    //тут именно на финише, чтобы в любом случае вся таблица всегда была видна.
    @Override
    protected void performFinish() throws Exception {
        getTable().ShowUI();
        for (Class dep : getTable().getFKDependencies().keySet()) {
            getDb().tables.get(dep).RefreshUI();
        }
    }
}
