package Visual_DVM_2021.Passes;

import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.EnvironmentValue.EnvironmentValue;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;

public abstract class ProcessPass<T> extends Pass_2021<T> {
    public Process process = null;
    protected String process_path = "";
    public int exit_code = Utils.Nan;
    public LinkedHashMap<String, String> envs = new LinkedHashMap<>();

    public void CreateProcess(String exec_path_in) throws Exception {
        output.clear();
        exit_code = Utils.Nan;
        process_path = exec_path_in;
        ProcessBuilder procBuilder = new ProcessBuilder(process_path);
        procBuilder.redirectErrorStream(true);
        for (String name : envs.keySet())
            procBuilder.environment().put(name, envs.get(name));
        envs.clear();
        // запуск программы
        process = procBuilder.start();
    }

    //ожидать завершения читая весь его поток в процессе выполнения.
    public void WaitForProcess() throws Exception {
        exit_code = process.waitFor();
        System.out.println("завершено с кодом " + exit_code);
        process = null;
        if (exit_code != 0)
            throw new Exception("Процесс завершился с кодом " + exit_code);
    }

    public void ReadAllOutput() {
        try {
            InputStream stdout = process.getInputStream();
            InputStreamReader isrStdout = new InputStreamReader(stdout);
            BufferedReader brStdout = new BufferedReader(isrStdout);
            String line;
            while ((line = brStdout.readLine()) != null) {
                System.out.println(line);
                output.add(line);
            }
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }

    public String ReadLine() throws Exception {
        InputStream stdout = process.getInputStream();
        InputStreamReader isrStdout = new InputStreamReader(stdout);
        BufferedReader brStdout = new BufferedReader(isrStdout);
        return brStdout.readLine();
    }

    public Vector<String> output = new Vector<String>();

    public void PerformScript(String script_text) throws Exception {
        PerformScript(Utils.CreateTempFile("script", Global.isWindows ? "bat" : "", script_text), true);
    }

    public void PerformScript(File script, boolean wait) throws Exception {
        if (!script.setExecutable(true)) throw new Exception("Не удалось создать исполняемый файл для скрипта");
        CreateProcess(script.getAbsolutePath());
        if (wait) {
            ReadAllOutput();
            WaitForProcess();
        }
    }

    @Override
    protected boolean needsAnimation() {
        return true;
    }
}
