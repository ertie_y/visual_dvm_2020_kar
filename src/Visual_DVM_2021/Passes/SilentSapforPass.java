package Visual_DVM_2021.Passes;
import Common.Current;
import Repository.Component.Sapfor.Sapfor;
public class SilentSapforPass extends CurrentProjectPass {
    protected Sapfor sapfor;
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    public boolean needsConfirmations() {
        return true;
    }
    protected String getSapforPassName() {
        return code().toString();
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args) && ((sapfor = Current.getSapfor()) != null);
    }
    @Override
    protected boolean validate() {
        switch (sapfor.getErrorCode()) {
            case Sapfor.empty_code:
            case Sapfor.canceled_code:
                return false;
            default:
                return isGoodCode();
        }
    }
    @Override
    protected void performFail() throws Exception {
        Log.Writeln_("Код возврата " + sapfor.getErrorCode());
        sapfor.ResetAllAnalyses();
    }
    protected boolean isGoodCode() {
        return true;
    }
}
