package Visual_DVM_2021.Passes;
import Common.Current;
import ProjectData.Project.db_project_info;
public abstract class CurrentProjectPass extends Pass_2021<db_project_info> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getProject()) != null;
    }
}
