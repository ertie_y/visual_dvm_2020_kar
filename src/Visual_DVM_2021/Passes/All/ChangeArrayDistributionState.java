package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.SapforData.Arrays.ArrayState;
import ProjectData.SapforData.Arrays.ProjectArray;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforModification;

import static ProjectData.SapforData.Arrays.ArrayState.None;
import static ProjectData.SapforData.Arrays.ArrayState.Selected;
public class ChangeArrayDistributionState extends SapforModification {
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    ProjectArray array = null;
    ArrayState new_state = null;
    boolean flag;
    boolean active;
    @Override
    protected String getSapforPassName() {
        return "SPF_SetDistributionFlagToArrays";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            //------
            active = false;
            array = (ProjectArray) args[0];
            flag = (boolean) args[1];
            //------
            new_state = flag ? Selected : None;
            if (passes.get(PassCode_2021.SPF_ParseFilesWithOrder).isDone()) {
                addOpt1 = array.UniqKey;
                addOpt2 = String.valueOf(new_state.ordinal());
                active = true;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        if (active)
            super.body();
    }
    @Override
    protected void performDone() throws Exception {
        array.State = new_state;
        array.SaveUserState();
        if (active)
            sapfor.ResetAllAnalyses();
    }
    @Override
    protected void showDone() throws Exception {
        Current.getProject().db.savedArrays.ShowUI();
        target.declaratedArrays.SetCurrentObjectUI(array.getPK());
    }
}
