package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.Windows.Dialog.PercentsForm;
import Repository.BugReport.BugReport;

public class UpdateBugReportProgress extends UpdateBugReportField{
    @Override
    protected String getIconPath() {
        return "/icons/Progress.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        PercentsForm f = new PercentsForm();
        if (f.ShowDialog("Завершённость работы над ошибкой", Current.getBugReport().percentage))
           return super.canStart("percentage", f.Result);
        return false;
    }
}
