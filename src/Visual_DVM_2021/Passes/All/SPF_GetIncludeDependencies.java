package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import ProjectData.Files.LanguageStyle;
import ProjectData.LanguageName;
import ProjectData.SapforData.Includes.DependencyInfo;
import ProjectData.SapforData.Includes.FileInfo;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;

import javax.swing.tree.DefaultMutableTreeNode;

public class SPF_GetIncludeDependencies extends SapforAnalysis {
    boolean update_current = false;

    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }

    @Override
    public String phase() {
        return "BUILD_INCLUDE_DEPENDENCIES";
    }

    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        target.numAddicted = 0;
        target.addictedFiles.clear();
        target.includes_root.removeAllChildren();
        target.allIncludes.clear();
        for (DBProjectFile file : target.db.files.Data.values())
            file.relativeHeaders.clear();
    }

    @Override
    public void unpack(String packed) throws Exception {
        // update_current = false;
        FileInfo fileInfo = null;
        String[] data_ = packed.split("@");
        for (int i = 0; i < data_.length; ++i) {
            String s = Utils.toW(data_[i]);
            if (i % 2 == 0) {
                if (target.db.files.Data.containsKey(s)) {
                    fileInfo = new FileInfo(s);
                } else fileInfo = null;
            } else {
                if ((fileInfo != null) && (s != "")) {
                    String[] data__ = s.split("\\|");
                    for (int j = 0; j < data__.length; ++j) {
                        if (data__[j].length() > 0) {
                            fileInfo.dependencies.add(
                                    new DependencyInfo(data__[j]));
                            //-------------------------------------
                            if (!target.addictedFiles.containsKey(fileInfo.file))
                                target.addictedFiles.put(fileInfo.file, fileInfo);
                            //-------------------------------------
                            String file_name = data__[j];
                            if (target.db.files.containsKey(file_name)) {
                                DBProjectFile d_file = target.db.files.Data.get(file_name);
                                //----------------->>
                                if (!target.allIncludes.containsKey(d_file.name))
                                    target.allIncludes.put(d_file.name, d_file);
                                //----------------->>
                                DBProjectFile father = target.db.files.Data.get(fileInfo.file);
                                father.relativeHeaders.put(Utils.getRelativeName(father.file.getParentFile(), d_file.file), d_file);
                                // if (d_file.fileType == FileType.none)
                                d_file.UpdateType(FileType.header);
                                if (d_file.languageName == LanguageName.n)
                                    d_file.UpdateLanguage(father.languageName);
                                if (d_file.style == LanguageStyle.none)
                                    d_file.UpdateStyle(father.style);
                                //----------------------------------------
                                if (Current.HasFile() && Current.getFile().name.equals(d_file.name))
                                    update_current = true;
                                //-----------------------------------------
                            }
                        }
                    }
                }
                fileInfo = null;
            }
        }
        //-теперь строим граф
        for (FileInfo file : target.addictedFiles.values()) {
            DefaultMutableTreeNode node = new DefaultMutableTreeNode(
                    file);
            for (DependencyInfo h : file.dependencies) {
                node.add(new DefaultMutableTreeNode(h));
            }
            target.includes_root.add(node);
        }
        target.numAddicted += target.addictedFiles.size();
        target.UpdateAddictedCount();
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowIncludes();
        if (update_current)
            Current.getFile().form.ShowProperties();
        super.showDone();
    }

    @Override
    protected void showPreparation() {
        UI.getNewMainWindow().getProjectWindow().ShowNoIncludes();
    }

    @Override
    protected void FocusResult() {
        super.FocusResult();
        UI.getNewMainWindow().getProjectWindow().FocusDependencies();
    }
}
