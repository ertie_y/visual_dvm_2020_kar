package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.CurrentComponentPass;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class CreateComponentBackUp extends CurrentComponentPass {
    @Override
    protected boolean canStart(Object ... args) throws Exception {
        return super.canStart(args)&& (UI.Question("Обновить компонент "
                + Utils.Brackets(target.getComponentType().getDescription())));
    }
    @Override
    protected void body() throws Exception {
        //form.ShowMessage2("копирование предыдущей версии...");
        if (target.getFile().exists())
            Files.copy(target.getFile().toPath(), target.getBackUpFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
    }
}
