package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.User.User;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteUser extends DeleteObjectPass<User> {
    public DeleteUser() {
        super(User.class);
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        Global.db.DropProjectsUser(target);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        boolean refresh_project =
                Current.HasProject() &&
                        (target.getPK().equals(Current.getProject().getInfo().user_id));
        if (refresh_project)
            UI.getNewMainWindow().getTestingWindow().ShowProjectUser();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
