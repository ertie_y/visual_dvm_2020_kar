package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.ScenarioCommand.ScenarioCommand;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditScenarioCommand extends EditObjectPass<ScenarioCommand> {
    public EditScenarioCommand() {
        super(ScenarioCommand.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
