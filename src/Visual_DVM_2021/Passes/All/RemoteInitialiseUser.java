package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.User.UserState;
import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;

import java.nio.file.Paths;

public class RemoteInitialiseUser extends CurrentConnectionPass {
    @Override
    protected void ServerAction() throws Exception {
        String workspace_name = Utils.getDateName("visual_dvm_workspace");
        String workspace = Utils.toU(Paths.get(sftpChannel.getHome(), workspace_name).toString());
        form.ShowMessage1("Создание рабочих папок...");
        //-------------------------------------
        sftpChannel.mkdir(workspace);
        sftpChannel.cd(workspace);
        sftpChannel.mkdir(projects);
        sftpChannel.mkdir(modules);
        sftpChannel.mkdir(compilers);
        sftpChannel.mkdir(tests);
        //------------------------------------
        sftpChannel.cd(modules);
        form.ShowMessage1("Закачка модулей...");
        put_resource(launcher_code);
        put_resource(starter_code);
        put_resource(Process_r_header);
        //-------------------------------------
        form.ShowMessage1("Сборка модулей...");
        //канал на исполнение независим, поэтому переход в папку отдельный
        Command(
                "cd " + Utils.DQuotes(sftpChannel.pwd()), //нужны ли тут кавычки?
                "g++ " + starter_code+" -o " + starter,
                "g++ " + launcher_code + " -o " + launcher,
                "chmod 0777 " + starter,
                "chmod 0777 " + launcher
        );
        //--------------------------------------
        user.workspace = workspace;
        user.state = UserState.ready_to_work;
        Global.db.Update(user);
    }

}
