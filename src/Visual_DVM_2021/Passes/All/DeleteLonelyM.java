package Visual_DVM_2021.Passes.All;
import Common.Current;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import ProjectData.Project.db_project_info;

import java.util.Vector;
public class DeleteLonelyM extends Pass_2021<db_project_info> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Root)) {
            target = Current.getRoot();
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        Vector<db_project_info> targets = target.getLonelyM();
        System.out.println(targets.size());
        for (db_project_info m : targets) {
            passes.get(PassCode_2021.DeleteVersion).Do(m);
        }
    }
}
