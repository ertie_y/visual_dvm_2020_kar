package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.Makefile.Makefile;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteMakefile extends DeleteObjectPass<Makefile> {
    public DeleteMakefile() {
        super(Makefile.class);
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        Global.db.DropProjectsMakefile(target);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        boolean refresh_project = Current.HasProject() && (target.getPK().equals(Current.getProject().getInfo().makefile_id));
        if (refresh_project)
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
