package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.Database;
import Common.UI.UI;
import Common.Utils.Utils;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.AddObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Main.TestsFilterForm;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Date;

public class AddGroup extends AddObjectPass<Group> {
    public AddGroup() {
        super(Group.class);
    }

    @Override
    protected void body() throws Exception {
        //тут надо наоборот. сгачал
        //--
        super.body();
    }

    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (super.canStart(args) && (!TestingSystem.db.groups.Data.containsKey(target.name)));
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.PublishGroup).Do(target);
    }
}

