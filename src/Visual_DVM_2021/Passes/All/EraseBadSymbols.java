package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.Transformation;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
public class EraseBadSymbols extends Transformation {

    @Override
    protected void body() throws Exception {
        for (DBProjectFile file : target.db.files.Data.values()) {
            String file_text = FileUtils.readFileToString(file.file);
            StringBuilder res_text = new StringBuilder();
            for (char c : file_text.toCharArray()) {
                if (Utils.isEnglishLetter(c) || Character.isDigit(c) || Utils.isSign(c)) {
                    res_text.append(c);
                } else if (Utils.isRussianLetter(c))
                    res_text.append(Utils.Translit(c));
                else res_text.append(' ');
            }
            File dst = Paths.get(target.last_version.Home.getAbsolutePath(), Global.isWindows ? file.name : file.name.replace('\\', '/')).toFile();
            FileUtils.write(dst, res_text.toString());
        }
    }
}
