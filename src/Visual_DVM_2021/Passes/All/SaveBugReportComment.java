package Visual_DVM_2021.Passes.All;

import Common.UI.UI;

public class SaveBugReportComment extends UpdateBugReportField{
    @Override
    protected String getIconPath() {
        return "/icons/Save.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("comment", UI.getNewMainWindow().getCallbackWindow().getBugReportCommentText());
    }
}
