package Visual_DVM_2021.Passes.All;
import Common.UI.Selectable;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.SapforData.Includes.DependencyInfo;
import ProjectData.SapforData.Includes.FileInfo;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforTransformation;

import java.util.Vector;
import java.util.stream.Collectors;
public class SPF_InsertIncludesPass extends SapforTransformation {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetIncludeDependencies;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            if (target.numAddicted <= 0) {
                Log.Writeln_("Не найдено файлов, имеющих зависимости по включению.");
                return false;
            }
            Vector<String> Result = new Vector<>();
            for (FileInfo fileInfo : target.addictedFiles.values()) {
                Vector<DependencyInfo> selected_children = fileInfo.dependencies.stream().filter(Selectable::isSelected).collect(Collectors.toCollection(Vector::new));
                if (!selected_children.isEmpty()) {
                    Result.add(fileInfo.file);
                    Result.add(String.valueOf(selected_children.size()));
                    for (DependencyInfo di : selected_children)
                        Result.add(di.file);
                }
            }
            if (Result.isEmpty()) {
                Log.Writeln_("Не отмечено ни одного заголовка для подстановки");
                return false;
            }
            Options = Utils.toU(String.join("|", Result));
            System.out.println(Utils.Brackets(Options));
            return true;
        }
        return false;
    }
    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().getProjectWindow().FocusDependencies();
    }
}
