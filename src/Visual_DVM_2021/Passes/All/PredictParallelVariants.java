package Visual_DVM_2021.Passes.All;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.VariantsMassPass;
public class PredictParallelVariants extends VariantsMassPass {
    @Override
    public PassCode_2021 getSubPassCode() {
        return PassCode_2021.SPF_PredictParallelVariant;
    }
}
