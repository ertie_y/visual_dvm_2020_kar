package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import Visual_DVM_2021.Passes.ChangeFilePass;
import org.apache.commons.io.FileUtils;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.nio.file.Paths;
public class AddFile extends ChangeFilePass<DBProjectFile> {
    enum Mode {
        Dialog,
        Import
    }
    Mode mode;
    File src;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        resetArgs();
        mode = Mode.Dialog;
        src = null;
        if (args.length > 0) {
            src = (File) args[0];
            mode = Mode.Import;
            fileName = src.getName();
            if (Utils.ContainsCyrillic(fileName)){
                Log.Writeln_("Имя файла " + Utils.Brackets(fileName) + " содержит кириллицу.");
                return false;
            }
            if (Utils.ContainsForbiddenName(fileName)){
                Log.Writeln_("Имя файла " + Utils.Brackets(fileName)
                        + " содержит запрещенные символы." +
                        "\n"+Utils.all_forbidden_characters_string);
                return false;
            }

        } else {
            (ff=new FileNameForm()).ShowDialog("Введите имя создаваемого файла");
            fileName = ff.Result;
            //тут имена фильтруются уже на этапе формы.
        }
        if (fileName != null) {
            //->
            parent_node = Current.getProjectCurrentParentNode();
            target_dir = (File) parent_node.getUserObject();
            //->
            dst = Paths.get(target_dir.getAbsolutePath(), fileName).toFile();
            if (dst.exists()) {
                Log.Writeln_("Файл с именем " + Utils.Brackets(fileName) + " уже существует");
                return false;
            }
            target = new DBProjectFile(dst, project);
            if (target.fileType.equals(FileType.forbidden)) {
                Log.Writeln_("Расширение " + Utils.Brackets(Utils.getExtension(dst)) + " недопустимо");
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        if (mode == Mode.Dialog) Utils.WriteToFile(target.file, "");
        else FileUtils.copyFile(src, dst);
        project.db.Insert(target);
        parent_node.add(dst_node = target.node = new DefaultMutableTreeNode(target));
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
