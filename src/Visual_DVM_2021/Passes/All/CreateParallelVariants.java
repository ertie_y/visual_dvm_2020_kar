package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.VariantsMassPass;
public class CreateParallelVariants extends VariantsMassPass {
    @Override
    public PassCode_2021 getSubPassCode() {
        return PassCode_2021.SPF_CreateParallelVariant;
    }
    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusVersions();
    }
}
