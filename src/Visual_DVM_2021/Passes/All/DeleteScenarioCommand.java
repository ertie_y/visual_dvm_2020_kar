package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.ScenarioCommand.ScenarioCommand;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteScenarioCommand extends DeleteObjectPass<ScenarioCommand> {
    public DeleteScenarioCommand() {
        super(ScenarioCommand.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
