package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Files.VFileChooser;
import Visual_DVM_2021.Passes.CurrentComponentPass;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class InstallComponentFromFolder extends CurrentComponentPass {
    VFileChooser fileChooser = new VFileChooser("выбор файла компонента",
            "jar","exe" );
    File file;
    @Override
    protected boolean canStart(Object...args) throws Exception {
        return super.canStart(args)&&((file=fileChooser.ShowDialog())!=null);
    }
    @Override
    protected void body() throws Exception {
        Files.copy(file.toPath(), target.getNewFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        target.Update();
    }
    @Override
    protected void performDone() throws Exception {
        target.InitialVersionCheck();
        if (target.CanBeUpdated())
            target.CheckIfNeedsUpdateOrPublish();
    }
    @Override
    protected void showDone() throws Exception {
        Global.RefreshUpdatesStatus();
    }
}
