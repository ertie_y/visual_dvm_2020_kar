package Visual_DVM_2021.Passes.All;

import Common.UI.UI;
import Common.Utils.Index;
import ProjectData.SapforData.Regions.ParallelRegion;
import Visual_DVM_2021.Passes.SapforAnalysis;

public class SPF_GetArrayDistributionOnlyRegions extends SapforAnalysis {
    @Override
    public String phase() {
        return "FILL_PAR_REGIONS_LINES";
    }

    @Override
    protected void unpack(String packed) throws Exception {
        String[] splited = packed.split("#");
        Index idx = new Index();
        int n = Integer.parseInt(splited[idx.Inc()]);
        for (int i = 0; i < n; ++i) {
            ParallelRegion p = new ParallelRegion(splited, idx);
            target.parallelRegions.put(p.regionId, p);
        }
    }

    @Override
    protected boolean isAtomic() {
        return false;
    }

    @Override
    protected boolean alwaysCheck() {
        return true;
    }

    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation();
        target.parallelRegions.clear();
    }
    @Override
    protected void showPreparation() {
        UI.getNewMainWindow().getProjectWindow().ShowNoRegions();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowRegions();
        super.showDone();
    }
    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusProject();
        UI.getNewMainWindow().getProjectWindow().FocusAnalysis();
    }
}
