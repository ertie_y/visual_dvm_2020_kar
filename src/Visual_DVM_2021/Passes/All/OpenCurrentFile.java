package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class OpenCurrentFile extends Pass_2021<DBProjectFile> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target= (args.length>0)?(DBProjectFile) args[0]:null;
        return (target!=null)&&(!Current.HasFile()||(!Current.getFile().file.equals(target.file)));
    }
    @Override
    protected void performPreparation() throws Exception {
        //закрыть старый текущий файл.
        passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        System.out.println(getDescription()+" "+target.file.getAbsolutePath());
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.File, target);
    }
    @Override
    protected void showDone() throws Exception {
        //отобразить новый текущий файл.
        UI.getNewMainWindow().getProjectWindow().ShowFile();
    }
}
