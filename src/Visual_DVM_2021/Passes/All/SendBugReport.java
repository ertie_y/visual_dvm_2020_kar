package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.BugReport.BugReportState;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

public class SendBugReport extends RepositoryPass_2021 {
    @Override
    protected void ServerAction() throws Exception {
        //отправить архив.
        SendFile(Current.getBugReport().getArchiveFile(), Current.getBugReport().getRemoteArchiveAddress());
        // синхрон бд
        Command(new ServerExchangeUnit_2021(ServerCode.PublishBugReport, "", Current.getBugReport()));
    }
    @Override
    protected void performFail() throws Exception {
        Current.getBugReport().state = BugReportState.draft;
    }
}
