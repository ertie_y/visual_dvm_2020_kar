package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.ChangeFilePass;
import Visual_DVM_2021.Passes.PassCode_2021;
import org.apache.commons.io.FileUtils;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.nio.file.Paths;
import java.util.Vector;
public class RenameDirectory extends ChangeFilePass {
    boolean current;
    DBProjectFile old_current_file;
    DBProjectFile new_current_file;
    @Override
    protected boolean canStart(Object... args) {
        resetArgs();
        current = false;
        old_current_file = null;
        new_current_file = null;
        dst_node = Current.getProjectNode();
        if ((dst_node != null) && (dst_node.getUserObject() instanceof File)) {
            target_dir = Current.getSelectedDirectory();
            if (current = Current.HasFile() && Utils.isAnchestor(Current.getFile().file, target_dir)) {
                old_current_file = Current.getFile();
            }
            if ((ff = new FileNameForm()).ShowDialog("Введите новое имя папки", target_dir.getName())) {
                fileName = ff.Result;
                dst = Paths.get(target_dir.getParentFile().getAbsolutePath(), fileName).toFile();
                if (dst.exists()) {
                    Log.Writeln("Файл с именем " + Utils.Brackets(fileName) + " уже существует");
                    return false;
                }
                if (target_dir.equals(project.Home)) {
                    Log.Writeln("Нельзя переименовывать домашнюю папку проекта.");
                    return false;
                }
                return true;
            }
        } else Log.Writeln("Папка не выделена.");
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (current)
            passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        FileUtils.moveDirectory(target_dir, dst);
        if (!dst.exists()) throw new Exception("Не удалось переименовать папку");
        //теперь все файлы и подпапки которые были в этой папке должны быть переосмыслены.
        Vector<DBProjectFile> to_rename = new Vector<>();
        for (DBProjectFile file : project.db.files.Data.values()) {
            if (Utils.isAnchestor(file.file, target_dir)) {
                to_rename.add(file);
            }
        }
        //так как имя первичный ключ приходится удалять их из бд проекта.
        for (DBProjectFile file : to_rename) {
            if (current && file.file.equals(old_current_file.file)) {
                new_current_file = file;
            }
            file.CleanAll();
            file.father.db.Delete(file);
            file.file = Paths.get(dst.getAbsolutePath(), Utils.getRelativeAddress(file.file, target_dir)).toFile();
            file.RefreshName();
            file.father.db.Insert(file);
        }
        dst_node.setUserObject(dst);
        for (int i = 0; i < dst_node.getChildCount(); ++i)
            Utils.renameSubdirs_r((DefaultMutableTreeNode) dst_node.getChildAt(i), target_dir, dst);
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
        Current.set(Current.SelectedDirectory, dst);
        if (current && new_current_file != null)
            passes.get(PassCode_2021.OpenCurrentFile).Do(new_current_file);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
