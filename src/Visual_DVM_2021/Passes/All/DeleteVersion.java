package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class DeleteVersion extends Pass_2021<db_project_info> {
    db_project_info parent;
    boolean current;
    @Override
    protected String getIconPath() {
        return "/icons/Delete.png";
    }
    @Override
    protected boolean canStart(Object... args) {
        if (args.length > 0) {
            target = (db_project_info) args[0];
            current = (Current.getVersion() != null) && Current.getVersion().Home.equals(target.Home);
            return true;
        } else {
            if (((target = Current.getVersion()) != null) && (!Current.hasUI() ||
                    UI.Warning("Удалить версию " + Utils.Brackets(target.name)))) {
                current = true;
                return true;
            }
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (target.parent != null)
            target.parent.checkLastModification(target);
        if (Current.HasProject()) {
            if ((Current.getProject().Home.getAbsolutePath().startsWith(target.Home.getAbsolutePath())))
                passes.get(PassCode_2021.CloseCurrentProject).Do();
        }
        if (Current.hasUI()) {
            UI.getNewMainWindow().getVersionsWindow().getMasterComparsionWindow().MasterCheckVersion(target);
        }
    }
    @Override
    protected void body() throws Exception {
        Utils.forceDeleteWithCheck(target.Home);
    }
    @Override
    protected void performDone() throws Exception {
        parent = target.parent;
        if (current)
            Current.set(Current.Version, null);
        if (parent != null) {
            target.node.removeFromParent();
            parent.versions.remove(target.name);
        } else
            Current.set(Current.Root, null);
    }
    @Override
    protected void showDone() throws Exception {
        if (parent != null)
            UI.getNewMainWindow().getVersionsWindow().RefreshVersionsTree();
        else
            UI.getNewMainWindow().ShowNoRoot();
    }
}
