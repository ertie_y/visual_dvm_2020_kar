package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.Passes.PassState;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SilentSapforPass;

public class SPF_GetGCovInfo extends SilentSapforPass {
    @Override
    protected String getIconPath() {
        return "/icons/NotPick.png";
    }

    protected String getDoneIconPath() {
        return "/icons/Pick.png";
    }

    @Override
    protected boolean hasStats() {
        return true;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        SPF_ParseFilesWithOrder.silent=true;
            return super.canStart(args)&&

                    (target.isGCOVDone()||passes.get(PassCode_2021.GCOV).Do())&&
                    (passes.get(PassCode_2021.SPF_ParseFilesWithOrder).isDone()||passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do());
    }

    @Override
    protected void performFinish() throws Exception {
        SPF_ParseFilesWithOrder.silent=false;
        super.performFinish();
    }

    @Override
    protected void performPreparation() throws Exception {
        sapfor.cd(target.Home);
        target.CleanInterruptFile();
    }

    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                target.getProjFile().getAbsolutePath());
    }

    @Override
    protected void performDone() throws Exception {
        if (!sapfor.getResult().isEmpty())
            unpack(sapfor.getResult());
    }

    protected void unpack(String packed) throws Exception {
        String[] byFiles = packed.split("@");

        for (int z = 0; z < byFiles.length; ++z) {
            DBProjectFile cur = target.db.files.get((byFiles[z++]).replace("/", "\\"));
            String[] inFile = byFiles[z].split(" ");
            for (int k = 0; k < (inFile.length / 2) * 2; k += 2) {
                int line = Integer.parseInt(inFile[k]);
                long execution = Long.parseLong(inFile[k + 1]);
                if (line>0) {
                    int v_line = line-1;
                    if (!cur.gcov_info.line_info.containsKey(v_line)){
                        cur.gcov_info.add_line(v_line,
                                (execution>=0)?execution:0);
                    }
                }
            }
            //завешение настроек.
            cur.gcov_info.setup();
        }
    }

    @Override
    public void Reset() {
        if (state == PassState.Done) {
            state = PassState.Inactive;
            if (menuItem != null)
                menuItem.setIcon(Utils.getIcon(getIconPath()));
            if (button != null)
                button.setIcon(Utils.getIcon(getIconPath()));
        }
    }

    @Override
    protected void showDone() throws Exception {
        if (Current.HasFile())
            Current.getFile().form.ShowGCOV();

        if (menuItem != null)
            menuItem.setIcon(Utils.getIcon(getDoneIconPath()));
        if (button != null)
            button.setIcon(Utils.getIcon(getDoneIconPath()));
    }

    @Override
    protected void FocusResult() {
        if (Current.HasFile())
            Current.getFile().form.FocusGCOVLog();
    }
}
