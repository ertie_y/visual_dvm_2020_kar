package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.SapforData.Functions.FuncCall;
import ProjectData.SapforData.Functions.FuncInfo;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforTransformation;

import java.util.Vector;
public class SPF_InlineProcedures extends SapforTransformation {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetGraphFunctions;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            Vector<String> Result = new Vector<>();
            for (FuncInfo fi : target.allFunctions.values()) {
                Vector<FuncCall> selected_children = new Vector<>();
                for (String calls_file : fi.own_calls.keySet()) {
                    for (FuncCall call : fi.own_calls.get(calls_file)) {
                        if (call.isSelected())
                            selected_children.add(call);
                    }
                }
                if (!selected_children.isEmpty()) {
                    Result.add(fi.funcName);
                    Result.add(String.valueOf(selected_children.size()));
                    for (FuncCall fc : selected_children) {
                        Result.add(fc.file);
                        Result.add(String.valueOf(fc.line));
                    }
                }
            }
            if (Result.isEmpty()) {
                Log.Writeln_(
                        (callsCount>1)?"Не отмечено ни одной точки вызова процедур для подстановки":
                        "Для точечной подстановки следует отметить\nнужные точки вызовов процедур в дереве."
                        );
                return false;
            }
            Options = Utils.toU(String.join("|", Result));
            System.out.println(Utils.Brackets(Options));
            Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.PARSE_FOR_INLINE, "1");
            SPF_ParseFilesWithOrder.silent=true;
            return passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do(false);
        }
        return false;
    }

    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().getProjectWindow().FocusPoints();
    }

    @Override
    protected void performFinish() throws Exception {
        SPF_ParseFilesWithOrder.silent=false;
        super.performFinish();
    }
}
