package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Passes.ToolBarPass;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.Pass_2021;
public class ApplyRunConfiguration extends CurrentProjectPass {
    boolean res;
    @Override
    protected String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args)&&Current.Check(Log, Current.RunConfiguration);
    }
    @Override
    protected void body() throws Exception {
        res = target.UpdateRunConfiguration(Current.getRunConfiguration());
    }
    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getTestingWindow().ShowProjectRunConfiguration();
    }
}
