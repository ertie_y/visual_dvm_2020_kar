package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachineType;
import GlobalData.User.User;
import Visual_DVM_2021.Passes.AddObjectPass;

public class AddUser extends AddObjectPass<User> {
    public AddUser() {
        super(User.class);
    }

    @Override
    protected Database getDb() {
        return Global.db;
    }

    @Override
    public Class<? extends DBObject> getOwner() {
        return Machine.class;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            Machine machine = Current.getMachine();
            if (machine.type.equals(MachineType.Local) && (machine.getUsers().size() > 0)) {
                Log.Writeln_("У локальной машины может быть только один пользователь");
                return false;
            }
            return true;
        }
        return false;
    }
}
