package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Machine.MachineType;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.RunStsRecord.RunStsRecord;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.TaskState;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import ProjectData.Project.db_project_info;

import java.util.Vector;

public class Run extends Pass_2021<db_project_info> {
    Pass_2021 subpass = null;
    CompilationTask compilationTask = null;
    RunConfiguration runConfiguration = null;
    Vector<RunTask> runTasks;

    @Override
    protected String getIconPath() {
        return "/icons/GreenStart.png";
    }

    @Override
    protected boolean hasStats() {
        return true;
    }

    @Override
    protected void performPreparation() throws Exception {
        runTasks = runConfiguration.generateRunTasks(target, compilationTask);
        for (RunTask runTask : runTasks)
            Global.db.Insert(runTask);
    }

    @Override
    protected void showPreparation() throws Exception {
        Global.db.runTasks.ShowUI();
    }

    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().FocusTesting();
        UI.getNewMainWindow().getTestingWindow().FocusRunTasks();
    }

    @Override
    protected boolean canStart(Object... args) {
        subpass = null;
        if (Current.Check(Log, Current.Project, Current.CompilationTask)) {
            //-
            target = Current.getProject();
            compilationTask = Current.getCompilationTask();
            //-
            if (!target.hasRunConfiguration())
                Log.Writeln_("Не задана конфигурация запуска для проекта.");
            else {
                //-
                runConfiguration = target.getRunConfiguration();
                //-
                if (target.getMachine().type.equals(MachineType.MVS_cluster) &&
                        target.getRunConfiguration().LauncherCall.isEmpty()
                ) {
                    Log.Writeln_("Запуск напрямую на кластере запрещён.Используйте для запуска DVM систему или MPI");
                }
            }
            if (!compilationTask.state.equals(TaskState.Done))
                Log.Writeln_("Текущая задача на компиляцию еще не выполнялась, или была завершена с ошибками");
            return Log.isEmpty();
        }
        return false;
    }

    @Override
    protected void body() throws Exception {
        switch (target.getMachine().type) {
            case Local:
                if (Global.isWindows) {
                    subpass = passes.get(PassCode_2021.WindowsLocalRun);
                } else
                    subpass = passes.get(PassCode_2021.LinuxLocalRun);
                break;
            case Undefined:
                throw new Exception("Запуск не реализован для типа машины " + Utils.DQuotes(target.getMachine().type));
            case MVS_cluster:
                subpass = passes.get(PassCode_2021.MVSRun);
                break;
            default:
                subpass = passes.get(PassCode_2021.ServerRun);
                break;
        }


        int i = 1;
        for (RunTask task : runTasks) {
            boolean task_completed = false;
            task.setProgress(i, runTasks.size());
            //-
            Global.db.runTasks.RefreshUI();
            Global.db.runTasks.SetCurrentObjectUI(task.id);
            //-
            subpass.Do(task, target);
            //-
            switch (task.state){
                case Done:
                case DoneWithErrors:
                    task_completed = true;
                    if (task.hasDvmSts) {
                        Utils.CheckAndCleanDirectory(Current.getProject().getStatisticDirectory());
                        Pass_2021.passes.get(PassCode_2021.SPF_StatisticAnalyzer).Do(task);
                    }
                    RunStsRecord record = new RunStsRecord(task);
                    Global.db.Insert(record);
                    if (task.hasDvmSts)
                        record.SaveSts(task);
                    Global.db.runStsRecords.RefreshUI();
                    break;

                case Finished:
                case Crushed:
                case AbortedByTimeout:
                    task_completed = true;
                    Global.db.Insert(new RunStsRecord(task));
                    Global.db.runStsRecords.RefreshUI();
                    break;
            }

            //-
            Global.db.runTasks.RefreshUI();

            UI.getNewMainWindow().getTestingWindow().ShowCurrentRunTask();
            //-
            if (!task_completed) break;
            ++i;
        }
    }

}
