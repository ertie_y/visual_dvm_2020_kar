package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.Windows.Dialog.Dialog;
import GlobalData.Compiler.CompilerType;
import GlobalData.CompilerOption.CompilerOption;
import GlobalData.CompilerOption.UI.CompilerOptionsFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;

public class PickCompilerOptions extends Pass_2021<String> {
    //-
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.DialogCompiler)) {
            if (!Current.getDialogCompiler().type.equals(CompilerType.dvm)) {
                Log.Writeln_("Выбор опций возможен только для DVM компилятора,");
                return false;
            }
            if (!(Current.getDialogCompiler().helpLoaded || passes.get(PassCode_2021.ShowCompilerHelp).Do(false)))
                return false;
            Dialog<String, CompilerOptionsFields> dialog = new Dialog<String, CompilerOptionsFields>(CompilerOptionsFields.class) {
                @Override
                public boolean NeedsScroll() {
                    return false;
                }

                @Override
                public void validateFields() {
                    for (CompilerOption option : Current.getDialogCompiler().options.Data.values()) {
                        if (option.isSelected())
                            if (option.hasParameter() && (option.parameterValue.isEmpty()))
                                Log.Writeln_("Отмеченная опция " + option.name + " предполагает непустое значение.");
                    }
                }
            };
            return dialog.ShowDialog("Назначение опций компилятора");
        }
        return false;
    }


    @Override
    protected void body() throws Exception {
        Vector<String> res = new Vector<>();
        for (CompilerOption option : Current.getDialogCompiler().options.Data.values()) {
            if (option.isSelected())
                res.add(option.toString());
        }
        target = String.join(" ", res);
    }
}
