package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.SapforAnalysis;
public class SPF_GetFileLineInfo extends SapforAnalysis {
    @Override
    public String phase() {
        return "FILE_LINE_INFO";
    }
    @Override
    public void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        target.numLines = 0;
        target.numSPF = 0;
        target.numDVM = 0;
    }
    @Override
    protected void showPreparation() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowNoMetrics();
    }
    @Override
    protected void unpack(String packed) throws Exception {
        //----------------------------------------
        String[] data_ = packed.split("@");
        DBProjectFile file_ = null;
        for (int i = 0; i < data_.length; ++i) {
            String s = data_[i];
            if (i % 2 == 0) {
                file_ = target.db.files.Data.get(Utils.toW(s));
            } else {
                String[] local = s.split("_");
                int num = Integer.parseInt(local[0]);
                target.numSPF += Integer.parseInt(local[1]);
                target.numDVM += Integer.parseInt(local[2]);
                if (file_ != null)
                    file_.lines_count = num;
                target.numLines += num;
            }
        }
        target.UpdateDVMCount();
        target.UpdateSPFCount();
        target.UpdateLinesCount();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowMetrics();
        super.showDone();
    }
    @Override
    protected void FocusResult() {
        super.FocusResult();
        UI.getNewMainWindow().getProjectWindow().FocusAnalysis();
    }
}
