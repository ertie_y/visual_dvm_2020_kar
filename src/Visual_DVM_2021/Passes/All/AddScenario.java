package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Scenario.Scenario;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddScenario extends AddObjectPass<Scenario> {
    public AddScenario() {
        super(Scenario.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
