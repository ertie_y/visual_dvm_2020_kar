package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import GlobalData.Scenario.Scenario;
import GlobalData.ScenarioCommand.ScenarioCommand;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforTransformation;
import Visual_DVM_2021.Passes.Transformation;

import java.util.LinkedHashMap;
import java.util.Vector;
public class RunScenario extends Pass_2021<Scenario> {
    String oldConfirmStart;
    String oldConfirmEnd;
    String oldFocusPasses;
    Vector<String> toPrint = new Vector<>();
    @Override
    protected String getIconPath() {
        return "/icons/Start.png";
    }
    @Override
    public void Interrupt() throws Exception {
        Current.getProject().CreateInterruptFile();
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Scenario, Current.Project)) {
            toPrint.clear();
            target = Current.getScenario();
            return true;
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        oldConfirmStart = Global.db.settings.get(SettingName.ConfirmPassesStart).Value;
        oldConfirmEnd = Global.db.settings.get(SettingName.ShowPassesDone).Value;
        oldFocusPasses = Global.db.settings.get(SettingName.FocusPassesResult).Value;
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.ConfirmPassesStart, "0");
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.ShowPassesDone, "0");
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.FocusPassesResult, "0");
    }
    @Override
    protected void body() throws Exception {
        LinkedHashMap<Integer, ScenarioCommand> commands = target.getCommands();
        form.ShowProgress(commands.size(), 0, true);
        int i = 0;
        for (ScenarioCommand command : commands.values()) {
            form.ShowProgress(commands.size(), i++, true);
            //-
            Pass_2021 subpass = passes.get(command.passCode);
            subpass.Reset();
            subpass.Do();
            toPrint.add(command.passCode.getDescription() + " : " + subpass.state.getDescription());
            switch (subpass.state) {
                case Done:
                    if ((subpass instanceof SapforTransformation) ||
                            (subpass instanceof Transformation)) {
                        passes.get(PassCode_2021.OpenCurrentProject).Do(
                                Current.getProject().last_version);
                    }
                    break;
                default:
                    return;
            }
        }
    }
    @Override
    protected void performFinish() throws Exception {
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.ConfirmPassesStart,
                oldConfirmStart);
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.ShowPassesDone,
                oldConfirmEnd);
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.FocusPassesResult,
                oldFocusPasses);
    }
    @Override
    protected void showFinish() throws Exception {
        UI.Info("Проходы:\n" + String.join("\n", toPrint));
    }
}
