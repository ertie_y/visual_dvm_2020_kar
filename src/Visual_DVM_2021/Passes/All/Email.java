package Visual_DVM_2021.Passes.All;
import Repository.EmailMessage;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.io.File;
import java.util.Arrays;
import java.util.Vector;
//http://java-online.ru/javax-mail.xhtml
//https://javarush.ru/groups/posts/1226-kak-otpravitjh-pisjhmo-iz-java-prilozhenija-s-primerom
public class Email extends RepositoryPass_2021<EmailMessage> {
    //этот емейл только для клиента. все что он делает отправляет на сервер то. что нужно отправить.
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target= (EmailMessage) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        //нужно разделить сообщение на несколько одинаковых, по числу адресатов.
        for (String recipient: target.targets){
            EmailMessage part = new EmailMessage();
            part.subject = target.subject;
            part.text = target.text;
            part.targets.add(recipient);
            part.files.putAll(target.files);
            Command(new ServerExchangeUnit_2021(ServerCode.Email, "", part));
        }
    }
}
