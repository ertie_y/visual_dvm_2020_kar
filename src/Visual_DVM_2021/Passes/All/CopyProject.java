package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.UI.Windows.Dialog.Dialog;
import Common.Utils.Utils;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
import ProjectData.SapforData.Regions.UI.ArrayAlignmentBar;
import ProjectData.SapforData.Regions.UI.ParallelRegionFields;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.UI.CopyProjectFields;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.Vector;

public class CopyProject extends CurrentProjectPass {
    protected File dstFile = null;
    protected boolean migrateData = false;

    @Override
    protected boolean hasStats() {
        return true;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {

            Dialog<Object, CopyProjectFields> dialog = new Dialog<Object, CopyProjectFields>(CopyProjectFields.class) {
                @Override
                public int getDefaultHeight() {
                    return 230;
                }

                @Override
                public void Init(Object... params) {
                    fields.tfParent.setText(Global.visualiser.getWorkspace().getAbsolutePath());
                }

                @Override
                public void validateFields() {
                    Utils.validateFileShortNewName(fields.tfName.getText(), Log);
                    if (!fields.tfParent.getText().isEmpty()) {
                        if (Utils.ContainsCyrillic(fields.tfParent.getText()))
                            Log.Writeln_("Путь к целевой папке не может содержать кириллицу!");

                    } else Log.Writeln_("Путь к целевой папке не может быть пустым!");

                    if (Log.isEmpty()) {
                        dstFile = Paths.get(fields.tfParent.getText(), fields.tfName.getText()).toFile();
                        if (dstFile.exists())
                            Log.Writeln_("Файл " + dstFile.getAbsolutePath() + " уже существует!");
                    }
                }
            };
            if (dialog.ShowDialog(getDescription())) {
                migrateData = dialog.fields.MigrateData.isSelected();
                return true;
            }
        }
        return false;
    }

    @Override
    protected String getIconPath() {
        return "/icons/Transformations/CopyProject.png";
    }

    @Override
    protected void body() throws Exception {
        FileUtils.forceMkdir(dstFile);
        target.Clone(dstFile, migrateData);
    }

    @Override
    protected void performDone() throws Exception {
        if (UI.Question("копия текущего проекта успешно создана по адресу\n" + dstFile.getAbsolutePath() + "\nОткрыть её")) {
            passes.get(PassCode_2021.CloseCurrentProject).Do();
            passes.get(PassCode_2021.OpenCurrentProject).Do(dstFile);
        }
    }
}
