package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.Passes.SilentSapforPass;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class SPF_StatisticAnalyzer extends SilentSapforPass {
    public RunTask task;

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            task = (RunTask) args[0];
            return true;
        }
        return false;
    }

    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                code().toString(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                Utils.DQuotes(task.getLocalStsFile().getAbsolutePath()) +
                        " "
                        + Utils.DQuotes(target.getStsFile().getAbsolutePath()));

    }

    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() == 0;
    }

    @Override
    public boolean needsConfirmations() {
        return false;
    }

    @Override
    protected void performDone() throws Exception {
        if (target.getStsFile().exists()) {
            Files.copy(target.getStsFile().toPath(), task.getLocalStsTextFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
}
