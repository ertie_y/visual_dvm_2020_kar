package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Passes.ToolBarPass;
import Common.UI.UI;
import GlobalData.Recipient.DBRecipient;
import Repository.BugReport.BugReport;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;

public class ExtractRecipients extends Pass_2021 {

    @Override
    public boolean needsConfirmations() {
        return true;
    }

    Vector<String> new_recipients = new Vector<>();

    @Override
    protected String getIconPath() {
        return "/icons/LastOpened.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void body() throws Exception {
        new_recipients.clear();
        for (BugReport bugReport : RepositoryServer.db.bugReports.Data.values()) {
            Vector<String> recipients = bugReport.getRecipients(false);
            for (String email : recipients)
                if (!Global.db.recipients.Data.containsKey(email)) {
                    Global.db.Insert(new DBRecipient("отредактируйте имя", email));
                    new_recipients.add(email);
                }
        }
    }

    @Override
    protected void showDone() throws Exception {
        Global.db.recipients.ShowUI();
        String finalMessage = "Найдено "+new_recipients.size()+" новых адресатов.";
        if (!new_recipients.isEmpty())
        finalMessage += "\n"+String.join("\n", new_recipients);
        UI.Info(finalMessage);
    }
}
