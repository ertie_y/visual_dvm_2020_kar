package Visual_DVM_2021.Passes.All;

import Common.Utils.Utils;
import Visual_DVM_2021.Passes.ProcessPass;

public class LocalGetCompilerHelp extends ProcessPass<String> {
    String command = "";
    @Override
    protected boolean canStart(Object... args) throws Exception {
        command = (String) args[0];
        return true;
    }
    @Override
    protected void body() throws Exception {
        target = Utils.readAllOutput(Runtime.getRuntime().exec(command));
    }
}
