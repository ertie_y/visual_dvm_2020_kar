package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.FileNameForm;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileType;
import Visual_DVM_2021.Passes.ChangeFilePass;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.io.File;
import java.nio.file.Paths;
public class RenameFile extends ChangeFilePass<DBProjectFile> {
    DBProjectFile old;
    boolean current;
    @Override
    protected boolean canStart(Object... args) {
        resetArgs();
        current = false;
        if (Current.Check(Log, Current.SelectedFile)) {
            old = Current.getSelectedFile();
            current = Current.HasFile() && Current.getFile().file.equals(old.file);
            if ((ff = new FileNameForm()).ShowDialog("Введите новое имя файла", old.file.getName())) {
                fileName = ff.Result;
                //->
                parent_node = Current.getProjectCurrentParentNode();
                target_dir = (File) parent_node.getUserObject();
                //->
                dst = Paths.get(target_dir.getAbsolutePath(), fileName).toFile();
                if (dst.exists()) {
                    Log.Writeln_("Файл с именем " + Utils.Brackets(fileName) + " уже существует");
                    return false;
                }
                target = new DBProjectFile(dst, project);
                if (target.fileType == FileType.forbidden)
                    Log.Writeln_("Расширение " + Utils.Brackets(Utils.getExtension(dst)) + " недопустимо");
                return true;
            }
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (current)
            passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        if (!old.file.renameTo(dst)) throw new Exception("Не удалось переименовать файл");
        project.db.Delete(old);
        old.node.setUserObject(target);
        target.node = old.node;
        target.file = dst;
        target.RefreshName();
        project.db.Insert(target);
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
        Current.set(Current.SelectedFile, target);
        if (current)
            passes.get(PassCode_2021.OpenCurrentFile).Do(target);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
