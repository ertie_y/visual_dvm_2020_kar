package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.Task;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import org.apache.commons.io.FileUtils;

import java.util.Vector;
import java.util.stream.Collectors;

public class DeleteDebugResults extends CurrentProjectPass {
    @Override
    protected void showPreparation() throws Exception {
        Global.db.compilationTasks.ClearUI();
        Global.db.runTasks.ClearUI();
    }

    @Override
    protected void body() throws Exception {
        Vector<Task> toDelete = Global.db.compilationTasks.Data.values().stream().filter(task -> task.belongsToProject(target)).collect(Collectors.toCollection(Vector::new));
        Global.db.runTasks.Data.values().stream().filter(task -> task.belongsToProject(target)).forEach(toDelete::add);
        for (Task task: toDelete){
            Utils.forceDeleteWithCheck(task.getLocalWorkspace());
            Global.db.Delete(task);
        }
    }

    @Override
    protected void showFinish() throws Exception {
        Global.db.compilationTasks.ShowUI();
        Global.db.runTasks.ShowUI();
    }
}
