package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;

public class DeleteSelectedRunTasks extends Pass_2021<Vector<RunTask>> {
    @Override
    protected String getIconPath() {
        return "/icons/Delete.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        Global.db.runTasks.Data.values().stream().filter(task -> task.isVisible() && task.isSelected() && task.isPassive()).forEach(task -> target.add(task));
        if (target.isEmpty()) {
            Log.Writeln_("Не отмечено ни одной задачи для удаления.");
            return false;
        } else return true;
    }

    @Override
    protected void showPreparation() throws Exception {
        Global.db.runTasks.ClearUI();
    }

    @Override
    protected void body() throws Exception {
        for (RunTask task : target) {
            Global.db.Delete(task);
            Utils.forceDeleteWithCheck(task.getLocalWorkspace());
        }

    }

    @Override
    protected void showFinish() throws Exception {
        Global.db.runTasks.ShowUI();
    }
}
