package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.DebugPrintLevel;
import Common.UI.UI;
import Common.Utils.Files.VDirectoryChooser;
import Common.Utils.Utils;
import GlobalData.DBLastProject.DBLastProject;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import ProjectData.Project.db_project_info;

import java.io.File;
public class OpenCurrentProject extends Pass_2021<db_project_info> {
    enum Mode {
        Undefined,
        Directory,
        ProjectInfo
    }
    Mode mode = Mode.Undefined;
    File dir = null;
    boolean root_changes;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("Выбор домашней папки проекта");
    void restoreBrowserPath() {
        String last_dir_home =
                Global.db.settings.get(SettingName.ProjectsSearchDirectory).Value;
        if (!last_dir_home.isEmpty())
            directoryChooser.SetCurrentDirectory(last_dir_home);
    }
    boolean needsOpen() {
        return !Current.HasProject() || !Current.getProject().Home.equals(dir);
    }
    //-----------------
    @Override
    protected boolean canStart(Object... args) throws Exception {
        mode = Mode.Directory;
        dir = null;
        target = null;
        if (args.length == 0) {
            restoreBrowserPath();
            dir = directoryChooser.ShowDialog();
        } else {
            Object arg = args[0];
            if (arg instanceof File) {
                dir = (File) arg;
            } else if (arg instanceof db_project_info) {
                mode = Mode.ProjectInfo;
                target = (db_project_info) arg;
                dir = target.Home;
                Global.Log.Print(DebugPrintLevel.Project, "готовая версия " + Utils.Brackets(dir.getAbsolutePath()));
                return needsOpen();
            }
        }
        if ((dir != null) && needsOpen()) {
            Global.Log.Print(DebugPrintLevel.Project, Utils.Brackets(dir.toString()));
            if (!dir.isDirectory()) {
                Log.Writeln_(Utils.Brackets(dir) + "\nне является папкой!");
                return false;
            }
            if (dir.getName().equals(db_project_info.data)) {
                Log.Writeln_(Utils.Brackets(dir) + "\nявляется служебной папкой визуализатора!");
                return false;
            }
            return Utils.validateFolder(dir, Log);
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
    }
    @Override
    protected void body() throws Exception {
        root_changes = true;
        switch (mode) {
            case Directory:
                if (Current.HasRoot()) {
                    db_project_info root = Current.getRoot();
                    db_project_info project = root.find_version_r(dir);
                    if (project != null) {
                        Global.Log.Print("версия найдена в текущем корне");
                        //версия уже существует. и выстраивать дерево второй раз не нужно.
                        //как и отображать дерево.
                        target = project;
                        target.Open();
                        root_changes = false;
                        return;
                    }
                }
                Global.Log.Print(DebugPrintLevel.Project, "построение дерева версий");
                target = new db_project_info(dir);
                target.CreateVersionsTree();
                target.Open();
                break;
            case ProjectInfo:
                //если нам суют версию. значит уже априори корень не сменится.
                root_changes = false;
                target.Open();
                break;
        }
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.Project, target);
        if (root_changes)
            Current.set(Current.Root, target.root);
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                SettingName.ProjectsSearchDirectory, dir.getParent()
        );
        DBLastProject lastProject;
        if (Global.db.lastProjects.containsKey(target.Home.getAbsolutePath())) {
            lastProject = Global.db.lastProjects.get(target.Home.getAbsolutePath());
            lastProject.RefreshOpenTime();
            Global.db.Update(lastProject, "lastOpened");
        } else {
            lastProject = new DBLastProject(target);
            Global.db.Insert(lastProject);
        }
        target.setInfo(lastProject);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().ShowProject();
        UI.getNewMainWindow().getTestingWindow().SelectProjectData(false);
        //криво. но при тихом режиме открытие файлов все равно не понадобится.
        passes.get(PassCode_2021.OpenCurrentFile).Do(target.getLastOpenedFile());
        if (root_changes)
            UI.getNewMainWindow().ShowRoot();
    }
    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusProject();
    }
}
