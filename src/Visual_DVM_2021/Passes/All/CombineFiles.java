package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.UI.Windows.Dialog.Text.ComboTextDialog;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.Includes.DependencyInfo;
import ProjectData.SapforData.Includes.FileInfo;
import Repository.Component.Sapfor.TransformationPermission;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Transformation;
import Visual_DVM_2021.UI.Main.CombineFilesDialog;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Vector;

public class CombineFiles extends Transformation {
    ComboTextDialog fd = null;
    File result = null;

    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetIncludeDependencies;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        switch (Global.transformationPermission) {
            case All:
                return super.canStart(args) && target.CheckSameStyle (Log) &&
                        (fd = new CombineFilesDialog()).ShowDialog("Выберите имя итогового файла", target.files_order);
            case VariantsOnly:
                if (getPermission().equals(TransformationPermission.VariantsOnly)) {
                    return super.canStart(args) && target.CheckSameStyle (Log) &&(
                            (fd = new CombineFilesDialog()).ShowDialog("Выберите имя итогового файла", target.files_order)
                    );
                } else {
                    Log.Writeln_("Разрешено только построение параллельных вариантов!");
                    return false;
                }
            case None:
                Log.Writeln_("Нет разрешения на выполнение преобразований");
                return false;
        }
        return false;
    }

    //правила инклудов https://docs.oracle.com/cd/E19957-01/805-4939/6j4m0vna1/index.html
    @Override
    protected void body() throws Exception {
        result = Paths.get(target.last_version.Home.getAbsolutePath(),
                Global.isWindows ? fd.Result : Utils.toU(fd.Result)).toFile();
        //-----------------------
        //получить список хедеров.

        //-----------------------

        Vector<String> result_lines = new Vector<>();
        Vector<String> all_includes = new Vector<>();
        //-----------------------------
        result_lines.add("!-Found " + target.allIncludes.size() + " headers");
        for (String name : target.allIncludes.keySet()) {
            all_includes.add("      include " + Utils.Quotes(name));
            result_lines.add("!      include " + Utils.Quotes(name));
        }
        result_lines.add("!-Collapse-" + target.files_order.size() + " files");
        int i = 1;
        for (String name : target.files_order) {
            result_lines.add("! -- " + i + ". " + Utils.Brackets(name));
            ++i;
        }
        result_lines.add("!--------------------");
        for (String name : target.files_order) {
            //если есть инклуды начинаем мучиться.
            if (target.addictedFiles.containsKey(name)) {
                DBProjectFile file = target.db.files.Data.get(name);
                //---------------------------------------------------->>>
                List<String> file_lines = FileUtils.readLines(file.file);
                for (String line : file_lines) {
                    String header = Utils.extractHeaderName(line);
                    if (header != null) {
                        if (file.relativeHeaders.containsKey(header))
                            result_lines.add("      include " + Utils.Quotes(file.relativeHeaders.get(header).name));
                    } else
                        result_lines.add(line);
                }
            } else {
                //инклудов нет. добавляем все подряд.
                result_lines.addAll(FileUtils.readLines(target.db.files.Data.get(name).file));
            }
        }

        FileUtils.writeLines(result, result_lines, false);
        //-------------------------------
        //теперь скопировать остальные файлы
        for (String name : target.db.files.Data.keySet()) {
            if (!target.files_order.contains(name)) {
                Files.copy(
                        target.db.files.Data.get(name).file.toPath(),
                        Paths.get(target.last_version.Home.getAbsolutePath(), name)
                );
            }
        }
    }
}
