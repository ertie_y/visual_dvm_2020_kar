package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Repository.BugReport.BugReport;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.DeleteObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class DeleteBugReport extends DeleteObjectPass<BugReport> {
    public DeleteBugReport() {
        super(BugReport.class);
    }

    @Override
    protected boolean canStart(Object...args) throws Exception {
        return super.canStart(args) && Current.getAccount().CheckAccessRights(target.sender_address, Log);
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.DeleteBugReportFromServer).Do(target);
    }
    @Override
    protected Database getDb() {
        return RepositoryServer.db;
    }
}
