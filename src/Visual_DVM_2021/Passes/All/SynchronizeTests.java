package Visual_DVM_2021.Passes.All;


import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;

import Repository.Server.RepositoryServer;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.RepositoryPass_2021;
import Visual_DVM_2021.Properties.PropertyName;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class SynchronizeTests extends RepositoryPass_2021 {
    File new_db_file;

    @Override
    protected String getIconPath() {
        return "/icons/ComponentsActual.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void showPreparation() throws Exception {
        TestingSystem.db.tests.ClearUI();
    }

    @Override
    protected void ServerAction() throws Exception {
        new_db_file = Utils.getTempFileName("tdb");
        ReceiveFile(RepositoryServer.home + "Data/" + Global.properties.get(PropertyName.TestsDBName), new_db_file);
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        TestingSystem.db.Disconnect();
        Utils.forceDeleteWithCheck(TestingSystem.db.getFile());
        FileUtils.moveFile(new_db_file, TestingSystem.db.getFile());
        TestingSystem.db.Connect();
        TestingSystem.db.Synchronize();
        //убрать пересоздание таблиц. ибо сносится связь с интерфейсом окна.
    }

    @Override
    protected void showDone() throws Exception {
        if (UI.HasNewMainWindow())
            UI.getNewMainWindow().getTestingWindow().ShowAll();
    }
}
