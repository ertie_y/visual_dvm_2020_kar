package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Index;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.Arrays.ProjectArray;
import Visual_DVM_2021.Passes.SapforAnalysis;

import java.math.BigInteger;
public class SPF_GetAllDeclaratedArrays extends SapforAnalysis {
    @Override
    public String phase() {
        return "GET_ALL_ARRAY_DECL";
    }
    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        target.declaratedArrays.clear();
        for (DBProjectFile f : target.db.files.Data.values())
            f.array_decls.clear();
    }
    @Override
    protected void showPreparation() {
        UI.getNewMainWindow().getProjectWindow().ShowNoArrays();
        if (Current.HasFile())
            Current.getFile().form.ShowNoArrays();
    }
    @Override
    public void unpack(String packed) throws Exception {
        if (packed.toCharArray()[packed.length() - 1] == '\0')
            packed = packed.substring(packed.length() - 1);
        if (packed.length() == 0)
            return;
        String[] splitedPackedList = packed.split("@");
        for (String elem : splitedPackedList) {
            String[] info = elem.split("#");
            Index idx = new Index();
            //тут знак.
            ProjectArray arrayToAdd = new ProjectArray(info, idx,
                    BigInteger.ONE);
            target.declaratedArrays.put(arrayToAdd.id, arrayToAdd);
        }
        for (DBProjectFile file : target.db.files.Data.values())
            file.ArrayGraphTitle = "Объявлений: " + file.array_decls.size();
        target.UpdateArraysCount();
    }
    @Override
    protected boolean alwaysCheck() {
        return true;
    }
    @Override
    protected boolean isAtomic() {
        return false;
    }
    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().getProjectWindow().FocusArrays();
        if (Current.HasFile())
            Current.getFile().form.FocusArrays();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowArrays();
        UI.getNewMainWindow().getProjectWindow().ShowArraysCount();
        if (Current.HasFile())
            Current.getFile().form.ShowArrays();
        super.showDone();
    }
}
