package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.ChangeFilePass;
import Visual_DVM_2021.Passes.PassCode_2021;
import org.apache.commons.io.FileUtils;
public class DeleteFile extends ChangeFilePass<DBProjectFile> {
    @Override
    protected boolean canStart(Object... args) {
        resetArgs();
        return (Current.Check(Log, Current.SelectedFile)) &&
                UI.Warning("Удалить файл "
                        + Utils.Brackets((target = Current.getSelectedFile()).name));
    }
    @Override
    protected void performPreparation() throws Exception {
        if (Current.HasFile()&&(Current.getFile().file.equals(target.file)))
            passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        target.node.removeFromParent();
        project.db.Delete(target);
        Utils.forceDeleteWithCheck(target.file);
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.SelectedFile, null);
        Current.set(Current.ProjectNode, null);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
