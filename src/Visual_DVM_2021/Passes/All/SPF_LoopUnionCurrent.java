package Visual_DVM_2021.Passes.All;
import Common.Current;
import Visual_DVM_2021.Passes.SapforFilesModification;
//  SPF_LoopUnionCurrent (addOpt1_c -> file, addOpt2_c-> line)
public class SPF_LoopUnionCurrent extends SapforFilesModification {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)&&Current.Check(Log, Current.File)){
            addOpt1 = Current.getFile().name;
            addOpt2 = String.valueOf(Current.getFile().form.getEditor().getCurrentLine());
            return true;
        }
        return false;
    }
}
