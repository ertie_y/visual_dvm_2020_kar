package Visual_DVM_2021.Passes.All;

import Common.UI.UI;

public class SaveBugReportDescription extends UpdateBugReportField{
    @Override
    protected String getIconPath() {
        return "/icons/Save.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("description", UI.getNewMainWindow().getCallbackWindow().getBugReportDescriptionText());
    }
}
