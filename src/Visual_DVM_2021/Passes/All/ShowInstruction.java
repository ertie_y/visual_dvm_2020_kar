package Visual_DVM_2021.Passes.All;

import Common.Global;
import Repository.Component.ComponentType;
import Visual_DVM_2021.Passes.Pass_2021;

import java.awt.*;
import java.io.File;

public class ShowInstruction extends Pass_2021<File> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = Global.Components.get(ComponentType.Instruction).getFile();
        if (!target.exists()){
            Log.Writeln("файл инструкции:\n"+target.getAbsolutePath()+"\nне найден! Обновите компонент 'Инструкция'");
            return false;
        }
        return true;
    }
    @Override
    protected void body() throws Exception {
        if (Desktop.isDesktopSupported()) {
            Desktop.getDesktop().open(target);
        }
    }
}
