package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import ProjectData.SapforData.Arrays.ProjectArray;
import Visual_DVM_2021.Passes.SilentSapforPass;

import java.util.Vector;

public class SPF_GetArrayLinks extends SilentSapforPass {

    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                target.getProjFile().getAbsolutePath());
    }

    protected void unpack(String packed) throws Exception {
        //----------------------------------------
        /*
        map<DIST::Array*, set<DIST::Array*» arrayLinksByFuncCalls;как
            id_1|number_1|link_id1|link_id2|link_id3|id_2|number_2|link_id1|link_id2|link_id3
         */
        //эту инфу суем в список объявленных массивов, чтобы не плодить лишних таблиц.

        System.out.println(packed);
        String[] data = packed.split("\\|");
        long current_id;
        int links_size=0;

        ProjectArray current_array = null;
        System.out.println("parsing..");
        int j=0;
        for (int i=0; i<data.length; ++i){
            switch (j){
                case 0:
                    current_id = Long.parseLong(data[i]);
                    current_array =
                            target.declaratedArrays.get(current_id);
                    current_array.links.clear();
                    System.out.print (current_array.id+"[");
                    j++;
                    break;
                case 1:
                    links_size = Integer.parseInt(data[i]);
                    System.out.print(links_size+"]-> {");
                    j++;
                    break;
                default:
                    if (links_size>0){
                        long link_id = Long.parseLong(data[i]);
                        System.out.print(link_id+",");
                        current_array.links.put(link_id,target.declaratedArrays.get(link_id));
                        links_size--;
                    }
                    if (links_size==0) {
                        j = 0;
                        current_array=null;
                        System.out.println("}");
                    }
                    break;
            }
        }
        System.out.println("DONE");
        /*
        for (long id: target.arraysLinks.keySet()){
            System.out.print(id+"|");
            System.out.print(target.arraysLinks.get(id).size()+"|");
            for (long link_id: target.arraysLinks.get(id))
                System.out.print(link_id+"|");
        }
         */
    }
    @Override
    protected void performDone() throws Exception {
        if (!sapfor.getResult().isEmpty()) {
            unpack(sapfor.getResult());
        }
    }
}
