package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.RunStsRecord.RunStsRecord;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteRunStsRecord extends DeleteObjectPass<RunStsRecord> {
    public DeleteRunStsRecord() {
        super(RunStsRecord.class);
    }
    @Override
    protected void body() throws Exception {
        super.body();
        Utils.forceDeleteWithCheck(target.getSts());
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
