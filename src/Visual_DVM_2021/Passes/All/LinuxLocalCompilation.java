package Visual_DVM_2021.Passes.All;

import Common.Current;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.Passes.TaskLocalPass;
import GlobalData.Tasks.Supervisor.Local.Linux.LinuxLocalCompilationSupervisor;
import ProjectData.Project.db_project_info;

public class LinuxLocalCompilation extends TaskLocalPass<LinuxLocalCompilationSupervisor> {
    public LinuxLocalCompilation() {
        super(LinuxLocalCompilationSupervisor.class);
    }
    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((CompilationTask) args[0], this,(db_project_info) args[1]);
        return true;
    }
}
