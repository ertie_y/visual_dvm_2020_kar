package Visual_DVM_2021.Passes.All;

import Repository.BugReport.BugReport;

public class SaveBugReportRecipients extends UpdateBugReportField{
    @Override
    protected String getIconPath() {
        return "/icons/Save.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("targets", BugReport.getPackedTargets());
    }
}
