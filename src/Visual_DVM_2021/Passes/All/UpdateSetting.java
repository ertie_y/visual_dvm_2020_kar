package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.UI.Windows.Dialog.PercentsForm;
import Common.UI.Windows.Dialog.SliderNumberForm;
import Common.UI.Windows.Dialog.Text.TextFieldDialog;
import Common.Utils.Files.VDirectoryChooser;
import Common.Utils.Files.VFileChooser;
import Common.Utils.Utils;
import GlobalData.Settings.DBSetting;
import GlobalData.Settings.UI.GlobalMenu;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.io.File;

public class UpdateSetting extends Pass_2021<DBSetting> {
    //----------
    public static GlobalMenu current_menu;
    public static MenuElement[] last_menu_path;
    String NewValue;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("");
    VFileChooser fileChooser = new VFileChooser("", "exe");
    boolean silent;

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = Global.db.settings.get(args[0]);
        NewValue = target.Value;
        silent = false;
        if (args.length == 1) {
            //интерфейсный режим. получение по клику на пункт меню.
            switch (target.settingType) {
                case SapforFlag:
                    NewValue = target.toBoolean() ? "0" : "1";
                    break;
                case PercentField:
                    PercentsForm f = new PercentsForm();
                    if (f.ShowDialog(target.Name.getDescription(), target.toInt32()))
                        NewValue = String.valueOf(f.Result);
                    break;
                case StringField:
                    switch (target.Name) {
                        case Workspace:
                            directoryChooser.setTitle(target.Name.getDescription());
                            File dir = directoryChooser.ShowDialog();
                            if (dir != null)
                                NewValue = dir.getAbsolutePath();
                            break;
                        case LocalMakePathWindows:
                            fileChooser.setTitle(target.Name.getDescription());
                            File file = fileChooser.ShowDialog();
                            if (file != null)
                                NewValue = file.getAbsolutePath();
                            break;
                        default:
                            TextFieldDialog ff = new TextFieldDialog();
                            if (ff.ShowDialog(target.Name.getDescription(), target.Value)) {
                                NewValue = ff.Result;
                                if (NewValue.length() == 0)
                                    NewValue = " ";
                            }
                            break;
                    }
                    break;
                case IntField:
                    int min = 0;
                    int max = 0;
                    switch (target.Name) {
                        case LastOpenedProjectsCount:
                            min = 1;
                            max = 25;
                            break;
                        case FastAccessPassesCount:
                            min = 5;
                            max = 15;
                            break;
                        case Kernels:
                            min=1;
                            max= Utils.getMaxKernels();
                            break;
                    }
                    SliderNumberForm fff = new SliderNumberForm();
                    if (fff.ShowDialog(target.Name.getDescription(), target.toInt32(), min, max))
                        NewValue = String.valueOf(fff.Result);
                    break;
            }
        } else {
            NewValue = args[1].toString();
            silent = true;
        }
        //программный, тихий режим.}
        //--
        System.out.println(target.Value + "/" + NewValue);
        return !NewValue.equals(target.Value);
    }

    @Override
    protected void body() throws Exception {
        target.Value = NewValue;
        Global.db.Update(target, "Value");
    }

    @Override
    protected void showDone() throws Exception {
        if (target.Visible) {
            target.Mark();
            switch (target.Name) {
                case GCOVLimit:
                    if (Current.HasFile())
                        Current.getFile().form.ShowGCOV();
                    break;
                case FastAccessPassesCount:
                    UI.getNewMainWindow().RefreshFastAccess();
                    break;
                case DarkThemeOn:
                    UI.refreshTheme();
                    break;
                case ShowFullArraysDeclarations:
                    if (Current.HasProject())
                        Current.getProject().declaratedArrays.ShowUI();
                    break;
                case ShowFullTabsNames:
                    UI.getNewMainWindow().getTestingWindow().RefreshTabsNames();
                    if (Current.HasProject())
                        UI.getNewMainWindow().getProjectWindow().RefreshTabsNames();
                    if (Current.HasFile())
                        Current.getFile().form.RefreshTabsNames();
                    break;
                case MPI_PROGRAM:
                    Pass_2021.passes.get(PassCode_2021.CleanAnalyses).Do();
                    break;
                case SmallScreen:
                    boolean small = target.toBoolean();
                    if (Current.HasProject())
                        UI.getNewMainWindow().SwitchScreen(small);
                    UI.getNewMainWindow().getCallbackWindow().SwitchScreen(small);
                    UI.getNewMainWindow().getTestingWindow().SwitchScreen(small);
                    break;
                case Precompilation:
                    Current.getSapfor().ResetAllAnalyses();
                    break;
            }
            if (!silent && current_menu != null) {
                switch (current_menu) {
                    case SapforSettings:
                        UI.getNewMainWindow().ShowSapforSettings();
                        break;
                    case VisualiserSettings:
                        UI.getNewMainWindow().ShowGlobalSettings();
                        break;
                    case ComparsionSettings:
                        UI.getNewMainWindow().getVersionsWindow().getMasterComparsionWindow().ShowSettings();
                        break;
                }
                MenuSelectionManager.defaultManager().setSelectedPath(last_menu_path);
            }
        }
    }


}
