package Visual_DVM_2021.Passes.All;

import Repository.BugReport.BugReportState;

public class OpenBugReport extends UpdateBugReportField{
    @Override
    protected String getIconPath() {
        return "/icons/OpenBugReport.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("state", BugReportState.active);
    }
}
