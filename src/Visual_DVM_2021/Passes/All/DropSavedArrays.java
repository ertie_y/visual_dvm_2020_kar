package Visual_DVM_2021.Passes.All;
import ProjectData.DBArray.DBArray;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class DropSavedArrays extends CurrentProjectPass {
    @Override
    protected void body() throws Exception {
        target.db.DeleteAll(DBArray.class);
    }
    @Override
    protected void showDone() throws Exception {
        target.db.savedArrays.ShowUI();
    }
}
