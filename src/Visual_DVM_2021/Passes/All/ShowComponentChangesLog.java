package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.Windows.Dialog.Text.ReadOnlyMultilineTextForm;
import Common.Utils.Utils;
import Repository.Component.Component;
import Visual_DVM_2021.Passes.RepositoryPass_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.List;
import java.util.Vector;

public class ShowComponentChangesLog extends RepositoryPass_2021<Component> {
    File res;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Component)) {
            target = Current.getComponent();
            return true;
        }
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        res = Utils.getTempFileName("component_changes");
    }
    @Override
    protected void ServerAction() throws Exception {
        // form.ShowMessage1("Загрузка журнала...");
        ReceiveFile(target.getChangesRemotePath(), res);
    }
    @Override
    protected boolean validate() {
        return res.exists();
    }
    @Override
    protected void showDone() throws Exception {
        ReadOnlyMultilineTextForm ff = new ReadOnlyMultilineTextForm();
        List<String> lines = FileUtils.readLines(res);
        Vector<String> res = new Vector<>();
        for (int i=lines.size()-1; i>=0; i--)
            res.add(lines.get(i));
      ff.ShowDialog("Журнал изменений компонента " +
              Utils.Brackets(target.getComponentType().getDescription()),
              String.join("\n", res)
      );
    }
}
