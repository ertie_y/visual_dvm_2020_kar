package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class ApplyUser extends CurrentProjectPass {
    @Override
    protected String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return  super.canStart(args)&&Current.Check(Log, Current.User);
    }
    @Override
    protected void body() throws Exception {
        target.UpdateUser(Current.getUser());
    }
    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getTestingWindow().ShowProjectUser();
    }
}
