package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.EmailMessage;
import Repository.Server.RepositoryServer;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.util.Date;
import java.util.Random;

public class AppendBugReportField extends RepositoryPass_2021<BugReport> {
    String fieldName;
    String oldValue;
    String addition;
    String newValue;

    @Override
    protected String getIconPath() {
        return "/icons/Append.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.BugReport)) {
            target = Current.getBugReport();
            if (!target.CheckNotDraft(Log))
                return false;
            fieldName = (String) args[0];
            addition = (String) args[1];
            if (addition.isEmpty()) {
                Log.Writeln_("Дополнение не может быть пустым.");
                return false;
            }
            return Current.getAccount().CheckAccessRights(target.sender_address, Log);
        }
        return false;
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ActualizeBugReport, target.id));
        target.Actualize((BugReport) response.object);
        oldValue = (String) BugReport.class.getField(fieldName).get(target);
        newValue = oldValue + "\n" + Utils.Brackets(Utils.print_date(
                new Date())) + " " + Current.getAccount().name
                + " : " + addition;
        //2. дописываем нужное поле.
        BugReport.class.getField(fieldName).set(target, newValue);
        //обновляем дату.
        target.change_date = new Date().getTime();
        RepositoryServer.db.Update(target, fieldName);
        RepositoryServer.db.Update(target, "change_date");
        //3. отправляем на сервер
        Command(new ServerExchangeUnit_2021(ServerCode.UpdateBugReportField, fieldName, target));
    }

    @Override
    protected void showFinish() throws Exception {
        RepositoryServer.db.bugReports.RefreshUI();
        UI.getNewMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }

    @Override
    protected void performDone() throws Exception {
        String message_header = target.getMailTitlePrefix();
        String message_text = "";
        switch (fieldName) {
            default:
                return;
            case "description":
                message_header += "описание дополнено";
                message_text = target.description
                ;
                break;
            case "comment":
                message_header += "комментарий дополнен";
                message_text = target.comment;
                break;
        }
        passes.get(PassCode_2021.Email).Do(
                new EmailMessage(
                        message_header + " " + Utils.Brackets(Current.getAccount().name),
                        message_text,
                        target.getRecipients()
                )
        );
    }
}
