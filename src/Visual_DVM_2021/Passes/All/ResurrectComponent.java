package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Files.VFileChooser;
import Visual_DVM_2021.Passes.CurrentComponentPass;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class ResurrectComponent extends CurrentComponentPass {
    File file;
    VFileChooser fileChooser;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            //тут таргет меняется, поэтому меняется и условие выбора файлов.
            fileChooser = new VFileChooser(
                    "Выбор версии компонента для восстановления",
                    new FileFilter() {
                        @Override
                        public boolean accept(File f) {
                            return f.isFile() &&
                                    f.getName().startsWith(target.getComponentType().toString());
                        }
                        @Override
                        public String getDescription() {
                            return target.getComponentType().toString() + "*";
                        }
                    }
            );
            fileChooser.SetCurrentDirectory(Global.BackUpsDirectory);
            return (file = fileChooser.ShowDialog()) != null;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        Files.copy(file.toPath(), target.getNewFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        target.Update();
    }
    @Override
    protected void performDone() throws Exception {
        target.InitialVersionCheck();
        if (target.CanBeUpdated())
            target.CheckIfNeedsUpdateOrPublish();

    }
    @Override
    protected void showDone() throws Exception {
        Global.RefreshUpdatesStatus();
    }
}
