package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;
import GlobalData.User.UserState;

import java.nio.file.Paths;

public class InitialiseUser extends Pass_2021 {
    @Override
    protected String getIconPath() {
        return "/icons/InitializeUser.png";
    }

    @Override
    public String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return Current.Check(Log, Current.Machine, Current.User);
    }

    @Override
    protected void body() throws Exception {
        switch (Current.getMachine().type){
            case Local:
                passes.get(PassCode_2021.LocalInitaliseUser).Do();
                break;
            case Server:
            case MVS_cluster:
                passes.get(PassCode_2021.RemoteInitialiseUser).Do();
                break;
            default:
                throw new Exception("Неопределенный тип пользователя");
        }
    }
    @Override
    protected void showFinish() throws Exception {
        Global.db.users.RefreshUI();
    }
}
