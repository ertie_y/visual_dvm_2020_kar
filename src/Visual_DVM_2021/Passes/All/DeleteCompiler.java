package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.Compiler.Compiler;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteCompiler extends DeleteObjectPass<Compiler> {
    public DeleteCompiler() {
        super(Compiler.class);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasProject()
                && Current.getProject().HasMakefile()
                && Current.getProject().getMakefile().DependsToCompiler(target))
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
