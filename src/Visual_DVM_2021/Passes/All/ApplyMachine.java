package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class ApplyMachine extends CurrentProjectPass {
    boolean res;
    @Override
    protected String getIconPath() {
        return "/icons/Apply.png";
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args)&&Current.Check(Log, Current.Machine);
    }
    @Override
    protected void body() throws Exception {
        res = target.UpdateMachine(Current.getMachine());
    }
    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getTestingWindow().ShowProjectMachine();
        if (res) {
            UI.getNewMainWindow().getTestingWindow().ShowProjectUser();
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
            UI.getNewMainWindow().getTestingWindow().ShowProjectRunConfiguration();
        }
    }
}
