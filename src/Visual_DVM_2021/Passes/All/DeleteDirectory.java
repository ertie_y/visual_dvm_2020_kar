package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.ChangeFilePass;
import Visual_DVM_2021.Passes.PassCode_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Vector;
public class DeleteDirectory extends ChangeFilePass {
    @Override
    protected boolean canStart(Object... args) {
        resetArgs();
        dst_node = Current.getProjectNode();
        if ((dst_node != null) && (dst_node.getUserObject() instanceof File)) {
            target_dir = Current.getSelectedDirectory();
            if (target_dir.equals(project.Home)) {
                Log.Writeln("Нельзя удалять домашнюю папку проекта.");
                return false;
            }
            return (UI.Warning("Удалить папку\n" + Utils.Brackets(target_dir.getAbsolutePath())
                    + "\n и всё её содержимое."));
        } else Log.Writeln_("Папка не выделена.");
        return false;
    }
    @Override
    protected void performPreparation() throws Exception {
        if (Current.HasFile() && (Utils.isAnchestor(Current.getFile().file, target_dir)))
            passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        FileUtils.deleteDirectory(target_dir);
        if (target_dir.exists()) throw new Exception("Не удалось удалить папку");
        dst_node.removeFromParent();
        Vector<DBProjectFile> to_delete = new Vector<>();
        for (DBProjectFile file : project.db.files.Data.values()) {
            if (Utils.isAnchestor(file.file, target_dir))
                to_delete.add(file);
        }
        //так как имя первичный ключ приходится удалять их из бд проекта.
        for (DBProjectFile file : to_delete) {
            file.CleanAll();
            file.father.db.Delete(file);
        }
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.SelectedDirectory, null);
        Current.set(Current.ProjectNode, null);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
