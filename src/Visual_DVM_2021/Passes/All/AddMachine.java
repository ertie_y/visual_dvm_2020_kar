package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachineType;
import GlobalData.User.User;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddMachine extends AddObjectPass<Machine> {
    public AddMachine() {
        super(Machine.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)){
            if (target.type.equals(MachineType.Local)&&Global.db.machines.LocalMachineExists()){
                Log.Writeln_("Локальная машина уже добавлена.");
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        getDb().Insert(new Compiler(
                target,
                "gfortran",
                CompilerType.gnu,
                "gfortran",
                "--version",
                "--help"
            ));
        getDb().Insert(new Compiler(
                target,
                "gcc",
                CompilerType.gnu,
                "gcc",
                "--version",
                "--help"
        ));
        getDb().Insert(new Compiler(
                target,
                "g++",
                CompilerType.gnu,
                "g++",
                "--version",
                "--help"
        ));
        if (target.type.equals(MachineType.Local))
            getDb().Insert(new User(target, "этот пользователь",""));
    }
}
