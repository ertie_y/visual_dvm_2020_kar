package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Scenario.Scenario;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditScenario extends EditObjectPass<Scenario> {
    public EditScenario() {
        super(Scenario.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
