package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.Database;
import Common.Global;
import GlobalData.Account.AccountRole;
import Repository.BugReport.BugReport;
import Repository.Component.ComponentType;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.AddObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Vector;

public class AddBugReport extends AddObjectPass<BugReport> {
    public AddBugReport() {
        super(BugReport.class);
    }

    @Override
    protected Database getDb() {
        return RepositoryServer.db;
    }

    @Override
    public boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().role.equals(AccountRole.Undefined)) {
            Log.Writeln_("Для создания отчёта требуется регистрация");
            return false;
        }
        target = d.newInstance();
        return Current.Check(Log, Current.Project);
    }

    @Override
    protected void performPreparation() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
    }

    @Override
    protected void body() throws Exception {
        String version = Current.getProject().Home.getAbsolutePath().substring(
                Current.getRoot().Home.getParent().length());
        if (version.toCharArray()[0] == '\\') version = version.substring(1);
        //------------------------------------------------------------------
        target.Init(Current.getAccount().name, Current.getAccount().email,
                "Черновик отчёта об ошибке.\nЗаполните описание ошибочной ситуации, и нажмите 'Опубликовать'",
                version);
        target.owner=Current.getProject().Home;
        super.body();

        Current.getRoot().cleanDepAndGCOVR(); //удаление депов и гкова

        //логи во вложения.
        File attachementsDir = Current.getProject().getAttachmentsDirectory();
        Vector<File> logs = new Vector<>();
        logs.add(Global.Log.getLogFile());
        logs.add(Paths.get(Global.ComponentsDirectory.getAbsolutePath(), "Sapfor_log.txt").toFile());
        logs.add(Paths.get(Global.ComponentsDirectory.getAbsolutePath(), "Server_log.txt").toFile());
        logs.add(Global.Components.get(ComponentType.Visualizer_2).getLogFile());
        for (File file : logs) {
            if (file.exists())
                Files.copy(file.toPath(), Paths.get(attachementsDir.getAbsolutePath(), file.getName()), StandardCopyOption.REPLACE_EXISTING);
        }
        //запаковка рута
        passes.get(PassCode_2021.ZipFolderPass).Do(Current.getRoot().Home.getAbsolutePath(), target.getArchiveFile().getAbsolutePath());
    }
}
