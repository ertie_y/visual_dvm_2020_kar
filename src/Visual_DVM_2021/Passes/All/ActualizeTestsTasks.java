package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import GlobalData.Tasks.TaskState;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.Tasks.TestTask;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.util.Vector;

public class ActualizeTestsTasks extends RepositoryPass_2021 {
    @Override
    protected boolean needsAnimation() {
        return true;
    }

    @Override
    protected String getIconPath() {
        return "/icons/ComponentsActual.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    protected boolean needRefresh = false;

    @Override
    protected void ServerAction() throws Exception {
        //проверяем теперь только задачи на запуск.
        needRefresh = false;
        Vector<Integer> ids = new Vector<>();
        for (TestRunTask task : TestingSystem.db.testRunTasks.Data.values()) {
            if (task.state.equals(TaskState.Waiting) || task.state.equals(TaskState.Running)
            || task.state.equals(TaskState.Finished)
            )
                ids.add(task.id);
        }
        Command(new ServerExchangeUnit_2021(ServerCode.ActualizeTestsTasks, "", ids));
        Vector<TestRunTask> actual = (Vector<TestRunTask>) response.object;
        System.out.println(actual.size());
        for (TestRunTask a_task : actual) {
            System.out.println("id = " + a_task.id + " state=" + a_task.state);
            if (TestingSystem.db.testRunTasks.containsKey(a_task.id)) {
                TestRunTask task = TestingSystem.db.testRunTasks.get(a_task.id);
                task.Refresh(a_task);
                TestingSystem.db.Update(task);
                needRefresh = true;
            }
        }
    }

    @Override
    protected void showDone() throws Exception {
        if (needRefresh) {
            TestRunTask rt = Current.getTestRunTask();
            TestingSystem.db.testRunTasks.ClearUI();
            if (rt != null)
                TestingSystem.db.testRunTasks.ShowUI(rt.id);
            else
                TestingSystem.db.testRunTasks.ShowUI();
        }
    }
}
