package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.Vector;

public class DeleteSelectedCompilationTasks extends Pass_2021<Vector<CompilationTask>> {
    @Override
    protected String getIconPath() {
        return "/icons/Delete.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = new Vector<>();
        Global.db.compilationTasks.Data.values().stream().filter(task -> task.isVisible() && task.isSelected() && task.isPassive()).forEach(task -> target.add(task));
        if (target.isEmpty()) {
            Log.Writeln_("Не отмечено ни одной задачи для удаления.");
            return false;
        } else return true;
    }

    @Override
    protected void showPreparation() throws Exception {
        Global.db.compilationTasks.ClearUI();
    }

    @Override
    protected void body() throws Exception {
        for (CompilationTask task : target) {
            for (RunTask runTask: task.getRunTasks().values()){
                Global.db.Delete(runTask);
                Utils.forceDeleteWithCheck(runTask.getLocalWorkspace());
            }
            Global.db.Delete(task);
            Utils.forceDeleteWithCheck(task.getLocalWorkspace());
        }
    }

    @Override
    protected void showFinish() throws Exception {
        Global.db.compilationTasks.ShowUI();
    }
}
