package Visual_DVM_2021.Passes.All;

import Common.Utils.Utils;
import Visual_DVM_2021.Passes.CurrentProjectPass;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.file.Paths;

public class MakeScreenShot extends CurrentProjectPass {
    File outputfile = null;

    @Override
    protected String getIconPath() {
        return "/icons/ScreenShot.png";
    }

    //http://www.nookery.ru/pishem-programmy-dly-sozdaniy-sreenshot/
    @Override
    protected void body() throws Exception {
        DisplayMode display = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDisplayMode();
        int x = display.getWidth();
        int y = display.getHeight();
        BufferedImage bufferedImage = new Robot().createScreenCapture(new Rectangle(x, y));
        //-
        String res_name = Utils.getDateName("screenshot") + ".png";
        outputfile = Paths.get(target.getAttachmentsDirectory().getAbsolutePath(), res_name).toFile();
        //-
        ImageIO.write(bufferedImage, "png", outputfile);
    }

    @Override
    protected void showDone() throws Exception {
       // UI.Info("Скриншот для текушего проекта сохранён во вложениях\nпод именем " + outputfile.getName());
        if (Desktop.isDesktopSupported()) {
          //  Desktop.getDesktop().open(outputfile.getParentFile());
            Desktop.getDesktop().open(outputfile);
        }
    }
}
