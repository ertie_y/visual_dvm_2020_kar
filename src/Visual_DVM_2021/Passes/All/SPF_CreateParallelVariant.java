package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.UI.UI;
import ProjectData.SapforData.Variants.ParallelVariant;
import Repository.Component.Sapfor.TransformationPermission;
import Visual_DVM_2021.Passes.SapforTransformation;

public class SPF_CreateParallelVariant extends SapforTransformation {
    @Override
    protected TransformationPermission getPermission() {
        return TransformationPermission.VariantsOnly;
    }

    ParallelVariant variant = null;

    @Override
    protected boolean hasStats() {
        return false;
    }

    @Override
    public boolean needsConfirmations() {
        return false;
    }

    @Override
    protected boolean needsAnimation() {
        return false;
    }

    @Override
    protected boolean resetAnalysesAfter() {
        return false;
    }

    @Override
    protected String getVersionLetter() {
        return "p";
    }

    @Override
    protected String getVersionDescription() {
        return variant.getDescription();
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            variant = (ParallelVariant) args[0];
            Options = variant.getVector();
            return true;
        }
        return false;
    }

    @Override
    protected void performDone() throws Exception {
        Global.transformationPermission = TransformationPermission.VariantsOnly;
        target.migrateFilesSettings(target.last_version, true, false, true);
        variant.UpdateStats(sapfor.getPredictorStats());
        variant.last_built = target.last_version;
    }

    @Override
    protected void performFinish() throws Exception {
    }

    @Override
    protected void showFinish() throws Exception {
    }

    @Override
    protected void FocusResult() {
    }
}
