package Visual_DVM_2021.Passes.All;

import Common.Database.Database;
import Common.Global;
import GlobalData.DVMParameter.DVMParameter;
import Visual_DVM_2021.Passes.EditObjectPass;

public class EditDVMParameter extends EditObjectPass<DVMParameter> {
    public EditDVMParameter() {
        super(DVMParameter.class);
    }

    @Override
    protected Database getDb() {
        return Global.db;
    }
}
