package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportState;
import Repository.EmailMessage;
import Repository.Server.RepositoryServer;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.io.Serializable;
import java.util.Date;

public class UpdateBugReportField extends RepositoryPass_2021<BugReport> {
    String fieldName;
    Serializable oldValue;
    Serializable actualValue;
    Serializable newValue;

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = Current.getBugReport();
        fieldName = (String) args[0];
        newValue = (Serializable) args[1];
        oldValue = (Serializable) BugReport.class.getField(fieldName).get(target);
        if (target.state.equals(BugReportState.draft)) {
            //если черновик, сразу меняем и не заморачиваемся, если нужно.
            //тут проход все равно сугубо интерфейсный.
            if (!newValue.equals(oldValue)) {
                //-
                BugReport.class.getField(fieldName).set(target, newValue);
                target.change_date = new Date().getTime();
                //-
                RepositoryServer.db.Update(target, fieldName);
                RepositoryServer.db.Update(target, "change_date");
                //-
                RepositoryServer.db.bugReports.RefreshUI();
            }
            return false;
        } else
            return Current.getAccount().CheckAccessRights(target.sender_address, Log);
    }

    @Override
    protected void ServerAction() throws Exception {
        //1. прежде чем дополнять поле(комментарий или описание) следует скачать с сервера
        //последнюю версию этого баг репорта.
        //все это должно быть в рамках одной транзакции с сервером!
        Command(new ServerExchangeUnit_2021(ServerCode.ActualizeBugReport, target.id));
        target.Actualize((BugReport) response.object);
        actualValue = (Serializable) BugReport.class.getField(fieldName).get(target);
        if (!newValue.equals(oldValue)) {
            //2. дописываем нужное поле.
            BugReport.class.getField(fieldName).set(target, newValue);
            //обновляем дату.
            target.change_date = new Date().getTime();
            //3. отправляем на сервер
            Command(new ServerExchangeUnit_2021(ServerCode.UpdateBugReportField, fieldName, target));
            RepositoryServer.db.Update(target, fieldName);
            RepositoryServer.db.Update(target, "change_date");
        }
    }

    @Override
    protected void showFinish() throws Exception {
        RepositoryServer.db.bugReports.RefreshUI();
        UI.getNewMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }

    @Override
    protected void performDone() throws Exception {
        String message_header = target.getMailTitlePrefix();

        /*
        String message_text = "DUMMY TEXT: ";
        Random random = new Random();
        for (int k = 0; k < random.nextInt(200); ++k)
            message_text += random.nextDouble();

         */
        String message_text = String.join("\n",
                target.description,
                target.comment);
        if (fieldName.equalsIgnoreCase("percentage"))
            message_header += "завершенность: " + Current.getBugReport().percentage + " %";
        switch (fieldName) {
            case "sender_address":
            case "sender_name":
            case "targets":
            case "percentage":
                return;
            case "description":
                message_header += "описание изменено";
                message_text = Utils.compareTexts(actualValue.toString(), target.description);
                break;
            case "comment":
                message_header += "комментарий изменён";
                message_text = Utils.compareTexts(actualValue.toString(), target.comment);;
                break;
            case "executor":
                message_header += target.executor + " назначается исполнителем";
                break;
            case "state":
                message_header += "состояние изменилось на " + Utils.Brackets(target.state.getDescription());
                break;
        }


        passes.get(PassCode_2021.Email).Do(
                new EmailMessage(message_header,
                        message_text,
                        target.getRecipients())
        );

        if (fieldName.equals("state")) {
            switch (target.state) {
                case closed:
                    if (target.percentage != 100)
                        passes.get(PassCode_2021.UpdateBugReportField).Do("percentage", 100);
                    break;
                case active:
                    if (target.percentage == 100)
                        passes.get(PassCode_2021.UpdateBugReportField).Do("percentage", 0);
                    break;
                default:
                    break;
            }
        }

    }
}
