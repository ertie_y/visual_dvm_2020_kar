package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;
public class SelectRemoteFile extends CurrentConnectionPass {
    boolean dialogOK;
    boolean needs_directory = false;
    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.Machine, Current.User)) {
            needs_directory = (boolean) args[0];
            Current.set(Current.RemoteFile, null);
            return true;
        }
        return false;
    }
    protected void ServerAction() throws Exception {
        dialogOK = (UI.getRemoteFileChooser().ShowDialog("Выбор " + (needs_directory ? "папки" : "файла"), this, needs_directory));
    }
    @Override
    protected boolean validate() {
        return dialogOK;
    }
}
