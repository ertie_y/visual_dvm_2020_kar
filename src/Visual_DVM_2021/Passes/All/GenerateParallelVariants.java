package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

import java.math.BigInteger;
public class GenerateParallelVariants extends CurrentProjectPass {
    @Override
    protected String getIconPath() {
        return "/icons/ParallelVariants.png";
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetArrayDistribution;
    }

    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().getProjectWindow().FocusDistribution();
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            BigInteger amount = target.getFilteredVariantsCount();
            if (amount.compareTo(BigInteger.valueOf(1600)) > 0) {
                Log.Writeln_("Количество вариантов " + amount + "\nпревышает 1600. Генерация запрещена.");
                return false;
            }
            return UI.Question("Будет сгенерировано " + amount + " вариантов. Хотите продолжить");
        }
        return false;
    }
    @Override
    protected void showPreparation() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowNoVariants();
    }
    @Override
    protected void body() throws Exception {
        target.gen_variants_vectors();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowVariants();
    }
}
