package Visual_DVM_2021.Passes.All;

import Common.UI.UI;

public class AppendBugReportDescription extends AppendBugReportField{
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("description",
                UI.getNewMainWindow().getCallbackWindow().getBugReportDescriptionAdditionText());
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        UI.getNewMainWindow().getCallbackWindow().ClearBugReportDescriptionAdditionText();
    }
}
