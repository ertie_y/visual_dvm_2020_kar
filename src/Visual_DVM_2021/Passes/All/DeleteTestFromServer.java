package Visual_DVM_2021.Passes.All;

import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

public class DeleteTestFromServer extends RepositoryPass_2021<Test> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (Test) args[0];
        return true;
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteTest, target.name));
    }
}
