package Visual_DVM_2021.Passes.All;

import Common.UI.UI;

public class AppendBugReportComment extends AppendBugReportField{
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("comment",
                UI.getNewMainWindow().getCallbackWindow().getBugReportCommentAdditionText());
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        UI.getNewMainWindow().getCallbackWindow().ClearBugReportCommentAdditionText();
    }
}
