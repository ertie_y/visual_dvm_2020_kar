package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.ProcessPass;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
public class DownloadRepository extends ProcessPass {
    String command;
    File dvmHome;
    File sapforHome;
    final String LOADED = "LOADED";
    @Override
    protected boolean canStart(Object... args) throws Exception {
        dvmHome = Paths.get(Global.RepoDirectory.getAbsolutePath(),
                "dvm").toFile();
        sapforHome = Paths.get(Global.RepoDirectory.getAbsolutePath(),
                "sapfor").toFile();
        return true;
    }
    private void synchronize(String src, File dst) throws Exception {
        File loadedFile = Paths.get(dst.getAbsolutePath(), LOADED).toFile();
        if (loadedFile.exists()) {
            PerformScript("cd " +
                    dst.getAbsolutePath() +
                    "\nsvn update\n");
        } else {
            Utils.CleanDirectory(dst);
            PerformScript("cd Repo\nsvn checkout " + src + "\n"); //export
            FileUtils.write(loadedFile, "+");
        }
    }
    @Override
    protected void body() throws Exception {
        form.ShowProgress(2, 0, true);
        synchronize("http://svn.dvm-system.org/svn/dvmhrepo/dvm", dvmHome);
        form.ShowProgress(2, 1, true);
        synchronize("http://svn.dvm-system.org/svn/dvmhrepo/sapfor\n", sapforHome);
        form.ShowProgress(2, 2, true);
    }
}
