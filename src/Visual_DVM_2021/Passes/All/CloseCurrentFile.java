package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
public class CloseCurrentFile extends Pass_2021<DBProjectFile> {
    @Override
    protected String getIconPath() {
        return "/icons/Close.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getFile()) != null;
    }
    @Override
    protected void body() throws Exception {
     //действия по закрытию. сохранение и т д.
        target.form.SaveSplitters();
        target.UpdateLastLine(target.form.getEditor().getCurrentLine());
        passes.get(PassCode_2021.Save).Do();
    }
    @Override
    protected void performDone() throws Exception {
        Current.set(Current.File, null);
        Current.set(Current.FileGraphElement,null);
        //-
        Current.set(Current.Notes,null);
        Current.set(Current.Warnings,null);
        Current.set(Current.Errors,null);
        //-
    }
    @Override
    protected void showDone() throws Exception {
        //отобразить отсутствие файла.
        UI.getNewMainWindow().getProjectWindow().ShowNoFile();
    }
}
