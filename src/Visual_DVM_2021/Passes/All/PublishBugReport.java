package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.BugReport.BugReportState;
import Repository.EmailMessage;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.io.File;
import java.util.Date;

public class PublishBugReport extends Pass_2021<BugReport> {
    @Override
    protected String getIconPath() {
        return "/icons/Publish.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.BugReport)){
            target = Current.getBugReport();
            if (!target.state.equals(BugReportState.draft)){
                Log.Writeln_("Отчёт об ошибке "+target.id+" уже опубликован!");
                return false;
            }
            if ((target.owner==null)||!target.owner.exists()){
                Log.Writeln_("Проект, соответствующий отчету, не существует.");
                return false;
            }
            if (target.executor.isEmpty()){
                UI.getNewMainWindow().getCallbackWindow().FocusRecipients();
                return UI.Question("Для отчёта об ошибке не назначен исполнитель.\nВсе равно опубликовать его");

            }
            if (!Current.HasProject()||!(Current.getProject().Home.equals(target.owner))){
                Log.Writeln("Чтобы опубликовать отчет, требуется открыть соответствующий ему проект\n"+
                        Utils.Brackets(target.owner)
                        );
                return false;
            }
            if  (!Current.getAccount().CheckAccessRights(target.sender_address, Log)&& (target.CheckDraft(Log))){
                return false;
            }
            if (Utils.getFileSizeMegaBytes(target.getArchiveFile()) > 15){
                Log.Writeln_("Размер вложения превышает 15 Мб");
                return false;
            }
            return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        target.targets = BugReport.getPackedTargets();
        target.change_date = new Date().getTime();
        RepositoryServer.db.Update(target);
        target.state = BugReportState.active;
    }
    @Override
    protected void performDone() throws Exception {
        //2 - Отправка на сервер, с активным состоянием?
        passes.get(PassCode_2021.SendBugReport).Do();
        RepositoryServer.db.Update(target, "state");
        //3- рассылка
        EmailMessage message = new EmailMessage("Обнаружена ошибка " + Utils.Brackets(target.id),
                target.getNewMailText(),
                target.getRecipients());
        message.addAttachement(target.getArchiveFile());

        //со скринами будет небольшой трабл. потому что теретически возможна ситуация,
        //что проект черновика бага уже закрыт.
        if (Current.HasProject()) {
            for (File screen : Current.getProject().getScreenShots())
                message.addAttachement(screen);
        }
        passes.get(PassCode_2021.Email).Do(message);
    }
    @Override
    protected void showDone() throws Exception {
        RepositoryServer.db.bugReports.RefreshUI();
        UI.getNewMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }
}
