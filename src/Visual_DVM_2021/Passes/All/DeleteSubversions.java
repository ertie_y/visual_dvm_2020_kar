package Visual_DVM_2021.Passes.All;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import ProjectData.Project.db_project_info;

import java.util.Vector;
public class DeleteSubversions extends CurrentProjectPass {
    @Override
    protected void body() throws Exception {
        Vector<db_project_info> targets = new Vector<>(target.versions.values());
        for (db_project_info m : targets) {
            passes.get(PassCode_2021.DeleteVersion).Do(m);
        }
    }
}
