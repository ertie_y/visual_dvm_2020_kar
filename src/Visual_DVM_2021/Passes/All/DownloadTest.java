package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

public class DownloadTest extends RepositoryPass_2021<Test> {
    @Override
    protected String getIconPath() {
        return "/icons/DownloadBugReport.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log) &&Current.Check(Log, Current.Test)) {
            target = Current.getTest();
            return true;
        }
        return false;
    }

    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
        Current.set(Current.Root, null); //чтобы гарантированно не существовало корня.
        Utils.forceDeleteWithCheck(target.getArchive());
        Utils.forceDeleteWithCheck(target.getHomePath());
    }

    @Override
    protected void showPreparation() throws Exception {
        super.showPreparation();
        UI.getNewMainWindow().ShowNoRoot();
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DownloadTest, target.name));
        response.Unpack(target.getArchive());
    }

    @Override
    protected boolean validate() {
        return target.getArchive().exists();
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();

        if (passes.get(PassCode_2021.UnzipFolderPass).Do(
                target.getArchive().getAbsolutePath(),
                Global.visualiser.getWorkspace().getAbsolutePath(), false
        ))
            if (UI.Question("Тестовый проект успешно загружен под именем\n" +
                    Utils.Brackets(target.getHomePath().getName()) +
                    "\nОткрыть его"))
                passes.get(PassCode_2021.OpenCurrentProject).Do(target.getHomePath());

    }
}

