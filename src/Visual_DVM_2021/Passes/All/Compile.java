package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.User.UserState;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

public class Compile extends Pass_2021<db_project_info> {
    Pass_2021 subpass = null;
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    @Override
    protected String getIconPath() {
        return "/icons/Start.png";
    }

    @Override
    protected boolean hasStats() {
        return true;
    }

    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.Project)) {
            target = Current.getProject();
            subpass = null;
            if (!target.hasMachine())
                Log.Writeln_("Не задана машина для проекта.");
            if (!target.hasUser())
                Log.Writeln_("Не задан пользователь для проекта.");
            else {
                if (target.getUser().state != UserState.ready_to_work)
                    Log.Writeln_("Пользователь " + Utils.Brackets(target.getUser().login) +
                            " не проинициализирован\nПерейдите на вкладку 'Настройки компиляции и запуска',\n" +
                            " и выполните команду 'Инициализация пользователя'\n");
            }
            if (!target.HasMakefile())
                Log.Writeln_("Не задан мейкфайл для проекта.");
            else
                target.getMakefile().Validate(Log);
            return Log.isEmpty();
        }
        return false;
    }

    @Override
    protected void performPreparation() throws Exception {
        target.CheckCompilationTask();
        Utils.forceDeleteWithCheck(target.getCompilationTask().getLocalWorkspace());
    }

    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().FocusTesting();
        UI.getNewMainWindow().getTestingWindow().FocusCompilationTasks();
    }

    @Override
    protected void showPreparation() throws Exception {
        Global.db.compilationTasks.ShowUI(target.getCompilationTask().getPK());
    }

    @Override
    protected void body() throws Exception {
        switch (target.getMachine().type) {
            case Local:
                if (Global.isWindows) {
                    subpass = passes.get(PassCode_2021.WindowsLocalCompilation);
                } else
                    subpass = passes.get(PassCode_2021.LinuxLocalCompilation);
                break;
            case Undefined:
                throw new Exception("Компиляция не реализована для типа машины " + Utils.DQuotes(target.getMachine().type));
            default:
                subpass = passes.get(PassCode_2021.RemoteCompilation);
                break;
        }
        subpass.Do(Current.getProject().getCompilationTask(), Current.getProject());
    }

    @Override
    protected boolean validate() {
        return (subpass != null) && subpass.isDone();
    }

    @Override
    protected void showFinish() throws Exception {
        Global.db.compilationTasks.ShowUI();
        UI.getNewMainWindow().getTestingWindow().SelectProjectData(false); //выбрать все данные по проекту
    }

    @Override
    protected void showCanNotStart() throws Exception {
        UI.getNewMainWindow().getTestingWindow().FocusCredentials();
    }
}
