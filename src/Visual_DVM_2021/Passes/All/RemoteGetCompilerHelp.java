package Visual_DVM_2021.Passes.All;

import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;

public class RemoteGetCompilerHelp extends CurrentConnectionPass<String> {
    String command = "";

    @Override
    protected boolean canStart(Object... args) {
        if (super.canStart(args)) {
            command = (String) args[0];
            return true;
        }
        return false;
    }

    @Override
    protected void ServerAction() throws Exception {
        target = ShellCommand(command);
    }
}
