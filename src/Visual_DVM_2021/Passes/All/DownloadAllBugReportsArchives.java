package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Repository.Server.RepositoryServer;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;

public class DownloadAllBugReportsArchives extends RepositoryPass_2021<File> {
    @Override
    protected String getIconPath() {
        return "/icons/DownloadAll.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentProject).Do();
        Current.set(Current.Root, null); //чтобы гарантированно не существовало корня.
        Utils.CleanDirectory(Global.BugReportsDirectory);
    }

    @Override
    protected int getTimeout() {
        return 60000;
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.ReceiveAllArchives));
        response.Unpack(target = Utils.getTempFileName("bugs"));
    }

    @Override
    protected boolean validate() {
        return target.exists();
    }

    @Override
    protected void performDone() throws Exception {
        File tempFolder = Utils.getTempFileName("bugsBuffer");
        //-
        passes.get(PassCode_2021.UnzipFolderPass).Do(
                target.getAbsolutePath(),
                tempFolder.getAbsolutePath()
        );
        //-
        //теперь скопировать это в папку Bugs, с нормальными именами через zip
        File t2 = Paths.get(tempFolder.getAbsolutePath(), "Bugs").toFile();
        File[] archives = t2.listFiles();
        if (archives != null) {
            for (File file : archives) {
                FileUtils.moveFile(file, Paths.get(Global.BugReportsDirectory.getAbsolutePath(), file.getName() + ".zip").toFile());
            }
        }
    }
}
