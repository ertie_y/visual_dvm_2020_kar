package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Makefile.UI.MakefilePreviewForm;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import ProjectData.Project.db_project_info;
public class ShowMakefilePreview extends Pass_2021<db_project_info> {
    @Override
    protected String getIconPath() {
        return "/icons/ShowPassword.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_ParseFilesWithOrder;
    }
    @Override
    protected boolean canStart(Object...args) {
        if (Current.Check(Log, Current.Project)) {
            target = Current.getProject();
            if (!target.HasMakefile()) {
                Log.Writeln_("мейкфайл не назначен или не существует");
            } else return true;
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        new MakefilePreviewForm().ShowDialog("Предпросмотр мейкфайла текущего проекта",
                target.getMakefile().Generate(target));
    }
}
