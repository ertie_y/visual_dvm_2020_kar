package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;
public class SPF_GetArrayDistribution extends SPF_GetArrayDistributionOnlyAnalysis {
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        SapforAnalysis code_analysis = (SapforAnalysis) passes.get(PassCode_2021.SPF_GetArrayDistributionOnlyAnalysis);
        if (!code_analysis.isDone()) {
            code_analysis.setDone();
            code_analysis.MarkAsDone();
        }
    }
    @Override
    protected void FocusResult() {
        super.FocusResult();
        UI.getNewMainWindow().getProjectWindow().FocusDistribution();
    }
}
