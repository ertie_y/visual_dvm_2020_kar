package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Repository.Server.RepositoryServer;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.util.Vector;

public class CompileCurrentTest extends RepositoryPass_2021<Test> {
    TestCompilationTask task = null;

    @Override
    protected String getIconPath() {
        return "/icons/Start.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.Check(Log, Current.Machine, Current.User, Current.Compiler, Current.Group, Current.Test)) {
            target = Current.getTest();
            return true;
        }
        return false;
    }

    @Override
    protected void ServerAction() throws Exception {
        task = new TestCompilationTask(Current.getMachine(), Current.getUser(), Current.getCompiler(), Current.getGroup(), Current.getTest());
        Command(new ServerExchangeUnit_2021(ServerCode.CompileTest, "", task));
        task = (TestCompilationTask) response.object;
        TestingSystem.db.Insert(task);
        System.out.println(task.id);
        System.out.println(task.runTasks);
        for (TestRunTask rt: task.runTasks) {
            System.out.println(rt.id);
            TestingSystem.db.Insert(rt);
        }
    }

    @Override
    protected void showDone() {
        TestingSystem.db.testCompilationTasks.ShowUI(task.id);
    }
}
