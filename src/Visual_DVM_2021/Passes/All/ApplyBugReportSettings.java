package Visual_DVM_2021.Passes.All;

import Common.Current;
import GlobalData.Settings.SettingName;
import Repository.BugReport.BugReport;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.LinkedHashMap;

public class ApplyBugReportSettings extends Pass_2021<BugReport> {
    @Override
    protected String getIconPath() {
        return "/icons/Apply.png";
    }

    @Override
    public boolean needsConfirmations() {
        return true;
    }

    @Override
    protected boolean canStart(Object... args) {
        if (Current.Check(Log, Current.BugReport)) {
            target = Current.getBugReport();
            long vv = target.visualiser_version;
            if (vv < 500) {
                Log.Writeln_("Автоматическое применение настроек поддерживается только в отчётах об ошибках,\n" +
                        "отправленных с версии визуализатора 500 и выше");
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void body() throws Exception {
        String[] lines = target.sapfor_settings.split("\n");
        LinkedHashMap<SettingName, String> to_apply = new LinkedHashMap<>();
        for (int i = 1; i < lines.length; ++i) {
            SettingName settingName = SettingName.getByDescription(lines[i].substring(4, lines[i].indexOf('=')));
            String settingValue = lines[i].substring(lines[i].indexOf('=') + 1);
            if (!settingName.equals(SettingName.Undefined))
                to_apply.put(settingName, settingValue);
        }
        for (SettingName settingName : to_apply.keySet())
            Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                    settingName,
                    to_apply.get(settingName));
    }
}
