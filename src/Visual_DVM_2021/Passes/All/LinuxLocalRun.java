package Visual_DVM_2021.Passes.All;

import Common.Current;
import GlobalData.Tasks.Passes.TaskLocalPass;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.Supervisor.Local.Linux.LinuxLocalRunSupervisor;
import ProjectData.Project.db_project_info;

public class LinuxLocalRun extends TaskLocalPass<LinuxLocalRunSupervisor> {
    public LinuxLocalRun() {
        super(LinuxLocalRunSupervisor.class);
    }

    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((RunTask) args[0], this,(db_project_info) args[1]);
        return true;
    }
}
