package Visual_DVM_2021.Passes.All;
import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import GlobalData.Machine.Machine;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddMakefile extends AddObjectPass<Makefile> {
    public AddMakefile() {
        super(Makefile.class);
    }
    @Override
    public Class<? extends DBObject> getOwner() {
        return Machine.class;
    }
    @Override
    protected void body() throws Exception {
        super.body();
        //создать модули для языков.
        getDb().Insert(new Module(LanguageName.fortran, target));
        getDb().Insert(new Module(LanguageName.c, target));
        getDb().Insert(new Module(LanguageName.cpp, target));
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
