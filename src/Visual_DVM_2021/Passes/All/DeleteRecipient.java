package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Recipient.DBRecipient;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteRecipient extends DeleteObjectPass<DBRecipient> {
    public DeleteRecipient() {
        super(DBRecipient.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
