package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import ProjectData.SapforData.Arrays.Distribution.Dimension;
import ProjectData.SapforData.Arrays.ProjectArray;
import ProjectData.SapforData.Regions.ParallelRegion;
import ProjectData.SapforData.Regions.UI.ArrayAlignmentBar;
import ProjectData.SapforData.Regions.UI.ParallelRegionFields;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforModification;

import javax.swing.*;
import java.awt.*;
import java.util.Vector;

public class SPF_ModifyArrayDistribution extends SapforModification {
    ParallelRegion region;

    @Override
    public boolean needsConfirmations() {
        return false;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        region = null;
        if (Current.Check(Log, Current.ParallelRegion) && super.canStart(args)&& passes.get(PassCode_2021.SPF_GetArrayLinks).Do()) {

            region = Current.getParallelRegion();
            DBObjectDialog dialog = new DBObjectDialog<ParallelRegion, ParallelRegionFields>(ParallelRegionFields.class) {
                @Override
                public int getDefaultHeight() {
                    return 250;
                }

                @Override
                public void fillFields() {
                    Vector<ArrayAlignmentBar> bars = new Vector<>();
                    for (ProjectArray array : Result.arrays.values()) {
                        if (array.canBeDistributed()&&array.align_template!=null) {
                            array.bar = null;
                            ArrayAlignmentBar bar = new ArrayAlignmentBar(array);
                            bars.add(bar);
                        }
                    }
                    JPanel panel = fields.Content;
                    panel.setLayout(new GridLayout(bars.size(), 1, 5, 12));
                    for (ArrayAlignmentBar bar : bars)
                        panel.add(bar);
                }

                @Override
                public void ProcessResult() {
                    for (ProjectArray array : Result.arrays.values()) {
                        if (array.bar != null) {
                            array.bar.apply_changes();
                            //заполнение новых коеффициентов.
                        }
                    }
                }
            };
            if (!dialog.ShowDialog(region.toString(), region))
                return false;

            boolean needsChange = false;
            for (ProjectArray array : region.arrays.values()) {
                if (array.canBeDistributed() && array.ac_need_change()) {
                    needsChange = true;
                    break;
                }
            }
            if (needsChange) {
                addOpt1 = region.regionId.toString();
                Vector<String> coeffs = new Vector<>();
                for (ProjectArray array : region.arrays.values()) {
                    if (array.ac_new.size() > 0) {
                        coeffs.add(String.valueOf(array.address));
                        for (int dim : array.ac_new.keySet()) {
                            Dimension dimension = array.ac_new.get(dim);
                            coeffs.add(String.valueOf(dimension.getS()));
                            coeffs.add(String.valueOf(dimension.K));
                            coeffs.add(String.valueOf(dimension.B));
                        }
                    }
                }
                coeffs.insertElementAt(String.valueOf(coeffs.size() + 1), 0);
                addOpt2 = String.join("|", coeffs);
                System.out.println("addOpt1=" + Utils.Brackets(addOpt1));
                System.out.println("addOpt2=" + Utils.Brackets(addOpt2));
                return true;
            }
            Log.Writeln("Коефициенты выравнивания актуальны");
        }
        return false;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.SPF_GetArrayDistribution).Do();
    }

    //распаковать журнал
    @Override
    protected void performFinish() throws Exception {
        target.updateLog(sapfor.getOutput());
    }
    //показать журнал.
    @Override
    protected void showFinish() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowProjectSapforLog();
    }
}
