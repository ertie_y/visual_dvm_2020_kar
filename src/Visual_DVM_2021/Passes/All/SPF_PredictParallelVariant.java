package Visual_DVM_2021.Passes.All;
import Common.Global;
import ProjectData.SapforData.Variants.ParallelVariant;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;
import Visual_DVM_2021.Passes.SilentSapforPass;

public class SPF_PredictParallelVariant extends SilentSapforPass {
    String Options = "";
    ParallelVariant variant = null;
    @Override
    protected boolean needsAnimation() {
        return false;
    }
    @Override
    public boolean needsConfirmations() {
        return false;
    }
    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() > 0;
    }
    @Override
    protected void body() throws Exception {
        sapfor.RunTransformation(
                "SPF_CreateParallelVariant",
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                target.getProjFile().getAbsolutePath(),
                "",
                Options
        );
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            variant = (ParallelVariant) args[0];
            Options = variant.getVector();
            return true;
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        variant.UpdateStats(sapfor.getPredictorStats());
    }
}
