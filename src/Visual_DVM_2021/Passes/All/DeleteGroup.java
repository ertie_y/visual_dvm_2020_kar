package Visual_DVM_2021.Passes.All;

import Common.Database.Database;
import TestingSystem.Group.Group;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.DeleteObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

public class DeleteGroup  extends DeleteObjectPass<Group> {
    public DeleteGroup() {
        super(Group.class);
    }

    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.DeleteGroupFromServer).Do(target);
    }
}
