package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileState;
import Visual_DVM_2021.Passes.Pass_2021;
public class ExcludeFile extends Pass_2021<DBProjectFile> {
    @Override
    protected String getIconPath() {
        return "/icons/Exclude.png";
    }
    @Override
    protected boolean canStart(Object... args) {
        return Current.Check(Log, Current.SelectedFile)
                && !(target = Current.getSelectedFile()).state.equals(FileState.Excluded);
    }
    @Override
    protected void body() throws Exception {
        target.Exclude();
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().RefreshProjectFiles();
    }
}
