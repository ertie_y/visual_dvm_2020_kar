package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Utils.Utils;
import Repository.Component.Component;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.ProcessPass;

import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
public class BuildComponent extends ProcessPass<Component> {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.DownloadRepository;
    }
    @Override
    protected boolean resetsNecessary() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target=Current.getComponent();
        return true;
    }
    @Override
    protected void performPreparation() throws Exception {
        Utils.forceDeleteWithCheck(target.getAssemblyFile());
    }
    @Override
    protected void body() throws Exception {
        form.ShowMessage1("Сборка " + target.getComponentType().getDescription());
        PerformScript(target.getAssemblyCommand());
        if (!target.getAssemblyFile().exists())
            throw new Exception("Сборка  не найдена");
        Files.copy(target.getAssemblyFile().toPath(),
                target.getNewFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        if (!target.getNewFile().exists())
            throw new Exception("Не удалось скопировать сборку для установки");
    }
}
