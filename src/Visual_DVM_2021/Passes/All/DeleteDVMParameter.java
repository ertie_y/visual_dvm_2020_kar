package Visual_DVM_2021.Passes.All;

import Common.Database.Database;
import Common.Global;
import GlobalData.DVMParameter.DVMParameter;
import Visual_DVM_2021.Passes.DeleteObjectPass;

public class DeleteDVMParameter extends DeleteObjectPass<DVMParameter> {
    public DeleteDVMParameter() {
        super(DVMParameter.class);
    }

    @Override
    protected Database getDb() {
        return Global.db;
    }
}
