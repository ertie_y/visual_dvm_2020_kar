package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import ProjectData.SapforData.Functions.FuncCall;
import ProjectData.SapforData.Functions.FuncCallH;
import ProjectData.SapforData.Functions.FuncInfo;
import ProjectData.SapforData.Functions.FunctionType;
import ProjectData.SapforData.Functions.UI.Graph.FunctionsGraphUI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SilentSapforPass;
import javafx.util.Pair;

import java.util.Collections;
import java.util.Vector;

public class SPF_GetGraphFunctionPositions extends SilentSapforPass {
    //-ФИЛЬТРАЦИЯ ГРАФА    -----------------------------------
    public static boolean showStandardFunctions = true;
    public static boolean showExternalFunctions = true;
    public static boolean showUnreachableFunctions = true;
    public static boolean showByCurrentFunction = false;
    public static boolean showIn = true;
    public static boolean showOut = true;
    public static int depth = 1;
    public static String filterName = "";

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return passes.get(PassCode_2021.SPF_GetGraphFunctions).isDone() && super.canStart(args);
    }

    //--------------------
    @Override
    public boolean needsConfirmations() {
        return false;
    }

    @Override
    protected boolean needsAnimation() {
        return false;
    }

    @Override
    protected PassCode_2021 necessary() {
        return null;
    }

    //    return PassCode_2021.SPF_GetGraphFunctions;
    @Override
    protected void performPreparation() throws Exception {
        target.functionsGraph.Clear();
    }

    @Override
    protected void showPreparation() {
        UI.getNewMainWindow().getProjectWindow().ShowNoFunctions();
    }

    @Override
    protected void performDone() throws Exception {
        if (!sapfor.getResult().isEmpty())
            unpack(sapfor.getResult());
    }

    public void findFuncMatches_r(FuncCallH level, String funcName, Vector<FuncCallH> matches) {
        if (funcName.equals(level.funcName)) {
            matches.add(level);
            return;
        }
        for (FuncCallH callH : level.calls)
            findFuncMatches_r(callH, funcName, matches);
    }

    public boolean isFunctionUnreachable(String funcName) {
        if (target.main_functionH == null) return true; //нет гпе. недостижимо всё!
        Vector<FuncCallH> res = new Vector<>();
        findFuncMatches_r(target.main_functionH, funcName, res);
        return res.isEmpty();
    }

    public String packFgSettings() {
        Pair<Integer, Integer> screenDims = UI.getNewMainWindow().getProjectWindow().getFunctionsGraphPanelSizes();
        int x = (int) (screenDims.getKey() * target.fgScreen);
        int y = (int) (screenDims.getValue() * target.fgScreen);
        Vector<String> visibleFuncNames = new Vector<>();
        //первый отсев.
        if (showByCurrentFunction) {
            if (Current.HasFunction()) {
                Vector<String> rawVisible =
                        new Vector<>(getNearestFunctions(Current.getFunction().funcName,
                                depth,
                                showIn,
                                showOut));
                for (String funcName : rawVisible)
                    if (!visibleFuncNames.contains(funcName))
                        visibleFuncNames.add(funcName);
            } else
                visibleFuncNames.addAll(target.allFunctions.keySet());
        }
            /*
            System.out.println("СМЕЖНЫЕ ФУНКЦИИ");
            System.out.println(String.join("\n", visibleFuncNames));
            UI.Info("+");
             */
        else {
            for (FuncInfo fi : target.allFunctions.values()) {
                boolean isVisible =
                        (showStandardFunctions || !fi.type.equals(FunctionType.Standard)) &&
                                (showExternalFunctions || !fi.type.equals(FunctionType.NotFound)) &&
                                (showUnreachableFunctions || !isFunctionUnreachable(fi.funcName)) &&
                                fi.funcName.contains(filterName);
                if (isVisible)
                    visibleFuncNames.add(fi.funcName);
            }
        }
        String res = "|" + target.fgIterations + "|" + target.fgResistance + "|" + x + "|" + y + "|" + visibleFuncNames.size();
        if (visibleFuncNames.size() > 0) res += "|" + String.join("|", visibleFuncNames);
        return res;
    }


    public void getNearestFunctions_r(Vector<String> res, String funcName, int depth, boolean in, boolean out) {
        if ((depth > 0)) {
            res.add(funcName);
            if (out) {
                FuncInfo fi = target.allFunctions.get(funcName);
                for (FuncCall fc : fi.calls) {
                    if (!res.contains(fc.funcName)) {
                        res.add(fc.funcName);
                        getNearestFunctions_r(res, fc.funcName, depth - 1, in, true);
                    }
                }
            }
            if (in) {
                //ищем кто нас вызывает
                for (FuncInfo fi : target.allFunctions.values()) {
                    for (FuncCall fc : fi.calls)
                        if (fc.funcName.equals(funcName)) {
                            res.add(fi.funcName);
                            getNearestFunctions_r(res, fi.funcName, depth - 1, true, out);
                        }
                }
            }
        }
    }

    public void getNearestFunctions_r(Vector<String> res, String funcName, boolean in, boolean out) {
        res.add(funcName);
        if (out) {
            FuncInfo fi = target.allFunctions.get(funcName);
            for (FuncCall fc : fi.calls) {
                if (!res.contains(fc.funcName)) {
                    res.add(fc.funcName);
                    getNearestFunctions_r(res, fc.funcName, in, true);
                }
            }
        }
        if (in) {
            //ищем кто нас вызывает
            for (FuncInfo fi : target.allFunctions.values()) {
                for (FuncCall fc : fi.calls)
                    if (fc.funcName.equals(funcName)) {
                        res.add(fi.funcName);
                        getNearestFunctions_r(res, fi.funcName, true, out);
                    }
            }
        }
    }

    public Vector<String> getNearestFunctions(String funcName, int depth, boolean in, boolean out) {
        Vector<String> res = new Vector<>();
        if (depth > 0)
            getNearestFunctions_r(res, funcName, depth, in, out);
        else
            getNearestFunctions_r(res, funcName, in, out);
        return res;
    }

    protected void unpack(String packed) throws Exception {
        String[] splited = packed.split("\\|");
        int j = 0;
        //-
        String currentFuncName = "";
        double x = 0;
        double y = 0;
        //-
        for (int i = 1; i < splited.length; ++i) {
            switch (j) {
                case 0:
                    currentFuncName = splited[i];
                    target.functionsGraph.addVertex(currentFuncName);
                    j++;
                    break;
                case 1:
                    x = Double.parseDouble(splited[i]);
                    j++;
                    break;
                case 2:
                    y = Double.parseDouble(splited[i]);
                    target.functionsGraph.vertexCoordinates.put(currentFuncName, new Pair<>(x, y));
                    j = 0;
                    break;
            }
        }
        /*
        System.out.println("Got "+target.functionsGraph.vertexMap.keySet().size()+" functions to draw");
        System.out.println(String.join("\n", target.functionsGraph.vertexMap.keySet()));
        System.out.println("===============================================");
         */

        //теперь добавить ребер.
        for (String funcName : target.functionsGraph.vertexMap.keySet()) {
            FuncInfo fi = target.allFunctions.get(funcName);
            for (FuncCall fc : fi.calls) {
                if (target.functionsGraph.vertexMap.containsKey(fc.funcName))
                    target.functionsGraph.addEdge(
                            funcName,
                            fc.funcName
                    );
            }
        }
    }

    @Override
    protected void body() throws Exception {
        sapfor.RunAnalysis(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings() +
                        packFgSettings(),
                target.getProjFile().getAbsolutePath());
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowFunctions();
    }
}
