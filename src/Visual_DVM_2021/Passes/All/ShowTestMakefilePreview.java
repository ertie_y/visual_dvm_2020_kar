package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import GlobalData.Makefile.UI.MakefilePreviewForm;
import ProjectData.Project.db_project_info;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.nio.file.Paths;

public class ShowTestMakefilePreview extends RepositoryPass_2021<Test> {
    @Override
    protected String getIconPath() {
        return "/icons/ShowPassword.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected boolean canStart(Object...args) {
        if (Current.Check(Log, Current.Compiler, Current.Test)) {
            target = Current.getTest();
            return true;
        }
        return false;
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DownloadTest, target.name));
        response.Unpack(target.getArchive());
    }

    @Override
    protected void performDone() throws Exception {

        if (passes.get(PassCode_2021.UnzipFolderPass).Do(
                target.getArchive().getAbsolutePath(),
                Global.TempDirectory.getAbsolutePath(), false
        )) {
            db_project_info project = new db_project_info(
                    Paths.get( Global.TempDirectory.getAbsolutePath(), target.name).toFile()

            );
            project.Open();
            project.Close();
            new MakefilePreviewForm().ShowDialog("Предпросмотр мейкфайла текущего теста",
                    Current.getGroup().GenerateMakefile(project, Current.getCompiler().call_command)

            );
        }
    }
}