package Visual_DVM_2021.Passes.All;
import Common.Current;
import Repository.Component.Component;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

public class DownloadComponent extends RepositoryPass_2021<Component> {
    @Override
    protected void ServerAction() throws Exception {
        target = Current.getComponent();
        ReceiveFile(target.getRemoteFilePath(), target.getNewFile());
    }
}
