package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Visual_DVM_2021.Passes.Pass_2021;
public class CompareFiles extends Pass_2021 {
    @Override
    protected String getIconPath() {
        return "/icons/Menu/Undo.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }
    @Override
    protected void body() throws Exception {
        UI.getNewMainWindow().getVersionsWindow().getMasterComparsionWindow().Compare();
    }
}
