package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import Repository.Component.Component;
import Repository.Component.ComponentType;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.util.LinkedHashMap;

public class GetComponentsActualVersions extends RepositoryPass_2021 {
    @Override
    protected void ServerAction() throws Exception {
        LinkedHashMap<ComponentType, String> request_actual_versions = new LinkedHashMap<>();
        for (Component component : Global.Components.Data.values())
            if (component.isVisible())
                request_actual_versions.put(component.getComponentType(),
                        component.getVersionRemotePath());
        Command(new ServerExchangeUnit_2021(ServerCode.GetComponentsActualVersions, "", request_actual_versions));
        //-
        LinkedHashMap<ComponentType, String> response_actual_versions = (LinkedHashMap<ComponentType, String>) response.object;
        for (ComponentType componentType : response_actual_versions.keySet()) {
            System.out.println(componentType+":"+ Utils.DQuotes(response_actual_versions.get(componentType)));
        }
        for (ComponentType componentType : response_actual_versions.keySet()) {
            Global.Components.get(componentType).getActualVersion(response_actual_versions.get(componentType));
        }
    }
}
