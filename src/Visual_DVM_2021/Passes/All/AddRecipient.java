package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.Recipient.DBRecipient;
import Visual_DVM_2021.Passes.AddObjectPass;
public class AddRecipient extends AddObjectPass<DBRecipient> {
    public AddRecipient() {
        super(DBRecipient.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
