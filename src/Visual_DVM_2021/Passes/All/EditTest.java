package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.Database;
import Common.Utils.Utils;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class EditTest extends EditObjectPass<Test> {
    public EditTest() {
        super(Test.class);
    }

    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditTestOnServer).Do(target);
    }
}
