package Visual_DVM_2021.Passes.All;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.SapforData.Functions.FuncCallH;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforTransformation;

import java.util.Vector;
public class SPF_InlineProceduresH extends SapforTransformation {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.SPF_GetGraphFunctions;
    }
    public void getCallsChainR(Vector<FuncCallH> chain, Vector<Vector<FuncCallH>> all) {
        if (chain.lastElement().calls.isEmpty())
            all.add(chain);
        else {
            for (FuncCallH descendant : chain.lastElement().calls) {
                Vector<FuncCallH> new_chain = new Vector<>(chain);
                new_chain.add(descendant);
                getCallsChainR(new_chain, all);
            }
        }
    }
    public void extractSelectionChains(Vector<FuncCallH> chain, Vector<Vector<FuncCallH>> res) {
        for (FuncCallH call : chain) {
            if (call.isSelected()) {
                //в цепочке есть хоть 1 выбранный элемент. значит берем ее.
                res.add(chain);
                return;
            }
        }
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (super.canStart(args)) {
            if (target.main_functionH == null) {
                Log.Writeln("Отсутствует главная программная единица");
                return false;
            }
            //получили все цепочки.
            Vector<Vector<FuncCallH>> all = new Vector<>();
            Vector<FuncCallH> first_chain = new Vector<>();
            first_chain.add(target.main_functionH);
            getCallsChainR(first_chain, all);
            //-
            //проверка наличия выбранных путей.
            Vector<Vector<FuncCallH>> selectedChains = new Vector<>();
            for (Vector<FuncCallH> chain : all)
                extractSelectionChains(chain, selectedChains);
            if (selectedChains.isEmpty()) {
                Log.Writeln_(
                        (callsCount > 1) ? "Не отмечено ни одной цепочки вызовов процедур для подстановки" :
                                "Для иерархической подстановки процедур следует отметить\n" +
                                        "хотя бы один вызов процедуры в иерархии."
                );
                return false;
            }
            Vector<String> Result = new Vector<>();
            //-
            Result.add(String.valueOf(selectedChains.size()));
            for (Vector<FuncCallH> chain : selectedChains) {
                Result.add(String.valueOf(chain.size()));
                for (FuncCallH call : chain) {
                    Result.add(call.funcName);
                    Result.add(call.file);
                    Result.add(String.valueOf(call.line));
                    //--
                    //todo со стороны сапфора. подставлять ли вызов в конкретной цепочке.
                    Result.add(call.isSelected() ? "1" : "0");
                }
            }
            Options = Utils.toU(String.join("|", Result));
            System.out.println(Utils.Brackets(Options));
           // UI.Info("+");
            Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(
                    SettingName.PARSE_FOR_INLINE, "1");
            SPF_ParseFilesWithOrder.silent=true;
            return passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do();
        }
        return false;
    }

    @Override
    protected void performFinish() throws Exception {
        SPF_ParseFilesWithOrder.silent=false;
        super.performFinish();
    }

    @Override
    protected void FocusBeforeStart() {
        UI.getNewMainWindow().getProjectWindow().FocusHierarchy();
    }
}