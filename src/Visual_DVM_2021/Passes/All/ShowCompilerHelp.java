package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.Windows.Dialog.Text.ReadOnlyMultilineTextForm;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.MachineType;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SSH.CurrentConnectionPass;

public class ShowCompilerHelp extends Pass_2021<String> {
    Pass_2021<String> subpass;
    boolean needsShow = true;
    Compiler compiler;

    //-
    @Override
    protected String getIconPath() {
        return "/icons/Help.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = null;
        if (args.length == 0) {
            if (Current.Check(Log, Current.Compiler)) {
                compiler = Current.getCompiler();
                needsShow = true;
                return true;
            }
        } else {
            if (args.length == 1) {
                if (Current.Check(Log, Current.DialogCompiler)) {
                    compiler = Current.getDialogCompiler();
                    needsShow = (boolean) args[0];
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    protected void body() throws Exception {
        subpass = null;
        compiler.ResetHelp();
        if (Current.getMachine().type.equals(MachineType.Local)) {
            subpass = passes.get(PassCode_2021.LocalGetCompilerHelp);
        } else {
            subpass = passes.get(PassCode_2021.RemoteGetCompilerHelp);
        }
        if (subpass != null) {
            if (compiler.helpLoaded = subpass.Do(compiler.getHelpCommand())) {
                target = subpass.target;
                compiler.helpText = target;
                compiler.ParseHelp();
            }
        }
    }

    @Override
    protected boolean validate() {
        return (subpass != null) && (target != null);
    }

    @Override
    protected void showDone() throws Exception {
        if (needsShow) {
            ReadOnlyMultilineTextForm ff = new ReadOnlyMultilineTextForm();
            ff.ShowDialog("Справка", target);
        }
    }
}
