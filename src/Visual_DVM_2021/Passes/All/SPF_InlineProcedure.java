package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import GlobalData.Settings.SettingName;
import ProjectData.SapforData.Functions.FuncCall;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforFilesModification;
public class SPF_InlineProcedure extends SapforFilesModification {
    FuncCall call = null;
  //  @Override
  //  protected PassCode_2021 necessary() {
  //      return null;
  //  }
    //  SPF_InlineProcedure (addOpt1_c -> name | file, addOpt2_c-> line)
    @Override
    protected boolean canStart(Object ... args) throws Exception {
        if (super.canStart(args)&&(call = (args.length > 0) ? (FuncCall) args[0] : null)!=null){
            addOpt1 = call.funcName + "|" + Current.getFile().name;
            addOpt2 = String.valueOf(call.line);
            Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.PARSE_FOR_INLINE, "1");
            SPF_ParseFilesWithOrder.silent=true;
            return passes.get(PassCode_2021.SPF_ParseFilesWithOrder).Do();
        }
        return false;
    }

    @Override
    protected void performFinish() throws Exception {
        SPF_ParseFilesWithOrder.silent=false;
        super.performFinish();
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        Current.getFile().form.getEditor().gotoLine(call.line);
    }
}
