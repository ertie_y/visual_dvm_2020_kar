package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import Common.Passes.PassCode;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Account.AccountRole;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.AddObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.UI.Main.TestsFilterForm;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;

public class AddTest extends AddObjectPass<Test> {
    public AddTest() {
        super(Test.class);
    }

    @Override
    public Class<? extends DBObject> getOwner() {
        return Group.class;
    }

    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (Current.getAccount().CheckRegistered(Log) &&
                Current.Check(Log, Current.Project) &&
                UI.Question("Добавить текущий проект в глобальную базу тестов") &&
                super.canStart(args)) {
            if (TestingSystem.db.tests.containsKey(target.name)) {
                Log.Writeln_("Уже существует тест с именем " + Utils.Brackets(target.name));
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    protected boolean fillObjectFields() throws Exception {
        target.name = Current.getProject().name;
        target.dim = Current.getProject().maxdim;
        target.date = new Date().getTime();
        target.sender_name = Current.getAccount().name;
        target.sender_address = Current.getAccount().email;
        target.group_name = Current.getGroup().name;
        return super.fillObjectFields();
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
       // TestingSystem.db.CheckGroup(target.group);
        //получить копию проекта без подверсий.

        //-
        File src = Paths.get(System.getProperty("user.dir"), "Temp", target.name).toFile();
        Utils.forceDeleteWithCheck(src);
        FileUtils.forceMkdir(src);
        Current.getProject().Clone(src);
        //архивация.
        File archive = Utils.getTempFileName("test");
        if (passes.get(PassCode_2021.ZipFolderPass).Do(src.getAbsolutePath(), archive.getAbsolutePath())) {
            //отправка.
            passes.get(PassCode_2021.PublishTest).Do(target, archive);
        }
    }
    @Override
    protected void showDone() throws Exception {
       // ((TestsFilterForm)TestingSystem.db.tests.getFilterUI()).selectLanguage(Current.getProject().languageName);
        super.showDone();
    }
}
