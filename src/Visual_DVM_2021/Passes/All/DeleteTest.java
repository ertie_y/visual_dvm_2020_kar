package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Database.Database;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.DeleteObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

public class DeleteTest extends DeleteObjectPass<Test> {
    public DeleteTest() {
        super(Test.class);
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args)&&Current.getAccount().CheckAccessRights(target.sender_address,Log);
    }

    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        passes.get(PassCode_2021.DeleteTestFromServer).Do(target);
    }
}
