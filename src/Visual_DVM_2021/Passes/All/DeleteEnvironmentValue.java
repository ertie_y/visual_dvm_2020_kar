package Visual_DVM_2021.Passes.All;
import Common.Database.Database;
import Common.Global;
import GlobalData.EnvironmentValue.EnvironmentValue;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteEnvironmentValue extends DeleteObjectPass<EnvironmentValue> {
    public DeleteEnvironmentValue() {
        super(EnvironmentValue.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
