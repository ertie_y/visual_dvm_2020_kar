package Visual_DVM_2021.Passes.All;
import Common.Current;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class DropAnalyses extends CurrentProjectPass {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args) && (Current.getSapfor() != null);
    }
    @Override
    protected void body() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
    }
}
