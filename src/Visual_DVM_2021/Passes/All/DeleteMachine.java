package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.Machine.Machine;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteMachine extends DeleteObjectPass<Machine> {
    public DeleteMachine() {
        super(Machine.class);
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
    @Override
    protected void performDone() throws Exception {
        Global.db.DropProjectsMachine(target);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasProject() &&
                target.getPK().equals(Current.getProject().getInfo().machine_id)) {
            UI.getNewMainWindow().getTestingWindow().ShowProjectMachine();
            UI.getNewMainWindow().getTestingWindow().ShowProjectUser();
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
            UI.getNewMainWindow().getTestingWindow().ShowProjectRunConfiguration();
        }
    }
}