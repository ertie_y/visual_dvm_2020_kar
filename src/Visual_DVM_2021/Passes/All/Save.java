package Visual_DVM_2021.Passes.All;
import Common.Current;
import ProjectData.Files.DBProjectFile;
import Visual_DVM_2021.Passes.Pass_2021;
import org.apache.commons.io.FileUtils;
public class Save extends Pass_2021<DBProjectFile> {
    @Override
    protected boolean canStart(Object... args) {
        if (Current.HasFile()) {
            target = Current.getFile();
            return (target.NeedsSave);
        }
        return false;
    }
    @Override
    protected void body() throws Exception {
        FileUtils.write(target.file, target.form.getEditor().getText());
        target.NeedsSave = false;
    }
    @Override
    protected void showDone() throws Exception {
        if (button != null)
            button.setEnabled(false);
    }
    @Override
    public void Reset() {
        super.Reset();
        if (button != null)
            button.setEnabled(true);
    }
}
