package Visual_DVM_2021.Passes.All;

import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.RepositoryPass_2021;


//только для админов. группы удалять это явно не то, что нужно обычным пользователям.
public class DeleteGroupFromServer extends RepositoryPass_2021<Group> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (Group) args[0];
        return true;
    }
    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.DeleteGroup, target.name));
    }
}
