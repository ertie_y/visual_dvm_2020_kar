package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.DeleteObjectPass;
public class DeleteRunConfiguration extends DeleteObjectPass<RunConfiguration> {
    public DeleteRunConfiguration() {
        super(RunConfiguration.class);
    }
    @Override
    protected void performDone() throws Exception {
        super.performDone();
        Global.db.DropProjectsRunConfiguration(target);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        boolean refresh_project = Current.HasProject() && (target.getPK().equals(Current.getProject().getInfo().run_configuration_id));
        if (refresh_project)
            UI.getNewMainWindow().getTestingWindow().ShowProjectRunConfiguration();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
