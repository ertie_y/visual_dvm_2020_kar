package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import Repository.Component.Component;
import Repository.Component.UI.PublishForm;
import Repository.EmailMessage;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Date;
import java.util.Vector;
public class PublishComponent extends RepositoryPass_2021<Component> {
    String version_mail_header = "";
    String version_text = "";
    PublishForm f = new PublishForm();
    @Override
    protected boolean canStart(Object... args) throws Exception {
        if (!Global.isWindows) {
            Log.Writeln_("Публикация компонент разрешена только для Windows");
            return false;
        }
        if (Current.Check(Log, Current.Component)) {
            target = Current.getComponent();
            return target.isValidVersion(Log, "публикуемого")
                    && f.ShowDialog(code().getDescription());
        }
        return false;
    }
    @Override
    protected void ServerAction() throws Exception {
        version_text = target.getVersionText();
        //1 отправка файла.
        SendFile(target.getFile(), target.getRemoteFilePath());
        //2 запись версии
        File new_version = Utils.CreateTempFile("version", version_text);
        SendFile(new_version, target.getVersionRemotePath());
        //-
        String change_record_header = String.join(" ",
                Utils.Brackets(Utils.print_date(new Date())) + ":",
                Current.getAccount().name + Utils.RBrackets(Current.getAccount().email),
                "публикует версию", Utils.DQuotes(target.version)
        );
        String change_description = (f.Result == null) ? "не указаны" : f.Result;
        String change_record = String.join("\n", change_record_header, "Изменения:",
                change_description,
                RepositoryServer.separator, ""
        );
        File old = Utils.getTempFileName("changes");
        ReceiveFile(target.getChangesRemotePath(), old);
        //  form.ShowMessage1("Дополнение журнала изменений");
        FileWriter writer = new FileWriter(old.getAbsolutePath(), true);
        BufferedWriter bufferWriter = new BufferedWriter(writer);
        bufferWriter.write(change_record);
        bufferWriter.close();
        //  form.ShowMessage1("Отправка журнала изменений");
        SendFile(old, target.getChangesRemotePath());
    }
    @Override
    protected void performDone() throws Exception {
        target.actual_version = target.version;
        target.CheckIfNeedsUpdateOrPublish();
        if ((f.Result != null)) {
            version_mail_header = String.join(" ",
                    "Опубликована версия",
                    Utils.DQuotes(target.version),
                    "компонента",
                    Utils.DQuotes(target.getComponentType().getDescription()));

            EmailMessage message =
                    new EmailMessage(version_mail_header,
                    f.Result,
                    new Vector<>(RepositoryServer.db.subscribers.Data.keySet()));
            if (f.fields.cbForceMail.isSelected())
                message.addAttachement(target.getFile());

            Pass_2021.passes.get(PassCode_2021.Email).Do(message);
        }
    }
    @Override
    protected void showDone() throws Exception {
        Global.RefreshUpdatesStatus();
    }
}

