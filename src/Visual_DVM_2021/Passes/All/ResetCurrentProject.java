package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Visual_DVM_2021.Passes.CurrentProjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;
public class ResetCurrentProject extends CurrentProjectPass {
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.DeleteDebugResults).Do();
        passes.get(PassCode_2021.CloseCurrentProject).Do();
        Current.set(Current.Root, null);
    }
    @Override
    protected void showPreparation() throws Exception {
        UI.getNewMainWindow().ShowNoRoot();
    }
    @Override
    protected void body() throws Exception {
        target.CleanVersions();
        target.ResetDB();
        Global.db.Delete(target.getInfo());
    }
    @Override
    protected void performDone() throws Exception {
        passes.get(PassCode_2021.OpenCurrentProject).Do(target.Home);
    }
}
