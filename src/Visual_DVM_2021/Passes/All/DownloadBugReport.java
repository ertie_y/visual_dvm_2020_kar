package Visual_DVM_2021.Passes.All;
import Common.Global;
import Common.Utils.Utils;
import Repository.BugReport.BugReport;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

public class DownloadBugReport extends RepositoryPass_2021<BugReport> {
    @Override
    protected boolean canStart(Object... args) {
        target = (BugReport) args[0];
        return !target.getArchiveFile().exists();
    }
    @Override
    protected void performPreparation() throws Exception {
        Utils.CheckDirectory(Global.BugReportsDirectory);
    }

    @Override
    protected void ServerAction() throws Exception {
        ReceiveFile(target.getRemoteArchiveAddress(), target.getArchiveFile());
    }
    @Override
    protected boolean validate() {
        return target.getArchiveFile().exists();
    }
}
