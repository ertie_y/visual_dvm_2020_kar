package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Account.AccountRole;
import GlobalData.Account.AccountSubscribeState;
import Repository.BugReportsDatabase;
import Repository.Server.RepositoryServer;
import Visual_DVM_2021.Passes.RepositoryPass_2021;
import Visual_DVM_2021.Properties.PropertyName;
import com.sun.org.glassfish.gmbal.Description;
import org.apache.commons.io.FileUtils;

import java.io.File;

public class SynchronizeBugReports extends RepositoryPass_2021 {
    @Override
    protected String getIconPath() {
        return "/icons/ComponentsActual.png";
    }
    @Override
    protected String getButtonText() {
        return "";
    }

    File new_db_file;

    @Override
    protected void showPreparation() throws Exception {
        RepositoryServer.db.bugReports.ClearUI();
    }

    @Override
    protected void ServerAction() throws Exception {
        new_db_file = Utils.getTempFileName("bdb");
        ReceiveFile(RepositoryServer.home + "Data/" + Global.properties.get(PropertyName.BugReportsDBName), new_db_file);
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        RepositoryServer.db.Disconnect();
        Utils.forceDeleteWithCheck(RepositoryServer.db.getFile());
        FileUtils.moveFile(new_db_file, RepositoryServer.db.getFile());
        RepositoryServer.db.Connect();
        RepositoryServer.db.Synchronize();
        //убрать пересоздание таблиц. ибо сносится связь с интерфейсом окна.
        if (Current.getAccount().role != AccountRole.Undefined) {
            Current.getAccount().subscription =
                    (RepositoryServer.db.subscribers.Data.containsKey(Current.getAccount().email)) ?
                            AccountSubscribeState.Active : AccountSubscribeState.None;
        }
    }

    @Override
    protected void showDone() throws Exception {
        if (UI.HasNewMainWindow())
            UI.getNewMainWindow().getCallbackWindow().ShowBugReports();
    }
}
