package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.UI.UI;
import Common.Utils.Index;
import ProjectData.SapforData.Regions.ParallelRegion;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;

import java.util.Arrays;
import java.util.Collections;
import java.util.Vector;

public class SPF_GetArrayDistributionOnlyAnalysis extends SapforAnalysis {
    @Override
    protected Vector<PassCode_2021> getForbiddenPhases() {
        return new Vector<>(Collections.singletonList(PassCode_2021.SPF_GetArrayDistributionOnlyRegions));
    }

    @Override
    public String phase() {
        return "CREATE_DISTR_DIRS";
    }

    @Override
    protected boolean isAtomic() {
        return false;
    }

    @Override
    protected void unpack(String packed) throws Exception {
        String[] splited = packed.split("#");
        Index idx = new Index();
        int n = Integer.parseInt(splited[idx.Inc()]);
        for (int i = 0; i < n; ++i) {
            ParallelRegion p = new ParallelRegion(splited, idx);
            target.parallelRegions.put(p.regionId, p);
        }
        target.GenRules();
    }

    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        //как то вывести эти методы в проект. чтобы и при очистке юзать.
        //иначе анархия.
        target.parallelRegions.clear();
        target.templates.clear();
        target.parallelVariants.clear();
        //------------------------------------------------
        target.CheckedVariantsCounter.Reset();
    }

    @Override
    protected void showPreparation() {
        UI.getNewMainWindow().getProjectWindow().ShowNoProjectDistribution();
        UI.getNewMainWindow().getProjectWindow().ShowNoVariantsFilter();
        UI.getNewMainWindow().getProjectWindow().ShowNoVariants();
        UI.getNewMainWindow().getProjectWindow().ShowNoRegions();
        UI.getNewMainWindow().getProjectWindow().ShowNoProjectMaxDim();
    }

    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getProjectWindow().ShowProjectDistribution();
        UI.getNewMainWindow().getProjectWindow().ShowVariantsFilter();
        UI.getNewMainWindow().getProjectWindow().ShowTotalVariantsCount();
        UI.getNewMainWindow().getProjectWindow().ShowFilteredVariantsCount();
        UI.getNewMainWindow().getProjectWindow().ShowCheckedVariantsCount();
        UI.getNewMainWindow().getProjectWindow().ShowRegions();
        UI.getNewMainWindow().getProjectWindow().ShowProjectMaxDim();
        super.showDone();
    }

    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusProject();
        UI.getNewMainWindow().getProjectWindow().FocusAnalysis();
    }
}