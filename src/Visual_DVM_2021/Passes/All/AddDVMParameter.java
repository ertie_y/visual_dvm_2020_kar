package Visual_DVM_2021.Passes.All;

import Common.Database.DBObject;
import Common.Database.Database;
import Common.Global;
import GlobalData.DVMParameter.DVMParameter;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.AddObjectPass;

public class AddDVMParameter extends AddObjectPass<DVMParameter> {
    public AddDVMParameter() {
        super(DVMParameter.class);
    }

    @Override
    public Class<? extends DBObject> getOwner() {
        return RunConfiguration.class;
    }

    @Override
    protected Database getDb() {
        return Global.db;
    }
}
