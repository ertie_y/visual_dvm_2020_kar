package Visual_DVM_2021.Passes.All;

import Common.Utils.Utils;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import TestingSystem.Test.Test;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import java.io.File;

public class PublishTest extends RepositoryPass_2021<Test> {
    File project_archive = null;

    @Override
    protected boolean canStart(Object... args) throws Exception {
        target = (Test) args[0];
        project_archive = (File) args[1];
        target.project_archive_bytes = Utils.packFile(project_archive);
        return true;
    }

    @Override
    protected void ServerAction() throws Exception {
        Command(new ServerExchangeUnit_2021(ServerCode.PublishTest, "", target));
    }
}
