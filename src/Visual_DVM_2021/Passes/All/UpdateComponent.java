package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.Passes.PassCode;
import Common.Passes.ToolBarPass;
import Repository.Component.Component;
import Repository.Component.ComponentType;
import Repository.Component.PerformanceAnalyzer.PerformanceAnalyzer;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

public class UpdateComponent extends Pass_2021<Component> {
    @Override
    protected PassCode_2021 necessary() {
        return PassCode_2021.CreateComponentBackUp;
    }

    @Override
    protected boolean resetsNecessary() {
        return true;
    }

    @Override
    protected void body() throws Exception {
        PassCode_2021 subPass = PassCode_2021.Undefined;
        //<editor-fold desc="получение нового файла компонента">
        switch (Current.getComponent().getComponentType()) {
            case Sapfor_F:
            case Visualizer_2:
                subPass = Global.isWindows ? PassCode_2021.DownloadComponent : PassCode_2021.BuildComponent;
                break;
            case Visualiser:
            case Instruction:
            case PerformanceAnalyzer:
                subPass = PassCode_2021.DownloadComponent;
                break;
        }
        if ((subPass != PassCode_2021.Undefined) && passes.get(subPass).Do()) {
            //</editor-fold>
            if (!Current.getComponent().getNewFile().exists())
                throw new Exception("Не удалось получить новый файл для компонента " + Current.getComponent().getComponentType().getDescription());
            //непосредственное обновление.
            Current.getComponent().Update();
        }
    }

    @Override
    protected void performFinish() throws Exception {
        Current.getComponent().CheckIfNeedsUpdateOrPublish();
    }

    @Override
    protected void showDone() throws Exception {
        Global.RefreshUpdatesStatus();
    }
}
