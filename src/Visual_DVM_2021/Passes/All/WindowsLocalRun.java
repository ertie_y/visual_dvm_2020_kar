package Visual_DVM_2021.Passes.All;

import Common.Current;
import GlobalData.Tasks.Passes.TaskLocalPass;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.Supervisor.Local.Windows.WindowsLocalRunSupervisor;
import ProjectData.Project.db_project_info;

public class WindowsLocalRun extends TaskLocalPass<WindowsLocalRunSupervisor> {
    public WindowsLocalRun() {
        super(WindowsLocalRunSupervisor.class);
    }

    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((RunTask) args[0], this,(db_project_info) args[1]);
        return true;
    }
}
