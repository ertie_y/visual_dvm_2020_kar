package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.SSH.TaskConnectionPass;
import GlobalData.Tasks.Supervisor.Remote.ServerRunSupervisor;
public class ServerRun extends TaskConnectionPass {
    public ServerRun() {
        super(ServerRunSupervisor.class);
    }
    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((RunTask) args[0], this,(db_project_info) args[1]);
        return true;
    }
}
