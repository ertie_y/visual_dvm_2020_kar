package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Account.AccountRole;
import GlobalData.Account.AccountSubscribeState;
import GlobalData.Recipient.DBRecipient;
import GlobalData.Recipient.UI.DBRecipientForm;
import Repository.EmailMessage;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.util.Arrays;
import java.util.Vector;
public class RegisterAccount extends Email {
    @Override
    protected String getIconPath() {
        return "/icons/Registry.png";
    }

    String name;
    String email;
    String password;
    DBRecipientForm f = new DBRecipientForm();
    public static int getRandomIntegerBetweenRange(int min, int max) {
        int x = (int) ((Math.random() * ((max - min) + 1)) + min);
        return x;
    }
    @Override
    protected boolean canStart(Object... args) throws Exception {
        DBRecipient res = new DBRecipient();
        if (f.ShowDialog("Регистрация", res)) {
            if (!Utils.validateEmail(res.Email, Log)) {
                return false;
            }
            name = res.Name;
            email = res.Email;
            Vector<String> rec = new Vector<>();
            rec.add(email);
            password = String.valueOf(getRandomIntegerBetweenRange(1111, 9999));
            return super.canStart( new EmailMessage("Код регистрации визуализатора для: " + Utils.Brackets(name),password, rec));
        }
        return false;
    }
    @Override
    protected void performDone() throws Exception {
        String attempt = null;
        do {
            attempt = JOptionPane.showInputDialog(null,
                    new String[]{"Введите код активации, полученный по почте, чтобы завершить регистрацию"},
                    "Регистрация",
                    JOptionPane.INFORMATION_MESSAGE);
            if (attempt != null) {
                System.out.println("Введенный код: " + Utils.Brackets(attempt));
                if (attempt.equals(password)) {
                    Current.getAccount().name = name;
                    Current.getAccount().email = email;
                    Current.getAccount().role =
                            (new Vector<>(Arrays.asList(Global.admins_mails)).contains(email)) ?
                                    AccountRole.Admin : AccountRole.User;
                    //раз регистрация новая то подписки нет.
                    Current.getAccount().subscription = AccountSubscribeState.None;
                    Global.db.Update(Current.getAccount());
                    UI.Info("Регистрация успешно завершена");
                    attempt = null;
                    passes.get(PassCode_2021.Subscribe).Do(); //тут же пусть будет и подписка.
                } else {
                    UI.Error("Неверный код регистрации.\nПовторите попытку.");
                }
            } else UI.Info("Регистрация отменена");
        } while (attempt != null);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getCallbackWindow().ShowAccount();
        if (Current.HasBugReport())
            UI.getNewMainWindow().getCallbackWindow().ShowCurrentBugReport();
    }
}
