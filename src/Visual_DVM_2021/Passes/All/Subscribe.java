package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import GlobalData.Account.AccountRole;
import GlobalData.Account.AccountSubscribeState;
import Repository.Server.RepositoryServer;
import Repository.Server.ServerCode;
import Repository.Server.ServerExchangeUnit;
import Repository.Server.ServerExchangeUnit_2021;
import Repository.Subscribes.Subscriber;
import Visual_DVM_2021.Passes.RepositoryPass_2021;

import static Common.UI.UI.Question;
public class Subscribe extends RepositoryPass_2021 {
    @Override
    protected String getIconPath() {
        return "/icons/Subscribe.png";
    }

    Subscriber subscriber;
    ServerExchangeUnit operation;
    @Override
    protected boolean canStart(Object... args) {
        if (Current.getAccount().role.equals(AccountRole.Undefined))
            Log.Writeln_("Пользователь не зарегистрирован. Работа с подпиской невозможна");
        else {
            subscriber = null;
            operation = null;
            if (RepositoryServer.db.subscribers.Data.containsKey(Current.getAccount().email))
                subscriber = RepositoryServer.db.subscribers.Data.get(Current.getAccount().email);
            switch (Current.getAccount().subscription) {
                case Undefined:
                    break;
                case Active:
                    return Question("Аннулировать подписку");
                case None:
                    return Question("Подписаться");
            }
        }
        return Log.isEmpty();
    }
    @Override
    protected void ServerAction() throws Exception {
        switch (Current.getAccount().subscription) {
            case Undefined:
                break;
            case Active:
                Command(new ServerExchangeUnit_2021(ServerCode.DeleteSubscriber, subscriber.address));
                RepositoryServer.db.Delete(subscriber);
                Current.getAccount().subscription = AccountSubscribeState.None;
                break;
            case None:
                Command(new ServerExchangeUnit_2021(ServerCode.AddSubscriber, "", subscriber = new Subscriber(Current.getAccount().email)));
                RepositoryServer.db.Insert(subscriber);
                Current.getAccount().subscription = AccountSubscribeState.Active;
                break;
        }
        if (Current.getAccount().subscription.equals(AccountSubscribeState.Undefined))
            Current.getAccount().subscription = AccountSubscribeState.None;
        //----------------------------------------------------------------
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getCallbackWindow().ShowSubscription();
    }
}
