package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.Module.Module;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditModule extends EditObjectPass<Module> {
    public EditModule() {
        super(Module.class);
    }
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasProject() && (target.makefile_id == Current.getProject().getInfo().makefile_id))
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
