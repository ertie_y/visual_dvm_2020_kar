package Visual_DVM_2021.Passes.All;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.*;
import java.net.URI;
import java.util.Deque;
import java.util.LinkedList;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
public class ZipFolderPass extends Pass_2021 {
    protected String src;
    protected String dst;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        src = (String) args[0];
        dst = (String) args[1];
        return true;
    }
    @Override
    protected void body() throws Exception {
        File directory = new File(src);
        int total = Utils.getFilesCount(directory);
        String root_name = directory.getName() + "/";
        URI base = directory.toURI();
        Deque<File> queue = new LinkedList<File>();
        queue.push(directory);
        OutputStream out = new FileOutputStream(dst);
        Closeable res = out;
        int count = 0;
        try {
            ZipOutputStream zout = new ZipOutputStream(out);
            res = zout;
            while (!queue.isEmpty()) {
                directory = queue.pop();
                for (File child : directory.listFiles()) {
                    String name = root_name + base.relativize(child.toURI()).getPath();
                    // form.ShowMessage1(name);
                    // form.ShowProgress(total, count, true);
                    count++;
                    if (child.isDirectory()) {
                        queue.push(child);
                        name = name.endsWith("/") ? name : name + "/";
                        zout.putNextEntry(new ZipEntry(name));
                    } else {
                        zout.putNextEntry(new ZipEntry(name));
                        InputStream in = new FileInputStream(child);
                        try {
                            byte[] buffer = new byte[1024];
                            while (true) {
                                int readCount = in.read(buffer);
                                if (readCount < 0) {
                                    break;
                                }
                                zout.write(buffer, 0, readCount);
                            }
                        } finally {
                            in.close();
                        }
                        zout.closeEntry();
                    }
                }
            }
        } finally {
            res.close();
        }
    }

    @Override
    protected boolean validate() {
        return new File(dst).exists();
    }
}
