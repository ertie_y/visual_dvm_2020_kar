package Visual_DVM_2021.Passes.All;

import Common.Database.Database;
import TestingSystem.Group.Group;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.EditObjectPass;
import Visual_DVM_2021.Passes.PassCode_2021;

public class EditGroup extends EditObjectPass<Group> {
    public EditGroup() {
        super(Group.class);
    }
    @Override
    protected Database getDb() {
        return TestingSystem.db;
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        //отправка.
        passes.get(PassCode_2021.EditGroupOnServer).Do(target);
    }
}
