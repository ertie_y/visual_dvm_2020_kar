package Visual_DVM_2021.Passes.All;

import Common.Global;
import Common.Utils.Utils;
import GlobalData.RunStsRecord.RunStsRecord;
import Visual_DVM_2021.Passes.Pass_2021;

public class ClearRunSts extends Pass_2021 {
    @Override
    protected String getIconPath() {
        return "/icons/Clean.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected void showPreparation() throws Exception {
        Global.db.runStsRecords.ClearUI();
    }
    @Override
    protected void body() throws Exception {
        Global.db.DeleteAll(RunStsRecord.class);
        Utils.CleanDirectory(Global.StsDirectory);
    }

    @Override
    protected void showFinish() throws Exception {
        Global.db.runStsRecords.ShowUI();
    }
}
