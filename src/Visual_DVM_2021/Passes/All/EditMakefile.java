package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.Makefile.Makefile;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditMakefile extends EditObjectPass<Makefile> {
    public EditMakefile() {
        super(Makefile.class);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasProject() && (target.getPK().equals(Current.getProject().getInfo().makefile_id)))
            UI.getNewMainWindow().getTestingWindow().ShowProjectMakefile();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
