package Visual_DVM_2021.Passes.All;

import Common.Current;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;
import ProjectData.Files.DBProjectFile;
import ProjectData.Files.FileState;
import Repository.Component.Sapfor.TransformationPermission;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.SapforAnalysis;
import org.apache.commons.io.FileUtils;

import java.util.Vector;

public class SPF_ParseFilesWithOrder extends SapforAnalysis {
    int goodCount = 0;
    int badCount = 0;
    Vector<String> projLines = new Vector<>();

    @Override
    public String phase() {
        return "PARSE_FILES";
    }

    @Override
    protected PassCode_2021 necessary() {
        return null;
    }

    public static boolean silent = false;
    public static boolean precompilation_done = false;

    @Override
    protected boolean canStart(Object... args) throws Exception {
        precompilation_done = false;
        return (
                silent ||
                        (!Global.db.settings.get(SettingName.Precompilation).toBoolean() ||
                                (precompilation_done = passes.get(PassCode_2021.Precompilation).Do()))) && super.canStart(args);
    }

    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() == 0;
    }

    @Override
    public boolean needsConfirmations() {
        return !silent;
    }

    @Override
    protected void unpackMessages() throws Exception {
        target.updateLog(sapfor.getOutput());
        projLines.clear();
        goodCount = 0;
        badCount = 0;

        for (DBProjectFile f : target.db.files.Data.values())
            if (f.isActive()) f.state = FileState.OK;

        for (DBProjectFile f : target.db.files.Data.values()) {
            f.ReadParseMessages();
            f.father.db.Update(f, "state");
            if (f.isActive()) {
                switch (f.state) {
                    case OK:
                    case HasNotes:
                    case HasWarnings:
                        if (f.isActiveProgram())
                            projLines.add(Utils.toU(f.getDepFile().getAbsolutePath()));
                        goodCount++;
                        break;
                    case HasErrors:
                        badCount++;
                        break;
                    default:
                        break;
                }
            }
        }
        FileUtils.writeLines(target.getProjFile(), projLines, false);
    }

    @Override
    protected boolean validate() {
        if (super.validate()) {
            if (badCount > 0) {
                Log.Writeln_("Найдено файлов с синтаксическими ошибками: " + badCount);
                return false;
            }
            if (goodCount == 0) {
                Log.Writeln_("Не найдено ни одного успешно проанализированного файла на языке проекта :" +
                        Utils.DQuotes(target.languageName.getDescription()));
                return false;
            }
            return true;
        }
        return false;
    }

    @Override
    protected void unpack(String packed) throws Exception {
        for (String fileName_ : packed.split("\\|"))
            target.files_order.add(Utils.toW(fileName_));
    }

    @Override
    protected void performPreparation() throws Exception {
        super.performPreparation(); //удаление интеррупта.
        if (!precompilation_done) {
            passes.get(PassCode_2021.Save).Do();
            target.CleanAnalyses();
        }
        //перезапуск сапфора.------
        sapfor.Restart();
        sapfor.cd(target.Home);
        sapfor.GetIntrinsics();
        //--------------------------
        projLines.clear();
        Global.transformationPermission = TransformationPermission.None;

        target.CreateParserOptionsDirs();
        //---------------------------
        for (DBProjectFile f : target.db.files.Data.values()) {
            if (f.isActiveProgram()) {
                projLines.add(Utils.toU(f.file.getAbsolutePath()));
                f.CreateParserOptions();
            }
        }
        FileUtils.writeLines(target.getProjFile(), projLines, false);
        /*
        if (projLines.isEmpty())
            throw new Exception("Язык проекта "+
                    Utils.DQuotes(target.languageName.getDescription())+",\n" +
                    "но не найдено ни одного файла на этом языке");
         */
    }

    @Override
    protected void showPreparation() throws Exception {
        if (!precompilation_done) {
            UI.getNewMainWindow().getProjectWindow().ShowNoAnalyses();
            if (Current.HasFile()) {
                Current.getFile().form.ShowNoMessages();
                Current.getFile().form.ShowNoAnalyses();
            }
        }
    }

    @Override
    protected void performDone() throws Exception {
        super.performDone();
        sapfor.Restart(); //рестарт скидывает все анализы в том числе парс и текущую папку.
        sapfor.cd(target.Home);
        passes.get(PassCode_2021.RestoreSavedArrays).Do();
        setDone();
        /*
        //особенность парса. журнал появляется только сейчас.
        if (!sapfor.getOutput().isEmpty()) {
           target.updateLog(sapfor.getOutput());
        }
         */
        Global.transformationPermission = TransformationPermission.All;
        Global.enable_text_changed = true;
    }

    /*
    @Override
    protected void showDone() throws Exception {
        super.showDone();
        UI.getNewMainWindow().getProjectWindow().ShowProjectSapforLog();
    }
     */

    @Override
    protected void performFinish() throws Exception {
        Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(SettingName.PARSE_FOR_INLINE, "0");
        super.performFinish();
    }

    @Override
    protected void showFinish() throws Exception {
        super.showFinish();
        UI.getNewMainWindow().getProjectWindow().ShowAllAnalyses();
        if (Current.HasFile())
            Current.getFile().form.ShowAllAnalyses();
    }

    @Override
    protected void showFail() throws Exception {
        super.showFail();
    }

    @Override
    protected void FocusResult() {
        super.FocusResult();
        UI.getNewMainWindow().getProjectWindow().FocusFile();
    }

    @Override
    protected void performFail() throws Exception {
        super.performFail();
        Utils.forceDeleteWithCheck(target.getProjFile());
    }
}
