package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.Pass_2021;

import java.io.File;
public class CloseCurrentProject extends Pass_2021<db_project_info> {
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return (target = Current.getProject()) != null;
    }
    @Override
    protected void performPreparation() throws Exception {
        passes.get(PassCode_2021.CloseCurrentFile).Do();
    }
    @Override
    protected void body() throws Exception {
        target.Close();
    }
    @Override
    protected void performDone() throws Exception {
        Current.getSapfor().ResetAllAnalyses();
        Current.getSapfor().cd(new File(Global.Home));
        Current.set(Current.Project, null);
        Current.set(Current.File, null);
        Current.set(Current.Function, null);
        Current.set(Current.ProjectNode, null);
        Current.set(Current.SelectedFile, null);
        Current.set(Current.SelectedDirectory, null);
        //-мб перестраховка. мб и нет.
        Current.set(Current.ParallelVariant,null);
        Current.set(Current.Dimensions,null);
        Current.set(Current.Array,null);
        Current.set(Current.DBArray,null);
        //-
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().ShowNoProject();
       // UI.HideFunctionsFilterForm();
    }
}
