package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.SessionMaxtimeDialog;
import Visual_DVM_2021.Passes.CurrentProjectPass;
public class EditProjectCompilationMaxtime extends CurrentProjectPass {
    SessionMaxtimeDialog f;
    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart(args) &&
                (f = new SessionMaxtimeDialog()).ShowDialog("максимальное время компиляции", Current.getProject().compilation_maxtime);
    }
    @Override
    protected void body() throws Exception {
        target.UpdateCompilationMaxtime(f.Result);
    }
    @Override
    protected void showDone() throws Exception {
        UI.getNewMainWindow().getTestingWindow().ShowProjectMaxCompilationTime();
    }
}
