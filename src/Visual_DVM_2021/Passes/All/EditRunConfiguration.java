package Visual_DVM_2021.Passes.All;
import Common.Current;
import Common.Database.Database;
import Common.Global;
import Common.UI.UI;
import GlobalData.RunConfiguration.RunConfiguration;
import Visual_DVM_2021.Passes.EditObjectPass;
public class EditRunConfiguration extends EditObjectPass<RunConfiguration> {
    public EditRunConfiguration() {
        super(RunConfiguration.class);
    }

    @Override
    protected void showDone() throws Exception {
        super.showDone();
        if (Current.HasProject() && (target.getPK().equals(Current.getProject().getInfo().run_configuration_id)))
            UI.getNewMainWindow().getTestingWindow().ShowProjectRunConfiguration();
    }
    @Override
    protected Database getDb() {
        return Global.db;
    }
}
