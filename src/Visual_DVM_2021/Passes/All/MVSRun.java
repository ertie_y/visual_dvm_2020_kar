package Visual_DVM_2021.Passes.All;
import Common.Current;
import GlobalData.Tasks.RunTask.RunTask;
import ProjectData.Project.db_project_info;
import Visual_DVM_2021.Passes.SSH.TaskConnectionPass;
import GlobalData.Tasks.Supervisor.Remote.MVSRunSupervisor;
public class MVSRun extends TaskConnectionPass {
    public MVSRun() {
        super(MVSRunSupervisor.class);
    }
    @Override
    protected boolean canStart(Object... args) {
        supervisor.Init((RunTask) args[0], this,(db_project_info) args[1]);
        return true;
    }
}
