package Visual_DVM_2021.Passes.All;

import Repository.BugReport.BugReportState;

public class CloseBugReport extends UpdateBugReportField {
    @Override
    protected String getIconPath() {
        return "/icons/CloseBugReport.png";
    }

    @Override
    protected String getButtonText() {
        return "";
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        return super.canStart("state", BugReportState.closed);
    }
}
