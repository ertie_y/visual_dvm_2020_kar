package Visual_DVM_2021.Passes;
import Common.Database.DBObject;
import Common.Database.DBTable;
import Common.Database.Database;
import Common.Passes.ToolBarPass;
public abstract class ObjectPass<D extends DBObject> extends Pass_2021<D> {
    protected Class<D> d; //класс объектов.
    public ObjectPass(Class<D> d_in) {
        d = d_in;
    }
    protected abstract Database getDb(); //источник данных
    public DBTable getTable() {
        return getDb().tables.get(d);
    } //таблица в источнике данных
    @Override
    protected String getButtonText() {
        return "";
    }
}
