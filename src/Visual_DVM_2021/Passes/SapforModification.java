package Visual_DVM_2021.Passes;
import Common.Current;
import Common.Global;
import Common.UI.UI;
import ProjectData.Files.DBProjectFile;

public class SapforModification extends SilentSapforPass {
    public String addOpt1 = "";
    public String addOpt2 = "";
    @Override
    protected boolean isGoodCode() {
        return sapfor.getErrorCode() >= 0;
    }
    @Override
    protected void body() throws Exception {
        sapfor.RunModification(
                getSapforPassName(),
                -Global.messagesServer.getPort(),
                Global.db.settings.packSapforSettings(),
                target.getProjFile().getAbsolutePath(),
                "",
                addOpt1,
                addOpt2);
    }
}
