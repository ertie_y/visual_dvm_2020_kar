package Visual_DVM_2021.Passes;
import Common.Global;
import Common.UI.UI;
import Repository.Component.Sapfor.TransformationPermission;

public class Transformation extends CurrentProjectPass {
    protected TransformationPermission getPermission() { return TransformationPermission.All; }
    //todo - возможно сделать все же интерфейс для преобразований. чтобы наконец обобщить сапфоровские и несапфоровские? хз
    @Override
    protected void performPreparation() throws Exception {
        target.createEmptyVersion(getVersionLetter(), getVersionDescription());
    }
    protected String getVersionDescription() {
        return getDescription();
    }
    @Override
    public String getIconPath() {
        return "/icons/Transformations/" + code().toString() + ".png";
    }
    protected String getVersionLetter() {
        return "v";
    }
    @Override
    protected boolean hasStats() {
        return true;
    }
    @Override
    protected void FocusResult() {
        UI.getNewMainWindow().FocusVersions();
    }
    @Override
    protected void performDone() throws Exception {
        //миграция типов файлов.
        //это несапфоровские дела.тут копируем хедеры сами.
        target.migrateFilesSettings(target.last_version, true, true, false);
    }
    @Override
    protected void performFail() throws Exception {
        super.performFail();
        target.undoLastTransformation();
        UI.getNewMainWindow().getVersionsWindow().RefreshVersionsTree();
    }

}
