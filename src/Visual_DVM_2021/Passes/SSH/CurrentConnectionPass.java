package Visual_DVM_2021.Passes.SSH;
import Common.Current;
//завязан на текущие машину и юзера
public abstract class CurrentConnectionPass<T> extends ConnectionPass<T> {
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    protected boolean canStart(Object... args) {
        return Current.Check(Log, Current.User);
    }
    @Override
    public void Connect() throws Exception {
        machine = Current.getMachine();
        user = Current.getUser();
        super.Connect();
    }
}
