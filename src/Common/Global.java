package Common;

import Common.Database.DataSet;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.GlobalDatabase;
import Repository.Component.*;
import Repository.Component.PerformanceAnalyzer.PerformanceAnalyzer;
import Repository.Component.Sapfor.MessagesServer;
import Repository.Component.Sapfor.Sapfor_F;
import Repository.Component.Sapfor.TransformationPermission;
import Repository.Server.RepositoryServer;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Properties.PropertyName;
import Visual_DVM_2021.UI.Interface.Loggable;
import org.apache.commons.io.FileUtils;
import org.fife.ui.rsyntaxtextarea.AbstractTokenMakerFactory;
import org.fife.ui.rsyntaxtextarea.TokenMakerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.Vector;
import java.util.stream.Collectors;

public class Global {
    public static LinkedHashMap<PropertyName, String> properties = new LinkedHashMap<>();

    public static void migrateOLDDbPatch() throws Exception {
        File oldDBFile = Paths.get(System.getProperty("user.dir"),
                "Data", "db6.sqlite").toFile();
        File newDBFile =
                Paths.get(System.getProperty("user.dir"),
                        "Data",
                        Global.properties.get(PropertyName.GlobalDBName)
                ).toFile();
        if (oldDBFile.exists() && !newDBFile.exists())
            FileUtils.copyFile(oldDBFile, newDBFile);
    }

    public static void SynschronizeProperties() {
        //создать настройки по умолчанию.
        if (Current.mode.equals(Current.Mode.Server)) {
            properties.put(PropertyName.SMTPHost, "smtp.mail.ru");
            properties.put(PropertyName.SMTPPort, "465");
            properties.put(PropertyName.MailSocketPort, "465");
            properties.put(PropertyName.MailAddress, "sapfor.tracker@internet.ru");
            properties.put(PropertyName.MailPassword, "3s4w9e5fs3c1a89AA");
            properties.put(PropertyName.BackupWorkspace, "_sapfor_x64_backups");
            properties.put(PropertyName.BackupHour, "19");
            properties.put(PropertyName.BackupMinute, "30");
            //-
        } else {
            properties.put(PropertyName.Testing, "0");
            properties.put(PropertyName.AutoUpdateSearch, "1");
            properties.put(PropertyName.AutoBugsSynchronize, "1");
            properties.put(PropertyName.AutoTestsSynchronize, "1");
            properties.put(PropertyName.ProjectDBName, "new_project_base.sqlite");
            properties.put(PropertyName.GlobalDBName, "db7.sqlite");
            properties.put(PropertyName.ComponentsWindowWidth, "650");
            properties.put(PropertyName.ComponentsWindowHeight, "250");
        }
        properties.put(PropertyName.ProjectDBName, "new_project_base.sqlite");
        properties.put(PropertyName.TestsDBName, "tests.sqlite");
        // properties.put(PropertyName.ServerHome, "/home/testuser/_sapfor_x64/");
        properties.put(PropertyName.ServerAddress, "alex-freenas.ddns.net");
        properties.put(PropertyName.SocketTimeout, "5000");
        properties.put(PropertyName.BugReportsDBName, "bug_reports.sqlite");
        try {
            File propertiesFile = Paths.get(System.getProperty("user.dir"),
                    "config.properties").toFile();
            if (!propertiesFile.exists()) {
                //файла нет. просто пишем туда умолчания.
                Vector<String> lines = properties.keySet().stream().map(name -> name + "=" + properties.get(name)).collect(Collectors.toCollection(Vector::new));
                FileUtils.writeLines(propertiesFile, lines, false);
            } else {
                //если есть новые настройки и не прописаны в старом файле надо это учесть.
                Vector<String> newProperties = new Vector<>();
                Properties property = new Properties();
                FileInputStream fis = new FileInputStream(propertiesFile);
                property.load(fis);
                for (PropertyName propertyName : properties.keySet()) {
                    String value = property.getProperty(propertyName.toString());
                    if (value != null) properties.replace(propertyName, value);
                    else newProperties.add(propertyName + "=" + properties.get(propertyName));
                }
                if (!newProperties.isEmpty())
                    FileUtils.writeLines(propertiesFile, newProperties, true);
            }
            //времянка пока тесты не отлажены.
            properties.replace(PropertyName.AutoTestsSynchronize, "0");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void UpdateProperties() {
        try {
            File propertiesFile = Paths.get(System.getProperty("user.dir"),
                    "config.properties").toFile();
            Vector<String> values = properties.keySet().stream().map(propertyName -> propertyName + "=" + properties.get(propertyName)).collect(Collectors.toCollection(Vector::new));
            FileUtils.write(propertiesFile, String.join("\n", values), false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final String dateNaN = "NaN";
    //--------------------------------------------------
    //текущая папка системы. в отличие от шарпа никогда не должна меняться.
    public static final String components = "Components";
    public static final String data = "Data";
    public static final String Bugs = "Bugs";
    public static final String BackUps = "BackUps";
    public static final String Temp = "Temp";
    public static final String Projects = "Projects";
    public static final String CompilationTasks = "CompilationTasks";
    public static final String RunTasks = "RunTasks";
    public static final String Sts = "Sts";
    public static final String Repo = "Repo";
    public static final String Tests = "Tests";
    public static final String PerformanceAnalyzer = "PerformanceAnalyzer";
    //</editor-fold>
    //------------------------------------------------------
    public static boolean enable_text_changed = false;
    public static TransformationPermission transformationPermission = TransformationPermission.None;
    //??
    public static DataSet<ComponentType, Component> Components = new ComponentsSet();
    public static MessagesServer messagesServer = null;
    //--------------------------------------------------
    public static GlobalDatabase db = null;
    public static String[] admins_mails = new String[]{
            "vmk-post@yandex.ru",
            "79854210702@ya.ru"
    };
    public static String Home;
    public static File ComponentsDirectory;
    public static File DataDirectory;
    public static File BugReportsDirectory;
    public static File BackUpsDirectory;
    public static File TempDirectory;
    public static File ProjectsDirectory;
    public static File CompilationTasksDirectory;
    public static File RunTasksDirectory;
    public static File StsDirectory;
    public static File RepoDirectory;
    public static File TestsDirectory;
    public static File PerformanceAnalyzerDirectory;
    //------------------------------------------------------------------
    public static Visualiser visualiser = null;
    public static Visualizer_2 visualizer_2 = null;
    public static PerformanceAnalyzer performanceAnalyzer = null;
    //------------------------------------------------------------------
    public static boolean isWindows;
    public static int bad_state = 0;
    public static int need_update = 0;
    public static int need_publish = 0;

    public static String getRepository() {
        return RepositoryServer.home + "Components/";
    }

    //--------------------------------------------------------------------
    public static void CheckVisualiserDirectories() throws Exception {
        Home = System.getProperty("user.dir");
        System.out.println("home directory is" + Utils.Brackets(Home));
        isWindows = System.getProperty("os.name").startsWith("Windows");
        //----------------------------------------------------------
        ComponentsDirectory = Paths.get(Home, components).toFile();
        DataDirectory = Paths.get(Home, data).toFile();
        BugReportsDirectory = Paths.get(Home, Bugs).toFile();
        BackUpsDirectory = Paths.get(Home, BackUps).toFile();
        TempDirectory = Paths.get(Home, Temp).toFile();
        ProjectsDirectory = Paths.get(Home, Projects).toFile();
        CompilationTasksDirectory = Paths.get(Home, CompilationTasks).toFile();
        RunTasksDirectory = Paths.get(Home, RunTasks).toFile();
        StsDirectory = Paths.get(Home, Sts).toFile();
        RepoDirectory = Paths.get(Home, Repo).toFile();
        TestsDirectory = Paths.get(Home, Tests).toFile();
        PerformanceAnalyzerDirectory = Paths.get(Home, PerformanceAnalyzer).toFile();
        //-
        Utils.CheckDirectory(RepoDirectory);
        //-----------------------------------------------------------
        Utils.CheckDirectory(ComponentsDirectory);
        Utils.CheckDirectory(DataDirectory);
        //---------------------------------------------
        Utils.CheckDirectory(BugReportsDirectory);
        //---------------------------------------------
        Utils.CheckDirectory(BackUpsDirectory);
        //-----------------------------------
        Utils.CheckDirectory(TempDirectory);
        Utils.CleanDirectory(TempDirectory);
        //--------------------------------------------------------------
        Utils.CheckDirectory(ProjectsDirectory);
        Utils.CheckDirectory(CompilationTasksDirectory);
        Utils.CheckDirectory(RunTasksDirectory);
        Utils.CheckDirectory(StsDirectory);
        Utils.CheckDirectory(TestsDirectory);
        Utils.CheckDirectory(PerformanceAnalyzerDirectory);
    }

    public static void CheckServerDirectories() throws Exception {
        Home = System.getProperty("user.dir");
        System.out.println("home directory is" + Utils.Brackets(Home));
        isWindows = false; //сервер всегда линух.
        //----------------------------------------------------------
        ComponentsDirectory = Paths.get(Home, components).toFile();
        DataDirectory = Paths.get(Home, data).toFile();
        BugReportsDirectory = Paths.get(Home, Bugs).toFile();
        BackUpsDirectory = Paths.get(Home, BackUps).toFile();
        TempDirectory = Paths.get(Home, Temp).toFile();
        CompilationTasksDirectory = Paths.get(Home, CompilationTasks).toFile();
        RunTasksDirectory = Paths.get(Home, RunTasks).toFile();
        TestsDirectory = Paths.get(Home, Tests).toFile();
        //-
        //-----------------------------------------------------------
        Utils.CheckDirectory(ComponentsDirectory);
        Utils.CheckDirectory(DataDirectory);
        //---------------------------------------------
        Utils.CheckDirectory(BugReportsDirectory);
        //---------------------------------------------
        Utils.CheckDirectory(BackUpsDirectory);
        //-----------------------------------
        Utils.CheckDirectory(TempDirectory);
        Utils.CleanDirectory(TempDirectory);
        //--------------------------------------------------------------
        Utils.CheckDirectory(CompilationTasksDirectory);
        Utils.CheckDirectory(RunTasksDirectory);
        Utils.CheckDirectory(TestsDirectory);
    }

    public static void FinishApplication() {
        try {
            if (db != null) db.Disconnect();
            if (RepositoryServer.db != null)
                RepositoryServer.db.Disconnect();
            if (TestingSystem.db != null)
                TestingSystem.db.Disconnect();
            if (visualizer_2 != null)
                visualizer_2.Shutdown();
            if (messagesServer != null)
                messagesServer.Shutdown();
            performanceAnalyzer.Shutdown();
        } catch (Exception ex) {
            if (Log != null) {
                Log.PrintException(ex);
            } else {
                ex.printStackTrace();
            }
        }
        System.exit(0);
    }

    public static void ActivateDB() throws Exception {
        db = new GlobalDatabase();
        db.Connect();
        db.CreateAllTables();
        db.Synchronize();
        //-
        RepositoryServer.ActivateDB();
        TestingSystem.ActivateDB();
    }

    public static void RefreshUpdatesStatus() {
        Components.RefreshUI();
        ValidateComponentsStates();
        if (UI.HasNewMainWindow())
            UI.getNewMainWindow().ShowUpdatesIcon();
    }

    public static boolean ValidateComponentsStates() {
        bad_state = need_update = need_publish = 0;
        for (Component component : Components.Data.values()) {
            if (component.isVisible() && component.isNecessary()
            ) {
                switch (component.getState()) {
                    case Not_found:
                    case Unknown_version:
                    case Old_version:
                        bad_state++;
                        break;
                    case Needs_update:
                        need_update++;
                        break;
                    case Needs_publish:
                        need_publish++;
                        break;
                }
            }
        }
        return (bad_state == 0);
    }

    //------------------------------------------------------------------------
    public static Loggable Log;

    //-
    //не надо делать его проходом.
    // потому что не попадает под схему с журналом визуализатора.
    public static void Init(String... args) {
        Current.mode = (((args.length != 2) || (!args[0].equals("--port")))) ? Current.Mode.Server : Current.Mode.Normal;
        SynschronizeProperties();

        try {
            switch (Current.mode) {
                case Normal:
                    CheckVisualiserDirectories(); //первым делом проверять папки.

                    Log = new Loggable() {
                        @Override
                        public String getLogHomePath() {
                            return Global.ComponentsDirectory.getAbsolutePath();
                        }

                        @Override
                        public String getLogName() {
                            return "VisualDVM";
                        }
                    };
                    Log.ClearLog();
                    //-
                    visualizer_2 = new Visualizer_2( Integer.parseInt(args[1]));
                    visualizer_2.Connect();
                    //если делать раньше, то не удастся убить сервер.
                    if (Utils.ContainsCyrillic(Global.Home)) {
                        UI.Info("В пути к корневой папке " + Utils.DQuotes(Global.Home) + "\n" +
                                "Найдены русские буквы.\n" +
                                "Визуализатор завершает работу."); //
                        FinishApplication();
                    }
                    messagesServer = new MessagesServer();
                    messagesServer.Start();
                    //создание списков служебных объектов
                    Current.CreateAll();
                    UI.CreateAll();
                    Components.put(ComponentType.Visualiser, visualiser = new Visualiser());
                    Components.put(ComponentType.Sapfor_F, (Component) Current.set(Current.Sapfor, new Sapfor_F()));
                    Components.put(ComponentType.Visualizer_2, visualizer_2);
                    Components.put(ComponentType.PerformanceAnalyzer, performanceAnalyzer = new PerformanceAnalyzer());
                    Components.put(ComponentType.Instruction, new Instruction());
                    //-
                    for (Component component : Components.Data.values())
                        if (component.isVisible()) component.InitialVersionCheck();
                    //-------------------------------------------------------------
                    Pass_2021.CreateAll();
                    //-
                    Utils.init();
                    //оно не зависит от бд. значит можно создать уже сейчас.
                    //но зависит от таблиц. значит создается после них.??
                    UI.CreateComponentsForm();
                    //пользовательская подсвитка.
                    AbstractTokenMakerFactory atmf = (AbstractTokenMakerFactory) TokenMakerFactory.getDefaultInstance();
                    atmf.putMapping("text/FortranSPF", "Common.UI.Themes.FortranSPFTokenMaker");
                    atmf.putMapping("text/FreeFortranSPF", "Common.UI.Themes.FreeFortranSPFTokenMaker");
                    //  FoldParserManager.get().addFoldParserMapping("text/FortranSPF", new FortranFolder());
                    //-
                    if (properties.get(PropertyName.AutoUpdateSearch).equals("1"))
                        Pass_2021.passes.get(PassCode_2021.GetComponentsActualVersions).Do();
                    //-
                    boolean flag = true;
                    do {
                        UI.ShowComponentsWindow();
                        if (flag = (!ValidateComponentsStates())) {
                            if (!UI.Question("Найдено " + bad_state + " некорректных необходимых компонент.Работа визуализатора невозможна.\n" +
                                    "Вернуться к окну компонент"
                            )) {
                                UI.Info("Визуализатор завершает работу.");
                                FinishApplication();
                            }
                        }
                    } while (flag);
                    //-------------------------
                    migrateOLDDbPatch();
                    ActivateDB();
                    if (properties.get(PropertyName.AutoBugsSynchronize).equals("1"))
                        Pass_2021.passes.get(PassCode_2021.SynchronizeBugReports).Do();
                    if (properties.get(PropertyName.AutoTestsSynchronize).equals("1"))
                        Pass_2021.passes.get(PassCode_2021.SynchronizeTests).Do();
                    Pass_2021.CheckAllStats();
                    UI.CreateMenus();
                    UI.CreateWindows();
                    break;
                case Server:
                    System.out.println("server started");
                    CheckServerDirectories(); //первым делом проверять папки.

                    Log = new Loggable() {
                        @Override
                        public String getLogHomePath() {
                            return Paths.get(System.getProperty("user.dir")).toString();
                        }

                        @Override
                        public String getLogName() {
                            return "VisualDVM";
                        }
                    };
                    Log.ClearLog();
                    System.out.println(Log.getLogFile());
                    RepositoryServer server = new RepositoryServer();
                    server.Start();
                    System.out.println("+");
                    System.exit(0);
                    break;
                case Undefined:
                    break;
            }
        } catch (Exception ex) {
            System.out.println("VISUALISER FAILED");
            ex.printStackTrace();
            if (Global.Log != null)
                Global.Log.PrintException(ex);
            System.exit(0);
        }
    }

    //---->>
    public static Thread testing = new Thread(() -> {
        while (true) {
            try {
                Thread.sleep(5000);
                if (UI.HasNewMainWindow()) {
                    System.out.println("Testing system thread started...");
                    Pass_2021.passes.get(PassCode_2021.ActualizeTestsTasks).Do();
                    System.out.println("Testing system thread done");
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    });
}
