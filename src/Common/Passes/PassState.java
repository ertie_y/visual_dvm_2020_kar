package Common.Passes;
public enum PassState {
    Inactive,
    Interrupted,
    CantStart,
    Failed,
    Crushed,
    Done,
    ExternalCrushed //исключение за рамками Action. неординарная ситуация.
    ;
    public String getDescription() {
        switch (this) {
            case ExternalCrushed:
                return "внешнее исключение";
            case Inactive:
                return "неактивен";
            //-----------------------------------------
            case Interrupted:
                return "остановлен пользователем";
            case CantStart:
                return "не может быть выполнен";
            case Failed:
                return "завершён с ошибками";
            case Crushed:
                return "завершён с исключением";
            case Done:
                return "успешно выполнен";
            default:
                break;
        }
        return "";
    }
}
