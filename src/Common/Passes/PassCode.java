package Common.Passes;
public enum PassCode {
    Undefined,
    CopyProject,
    //--------------
    ApplyRecommendedTransformations,
    ApplyRecommendedSettings,
    AttachFileToProject,

    DeleteAttachments,
    //--
    ;
    public String getDescription() {
        switch (this) {
            case DeleteAttachments:
                return "Удалить все вложения";
            case AttachFileToProject:
                return "Прикрепить вложение к проекту";
            case ApplyRecommendedSettings:
                return "Применить рекомендованные настройки";
            case ApplyRecommendedTransformations:
                return "Выполнить рекомендованные преобразования";
            case CopyProject:
                return "Копирование проекта";
            default:
                return toString();
        }
    }
}
