package Common.Passes.UI;
import Common.Global;
import Common.UI.Windows.Dialog.Dialog;
import Visual_DVM_2021.Passes.Pass_2021;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
public class PassForm extends Dialog<Pass_2021, PassFields> {
    public PassForm(Pass_2021 pass_in) {
        super(PassFields.class);
        Result = pass_in;
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                Result.animation_sem.release();
            }
        });
    }
    public boolean ShowDialog() {
        return super.ShowDialog("");
    }
    @Override
    public int getDefaultWidth() {
        return 600;
    }
    @Override
    public int getDefaultHeight() {
        return 250;
    }
    //кнопок нет
    @Override
    public void CreateButtons() {
    }
/*
    @Override
    public boolean isOnTop() {
        return false;
    }
 */

    @Override
    protected void onCancel() {
        try {
            Result.Interrupt();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
    @Override
    public String getTitleText() {
        return Result.getDescription();
    }
    public void ShowSapforMessage(String message) {
        String[] data = message.split(":");
        switch (data[0]) {
            case "message_1":
                fields.Label1.setText((data.length > 1) ? data[1] : "");
                break;
            case "message_2":
                fields.Label2.setText((data.length > 1) ? data[1] : "");
                break;
            case "progress":
                fields.ProgressBar.setValue((data.length > 1) ? Integer.parseInt(data[1]) : 0);
                break;
        }
    }
    public void ShowMessage1(String message) {
        fields.Label1.setText((message.length() > 1) ? message : "");
    }
    public void ShowMessage2(String message) {
        fields.Label2.setText((message.isEmpty() ? "" : message));
    }
    public void ShowProgress(int total, int current, boolean auto) {
        lTitle.setText(Result.getDescription() + " " + (current + 1) + "/" + total);
        if (auto) {
            double dp = ((double) current / total) * 100;
            int p = (int) dp;
            fields.ProgressBar.setValue(p);
        }
    }
    public void ShowProgress2(int total, int current, String comment) {
        fields.Label2.setText(comment + " " + current + "/" + total);
        double dp = ((double) current / total) * 100;
        int p = (int) dp;
        fields.ProgressBar.setValue(p);
    }
    public void ShowProgressTextOnly(int total, int current, String comment){
        lTitle.setText(lTitle.getText()+" : "+comment + " " + current + "/" + total);
    }
}
