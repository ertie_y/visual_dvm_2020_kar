package Common.Passes.UI;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class PassFields implements DialogFields {
    public JPanel content;
    public JProgressBar ProgressBar;
    public JLabel Label1;
    public JLabel Label2;
    @Override
    public Component getContent() {
        return content;
    }
}