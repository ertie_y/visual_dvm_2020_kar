package Common.Passes;
public abstract class ToolBarPass<R> extends Pass<R> {
    @Override
    public String getButtonText() {
        return "";
    }
    @Override
    public boolean HasButton() {
        return true;
    }
    @Override
    public boolean HasStats() {
        return false;
    }
}
