package Common.Passes;
import Common.Current;
import Common.Global;
import Common.Passes.UI.PassForm;
import Common.UI.Button.StyledButton;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.UI;
import Common.Utils.Stopwatch;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.All.EditProjectCompilationMaxtime;
import Visual_DVM_2021.Passes.All.EditProjectRunMaxtime;
import Visual_DVM_2021.Passes.OLD.DeleteAttachments;
import Visual_DVM_2021.Passes.OLD.AttachFileToProject;
import Visual_DVM_2021.Passes.OLD.ApplyRecommendedSettings;
import Visual_DVM_2021.Passes.OLD.ApplyRecommendedTransformations;
import Visual_DVM_2021.Passes.OLD.CopyProject;
import Visual_DVM_2021.PassStats.PassStats;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.LinkedHashMap;
import java.util.Stack;
import java.util.Vector;
import java.util.concurrent.Semaphore;
public abstract class Pass<T> {
    public static boolean ActiveScenario = false; //?
    public static LinkedHashMap<PassCode, Pass> Passes = new LinkedHashMap<>();
    public static Vector<Pass> FAPasses = new Vector<>();
    public JMenuItem menu_item = null;
    public JButton button = null;
    public boolean has_bad_file = false;
    public PassForm form = null;
    public Semaphore animation_sem;
    public Object last_error = null;
    public PassStats stats = null;
    //редко но бывает нужен.
    public T target = null;
    public String getDescription() {
        return code().getDescription();
    }
    //<editor-fold desc="поля">
    public PassState state = PassState.Inactive;
    protected TextLog Log = new TextLog();
    protected SwingWorker dispatcher = null;
    public Pass() {
        AbstractAction default_action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                } catch (Exception ex) {
                    Global.Log.PrintException(ex);
                }
                Do();
            }
        };
        AbstractAction analysis_action = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (Done()) FocusResult();
                else {
                    try {
                    } catch (Exception ex) {
                        Global.Log.PrintException(ex);
                    }
                    Do();
                }
            }
        };
        if (HasMenuItem()) {
            menu_item =
                    (getIconPath() != null) ?
                            new StyledMenuItem(code().getDescription(), getIconPath()) :
                            new StyledMenuItem(code().getDescription());
            menu_item.addActionListener(isAnalysis() ? analysis_action : default_action);
        }
        if (HasButton()) {
            button =
                    (getIconPath() != null) ?
                            new StyledButton(getButtonText(), getIconPath()) :
                            new StyledButton(getButtonText());
            button.setToolTipText(code().getDescription());
            button.addActionListener(isAnalysis() ? analysis_action : default_action);
        }
    }
    public static void CheckAllStats() throws Exception {
        //  for (Pass pass : FAPasses) pass.CheckStats();
        //  FAPasses.sort(new SortPassesByStats());
    }
    public static void AddPass(Pass pass) {
        Passes.put(pass.code(), pass);
        if (pass.HasStats()) FAPasses.add(pass);
    }
    public static void CreateAll() {
        AddPass(new CopyProject());
        //------------------------------
        //-----------------------------
        AddPass(new ApplyRecommendedTransformations());
        AddPass(new ApplyRecommendedSettings());
        AddPass(new AttachFileToProject());
        AddPass(new DeleteAttachments());
    }
    public static Throwable getCauseRec(Throwable ex) {
        Throwable cause = ex.getCause();
        return (cause == null) ? ex : getCauseRec(cause);
    }
    //</editor-fold>
    protected PassCode necessary() {
        return PassCode.Undefined;
    }
    protected PassCode opposite() {
        return PassCode.Undefined;
    }
    protected boolean ResetNecessary() {
        return false;
    }
    public boolean HasMenuItem() {
        return false;
    }
    public boolean HasButton() {
        return false;
    }
    public boolean HasStats() {
        return HasButton();
    }
    protected String getIconPath() {
        return null;
    }
    public boolean isAnalysis() {
        return false;
    }
    public String getButtonText() {
        return code().getDescription();
    }
    public void Mark() {
    }
    protected boolean NeedsUI() {
        return false;
    }
    protected boolean isSilent() {
        return true;
    }
    protected void FocusResult() {
    }
    protected boolean NeedsNecessary() {
        return (necessary() != PassCode.Undefined) &&
                (ResetNecessary() || (!Passes.get(necessary()).Done()));
    }
    //https://pro-java.ru/java-dlya-nachinayushhix/peremennoe-chislo-argumentov-metodov-yazyka-java/
    protected void InitArgs(Object... args) throws Exception {
    }
    protected boolean CanStart() {
        return true;
    }
    protected void Prepare() throws Exception {
    }
    protected abstract void Action() throws Exception;
    protected void Validate() throws Exception {
        state = PassState.Done;
    }
    protected void PerformCanNotStart() throws Exception {
    }
    protected void PerformFinish() throws Exception {
        Mark();
    }
    protected void PerformDone() throws Exception {
    }
    protected void PerformFail() throws Exception {
    }
    protected void Body() throws Exception {
        Action();
        Validate();
    }
    protected void LogException(Exception ex) {
        Log.Writeln("исключение:\n" + ex.getMessage());
        Global.Log.PrintException(ex);
    }
    protected void ShowFailSummary() {
        UI.Error("Проход " + Utils.DQuotes(code().getDescription()) + "\n" + state.getDescription() + "\n\n" + Log);
    }
    public boolean Reset() {
        if (state.equals(PassState.Done)) {
            state = PassState.Inactive;
            Mark();
            return true;
        }
        Mark();
        return false;
    }
    public void Interrupt() throws Exception {
        UI.Info("Проход " + Utils.Brackets(code().getDescription()) + " не разрешено прерывать.");
    }
    protected boolean Start(Object... args) {
        Stopwatch timer = new Stopwatch();
        timer.Start();
        state = PassState.Inactive;
        last_error = null;
        target = null;
        Log.Clear();
        try {
            InitArgs(args);
            if (CanStart()) {
                //  UI.Print(DebugPrintLevel.Passes, code().getDescription() + " стартует..");
                Prepare();
                if (Current.hasUI() && NeedsUI()) {
                    animation_sem = new Semaphore(1);
                    animation_sem.acquire();
                    // Current.set(Current.PassForm, form = new PassForm(this));
                    //todo перейти на нить чтобы можно было хоть как то ее убивать.ну это потом.
                    dispatcher = new SwingWorker() {
                        @Override
                        protected Object doInBackground() throws Exception {
                            //  UI.Print(DebugPrintLevel.Passes, "фоновое выполнение началось");
                            try {
                                Body();
                            } catch (Exception ex) {
                                last_error = ex;
                            }
                            //  UI.Print(DebugPrintLevel.Passes, "Ожидание активации окна анимации ");
                            //ждем пока окно откроется и освободит его.
                            try {
                                animation_sem.acquire();
                            } catch (InterruptedException e) {
                                Global.Log.PrintException(e);
                            }
                            //и уничтожаем его.
                            //  UI.Print(DebugPrintLevel.Passes, "Окно анимации активировано.");
                            form.dispose();
                            form = null;
                            //   UI.Print(DebugPrintLevel.Passes, "Окно анимации закрыто.");
                            animation_sem.release();
                            return null;
                        }
                        @Override
                        protected void done() {
                            //тут ничего не анализируем. события - отдельный поток. тут это не нужно
                        }
                    };
                    dispatcher.execute();
                    form.ShowDialog();
                    //  UI.Print(DebugPrintLevel.Passes, "фоновое выполнение закончено, обработка резульатов");
                    if (last_error != null) {
                        state = PassState.Crushed;
                        LogException((Exception) last_error);
                    }
                    //  UI.Print(DebugPrintLevel.Passes, "OK");
                    dispatcher = null;
                } else
                    Body();
                timer.Stop();
                //    UI.Print(DebugPrintLevel.Passes, code().getDescription() + " окончен за " + timer.Print());
            } else {
                state = PassState.CantStart;
            }
        } catch (Exception ex) {
            state = PassState.Crushed;
            LogException(ex);
        }
        if (state == PassState.CantStart) {
            try {
                PerformCanNotStart();
            } catch (Exception e) {
                LogException(e);
                Global.Log.PrintException(e);
            }
            if (!Log.isEmpty())
                ShowFailSummary();
        } else {
            try {
                //  UI.Print(DebugPrintLevel.Passes, code().getDescription() + " PERFORM FINISH");
                PerformFinish();
            } catch (Exception ex) {
                LogException(ex);
                ShowFailSummary();
            }
            switch (state) {
                case Done:
                    try {
                        if (!opposite().equals(PassCode.Undefined))
                            Passes.get(opposite()).Reset();
                        PerformDone();
                    } catch (Exception ex) {
                        LogException(ex);
                        ShowFailSummary();
                    }
                    break;
                case Failed:
                case Crushed:
                case Interrupted:
                    try {
                        PerformFail();
                    } catch (Exception ex) {
                        LogException(ex);
                    }
                    ShowFailSummary();
                    break;
            }
        }
        return Done();
    }
    private void FillR(PassCode code_in, Stack ToDo, boolean reset, Vector<String> ToPrint) {
        Pass p = Passes.get(code_in);
        if (reset) p.Reset();
        ToDo.push(p);
        if (!p.isSilent())
            ToPrint.insertElementAt(p.code().getDescription(), 0);
        if (p.NeedsNecessary()) FillR(p.necessary(), ToDo, p.ResetNecessary(), ToPrint);
    }
    //как и в шарпе ограничение. зависимые проходы не должны иметь аргументов.
    public boolean Do(Object... args) {
        Stack ToDo = new Stack();
        Vector<String> ToPrint = new Vector<>();
        ToPrint.add(code().getDescription());
        if (NeedsNecessary()) {
            FillR(necessary(), ToDo, ResetNecessary(), ToPrint); //заполнить всеми необходимыми проходами.
        }
        /*
        if (!isSilent() &&
                Global.visualiser.Settings.get(SettingName.ConfirmPassesStart).toBoolean() &&
                !UI.Question("Выполнить проход(ы)\n\n" + String.join("\n", ToPrint)))
            return false;

         */
        while (ToDo.size() > 0) {
            if (!((Pass) ToDo.pop()).Start()) return false;
        }
        //теперь можем выполнить сам проход и показать его вкладку если надо.
        // if (Global.visualiser.Settings.get(SettingName.FocusPassesResult).toBoolean())
        //     FocusResult();
        /*
            if (!isSilent() && Global.visualiser.Settings.get(SettingName.ShowPassesDone).toBoolean()) {
                UI.Info("Проход(ы)\n\n" + String.join("\n", ToPrint) +
                        "\nуспешно выполнен(ы)!");
            }
             */
        return Start(args);
    }
    public PassCode code() {
        return PassCode.valueOf(this.getClass().getSimpleName());
    }
    public boolean Done() {
        return state == PassState.Done;
    }
    public void SetDone() {
        if (state != PassState.Done) {
            state = PassState.Done;
            Mark();
        }
    }
}