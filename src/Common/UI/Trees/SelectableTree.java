package Common.UI.Trees;
import Common.Current;
import Common.UI.Selectable;

import javax.swing.tree.DefaultMutableTreeNode;
public class SelectableTree extends DataTree {
    public SelectableTree(DefaultMutableTreeNode root_in) {
        super(root_in);
    }
    @Override
    public void LeftMouseAction1() {
        Object element = Current.get(getCurrent());
        if ((element instanceof Selectable)) {
            ((Selectable) element).SwitchSelection();
            updateUI();
        }
    }
}
