package Common.UI.Trees;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;
import ProjectData.SapforData.FileObjectWithMessages;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
public class GraphTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof FileObjectWithMessages) {
            FileObjectWithMessages target = (FileObjectWithMessages) o;
            setIcon(Utils.getIcon(target.ImageKey()));
            setFont(Current.getTheme().Fonts.get(target.getFont()));
        } else {
            setIcon(null);
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
        }
        setForeground(tree.getForeground());
        return this;
    }
}
