package Common.UI.Trees;
import Common.UI.ControlForm;
public class TreeForm<C extends StyledTree> extends ControlForm<C> {
    public TreeForm(Class<C> class_in) {
        super(class_in);
    }
    //временно, чтобы не затрагивать коды раньше времени.
    public StyledTree getTree() {
        return control;
    }
    @Override
    protected void refresh() {
        getTree().updateUI();
        getTree().ExpandAll();
    }
}
