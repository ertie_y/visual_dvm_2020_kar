package Common.UI.Trees;
import Common.Current;
import Common.UI.Menus.GraphMenu;
import Common.UI.Themes.ThemeElement;
import Common.UI.UI;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
public class StyledTree extends JTree implements ThemeElement {
    public DefaultMutableTreeNode root = null;
    //-включение /отключение событий --
    protected boolean events_on = true;
    //---------------------------------
    private final GraphMenu menu;
    public StyledTree(DefaultMutableTreeNode root_in) {
        super(root_in);
        root = root_in;
        setOpaque(true);
        setToggleClickCount(0);  //отключение сворачивание разворачивания по двойному клику
        //--
        if (!getRenderer().equals(TreeRenderers.RendererUndefined))
            setCellRenderer(UI.TreeRenderers.get(getRenderer()));
        //--
        getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        addTreeSelectionListener(e -> {
            if (events_on && e.isAddedPath())
                SelectionAction(e.getPath());
        });
        addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 1) {
                    switch ((e.getButton())) {
                        //левая клавиша.
                        case MouseEvent.BUTTON1:
                            if (getSelectionCount() > 0) LeftMouseAction1();
                            break;
                        //колесо
                        case MouseEvent.BUTTON2:
                            break;
                        //правая клавиша
                        case MouseEvent.BUTTON3:
                            if (menu != null)
                                menu.Show(e);
                            break;
                    }
                } else {
                    if (e.getClickCount() == 2) {
                        switch ((e.getButton())) {
                            case MouseEvent.BUTTON1:
                                if (getSelectionCount() > 0) LeftMouseAction2();
                                break;
                        }
                    }
                }
            }
        });
        menu = createMenu();
        applyTheme();
        ExpandAll();
    }
    public TreeRenderers getRenderer() {
        return TreeRenderers.RendererUndefined;
    }
    public void EventsOn() {
        events_on = true;
    }
    public void EventsOff() {
        events_on = false;
    }
    public String getBranchesName() {
        return "";
    }
    public void LeftMouseAction1() {
    }
    public void LeftMouseAction2() {
    }
    public void SelectionAction(TreePath e) {
    }
    @Override
    public void applyTheme() {
        setBackground(Current.getTheme().trees_background);
        setForeground(Current.getTheme().foreground);
        menu.applyTheme();
    }
    protected GraphMenu createMenu() {
        return new GraphMenu(this);
    }
    public DefaultMutableTreeNode getNodeByRow(int row) {
        DefaultMutableTreeNode res = null;
        if (row >= 0) {
            TreePath p = getPathForRow(row);
            res = (DefaultMutableTreeNode) p.getLastPathComponent();
        }
        return res;
    }
    protected int getStartLine(){
        return 0;
    }

    public void ExpandAll() {
        for (int i = getStartLine(); i < getRowCount(); ++i)
            expandRow(i);
    }
    public void CollapseAll() {
        for (int i = getRowCount() - 1; i >= getStartLine(); --i) {
            collapseRow(i);
        }
    }
    protected boolean findNode(Object userObject, Object criteria) {
        return true;
    }
    public DefaultMutableTreeNode FindByCriteria(Object criteria) {
        DefaultMutableTreeNode res = root;
        for (int i = 0; i < getRowCount(); ++i) {
            DefaultMutableTreeNode node = getNodeByRow(i);
            if (findNode(node.getUserObject(), criteria)) {
                res = node;
                break;
            }
        }
        return res;
    }
    public TreePath ShowNode(DefaultMutableTreeNode node) {
        TreePath res = new TreePath(node.getPath());
        scrollPathToVisible(res);
        return res;
    }
    public TreePath ShowNodeByCriteria(Object criteria) {
        return ShowNode(FindByCriteria(criteria));
    }
    public void SelectNodeByCriteria(Object criteria) {
        EventsOff();
        TreePath res = ShowNodeByCriteria(criteria);
        setSelectionPath(res);
        EventsOn();
    }
    /*
    public void RedrawNode(DefaultMutableTreeNode node) {
        ((DefaultTreeModel) getModel()).nodeStructureChanged(node);
    }
     */
    public void SelectNode(DefaultMutableTreeNode node) {
        setSelectionPath(new TreePath(node));
    }
    public void ExpandNode(DefaultMutableTreeNode node){
        expandPath(new TreePath(node));
    }
}
