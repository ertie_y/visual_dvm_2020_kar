package Common.UI.Trees;
public enum TreeRenderers {
    RendererUndefined,
    //-
    RendererGraph,
    RendererFile,
    RendererRemoteFile,
    RendererVersion,
    RendererRule,
    RendererAttachment,
    RendererSelection
}
