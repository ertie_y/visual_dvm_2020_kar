package Common.UI.Trees;
import Common.Current;
import Common.UI.Selectable;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
public class SelectionTreeCellRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof Selectable) {
            Selectable selectable = (Selectable) o;
            setText(selectable.getSelectionText());
            setIcon(selectable.GetSelectionIcon());
        } else {
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreeItalic));
            setIcon(null);
        }
        setForeground(tree.getForeground());
        return this;
    }
}
