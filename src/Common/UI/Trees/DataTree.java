package Common.UI.Trees;
import Common.Current;
import Common.Global;
import Common.UI.DataControl_OLD;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
public class DataTree extends StyledTree implements DataControl_OLD {
    public DataTree(DefaultMutableTreeNode root_in) {
        super(root_in);
    }
    public void ChangeCurrentObject(DefaultMutableTreeNode node) {
        if (getCurrent() != Current.Undefined)
            Current.set(getCurrent(), node.getUserObject());
    }
    @Override
    public void ShowCurrentObject() throws Exception {
    }
    @Override
    public void ShowNoCurrentObject() throws Exception {
    }
    @Override
    public void SelectionAction(TreePath path) {
        ChangeCurrentObject((DefaultMutableTreeNode) path.getLastPathComponent());
        try {
            ShowCurrentObject();
        }
        catch (Exception ex){
            Global.Log.PrintException(ex);
        }
    }
    public void SelectNodeByCriteria(Object criteria) {
        EventsOff();
        TreePath res = ShowNodeByCriteria(criteria);
        setSelectionPath(res);
        ChangeCurrentObject((DefaultMutableTreeNode) res.getLastPathComponent());
        EventsOn();
    }
}

