package Common.UI.Button;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;

import javax.swing.*;
public class StyledButton extends JButton {
    public StyledButton(String text) {
        super(text);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        setFocusable(true);
        setFocusPainted(true);
        setBorderPainted(false);
    }
    public StyledButton(String text, String icon_path, boolean small) {
        super(text);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        setIcon(small? Utils.getTabIcon(icon_path):Utils.getIcon(icon_path));
        setFocusable(true);
        setFocusPainted(true);
        setBorderPainted(false);
    }
    public StyledButton(String text, String icon_path) {
        this(text, icon_path, false);
    }
}
