package Common.UI.ComboBox;

import Common.UI.Menus.TextComboBoxMenu;
import Common.UI.Menus.TextEditorMenu;

import javax.swing.*;
import javax.swing.text.JTextComponent;

public class StyledTextComboBox extends JComboBox<String> {
    public StyledTextComboBox(){
        setComponentPopupMenu(new TextComboBoxMenu(this));
    }
}
