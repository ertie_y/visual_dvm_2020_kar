package Common.UI;

import Common.Current;
import Common.Global;
import Common.Passes.Pass;
import Common.Passes.PassCode;
import Common.UI.Menus.AttachementsMenu;
import Common.UI.Tables.*;
import Common.UI.Themes.*;
import Common.UI.Trees.GraphTreeCellRenderer;
import Common.UI.Trees.SelectionTreeCellRenderer;
import Common.UI.Windows.FormType;
import Common.UI.Windows.SearchReplaceForm;
import GlobalData.CompilerEnvironment.UI.CompilerEnvironmentValueEditor;
import GlobalData.CompilerEnvironment.UI.CompilerEnvironmentValueRenderer;
import GlobalData.CompilerOption.UI.CompilerOptionParameterNameRenderer;
import GlobalData.CompilerOption.UI.CompilerOptionParameterValueEditor;
import GlobalData.CompilerOption.UI.CompilerOptionParameterValueRenderer;
import GlobalData.DBLastProject.UI.LastProjectsMenu;
import GlobalData.EnvironmentValue.UI.EnvironmentValuesMenu;
import GlobalData.RemoteFile.UI.RemoteFileChooser;
import GlobalData.RemoteFile.UI.RemoteFileRenderer;
import GlobalData.Settings.SettingName;
import GlobalData.Settings.UI.ComparsionMenu;
import GlobalData.Settings.UI.GlobalMenu;
import GlobalData.Settings.UI.SapforSettingsMenu;
import GlobalData.Settings.UI.VisualiserSettingsMenu;
import ProjectData.Files.UI.FileLanguageMenu;
import ProjectData.Files.UI.FileStyleMenu;
import ProjectData.Files.UI.FileTypeMenu;
import ProjectData.Files.UI.FilesTreeCellRenderer;
import ProjectData.Project.UI.CleaningMenu;
import ProjectData.Project.UI.ProjectLanguageMenu;
import ProjectData.Project.UI.VersionsTreeCellRenderer;
import ProjectData.SapforData.Arrays.UI.DimensionRenderer;
import ProjectData.SapforData.Arrays.UI.DimensionStateChanger;
import ProjectData.SapforData.Arrays.UI.RulesTreeCellRenderer;
import ProjectData.SapforData.Variants.UI.VariantRankRenderer;
import Repository.Component.UI.ComponentsForm;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.UI.AnalysesMenu;
import Visual_DVM_2021.Passes.UI.DebugMenu;
import Visual_DVM_2021.Passes.UI.TransformationsMenu;
import Visual_DVM_2021.UI.Interface.MainWindow;
import Visual_DVM_2021.UI.Main.MainForm_2021;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import javax.accessibility.Accessible;
import javax.accessibility.AccessibleContext;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableCellEditor;
import javax.swing.table.TableCellRenderer;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import javax.swing.tree.TreeCellRenderer;
import java.awt.*;
import java.util.LinkedHashMap;
import java.util.Vector;

import static Common.UI.Tables.TableEditors.*;
import static Common.UI.Tables.TableRenderers.*;
import static Common.UI.Trees.TreeRenderers.*;
import static javax.swing.JOptionPane.showMessageDialog;

public class UI {

    //<editor-fold desc="OLD">
    public static final Highlighter.HighlightPainter GoodLoopPainter = new DefaultHighlighter.DefaultHighlightPainter(new Color(152, 251, 152, 90));
    public static final Highlighter.HighlightPainter BadLoopPainter = new DefaultHighlighter.DefaultHighlightPainter(new Color(240, 128, 128, 90));
    public static LinkedHashMap<Common.UI.Tables.TableRenderers, TableCellRenderer> TableRenderers = new LinkedHashMap<>();
    public static LinkedHashMap<Common.UI.Tables.TableEditors, TableCellEditor> TableEditors = new LinkedHashMap<>();
    public static LinkedHashMap<Common.UI.Trees.TreeRenderers, TreeCellRenderer> TreeRenderers = new LinkedHashMap<>();
    public static LinkedHashMap<VisualiserThemeName, VisualiserTheme> themes = new LinkedHashMap<>();
    public static LinkedHashMap<GlobalMenu, JPopupMenu> menus = new LinkedHashMap<>();
    public static LinkedHashMap<FormType, ThemeElement> windows = new LinkedHashMap<>();

    //-
    public static void Info(String message) {
        showMessageDialog(null, message, "", 1);
    }

    public static void Error(String message) {
        showMessageDialog(null, message, "", 0);
    }

    public static void ShowDropDownUnder(Component button, JPopupMenu menu) {
        menu.show(button, 0, button.getSize().height);
    }

    public static void ShowDropDownLeft(JButton button, JPopupMenu menu) {
        menu.show(button, -menu.getPreferredSize().width, 0);
    }

    public static void Clear(Container container) {
        container.removeAll();
        container.repaint();
        container.revalidate();
    }

    public static boolean Warning(String text) {
        return !Current.hasUI() ||
                JOptionPane.showConfirmDialog(null,
                        text + "\nВы уверены?",
                        "Подтверждение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.WARNING_MESSAGE) == 0;
    }

    public static boolean Question(String text) {
        return !Current.hasUI() ||
                JOptionPane.showConfirmDialog(null,
                        text + "?",
                        "Подтверждение",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == 0;
    }

    public static void addOld(JToolBar bar, PassCode... codes) {
        for (PassCode code : codes)
            bar.add(Pass.Passes.get(code).button);
    }

    public static void add(JToolBar bar, PassCode_2021... codes) {
        for (PassCode_2021 code : codes)
            bar.add(Pass_2021.passes.get(code).getButton());
    }

    // http://java-online.ru/swing-joptionpane.xhtml
    public static <T> void TrySelect(JComboBox box, T value_in) {
        if (value_in != null) {
            for (int i = 0; i < box.getItemCount(); ++i) {
                T value = (T) box.getItemAt(i);
                if (value.equals(value_in)) {
                    box.setSelectedIndex(i);
                    return;
                }
            }
            box.addItem(value_in);
            box.setSelectedIndex(box.getItemCount() - 1);
        }
    }

    public static void TrySelect_s(JComboBox box, String value_string_in) {
        for (int i = 0; i < box.getItemCount(); ++i) {
            Object value = box.getItemAt(i);
            if (value.toString().equals(value_string_in)) {
                box.setSelectedIndex(i);
                return;
            }
        }
    }

    public static SearchReplaceForm getSearchReplaceForm() {
        return (SearchReplaceForm) windows.get(FormType.SearchReplace);
    }

    public static RemoteFileChooser getRemoteFileChooser() {
        return (RemoteFileChooser) windows.get(FormType.RemoteFileChooser);
    }

    public static ComponentsForm getComponentsWindow() {
        return (ComponentsForm) windows.get(FormType.Components);
    }

    public static boolean HasNewMainWindow() {
        return getNewMainWindow() != null;
    }

    public static void ShowSearchForm(RSyntaxTextArea editor, boolean replace) {
        getSearchReplaceForm().setMode(replace);
        getSearchReplaceForm().ShowMode();
        ShowSearchForm(editor);
    }

    /*
    public static void HideFunctionsFilterForm() {
        if (getFunctionsFilterForm().isVisible())
            getFunctionsFilterForm().setVisible(false);
    }
     */

    public static void HideSearchForm() {
        if (getSearchReplaceForm().isVisible())
            getSearchReplaceForm().setVisible(false);
    }

    public static void ShowSearchForm(RSyntaxTextArea editor) {
        getSearchReplaceForm().setEditor(editor);
        if (getSearchReplaceForm().isVisible())
            getSearchReplaceForm().Refresh();
        else
            getSearchReplaceForm().Show();
    }

    public static void ShowComponentsWindow() {
        getComponentsWindow().ShowDialog("Управление версиями компонент");
    }

    public static void refreshTheme() {
        Current.set(Current.Theme, themes.get(Global.db.settings.get(SettingName.DarkThemeOn).toBoolean() ? VisualiserThemeName.Dark : VisualiserThemeName.Light));
        // refreshTheme_r(Visualiser.getMainWindow().MainPanel);
        /*
        refreshTheme_r(Visualiser.searchReplaceForm.MainPanel);
        refreshTheme_r(Visualiser.ComponentsWindow.fields.MainPanel);
        refreshTheme_r(Visualiser.remoteFileChooser.fields.MainPanel);
        refreshTheme_r(Visualiser.runStatisticForm.fields.MainPanel);
        //-----------------------------------------------------------------

        UIManager.put("ToolTip.background", Current.getTheme().background);
        UIManager.put("ToolTip.foreground", Current.getTheme().foreground);
        UIManager.put("Panel.background", Current.getTheme().background);
        UIManager.put("Panel.foreground", Current.getTheme().foreground);

        UIManager.put("TextPane.background", Current.getTheme().background);
        UIManager.put("TextPane.foreground", Current.getTheme().foreground);

        UIManager.put("Label.background", Current.getTheme().background);
        UIManager.put("Label.foreground", Current.getTheme().foreground);

        UIManager.put("ToolBar.background", Current.getTheme().background);
        UIManager.put("ToolBar.foreground", Current.getTheme().foreground);

        for (TableCellRenderer renderer : TableRenderers.values()) {
            if (renderer instanceof WindowControl) {
                ((WindowControl) renderer).applyTheme();
            }
        }
        for (TreeCellRenderer renderer : TreeRenderers.values()) {
            if (renderer instanceof WindowControl) {
                ((WindowControl) renderer).applyTheme();
            }
        }
        //для текущего файла отдельно
        ///  if (Current.HasFile())
        //     Current.getFile().form.Body.applyTheme();
        //------------
        //  UIManager.put("TabbedPane.unselectedForeground", Color.red);
        //  UIManager.put("TabbedPane.selectedBackground", Color.black);
        //----------------------------
         */
    }

    public static void CreateMenus() {
        //</editor-fold>
        //<editor-fold desc="Меню// ">
        menus.put(GlobalMenu.Analyses, new AnalysesMenu());
        menus.put(GlobalMenu.Transformations, new TransformationsMenu());
        menus.put(GlobalMenu.Debug, new DebugMenu());
        //-
        menus.put(GlobalMenu.Cleaning, new CleaningMenu());
        //-
        menus.put(GlobalMenu.SapforSettings, new SapforSettingsMenu());
        menus.put(GlobalMenu.VisualiserSettings, new VisualiserSettingsMenu());
        menus.put(GlobalMenu.ComparsionSettings, new ComparsionMenu());
        //-
        menus.put(GlobalMenu.LastProjects, new LastProjectsMenu());
        //-
        menus.put(GlobalMenu.ProjectLanguage, new ProjectLanguageMenu());
        //-
        menus.put(GlobalMenu.FileLanguage, new FileLanguageMenu());
        menus.put(GlobalMenu.FileType, new FileTypeMenu());
        menus.put(GlobalMenu.FileStyle, new FileStyleMenu());
        //-
        menus.put(GlobalMenu.ConfigurationEnvironments, new EnvironmentValuesMenu());
        menus.put(GlobalMenu.Attachments, new AttachementsMenu());
        //</editor-fold>
    }

    public static void CreateWindows() {
        windows.put(FormType.SearchReplace, new SearchReplaceForm());
        windows.put(FormType.RemoteFileChooser, new RemoteFileChooser());
        windows.put(FormType.Main, new MainForm_2021());
        //---------------------------------------
        getNewMainWindow().Show();
    }

    public static MainWindow getNewMainWindow() {
        return (MainWindow) windows.get(FormType.Main);
    }

    //-
    public static void CreateAll() {
        //<editor-fold desc="Локализация компонентов окна JFileChooser">
        UIManager.put(
                "FileChooser.openButtonText", "Открыть");
        UIManager.put(
                "FileChooser.saveButtonText", "Сохранить");
        UIManager.put(
                "FileChooser.cancelButtonText", "Отмена");
        UIManager.put(
                "FileChooser.fileNameLabelText", "Наименование файла");
        UIManager.put(
                "FileChooser.filesOfTypeLabelText", "Типы файлов");
        UIManager.put(
                "FileChooser.lookInLabelText", "Директория");
        UIManager.put(
                "FileChooser.saveInLabelText", "Сохранить в директории");
        UIManager.put(
                "FileChooser.folderNameLabelText", "Путь директории");
        //</editor-fold>
        //<editor-fold desc="Локализация компонентов окна подтверждения">
        UIManager.put("OptionPane.yesButtonText", "Да");
        UIManager.put("OptionPane.noButtonText", "Нет");
        UIManager.put("OptionPane.cancelButtonText", "Отмена");
        UIManager.put("OptionPane.okButtonText", "Готово");
        //</editor-fold>
        //<editor-fold desc="Темы(всегда создавать первыми)">
        themes.put(VisualiserThemeName.Light, new LightVisualiserTheme());
        themes.put(VisualiserThemeName.Dark, new DarkVisualiserTheme());
        //по умолчанию поставить светлую тему. потому что до загрузки бд работаем с таблицей компонент.
        Current.set(Current.Theme, themes.get(VisualiserThemeName.Light));
        //</editor-fold>
        //<editor-fold desc="Объекты отрисовки и редактирования деревьев и таблиц">
        TableRenderers.put(RendererDate, new DateRenderer_());
        TableRenderers.put(RendererProgress, new ProgressBarRenderer());
        TableRenderers.put(RendererSelect, new DBObjectSelectionRenderer());
        TableRenderers.put(RendererDimension, new DimensionRenderer());
        TableRenderers.put(RendererMultiline, new MultilineRenderer());
        TableRenderers.put(RendererHyperlinks, new HyperlinksRenderer());
        TableRenderers.put(RendererTopLeft, new TopLeftRenderer());
        TableRenderers.put(RendererMaskedInt, new MaskedIntegerValueRenderer());
        TableRenderers.put(RendererVariantRank, new VariantRankRenderer());
        TableRenderers.put(RendererHiddenList, new HiddenListRenderer());
        TableRenderers.put(RendererWrapText, new WrapTextRenderer());
        TableRenderers.put(RendererCompilerOptionParameterValue, new CompilerOptionParameterValueRenderer());
        TableRenderers.put(RendererCompilerOptionParameterName, new CompilerOptionParameterNameRenderer());
        TableRenderers.put(RendererCompilerEnvironmentValue, new CompilerEnvironmentValueRenderer());
        TableRenderers.put(RendererStatusEnum, new StatusEnumRenderer());
        //---------------------------------------------
        TreeRenderers.put(RendererGraph, new GraphTreeCellRenderer());
        TreeRenderers.put(RendererRemoteFile, new RemoteFileRenderer());
        TreeRenderers.put(RendererFile, new FilesTreeCellRenderer());
        TreeRenderers.put(RendererVersion, new VersionsTreeCellRenderer());
        TreeRenderers.put(RendererRule, new RulesTreeCellRenderer());
        TreeRenderers.put(RendererSelection, new SelectionTreeCellRenderer());
        //----------------------------------------------
        TableEditors.put(EditorSelect, new DBObjectSelector());
        TableEditors.put(EditorHyperlinks, new VectorEditor());
        TableEditors.put(EditorDimension, new DimensionStateChanger());
        TableEditors.put(EditorCompilerOptionParameterValue, new CompilerOptionParameterValueEditor());
        TableEditors.put(EditorCompilerEnvironmentValue, new CompilerEnvironmentValueEditor());
        //</editor-fold>
    }

    public static void printAccessible_r(Accessible accessible) {
        System.out.print(accessible.getClass().getSimpleName() + ": children_count: ");
        AccessibleContext context = accessible.getAccessibleContext();
        System.out.println(context.getAccessibleChildrenCount());
        for (int i = 0; i < context.getAccessibleChildrenCount(); ++i) {
            printAccessible_r(context.getAccessibleChild(i));
        }
    }

    public static void refreshTheme_r(Accessible accessible) {
        //  System.out.println(accessible.getClass().getSimpleName() + ": children_count: ");
        AccessibleContext context = accessible.getAccessibleContext();
        if (accessible instanceof ThemeElement) {
            // System.out.println(accessible.getClass().getSimpleName() + ": refresh theme");
            ((ThemeElement) accessible).applyTheme();
        } else {
            if ((accessible instanceof JPanel) ||
                    (accessible instanceof JLabel) ||
                    (accessible instanceof JToolBar) ||
                    (accessible instanceof JTabbedPane) ||
                    (accessible instanceof JScrollPane) ||
                    (accessible instanceof JSplitPane) ||
                    (accessible instanceof JTextField) ||
                    (accessible instanceof JCheckBox)
            ) {
            } else if (accessible instanceof JTextArea) {
                accessible.getAccessibleContext().getAccessibleComponent().setBackground(Current.getTheme().background);
                accessible.getAccessibleContext().getAccessibleComponent().setForeground(Current.getTheme().trees_background);
            }
        }
        for (int i = 0; i < context.getAccessibleChildrenCount(); ++i) {
            refreshTheme_r(context.getAccessibleChild(i));
        }
    }

    public static void CreateComponentsForm() {
        windows.put(FormType.Components, new ComponentsForm());
    }

    //</editor-fold>
    //-----
    public static void ShowTabsNames(JTabbedPane tabs) {
        ShowTabsNames(tabs, 0);
    }

    public static void ShowTabsNames(JTabbedPane tabs, int startIndex) {
        boolean flag = Global.db.settings.get(SettingName.ShowFullTabsNames).toBoolean();
        for (int i = startIndex; i < tabs.getTabCount(); ++i)
            tabs.setTitleAt(i, flag ? tabs.getToolTipTextAt(i) : "");
    }

    public static boolean Contains(Vector<String> list, String line, int max_index) {
        int last_index = -1;
        for (int i = 0; i < list.size(); ++i)
            if (list.get(i).equals(line)) last_index = i;
        return (last_index >= max_index);
    }

    public static void MakeSpinnerRapid(JSpinner spinner, ChangeListener listener){

        JComponent comp = spinner.getEditor();
        JFormattedTextField field = (JFormattedTextField) comp.getComponent(0);
        DefaultFormatter formatter = (DefaultFormatter) field.getFormatter();
        formatter.setCommitsOnValidEdit(true);
        formatter.setAllowsInvalid(true);
        spinner.addChangeListener(listener);
    }
}
