package Common.UI;

import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;

public class EmptyDialogFields implements DialogFields {
    private JPanel content;

    @Override
    public Component getContent() {
        return content;
    }
}
