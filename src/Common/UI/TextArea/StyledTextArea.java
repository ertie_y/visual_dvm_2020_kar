package Common.UI.TextArea;

import Common.UI.Menus.TextEditorMenu;

import javax.swing.*;

public class StyledTextArea  extends JTextArea {
    public StyledTextArea(){
        setComponentPopupMenu(new TextEditorMenu(this));
    }
}
