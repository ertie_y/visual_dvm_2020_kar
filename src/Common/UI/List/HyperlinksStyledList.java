package Common.UI.List;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
public class HyperlinksStyledList extends StyledList {
    @Override
    public void applyTheme() {
        super.applyTheme();
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
    }
}
