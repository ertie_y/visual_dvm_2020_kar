package Common.UI.List;
import Common.Current;
import Common.UI.Themes.ThemeElement;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
public class StyledList extends JList implements ThemeElement {
    public StyledList() {
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
        applyTheme();
    }
    @Override
    public void applyTheme() {
        setBackground(Current.getTheme().table_background);
        setForeground(Current.getTheme().foreground);
    }
}
