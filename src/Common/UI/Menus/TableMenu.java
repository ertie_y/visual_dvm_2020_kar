package Common.UI.Menus;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class TableMenu extends StyledPopupMenu {
    int row = Utils.Nan;
    int column = Utils.Nan;
    Object target = null;
    //-
    JTable owner = null;
    StyledMenuItem mcopy;
    public TableMenu(JTable owner_in) {
        owner = owner_in;
        mcopy = new StyledMenuItem("Копировать текст текущей ячейки", "/icons/Editor/Copy.png");
        //если удалось нажать значит все условия выполнены
        mcopy.addActionListener(e -> Utils.CopyToClipboard(target.toString()));
        add(mcopy);
    }
    @Override
    public void CheckElementsVisibility() {
        row = owner.getSelectedRow();
        column = owner.getSelectedColumn();
        if ((row >= 0) && (column >= 0)) {
            target = owner.getValueAt(row, column);
            mcopy.setVisible(true);
        } else mcopy.setVisible(false);
        super.CheckElementsVisibility();
    }
}
