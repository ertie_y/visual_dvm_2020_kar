package Common.UI.Menus;
import Common.Database.DataSet;
import Common.UI.Tables.StyledTable;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class DataTableMenu extends TableMenu {
    StyledMenuItem mCheckAll;
    StyledMenuItem mUncheckAll;
    public DataTableMenu(JTable owner_in, DataSet source_in, boolean hasCheckBox) {
        super(owner_in);
        if (hasCheckBox) {
            mCheckAll = new StyledMenuItem("Выбрать все");
            mCheckAll.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    source_in.CheckAll(true);
                }
            });
            mUncheckAll = new StyledMenuItem("Отменить все");
            mUncheckAll.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    source_in.CheckAll(false);
                }
            });
            add(mCheckAll);
            add(mUncheckAll);
        }
    }
}
