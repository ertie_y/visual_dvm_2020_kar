package Common.UI.Menus;
import Common.Current;
import Common.Global;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;
import GlobalData.Settings.SettingName;

import javax.swing.*;
public class SettingsSubmenu extends JMenu {
    public SettingsSubmenu(String title, String icon, SettingName... settings) {
        super(title);
        if (icon != null)
            setIcon(Utils.getIcon(icon));
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        for (SettingName c : settings) {
            add(Global.db.settings.get(c).getMenuItem());
        }
    }
}
