package Common.UI.Menus;
import Visual_DVM_2021.Passes.All.UpdateSetting;

import javax.swing.*;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicMenuItemUI;
//неичезающий меню итем. нужен для настроек
class StableItemUI extends BasicMenuItemUI {
    public static ComponentUI createUI(JComponent c) {
        return new StableItemUI();
    }
    @Override
    protected void doClick(MenuSelectionManager msm) {
        menuItem.doClick(0);
    }
}
public class StableMenuItem extends StyledMenuItem {
    {
        getModel().addChangeListener(e -> {
            if (getModel().isArmed() && isShowing())
                UpdateSetting.last_menu_path = MenuSelectionManager.defaultManager().getSelectedPath();
        });
    }
    public StableMenuItem(String text) {
        super(text);
        setUI(new StableItemUI());
    }
}
