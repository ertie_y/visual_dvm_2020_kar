package Common.UI.Menus;
import Common.Current;
import Common.UI.Themes.ThemeElement;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
public class StyledPopupMenu extends JPopupMenu implements ThemeElement {
    public StyledPopupMenu() {
        addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                CheckElementsVisibility();
            }
            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            }
            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {
            }
        });
    }
    private void refreshTheme_r(MenuElement element) {
        element.getComponent().setBackground(Current.getTheme().background);
        element.getComponent().setForeground(Current.getTheme().foreground);
        for (MenuElement se : element.getSubElements())
            refreshTheme_r(se);
    }
    @Override
    public void applyTheme() {
        setBackground(Current.getTheme().background);
        setForeground(Current.getTheme().foreground);
        refreshTheme_r(this);
    }
    public void CheckElementsVisibility() {
        applyTheme();
    }
}
