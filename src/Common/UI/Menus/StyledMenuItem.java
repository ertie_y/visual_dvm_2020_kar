package Common.UI.Menus;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.Utils.Utils;

import javax.swing.*;
public class StyledMenuItem extends JMenuItem {
    public StyledMenuItem(String text) {
        super(text, null);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
    }
    public StyledMenuItem(String text, String icon_path) {
        super(text);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        if (icon_path != null)
            setIcon(Utils.getIcon(icon_path));
    }
}
