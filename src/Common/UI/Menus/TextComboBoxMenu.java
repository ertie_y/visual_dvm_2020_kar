package Common.UI.Menus;

import Common.UI.UI;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;

public class TextComboBoxMenu extends StyledPopupMenu {
    protected JComboBox<String> box;
    protected String selectedText = null;
    //-------------------------------------------------
    JMenuItem m_copy;
    JMenuItem m_paste;

    //-------------------------------------------------
    public TextComboBoxMenu(JComboBox<String> box_in) {
        box = box_in;
        m_copy = new StyledMenuItem("Копировать", "/icons/Editor/Copy.png");
        m_copy.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Utils.CopyToClipboard(box.getSelectedItem().toString());
                    }
                });
        add(m_copy);
        m_paste = new StyledMenuItem("Вставить", "/icons/Editor/Paste.png");
        m_paste.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        UI.TrySelect(box, Utils.getFromClipboard());
                    }
                });
        add(m_paste);
    }

    @Override
    public void CheckElementsVisibility() {
        boolean visible_ = box.getSelectedIndex() >= 0;
        m_paste.setVisible(visible_);
        m_copy.setVisible(visible_);
        super.CheckElementsVisibility();
    }
}
