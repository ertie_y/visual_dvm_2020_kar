package Common.UI.Menus;
import Common.Current;
import Common.Passes.PassCode;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;

import static Common.Passes.Pass.Passes;
public class PassesSubMenu extends JMenu {
    public PassesSubMenu(String title, String icon, PassCode... passes) {
        super(title);
        setIcon(new ImageIcon(getClass().getResource(icon)));
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        for (PassCode c : passes) {
            add(Passes.get(c).menu_item);
        }
    }
}
