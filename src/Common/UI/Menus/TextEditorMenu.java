package Common.UI.Menus;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import java.awt.event.ActionEvent;
public class TextEditorMenu extends StyledPopupMenu {
    protected JTextComponent editor;
    protected String selectedText = null;
    //-------------------------------------------------
    JMenuItem m_cut;
    JMenuItem m_copy;
    JMenuItem m_paste;
    //-------------------------------------------------
    public TextEditorMenu(JTextComponent editor_in) {
        editor = editor_in;
        m_cut = new StyledMenuItem("Вырезать", "/icons/Editor/Cut.png");
        m_cut.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                editor.cut();
            }
        });
        add(m_cut);
        m_copy = new StyledMenuItem("Копировать", "/icons/Editor/Copy.png");
        m_copy.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editor.copy();
                    }
                });
        add(m_copy);
        m_paste = new StyledMenuItem("Вставить", "/icons/Editor/Paste.png");
        m_paste.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        editor.paste();
                    }
                });
        add(m_paste);
    }
    @Override
    public void CheckElementsVisibility() {
        selectedText = editor.getSelectedText();
        m_cut.setVisible(editor.isEditable() && (selectedText != null));
        m_paste.setVisible(editor.isEditable());
        m_copy.setVisible(selectedText != null);
        super.CheckElementsVisibility();
    }
}
