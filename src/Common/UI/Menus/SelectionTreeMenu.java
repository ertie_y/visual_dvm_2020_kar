package Common.UI.Menus;
import Common.Current;
import Common.UI.Selectable;
import Common.UI.Trees.DataTree;
import Common.UI.Trees.SelectableTree;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public abstract class SelectionTreeMenu extends GraphMenu<DataTree> {
    StyledMenuItem m_select_for_current;
    StyledMenuItem m_unselect_for_current;
    StyledMenuItem m_select_all;
    StyledMenuItem m_unselect_all;
    public SelectionTreeMenu(SelectableTree tree_in) {
        super(tree_in, "");
        addSeparator();
        //-
        m_select_all = new StyledMenuItem("Выбрать всё");
        m_select_all.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelectAll(true);
                tree.updateUI();
            }
        });
        add(m_select_all);
        m_unselect_all = new StyledMenuItem("Отменить всё");
        m_unselect_all.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                SelectAll(false);
                tree.updateUI();
            }
        });
        add(m_unselect_all);
        addSeparator();
        m_select_for_current = new StyledMenuItem("");
        m_select_for_current.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object o = Current.get(tree.getCurrent());
                if (o instanceof Selectable) {
                    ((Selectable) o).SelectAllChildren(true);
                }
                tree.updateUI();
            }
        });
        add(m_select_for_current);
        //--------
        m_unselect_for_current = new StyledMenuItem("");
        m_unselect_for_current.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object o = Current.get(tree.getCurrent());
                if (o instanceof Selectable) {
                    ((Selectable) o).SelectAllChildren(false);
                }
                tree.updateUI();
            }
        });
        //-------
        add(m_unselect_for_current);
    }
    public abstract Class getTargetClass(); //для кого позволяется выбирать всех потомков.
    public abstract void SelectAll(boolean select);
    @Override
    public void CheckElementsVisibility() {
        Object current = Current.get(tree.getCurrent());
        if ((current != null) && (current.getClass().equals(getTargetClass()))) {
            String name = ((Selectable) current).getSelectionText();
            m_select_for_current.setText("Выбрать всё для " +
                    Utils.Brackets(name));
            m_unselect_for_current.setText("Отменить выбор всех для " +
                    Utils.Brackets(name));
            //-
            m_select_for_current.setVisible(true);
            m_unselect_for_current.setVisible(true);
        } else {
            m_select_for_current.setVisible(false);
            m_unselect_for_current.setVisible(false);
        }
    }
}
