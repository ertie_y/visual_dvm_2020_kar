package Common.UI.Menus;
import Common.UI.Trees.StyledTree;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
public class GraphMenu<T extends StyledTree> extends StyledPopupMenu {
    public T tree;
    public GraphMenu(T tree_in, String branches_name) {
        tree = tree_in;
        JMenuItem m = null;
        m = new StyledMenuItem("Свернуть все " + branches_name);
        m.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tree.CollapseAll();
            }
        });
        add(m);
        m = new StyledMenuItem("Развернуть все " + branches_name);
        m.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tree.ExpandAll();
            }
        });
        add(m);
    }
    public GraphMenu(T tree) {
        this(tree, tree.getBranchesName());
    }
    public void Show(MouseEvent mouseEvent) {
        show(tree, mouseEvent.getX(), mouseEvent.getY());
    }
}
