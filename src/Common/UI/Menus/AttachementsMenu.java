package Common.UI.Menus;

import Common.Current;
import Common.Global;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class AttachementsMenu extends StyledPopupMenu {
    JMenuItem mscreenshot;
    JMenuItem mExplorer;

    public AttachementsMenu() {
        add(mscreenshot = Pass_2021.passes.get(PassCode_2021.MakeScreenShot).getMenuItem());
        addSeparator();
        mExplorer = new StyledMenuItem("Открыть вложения в проводнике...", "/icons/Explorer.png");
        mExplorer.addActionListener(
                new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Desktop.getDesktop().open(Current.getProject().getAttachmentsDirectory());
                        } catch (Exception ex) {
                            Global.Log.PrintException(ex);
                        }
                    }
                });
        add(mExplorer);
    }
}
