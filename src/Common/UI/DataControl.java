package Common.UI;
import Common.Database.DBObject;
public interface DataControl {
    DBObject getRowObject(int rowIndex); //получить объект, сответствующий данной строке.
    void SelectRowByPK(Object pk);
    //выделить строку где лежит объект с данным первичным ключом.
}
