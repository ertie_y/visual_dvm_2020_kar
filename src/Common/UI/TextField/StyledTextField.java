package Common.UI.TextField;

import Common.UI.Menus.StyledPopupMenu;
import Common.UI.Menus.TextEditorMenu;

import javax.swing.*;

public class StyledTextField extends JTextField {
    private StyledPopupMenu menu;
    public StyledTextField (){
        setComponentPopupMenu(new TextEditorMenu(this));
    }
}
