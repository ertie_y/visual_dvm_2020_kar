package Common.UI.TextField;

import Common.UI.Menus.StyledPopupMenu;
import Common.UI.Menus.TextEditorMenu;

import javax.swing.*;

public class StyledPasswordField extends JPasswordField {
    private StyledPopupMenu menu;
    public StyledPasswordField (){
        setComponentPopupMenu(new TextEditorMenu(this));
    }
}
