package Common.UI;

import Common.Current;
import Common.UI.Themes.VisualiserFonts;

import java.awt.*;

public interface StatusEnum {
    default Font getFont(){return Current.getTheme().Fonts.get(VisualiserFonts.UnknownState);}
    default String getDescription(){return toString();}
}
