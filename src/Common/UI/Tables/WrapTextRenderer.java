package Common.UI.Tables;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
public class WrapTextRenderer extends JTextArea implements TableCellRenderer {
    public WrapTextRenderer() {
        setLineWrap(true);
        setWrapStyleWord(true);
       // setOpaque(false);
        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Distribution).deriveFont(14.0f));
    }
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
        setText((value == null) ? "" : value.toString());
        setSize(table.getColumnModel().getColumn(column).getWidth(),
                getPreferredSize().height);
        if (table.getRowHeight(row) != getPreferredSize().height) {
            table.setRowHeight(row, getPreferredSize().height);
        }
        return this;
    }
}
