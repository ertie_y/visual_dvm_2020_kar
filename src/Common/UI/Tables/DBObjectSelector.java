package Common.UI.Tables;
import Common.Database.DBObject;
public class DBObjectSelector<T extends DBObject> extends DBObjectEditor<T> {
    @Override
    public void Action() {
        value.SwitchSelection();
        setIcon(value.GetSelectionIcon());
    }
    @Override
    public Object getCellEditorValue() {
        return value.isSelected();
    }
}
