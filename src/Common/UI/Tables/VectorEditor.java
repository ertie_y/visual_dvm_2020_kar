package Common.UI.Tables;
import ProjectData.Files.UI.FilesHyperlinksPanel;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import javax.swing.table.TableCellEditor;
import java.awt.*;
import java.util.EventObject;
import java.util.Objects;
import java.util.Vector;
public class VectorEditor extends FilesHyperlinksPanel implements TableCellEditor {
    protected transient ChangeEvent changeEvent;
    @Override
    public Component getTableCellEditorComponent(
            JTable table, Object value, boolean isSelected, int row, int column) {
        if (value instanceof Vector) {
            UpdateByCell((Vector) value);
            this.Hyperlinks.setBackground(table.getSelectionBackground());
            Hyperlinks.setSelectionBackground(table.getSelectionBackground());
            Hyperlinks.setSelectionForeground(table.getSelectionForeground());
        }
        return this;
    }
    @Override
    public Object getCellEditorValue() {
        return links;
    }
    //Copied from AbstractCellEditor
    //protected EventListenerList listenerList = new EventListenerList();
    @Override
    public boolean isCellEditable(EventObject e) {
        return true;
    }
    @Override
    public boolean shouldSelectCell(EventObject anEvent) {
        return true;
    }
    @Override
    public boolean stopCellEditing() {
        fireEditingStopped();
        return true;
    }
    @Override
    public void cancelCellEditing() {
        fireEditingCanceled();
    }
    @Override
    public void addCellEditorListener(CellEditorListener l) {
        listenerList.add(CellEditorListener.class, l);
    }
    @Override
    public void removeCellEditorListener(CellEditorListener l) {
        listenerList.remove(CellEditorListener.class, l);
    }
    public CellEditorListener[] getCellEditorListeners() {
        return listenerList.getListeners(CellEditorListener.class);
    }
    protected void fireEditingStopped() {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == CellEditorListener.class) {
                // Lazily create the event:
                if (Objects.isNull(changeEvent)) {
                    changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener) listeners[i + 1]).editingStopped(changeEvent);
            }
        }
    }
    protected void fireEditingCanceled() {
        // Guaranteed to return a non-null array
        Object[] listeners = listenerList.getListenerList();
        // Process the listeners last to first, notifying
        // those that are interested in this event
        for (int i = listeners.length - 2; i >= 0; i -= 2) {
            if (listeners[i] == CellEditorListener.class) {
                // Lazily create the event:
                if (Objects.isNull(changeEvent)) {
                    changeEvent = new ChangeEvent(this);
                }
                ((CellEditorListener) listeners[i + 1]).editingCanceled(changeEvent);
            }
        }
    }
}
