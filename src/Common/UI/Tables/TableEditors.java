package Common.UI.Tables;
public enum TableEditors {
    EditorDefault,
    EditorSelect,
    EditorHyperlinks,
    EditorDimension,
    EditorCompilerEnvironmentValue, EditorCompilerOptionParameterValue
}
