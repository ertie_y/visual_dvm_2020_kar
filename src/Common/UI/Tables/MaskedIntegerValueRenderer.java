package Common.UI.Tables;
import javax.swing.*;
public class MaskedIntegerValueRenderer extends RendererCell<Long> {
    @Override
    public Long Init(JTable table, Object value, int row, int column) {
        return (Long) value;
    }
    public long getMask() {
        return -1;
    }
    public String getMaskText() {
        return " — ";
    }
    public void Display() {
        if (value != null)
            setText(value.equals((getMask())) ? getMaskText() : String.valueOf(value));
    }
}
