package Common.UI.Tables;

import java.awt.*;

public class DBObjectSelectionRenderer extends DBObjectRenderer {
    /*
    @Override
    public Dimension getMinimumSize() {
        return new Dimension(25,25);
    }
     */

    @Override
    public void Display() {
        if (value != null)
            setIcon(value.GetSelectionIcon());
    }
}
