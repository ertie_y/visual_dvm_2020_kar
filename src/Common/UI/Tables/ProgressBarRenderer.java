package Common.UI.Tables;
import Common.UI.ProgressBar.StyledProgressBar;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
public class ProgressBarRenderer extends StyledProgressBar implements TableCellRenderer {
    @Override
    public java.awt.Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
        setValue((Integer) value);
        return this;
    }
}


