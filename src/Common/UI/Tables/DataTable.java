package Common.UI.Tables;
import Common.UI.DataControl;
import Common.UI.Menus.DataTableMenu;
import Common.UI.Menus.TableMenu;

import javax.swing.table.AbstractTableModel;
public abstract class DataTable extends StyledTable implements DataControl {
    public DataTable(AbstractTableModel model) {
        super(model);
    }
    @Override
    public void SelectRowByPK(Object pk) {
        for (int i=0; i<getRowCount(); ++i){
             if (getRowObject(i).getPK().equals(pk)){
                 SelectRow(i);
                 scrollToVisible(i,0);
                 return;
             }
        }
    }

    //-------------------------------
    /*
    public void SelectRow(int r) {
        getSelectionModel().setSelectionInterval(r, r);
    }
    public Object getRowKey(int r) {
        return getValueAt(r, 0);
    }
    public DBObject getRowObject(int row) {
        return (DBObject) getGrid().getDataSource().get(getRowKey(row));
    }
    //-------------------------------

    public int getRowByKey(Object key) {
        for (int i = 0; i < getRowCount(); ++i) {
            if (getRowKey(i).equals(key)) return i;
        }
        return -1;
    }
    public void SelectRowByKey(Object key) {
        int r = getRowByKey(key);
        if (r >= 0)
            SelectRow(r);
    }
    public void ClearSelectionSync() {
        events_on = false;
        getSelectionModel().clearSelection();
        current_row_i = Utils.Nan;
        events_on = true;
    }
    public void SelectRowByKeySync(Object key) {
        int r = getRowByKey(key);
        events_on = false;
        if (r >= 0)
            SelectRow(r);
        events_on = true;
        current_row_i = r;
    }

     */
}
