package Common.UI.Tables;

import Common.Utils.Utils;

public class ColumnInfo {
    private String Name = "?";
    private boolean visible = true;
    private boolean editable = false;
    private TableRenderers renderer = TableRenderers.RendererDefault;
    private TableEditors editor = TableEditors.EditorDefault;
    private int maxWidth = Utils.Nan;
    private int minWidth = Utils.Nan;
    //private int lastWidth = Utils.Nan;

   // public void setLastWidth(int width_in) {
 //       lastWidth = width_in;
  //  }

   // public int getLastWidth() {
   //     return lastWidth;
   // }
    public ColumnInfo(String name_in) {
        setName(name_in);
    }

    public ColumnInfo(String name_in, TableRenderers renderer_in, TableEditors editor_in) {
        setName(name_in);
        setRenderer(renderer_in);
        setEditable(true);
        setEditor(editor_in);
    }

    public ColumnInfo(String name_in, TableRenderers renderer_in) {
        setName(name_in);
        setRenderer(renderer_in);
    }

    public String getName() {
        return Name;
    }

    public void setName(String name_in) {
        Name = name_in;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible_in) {
        this.visible = visible_in;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable_in) {
        this.editable = editable_in;
    }

    public TableRenderers getRenderer() {
        return renderer;
    }

    public void setRenderer(TableRenderers renderer_in) {
        this.renderer = renderer_in;
    }

    public TableEditors getEditor() {
        return editor;
    }

    public void setEditor(TableEditors editor_in) {
        this.editor = editor_in;
        setEditable(editor != TableEditors.EditorDefault);
    }

    public boolean hasRenderer() {
        return getRenderer() != TableRenderers.RendererDefault;
    }

    public boolean hasEditor() {
        return getEditor() != TableEditors.EditorDefault;
    }

    public int getMaxWidth() {
        return maxWidth;
    }

    public void setMaxWidth(int maxWidth_in) {
        this.maxWidth = maxWidth_in;
    }

    public boolean hasMaxWidth() {
        return maxWidth != Utils.Nan;
    }

    //-
    public int getMinWidth() {
        return minWidth;
    }

    public void setMinWidth(int minWidth_in) {
        this.minWidth = minWidth_in;
    }

    public boolean hasMinWidth() {
        return minWidth != Utils.Nan;
    }

/*
    public boolean hasLastWidth() {
        return lastWidth != Utils.Nan;
    }
 */
    //-
}
