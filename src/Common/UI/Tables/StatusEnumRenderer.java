package Common.UI.Tables;
import Common.UI.StatusEnum;
import javax.swing.*;

public class StatusEnumRenderer extends RendererCell<StatusEnum> {
    @Override
    public StatusEnum Init(JTable table, Object value, int row, int column) {
        return (StatusEnum) value;
    }
    @Override
    public void Display() {
        if (value != null){
            setText(value.getDescription());
            setFont(value.getFont());
        }
    }
}
