package Common.UI.Tables;
import Common.UI.List.HyperlinksStyledList;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.util.Vector;
public class HyperlinksRenderer extends HyperlinksStyledList implements TableCellRenderer {
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        setBackground(isSelected ? table.getSelectionBackground() : table.getBackground());
        setListData((Vector) value);
        return this;
    }
}
