package Common.UI.Tables;
import Common.Utils.Utils;

import javax.swing.*;
import java.util.Date;
//название пересекается с встроенным классом поэтому подчеркивание.
public class DateRenderer_ extends RendererCell<Date> {
    private static final Date zero = new Date(0);
    @Override
    public Date Init(JTable table, Object value, int row, int column) {
        return (Date) value;
    }
    @Override
    public void Display() {
        if (value != null)
            setText(value.equals(zero) ? "нет" : Utils.print_date(value));
    }
}
