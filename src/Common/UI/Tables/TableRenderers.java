package Common.UI.Tables;
public enum TableRenderers {
    RendererDefault,
    RendererDate,
    RendererProgress,
    RendererSelect,
    RendererDimension,
    RendererMultiline,
    RendererHyperlinks,
    RendererTopLeft,
    RendererMaskedInt,
    RendererVariantRank,
    RendererHiddenList,
    RendererWrapText,
    RendererCompilerOptionParameterValue,
    RendererCompilerEnvironmentValue,
    RendererCompilerOptionParameterName,
    RendererStatusEnum
}
