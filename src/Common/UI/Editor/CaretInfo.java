package Common.UI.Editor;
import Common.Global;
import ProjectData.Files.UI.Editor.SPFEditor;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
public class CaretInfo {
    public String current_line = ""; //полный текст текущей строки
    public String before = ""; //кусок строки перед кареткой
    public String after = ""; //кусок строки после каретки
    public String prefix_word = ""; //слово перед кареткой
    public String suffix_word = ""; //слово после каретки
    public CaretInfo(RSyntaxTextArea Body) {
        try {
            int start = Body.getLineStartOffset(Body.getCaretLineNumber());
            int before_length = Body.getCaretOffsetFromLineStart();
            int end = Body.getLineEndOffset(Body.getCaretLineNumber());
            int after_length = end - start - before_length;
            before = Body.getText(start, before_length).toUpperCase();
            after = Body.getText(start + before_length, after_length).toUpperCase();
            //нужно чтобы перевод строки не влезал
            after = after.replace("\n", "");
            current_line = (before + after);
            prefix_word = SPFEditor.getLastWord(before, ' ', ',', ':', '.', '(', ')');
            suffix_word = SPFEditor.getFirstWord(after, ' ', ',', ':', '.', '(', ')');
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
    public CaretInfo() {
    }
}
