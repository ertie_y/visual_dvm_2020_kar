package Common.UI.Themes;

public enum VisualiserFonts {
    GoodState,
    BadState,
    Fatal,
    ProgressState,
    UnknownState,
    Hyperlink,
    Disabled,
    //бесцветные
    Distribution,
    //---
    TreeItalic,
    TreePlain,
    TreeBold,
    BlueState,
    //---
    Menu
}
