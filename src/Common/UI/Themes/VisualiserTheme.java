package Common.UI.Themes;
import Common.Global;
import org.fife.ui.rsyntaxtextarea.Theme;

import java.awt.*;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import java.util.LinkedHashMap;

import static java.awt.Font.MONOSPACED;
public abstract class VisualiserTheme {
    public Color foreground;
    public Color background;
    public Color selection_background;
    public Color trees_background;
    public Color bar_foreground;
    public Color bar_background;
    public Color table_background;
    public LinkedHashMap<VisualiserFonts, Font> Fonts = new LinkedHashMap<>();
    public LinkedHashMap<VisualiserColor, Color> Colors = new LinkedHashMap<>();
    public VisualiserTheme() {
        try {
            //-----------------------------------------------
            //-----------------------------------------------
            foreground = Color.decode(getForegroundHex());
            background = Color.decode(getBackgroundHex());
            selection_background = Color.decode(getSelectionBackgroundHex());
            trees_background = Color.decode(getTreeBackgroundHex());
            bar_foreground = Color.decode(getBarForegroundHex());
            bar_background = Color.decode(getBarBackgroundHex());
            table_background = Color.decode(getTableBackgroundHex());
            //<editor-fold desc="шрифты">
            Fonts.put(VisualiserFonts.GoodState,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getGoodFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.BadState,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getBadFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.BlueState,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getHyperlinkFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.Fatal,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getFatalFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.UnknownState,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getUnknownFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.Hyperlink,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, MONOSPACED);
                                    put(TextAttribute.FOREGROUND, getHyperlinkFontColor());
                                    put(TextAttribute.SIZE, 18);
                                    put(TextAttribute.WEIGHT, TextAttribute.WEIGHT_BOLD);
                                    put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.ProgressState,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, "Times New Roman");
                                    put(TextAttribute.FOREGROUND, getProgressFontColor());
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    ));
            Fonts.put(VisualiserFonts.Disabled, new Font(
                    new HashMap<TextAttribute, Object>() {
                        {
                            put(TextAttribute.FAMILY, MONOSPACED);
                            put(TextAttribute.BACKGROUND, Color.lightGray);
                            put(TextAttribute.FOREGROUND, Color.lightGray);
                            put(TextAttribute.SIZE, 18);
                            put(TextAttribute.UNDERLINE, TextAttribute.UNDERLINE_ON);
                        }
                    }
            ));
            Fonts.put(VisualiserFonts.Distribution,
                    new Font(
                            new HashMap<TextAttribute, Object>() {
                                {
                                    put(TextAttribute.FAMILY, MONOSPACED);
                                    put(TextAttribute.SIZE, 16);
                                }
                            }
                    )
            );
            Fonts.put(VisualiserFonts.TreePlain, new Font("Times New Roman", Font.PLAIN, 16));
            Fonts.put(VisualiserFonts.TreeItalic, new Font("Times New Roman", Font.ITALIC, 16));
            Fonts.put(VisualiserFonts.TreeBold, new Font("Times New Roman", Font.BOLD, 16));
            Fonts.put(VisualiserFonts.Menu, new Font("Times New Roman", Font.ITALIC, 16));
            // UI.Info("+");
            //</editor-fold>
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
    protected abstract String getEditorThemePath();
    protected abstract String getForegroundHex();
    protected abstract String getBackgroundHex();
    protected abstract String getSelectionBackgroundHex();
    protected abstract String getTreeBackgroundHex();
    protected abstract String getBarForegroundHex();
    protected abstract String getBarBackgroundHex();
    protected abstract String getTableBackgroundHex();
    protected abstract Color getGoodFontColor();
    protected abstract Color getProgressFontColor();
    protected abstract Color getBadFontColor();
    protected abstract Color getFatalFontColor();
    protected abstract Color getUnknownFontColor();
    protected abstract Color getHyperlinkFontColor();    //-
    //если использовать один и тот же объект на все едиторы пявляются странности
    //значит при применении выделяем новый экземпляр.
    public Theme getEditorTheme() {
        Theme res = null;
        try {
            res = Theme.load(getClass().getResourceAsStream(getEditorThemePath()));
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
        return res;
    }
}
