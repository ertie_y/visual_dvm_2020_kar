package Common.UI.Themes;

import java.awt.*;

public class LightVisualiserTheme extends VisualiserTheme {
    @Override
    protected String getEditorThemePath() {
        return "/Common/UI/Themes/light_editor.xml";
    }

    @Override
    protected String getForegroundHex() {
        return "#000000";
    }

    @Override
    protected String getBackgroundHex() {
        return "#f7f7f7";
    }

    @Override
    protected String getSelectionBackgroundHex() {
        return "#dae3f1";
    }

    @Override
    protected String getTreeBackgroundHex() {
        return "#ffffff";
    }

    @Override
    protected String getBarForegroundHex() {
        return "#637780";
    }

    @Override
    protected String getBarBackgroundHex() {
        return "#ffffff";
    }

    @Override
    protected String getTableBackgroundHex() {
        return "#ffffff";
    }

    @Override
    protected Color getGoodFontColor() {
        return Color.decode("#009738");
    }

    @Override
    protected Color getProgressFontColor() {
        return Color.decode("#f39a28");
    }

    @Override
    protected Color getBadFontColor() {
        return Color.decode("#ab0000");
    }

    @Override
    protected Color getFatalFontColor() {
        return Color.decode("#FF4500");
    }

    @Override
    protected Color getUnknownFontColor() {
        return Color.GRAY;
    }

    @Override
    protected Color getHyperlinkFontColor() {
        return Color.blue;
    }
}
