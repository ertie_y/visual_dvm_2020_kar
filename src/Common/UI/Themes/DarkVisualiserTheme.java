package Common.UI.Themes;
import java.awt.*;
public class DarkVisualiserTheme extends VisualiserTheme {
    @Override
    protected String getEditorThemePath() {
        return "/Common/UI/Themes/dark_editor.xml";
    }
    @Override
    protected String getForegroundHex() {
        return "#e0e5eb";
    }
    @Override
    protected String getBackgroundHex() {
        return "#484a4c";
    }
    @Override
    protected String getSelectionBackgroundHex() {
        return "#20355a";
    }
    @Override
    protected String getTreeBackgroundHex() {
        return "#293134";
    }
    @Override
    protected String getBarForegroundHex() {
        return "#000000";
    }
    @Override
    protected String getBarBackgroundHex() {
        return "#484a4c";
    }
    @Override
    protected String getTableBackgroundHex() {
        return "#293134";
    }
    @Override
    protected Color getGoodFontColor() {
        return Color.decode("#24ff58");
    }
    @Override
    protected Color getProgressFontColor() {
        return Color.orange;
    }
    @Override
    protected Color getBadFontColor() {
        return Color.decode("#8B0000");

    }
    @Override
    protected Color getFatalFontColor() {
        return Color.red;
    }
    @Override
    protected Color getUnknownFontColor() {
        return Color.decode("#c7c7c7");
    }
    @Override
    protected Color getHyperlinkFontColor() {
        return Color.decode("#00aee9");
    }
}
