package Common.UI.Themes;
public interface ThemeElement {
    default void FontUp() {
    }
    default void FontDown() {
    }
    void applyTheme();
}
