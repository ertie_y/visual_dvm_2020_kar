package Common.UI.Windows;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import Common.Utils.Utils;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rtextarea.SearchContext;
import org.fife.ui.rtextarea.SearchEngine;
import org.fife.ui.rtextarea.SearchResult;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

public class SearchReplaceForm extends Form {
    public JPanel MainPanel;
    public boolean forward = true;
    SearchContext context = null;
    RSyntaxTextArea editor = null;
    boolean replace_mode = false;
    private JTextField tfFind;
    private JTextField tfReplace;
    private JCheckBox replaceOn;
    private JCheckBox registerOn;
    private JCheckBox wholeWordOn;
    private JRadioButton rbUp;
    private JRadioButton rbDown;
    private JButton bAll;
    private JButton bNext;
    private JCheckBox loopOn;
    private SearchResult result = null;
    //https://techarks.ru/qa/java/kak-uznat-kakoj-iz-jlist-s-Y4/
    public SearchReplaceForm() {
        context = new SearchContext();
        //-
        context.setRegularExpression(true);
        //-
        rbDown.addActionListener(e -> SwitchDirection(!forward));
        rbUp.addActionListener(e -> SwitchDirection(!forward));
        bAll.addActionListener(e -> onAll());
        bNext.addActionListener(e -> onNext());
        replaceOn.addActionListener(e -> {
            setMode(replaceOn.isSelected());
        });
        bNext.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    bNext.doClick();
            }
        });
        tfFind.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                switch (e.getKeyCode()){
                    case KeyEvent.VK_ENTER:
                        bNext.requestFocus();
                        bNext.doClick();
                        break;
                }
            }
        });
    }
    public void ClearMarkers() {
        //сброс выделения. решается подсовыванием пустой строки
        context.setSearchFor("");
        SearchEngine.find(editor, context);
    }
    public void setEditor(RSyntaxTextArea editor_in) {
        editor = editor_in;
    }
    @Override
    protected JPanel getMainPanel() {
        return MainPanel;
    }
    @Override
    public void Close() {
        ClearMarkers();
        super.Close();
    }
    public void setMode(boolean replace) {
        replace_mode = replace;
        System.out.println("MODE CHANGED");
        tfReplace.setEnabled(replace_mode);
        String prefix = replace_mode ? "Заменить" : "Найти";
        bNext.setText(prefix + " далее");
        bAll.setText(prefix + " всё");
    }
    public void ShowMode() {
        replaceOn.setSelected(replace_mode);
    }
    @Override
    public Component getRelative() {
        return (Component) UI.getNewMainWindow();
    }
    @Override
    public int getDefaultWidth() {
        return 500;
    }
    @Override
    public int getDefaultHeight() {
        return 350;
    }
    @Override
    public void Show() {
        super.Show();
        Refresh();
        setAlwaysOnTop(true);
    }
    public void Refresh(){
        String text = editor.getSelectedText();
        if ((text != null) && !text.isEmpty())
            tfFind.setText(text);
        tfFind.requestFocus();
    }
    //-------------------------------
    public void SwitchDirection(boolean direction_in) {
        forward = direction_in;
        if (forward) {
            rbUp.setSelected(false);
            rbDown.setSelected(true);
        } else {
            rbDown.setSelected(false);
            rbUp.setSelected(true);
        }
    }
    public void applyParams() {
        String toFind = Utils.hideRegularMetasymbols(tfFind.getText());
        String toReplace = Utils.hideRegularMetasymbols(tfReplace.getText());
        System.out.println("toFind="+toFind);
        System.out.println("toReplace"+toReplace);
        System.out.println("============");
        context.setSearchFor(toFind);
        context.setMatchCase(registerOn.isSelected());
        context.setWholeWord(wholeWordOn.isSelected());
        if (replace_mode)
            context.setReplaceWith(toReplace);
    }
    public void onAll() {
        applyParams();
        result = replace_mode ?
                SearchEngine.replaceAll(editor, context) : SearchEngine.markAll(editor, context);
    }
    public void onNext() {
        applyParams();
        context.setSearchForward(forward);
        result = replace_mode ? SearchEngine.replace(editor, context) : SearchEngine.find(editor, context);
        if (loopOn.isSelected() && !result.wasFound()) SwitchDirection(!forward);
    }
    @Override
    protected FormType getFormType() {
        return FormType.SearchReplace;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfFind = new StyledTextField();
        tfReplace = new StyledTextField();
    }
}
