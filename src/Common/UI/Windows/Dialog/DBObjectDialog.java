package Common.UI.Windows.Dialog;
import Common.Database.DBObject;
public abstract class DBObjectDialog<T extends DBObject, F extends DialogFields> extends Dialog<T, F> {
    public boolean edit = false;
    public DBObjectDialog(Class<F> f) {
        super(f);
    }
    @Override
    public void Init(Object... params) {
        //тут входной параметр всегда объект. он же и есть Result
        //считаем что он всегда создан.
        Result = (T) params[0];
        fillFields();
    }
    public void SetEditLimits() {
        //установить ограничения если объект в режиме редактирования( например нельзя менять тип машины, или почту адресата)
    }
    public void fillFields() {
        //отобразить объект
    }
}
