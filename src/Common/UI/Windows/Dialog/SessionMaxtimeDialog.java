package Common.UI.Windows.Dialog;
import javax.swing.*;
public class SessionMaxtimeDialog extends SpinnerNumberForm {
    public SessionMaxtimeDialog() {
    }
    @Override
    public void InitFields() {
        fields.setModel(new SpinnerNumberModel(10, 5, 65535, 1));
    }
}
