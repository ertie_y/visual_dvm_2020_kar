package Common.UI.Windows.Dialog.Text;
import Common.UI.Windows.Dialog.Dialog;
import Common.UI.Windows.Dialog.DialogFields;
//текстовый диалог. возвращает текст. может иметь параметром исходный текст.
public abstract class TextDialog<F extends DialogFields> extends Dialog<String, F> {
    public TextDialog(Class<F> f) {
        super(f);
    }
    @Override
    public void Init(Object... params) {
        if (params.length > 0) setText((String) params[0]);
    }
    public abstract void setText(String text_in);
}

