package Common.UI.Windows.Dialog.Text;

import Common.UI.UI;
import Common.UI.Windows.Dialog.Dialog;
import Common.UI.Windows.Dialog.DialogTextComboBox;

import java.util.Vector;

public class ComboTextDialog extends Dialog<String, DialogTextComboBox> {
    public ComboTextDialog() {
        super(DialogTextComboBox.class);
    }

    @Override
    public void ProcessResult() {
        Result = (String) fields.getSelectedItem();
    }

    @Override
    public void validateFields() {
        if (fields.getSelectedItem() == null)
        Log.Writeln("Элемент не выбран");
    }

    @Override
    public void Init(Object... params) {
        Vector<String> sp = (Vector<String>) params[0];
        if (!sp.isEmpty()) {
            for (Object p : sp)
                fields.addItem(p.toString());
        }
    }
    @Override
    public int getDefaultWidth() {
        return 450;
    }

    @Override
    public int getDefaultHeight() {
        return 135;
    }
}
