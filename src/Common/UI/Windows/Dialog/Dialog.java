package Common.UI.Windows.Dialog;

import Common.Current;
import Common.Global;
import Common.UI.Themes.ThemeElement;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import Common.Utils.TextLog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

//T Тип объекта
//F Тип полей.
public class Dialog<T, F extends DialogFields> extends JDialog implements ThemeElement {
    public F fields; //рабочая область диалога.
    public String title_text;
    public JLabel lTitle = new JLabel();
    public boolean OK = false;
    //--------------------------------------
    public T Result = null;
    public TextLog Log = new TextLog(); //журнал валидации.
    protected JScrollPane scroll = null;

    //--------------------------------------
    public Dialog(Class<F> f) {
        setModal(true);
        toFront();
        getContentPane().setLayout(new BorderLayout());
        lTitle.setFont(Current.getTheme().Fonts.get(VisualiserFonts.Menu));
        //делаем титульную надпись в самом окне чтобы не зависеть от языковой политики ОС
        getContentPane().add(lTitle, BorderLayout.NORTH);
        //сюда добавляется содержимое.
        Component content = null;
        try {
            content = (fields = f.newInstance()).getContent();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
        InitFields(); //дополнительная инициализация полей..
        getContentPane().add(NeedsScroll() ? (scroll = new JScrollPane(content)) : content, BorderLayout.CENTER);
        CreateButtons();
        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
                onClose();
            }
        });
        //   pack(); //авторазмер окна.
        setLocationRelativeTo(null);
        LoadSize();
    }

    public void onClose(){

    }
    public void LoadSize() {
        setMinimumSize(new Dimension(getDefaultWidth(), getDefaultHeight()));
        setPreferredSize(new Dimension(getDefaultWidth(), getDefaultHeight()));
    }

    //бывает что у полей собственные скроллы
    public boolean NeedsScroll() {
        return true;
    }

    public int getDefaultWidth() {
        return 800;
    }

    public int getDefaultHeight() {
        return 450;
    }

    //создание полей формы( без заполнения)
    public void InitFields() {
    }

    public void CreateButtons() {
        JButton btnOK = new JButton("   OK   ");
        JButton btnCancel = new JButton("Отмена");
        JPanel jp = new JPanel();
        btnOK.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });
        jp.add(btnOK);
        jp.add(btnCancel);
        getContentPane().add(jp, BorderLayout.SOUTH);
    }

    public boolean isOnTop(){
        return true;

    }
    public boolean ShowDialog(String title, Object... params) {
        OK = false;
        title_text = title;
        Init(params);
        ShowTitle();
        setAlwaysOnTop(isOnTop());
        setVisible(true);
        return OK;
    }

    public void ShowTitle() {
        lTitle.setText(getTitleText());
    }

    public String getTitleText() {
        return title_text;
    }

    public void Init(Object... params) {
    }

    public void onOK() {
        Log.Clear();
        validateFields();
        if (Log.isEmpty()) {
            ProcessResult();
            OK = true;
            CloseAction();
        } else
            UI.Error("Валидация не пройдена:\n" + Log.toString());
    }

    protected void onCancel() {
        CloseAction();
    }

    public void CloseAction() {
        dispose();
    }

    public void validateFields() {
    }

    public void ProcessResult() {
    }

    @Override
    public void applyTheme() {
        //todo -> Применение темы
    }
}
