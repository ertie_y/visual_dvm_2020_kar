package Common.UI.Windows.Dialog;
public class PercentsForm extends SliderNumberForm {
    public PercentsForm() {
    }
    @Override
    public String getTitleText() {
        return super.getTitleText() + "%";
    }
    @Override
    public void InitFields() {
        fields.setPaintLabels(true);
        fields.setPaintTicks(true);
        fields.setPaintTrack(true);
        fields.setPaintLabels(true);
        fields.setSnapToTicks(true);
        fields.setMinorTickSpacing(1);
        fields.setMajorTickSpacing(25);
    }
    @Override
    public void Init(Object... params) {
        super.Init(params[0], 0, 100);
    }
}
