package Common.UI.Windows.Dialog;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
public class SliderNumberForm extends NumberDialog<DialogSlider> {
    public SliderNumberForm() {
        super(DialogSlider.class);
        setResizable(false);
    }
    @Override
    public void setNumber(int num_in) {
        fields.setValue(num_in);
    }
    //тут всегда должно быть три параметра
    //минимум нет смысла задавать меньше 1
    @Override
    public void Init(Object... params) {
        if (params.length == 3) {
            setNumber((Integer) params[0]);
            fields.setMinimum((Integer) params[1]);
            fields.setMaximum((Integer) params[2]);
        }
        fields.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                ShowTitle();
            }
        });
    }
    @Override
    public void InitFields() {
        fields.setPaintLabels(true);
        fields.setPaintTicks(true);
        fields.setPaintTrack(true);
        fields.setPaintLabels(true);
        fields.setSnapToTicks(false);
        fields.setMinorTickSpacing(1);
        fields.setMajorTickSpacing(1);
    }
    @Override
    public int getDefaultWidth() {
        return 600;
    }
    @Override
    public int getDefaultHeight() {
        return 200;
    }
    @Override
    public String getTitleText() {
        return title_text + " : " + fields.getValue();
    }
    @Override
    public void ProcessResult() {
        Result = fields.getValue();
    }
}
