package Common.UI.Windows;
public enum FormType {
    Undefined,
    Main,
    SearchReplace,
    Components,
    RemoteFileChooser,
    Statistic, //совместимость. или использовать  потом для анализатора?
    Functions,
    Main_2021, //для нового главного окна.
    FunctionsFilter,
    Other
}
