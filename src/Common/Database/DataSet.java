package Common.Database;

import Common.Current;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.UI.Windows.Dialog.DialogFields;
import Common.Utils.TextLog;
import Visual_DVM_2021.UI.Interface.FilterWindow;

import javax.swing.*;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Vector;
import java.util.stream.Collectors;

public class DataSet<K, D extends DBObject> extends DataSetAnchestor {
    public String Name;
    public Class<K> k; //класс первичного ключа.
    public Class<D> d; //класс объектов.
    public LinkedHashMap<K, D> Data = new LinkedHashMap<>(); //наполнение
    //-
    //<editor-fold desc="UI таблица">
    public DataSetControlForm ui;
    protected FilterWindow f_ui;

    //</editor-fold>
    //-
    public DataSet(Class<K> k_in, Class<D> d_in) {
        k = k_in;
        d = d_in;
        Name = d.getSimpleName();
    }

    public FilterWindow CreateFilterUI() {
        return null;
    }

    public FilterWindow getFilterUI() {
        return f_ui;
    }

    public void mountUI(JPanel content_in) {
        ui = createUI();
        ui.setContent(content_in);
        f_ui = CreateFilterUI();
    }

    public void ShowUI() {
        if (ui != null) {
            ui.Show();
            if (f_ui != null)
                f_ui.ShowMatchesCount(getRowCountUI());
        }
    }

    public void ShowUI(Object key) {
        if (ui != null) {
            ui.Show(key);
            if (f_ui != null)
                f_ui.ShowMatchesCount(getRowCountUI());
        }
    }

    public void ClearUI() {
        if ((ui != null) && ui.isShown()) {
            ui.ClearSelection();
            ui.Clear();
            if (f_ui != null)
                f_ui.ShowNoMatches();
        }
    }


    public void RefreshUI() {
        if (ui != null) ui.Refresh();
    }

    public int getRowCountUI() {
        return ui.getRowCount();
    }

    public void SetCurrentObjectUI(Object pk) {
        if (ui != null) {
            //todo возможно проверить, что текущий объект уже соответствует ключу, и если да, то ничего делать.
            ui.ClearSelection(); //сброс текущего объекта и всего что с ним связано.
            ui.Select(pk);
        }
    }

    public void ClearSelectionUI() {
        if (ui != null) ui.ClearSelection();
    }

    public JPanel getContentUI() {
        return ui.getContent();
    }

    //столбы/ потом переименовать обратно в getUIColumnNames.сейчас так для скорости переноса.
    public String[] getUIColumnNames() {
        return new String[]{};
    }

    protected DataSetControlForm createUI() {
        return null;
    }

    public boolean hasUI() {
        return ui != null;
    }

    public void CheckAll(boolean flag) {
        for (D object : Data.values()) {
            if (object.isVisible())
                object.Select(flag);
        }
        RefreshUI();
    }

    public D getFirstRecord() {
        return Data.values().stream().findFirst().orElse(null);
    }

    public Vector<D> getOrderedRecords(Comparator<D> comparator) {
        Vector<D> res = new Vector<>(Data.values());
        res.sort(comparator);
        return res;
    }

    @SuppressWarnings("unchecked")
    public DBObjectDialog<D, ? extends DialogFields> getDialog() {
        return null;
    }

    public boolean ShowAddObjectDialog(DBObject object) {
        return getDialog().ShowDialog(getDataDescription() + ": добавление", object);
    }

    public boolean ShowEditObjectDialog(DBObject object) {
        DBObjectDialog dialog = getDialog();
        dialog.edit=true;
        dialog.SetEditLimits();
        return dialog.ShowDialog(getDataDescription() + ": редактирование", object);
    }

    public boolean ShowDeleteObjectDialog(DBObject object) {
        return UI.Warning(getDataDescription() + " " + object.getBDialogName() + " будет удален(а)");
    }

    public String QName() {
        return "\"" + Name + "\"";
    }

    public String getPKName() {
        return "";
    } //получить имя ключевого поля. нужно для таблиц.

    public String getDataDescription() {
        return "";
    }

    //времянки
    public Current CurrentName() {
        return Current.Undefined;
    }

    public boolean CheckCurrent(TextLog log) {
        return Current.Check(log, CurrentName());
    }

    public boolean hasCurrent() {
        return Current.get(CurrentName()) != null;
    }

    public void dropCurrent() {
        Current.set(CurrentName(), null);
    }

    public D getCurrent() {
        return (D) Current.get(CurrentName());
    }

    public void setCurrent(D o) {
        Current.set(CurrentName(), o);
    }

    //-
    public void put(Object key, D object) {
        Data.put((K) key, object);
    }

    public D get(Object key) {
        return Data.get(key);
    }

    public Object getFieldAt(D object, int columnIndex) {
        return null;
    }

    public void clear() {
        Data.clear();
    }

    public int size() {
        return Data.size();
    }

    public boolean containsKey(Object key) {
        return Data.containsKey(key);
    }

    //-
    public Vector<K> getVisibleKeys() {
        Comparator<D> comparator = getComparator();
        Vector<K> res = new Vector<>();
        if (comparator == null) {
            for (K key : Data.keySet())
                if (Data.get(key).isVisible())
                    res.add(key);
        } else {
            Vector<D> raw = new Vector<>();
            for (D object : Data.values()) {
                if (object.isVisible())
                    raw.add(object);
            }
            raw.sort(comparator);
            for (D object : raw)
                res.add((K) object.getPK());
        }
        return res;
    }

    protected Comparator<D> getComparator() {
        return null;
    }

    public int getCheckedCount() {
        return (int) Data.values().stream().filter(d -> d.isVisible() && d.isSelected()).count();
    }

    public Vector<D> getCheckedItems() {
        return Data.values().stream().filter(d -> d.isVisible() && d.isSelected()).collect(Collectors.toCollection(Vector::new));
    }
}
