package Common.Database.SQLITE;
import Common.Database.DBObject;
import Common.Database.DBTable;
import Common.Database.DBTableColumn;
import Common.Database.Database;
import Common.Utils.Utils;

import java.io.File;
import java.sql.*;
import java.util.Vector;

import static Common.Utils.Utils.requireNonNullElse;
public abstract class SQLiteDatabase extends Database {
    protected Connection conn = null;
    protected Statement statement = null;
    protected PreparedStatement ps = null;
    protected ResultSet resSet = null;
    public SQLiteDatabase(File file_in) {
        super(file_in);
    }
    @Override
    protected void connect() throws Exception {
        Class.forName("org.sqlite.JDBC");
        conn = DriverManager.getConnection("jdbc:sqlite:" + file.getAbsolutePath());
        statement = conn.createStatement();
    }
    @Override
    protected void disconnect() throws Exception {
        if (conn != null) {
            conn.close();
            conn = null;
        }
        if (statement != null) {
            statement.close();
            statement = null;
        }
        if (ps != null) {
            ps.close();
            ps = null;
        }
        if (resSet != null) {
            resSet.close();
            resSet = null;
        }
    }
    @Override
    protected void beginTransaction() throws Exception {
        conn.setAutoCommit(false);
    }
    @Override
    protected void commit() throws Exception {
        conn.commit();
        conn.setAutoCommit(true);
    }
    @Override
    protected void executeQuery(String sql) throws Exception {
        resSet = statement.executeQuery(sql);
    }
    @Override
    protected void execute(String sql) throws Exception {
        statement.execute(sql);
    }
    @Override
    protected boolean TableExists(DBTable table) throws Exception {
        int count = 0;
        ExecuteQuery("SELECT count(*) FROM 'sqlite_master' WHERE type=\"table\" AND name=" + table.QName() + ";");
        if (resSet.next())
            count = resSet.getInt(1);
        return count > 0;
    }
    @Override
    protected void CreateTable(DBTable table) throws Exception {
        if (table.columns.size() > 0) {
            if (TableExists(table)) {
                Vector<String> existing_columns = new Vector<>();
                Vector<String> columns_to_create = new Vector<>();
                //предполагаем что первичный ключ и атрибуты не изменятся.
                //и что не будет столбов с одинаковыми именами но разными типами.
                ExecuteQuery("pragma table_info(" + table.QName() + ");");
                while (resSet.next())
                    existing_columns.add(resSet.getString(2));//, resSet.getString(3));
                for (String target_column : table.columns.keySet()) {
                    if (!existing_columns.contains(target_column))
                        columns_to_create.add(target_column); //нужно создать такой столбец.
                }
                for (String cn : columns_to_create)
                    Execute("ALTER TABLE " + table.QName() + " ADD COLUMN " + table.columns.get(cn));
            } else {
                //создаем столбцы по умолчанию
                String cmd = "CREATE TABLE if not exists " + table.QName() + " ";
                Vector<String> columns_names = new Vector<>();
                for (DBTableColumn column : table.columns.values())
                    columns_names.add(column.toString());
                cmd += Utils.RBrackets(String.join(",", columns_names)) + ";";
                Execute(cmd);
            }
        }
    }
    @Override
    protected <K, D extends DBObject> void loadAll(DBTable<K, D> table) throws Exception {
        ExecuteQuery("SELECT * FROM " + table.QName());
        while (resSet.next()) {
            D o = table.d.newInstance();
            for (DBTableColumn column : table.columns.values()) {
                Object field_value = null;
                switch (column.type) {
                    case DOUBLE:
                        field_value = requireNonNullElse(resSet.getDouble(column.Name), 0);
                        break;
                    case UNDEFINED:
                        break;
                    case INT:
                        field_value = requireNonNullElse(resSet.getInt(column.Name), 0);
                        break;
                    case LONG:
                        field_value = requireNonNullElse(resSet.getLong(column.Name), 0);
                        break;
                    case STRING:
                        if (table.d.getField(column.Name).getType().isEnum()) {
                            Class enum_class = Class.forName(table.d.getField(column.Name).getType().getName());
                            String string = resSet.getString(column.Name);
                            Object[] enum_constants = enum_class.getEnumConstants();
                            if (string != null) {
                                try {
                                    field_value = Enum.valueOf(enum_class, string);
                                } catch (Exception ignore) {
                                    System.out.println(Utils.Brackets(string)+"not found");
                                    field_value = enum_constants[0];
                                    System.out.println(field_value);
                                }
                            } else field_value = enum_constants[0];
                        } else
                            field_value = requireNonNullElse(resSet.getString(column.Name), "");
                        break;
                }
                if (field_value != null) {
                    table.d.getField(column.Name).set(o, field_value);
                } else
                    throw new Exception("Ошибка при загрузке поля " + Utils.Brackets(column.Name) + " класса " + Utils.Brackets(table.d.getSimpleName()));
            }
            table.Data.put((K) o.getPK(), o);
        }
    }
    @Override
    protected void insert(DBTable table, DBObject o) throws Exception {
        Vector<String> column_values = new Vector<>();
        Vector<String> column_names = new Vector<>();
        for (DBTableColumn column : table.columns.values()) {
            if (!column.AutoIncrement) {
                column_names.add(column.QName());
                column_values.add("?");
            }
        }
        String q = "INSERT OR REPLACE INTO " + table.QName() + " " +
                Utils.RBrackets(String.join(",", column_names)) + " " + "VALUES " +
                Utils.RBrackets(String.join(",", column_values))
                + ";";
        ps = conn.prepareStatement(q);
        int i = 1;
        for (DBTableColumn column : table.columns.values()) {
            if (!column.AutoIncrement) {
                ps.setObject(i, o.getClass().getField(column.Name).get(o));
                ++i;
            }
        }
        ps.execute();
        //автоинкременты могут быть только int
        for (DBTableColumn column : table.columns.values()) {
            if (column.AutoIncrement) {
                String query = "SELECT MAX(" +
                        column.QName() +
                        ") AS LAST FROM " + table.QName();
                ps = conn.prepareStatement(query);
                resSet = ps.executeQuery();
                String maxId = resSet.getString("LAST");
                int intMaxId = Integer.parseInt(maxId);
                o.getClass().getField(column.Name).set(o, intMaxId);
            }
        }
        // UI.Print(DebugPrintLevel.Database, q);
    }
    @Override
    protected void delete(DBTable table, DBObject o) throws Exception {
        String DeleteString = "DELETE FROM " + table.QName() + " WHERE " + table.PK.QName() + "= ?;";
        ps = conn.prepareStatement(DeleteString);
        ps.setObject(1, o.getPK());
        ps.executeUpdate();
    }
    @Override
    protected void deleteAll(DBTable table) throws Exception {
        String q = "DELETE FROM " + table.QName();
        ps = conn.prepareStatement(q);
        ps.executeUpdate();
    }
    @Override
    protected void update(DBTable table, DBObject o, String field_name) throws Exception {
        Vector<String> new_values = new Vector();
        //если не указано поле то обновляем все поля.
        if (field_name.isEmpty()) {
            for (DBTableColumn column : table.columns.values()) {
                if (!column.AutoIncrement)
                    new_values.add(column.QName() + "=?");
            }
        } else {
            DBTableColumn column = table.columns.get(field_name);
            new_values.add(column.QName() + "=?");
        }
        String q = "UPDATE " + table.QName() + " " +
                "SET " + String.join(",", new_values) + " " +
                "WHERE (" + table.PK.QName() + "=?);";
        ps = conn.prepareStatement(q);
        int i = 1;
        if (field_name.length() == 0) {
            for (DBTableColumn column : table.columns.values()) {
                if (!column.AutoIncrement) {
                    ps.setObject(i, o.getClass().getField(column.Name).get(o));
                    ++i;
                }
            }
        } else {
            DBTableColumn column = table.columns.get(field_name);
            ps.setObject(i, o.getClass().getField(column.Name).get(o));
            ++i;
        }
        ps.setObject(i, o.getPK());
        ps.executeUpdate();
    }
    @Override
    protected void resetAI(DBTable table) throws Exception {
        String q = "UPDATE SQLITE_SEQUENCE SET SEQ = 0 WHERE NAME =" + table.QName();
        ps = conn.prepareStatement(q);
        ps.executeUpdate();
    }
    //-------------------------------------------------------------------------------------------------------------------->>>
}