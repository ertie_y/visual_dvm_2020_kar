package Common.Database;
public class FKBehaviour {
    public FKDataBehaviour data; //поведение данных внешнего ключа при удалении/модификации
    public FKCurrentObjectBehaviuor ui; //поведение интерфейсов таблиц внешнего ключа при показе текущего объекта.
    public FKBehaviour(FKDataBehaviour data_in, FKCurrentObjectBehaviuor ui_in){
        data = data_in;
        ui=ui_in;
    }
}
