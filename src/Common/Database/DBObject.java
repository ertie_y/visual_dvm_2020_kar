package Common.Database;
import Common.UI.Selectable;
import Common.Utils.Index;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;
import javafx.util.Pair;

import java.awt.*;
import java.io.Serializable;
import java.util.Objects;
public abstract class DBObject implements Selectable, Serializable {
    //<editor-fold desc="Selectable">
    @Description("IGNORE")
    private boolean selected = false;
    @Override
    public boolean isSelected() {
        return selected;
    }
    @Override
    public void select(boolean flag) {
        if (selected != flag) {
            selected = flag;
            Index counter = getSelectionCounter();
            if (Objects.nonNull(counter)) {
                if (selected) counter.Inc();
                else counter.Dec();
            }
        }
    }
    //</editor-fold>
    public Index getSelectionCounter() {
        return null;
    }
    public boolean isVisible() {
        return true;
    }
    public abstract Object getPK();
    public String getBDialogName() {
        return Utils.Brackets(getDialogName());
    }
    public String getDialogName() {
        return getPK().toString();
    }
    //статус. например завершенность багрепорта или состояние задачи на запуск. как правило обладает цветным шрифтом.

    //как объект будут называть по внешним ключам.
    public String getFKName() {
        return null;
    }
    public Object getEmptyFK() {
        return null;
    }
    @Override
    public String toString() {
        return getBDialogName();
    }
}
