package Common.Database;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;
public abstract class DBTable<K, D extends DBObject> extends DataSet<K, D> {
    private Database db = null; //база данных - владелец таблицы.
    public void setDb(Database db_in) {
        db = db_in;
    }
    public Database getDb() {
        return db;
    }
    //-
    public DBTableColumn PK = null;
    public DBTable(Class<K> k_in, Class<D> d_in) {
        super(k_in, d_in);
        for (Field field : d.getFields()) {
            DBTableColumn column = new DBTableColumn(field);
            if ((!column.Ignore) && !columns.containsKey(column.Name)) {
                columns.put(column.Name, column);
                if (column.PrimaryKey) PK = column;
            }
        }
    }
    @Override
    public String getPKName() {
        return PK.Name;
    }
    @Override
    public String toString() {
        StringBuilder res = new StringBuilder(Name + "\n");
        for (DBTableColumn c : columns.values())
            res.append(c).append("\n");
        return res.toString();
    }
}
