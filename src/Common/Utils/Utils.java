package Common.Utils;

import Common.Global;
import Common.UI.UI;
import Visual_DVM_2021.Passes.Pass_2021;
import org.apache.commons.io.FileUtils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.io.*;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.URL;
import java.nio.file.*;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {
    public static final int Nan = -1;
    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    //https://losst.ru/komandy-terminala-linux
    public static String[] linux_system_commands = new String[]{
            //<editor-fold desc="все линуксовые команды. их МНОГО">
            "﻿a2p",
            "ac",
            "addgroup",
            "adduser",
            "agrep",
            "alias",
            "apropos",
            "apt",
            "aptitude",
            "ar",
            "arch",
            "arp",
            "as",
            "aspell",
            "at",
            "awk",
            "basename",
            "bash",
            "bс",
            "bdiff",
            "bfs",
            "bg",
            "biff",
            "break",
            "bs",
            "bye",
            "cal",
            "calendar",
            "cancel",
            "cat",
            "cc",
            "cd",
            "cfdisk",
            "chdir",
            "checkeq",
            "checknr",
            "chfn",
            "chgrp",
            "chmod",
            "chown",
            "chroot",
            "chsh",
            "cksum",
            "clear",
            "cmp",
            "col",
            "comm",
            "compress",
            "continue",
            "cp",
            "cpio",
            "crontab",
            "csh",
            "csplit",
            "ctags",
            "cu",
            "curl",
            "cut",
            "date",
            "dc",
            "dd",
            "delgroup",
            "deluser",
            "depmod",
            "deroff",
            "df",
            "dhclient",
            "dig",
            "dircmp",
            "dirname",
            "dmesg",
            "dos2unix",
            "dpkg",
            "dpost",
            "du",
            "echo",
            "ed",
            "edit",
            "egrep",
            "eject",
            "elm",
            "emacs",
            "emerge",
            "enable",
            "env",
            "eqn",
            "ex",
            "exit",
            "expand",
            "expr",
            "fc",
            "fdisk",
            "fg",
            "fgrep",
            "file",
            "find",
            "findsmb",
            "finger",
            "fmt",
            "fold",
            "for",
            "foreach",
            "free",
            "fsck",
            "ftp",
            "fuser",
            "gawk",
            "getfacl",
            "gpasswd",
            "gprof",
            "grep",
            "groupadd",
            "groupdel",
            "groupmod",
            "gnuzip",
            "gview",
            "gvim",
            "gzip",
            "halt",
            "head",
            "help",
            "history",
            "host",
            "hostid",
            "hostname",
            "htop",
            "id",
            "ifconfig",
            "ifdown",
            "ifquery",
            "ifup",
            "info",
            "insmod",
            "iostat",
            "ip",
            "iwconfig",
            "jobs",
            "join",
            "kill",
            "killall",
            "ksh",
            "last",
            "ld",
            "ldd",
            "less",
            "link",
            "ln",
            "lo",
            "locate",
            "login",
            "logname",
            "logout",
            "losetup",
            "ls",
            "lsmod",
            "lsof",
            "lzcat",
            "lzma",
            "mach",
            "mailx",
            "make",
            "man",
            "merge",
            "mesg",
            "mkdir",
            "mkfs",
            "mkswap",
            "modinfo",
            "modprobe",
            "more",
            "mount",
            "mt",
            "mv",
            "mysql",
            "mysqldump",
            "nc",
            "netstat",
            "newgrp",
            "nice",
            "niscat",
            "nischmod",
            "nischown",
            "nischttl",
            "nisdefaults",
            "nistbladm",
            "nl",
            "nmap",
            "nohup",
            "nroff",
            "nslookup",
            "od",
            "on",
            "onintr",
            "pack",
            "pacman",
            "pagesize",
            "parted",
            "partprobe",
            "passwd",
            "paste",
            "pax",
            "pact",
            "perl",
            "pg",
            "pico",
            "pine",
            "pkill",
            "poweroff",
            "pr",
            "printenv",
            "printf",
            "ps",
            "pstree",
            "pvs",
            "pwd",
            "quit",
            "rcp",
            "readlink",
            "reboot",
            "red",
            "rename",
            "repeat",
            "replace",
            "rlogin",
            "rm",
            "rmdir",
            "rmmod",
            "route",
            "rpcinfo",
            "rsh",
            "rsync",
            "s2p",
            "scp",
            "screen",
            "script",
            "sdiff",
            "sed",
            "sendmail",
            "service",
            "set",
            "setfacl",
            "sfdisk",
            "sftp",
            "sh",
            "shred",
            "shutdown",
            "sleep",
            "slogin",
            "smbclient",
            "sort",
            "spell",
            "split",
            "startx",
            "ss",
            "ssh",
            "stat",
            "stop",
            "strftime",
            "strip",
            "stty",
            "su",
            "sudo",
            "swapoff",
            "swapon",
            "systemctl",
            "tabs",
            "tac",
            "tail",
            "talk",
            "tar",
            "tcopy",
            "tcpdump",
            "tcsh",
            "tee",
            "telnet",
            "test",
            "time",
            "timex",
            "todos",
            "top",
            "touch",
            "traceroute",
            "tree",
            "tty",
            "umask",
            "umount",
            "unalias",
            "uname",
            "uncompress",
            "uniq",
            "unlink",
            "unlzma",
            "unpack",
            "until",
            "unxz",
            "unzip",
            "uptime",
            "useradd",
            "userdel",
            "usermod",
            "vacation",
            "vi",
            "vim",
            "w",
            "wait",
            "wall",
            "wc",
            "wget",
            "whereis",
            "which",
            "while",
            "who",
            "whoami",
            "whois",
            "Xorg",
            "xargs",
            "xfd",
            "xhost",
            "xlsfonts",
            "xrdb",
            "xset",
            "xz",
            "xzcat",
            "yacc",
            "yes",
            "yppasswd",
            "yum",
            "zcat",
            "zipcloack",
            "zipinfo",
            "zipnote",
            "zipsplit",
            "zypper"
            //</editor-fold>
    };
    public static char[] forbidden_file_name_characters = new char[]{

            '#', '%', '&', '{', '}',
            '<', '>', '*', '?', '!',
            '$', '\'', '\"', '@', '+',
            '`', '|', '=', '#', ':', '/', '\\',
            '~', '^'
    };

    public static char[] regular_metasymbols = new char[]{
            '<', '>', '(', ')', '[', ']', '{', '}', '^', '-', '=', '$', '!', '|', '?', '*', '+', '.'
    };

    public static String hideRegularMetasymbols(String word) {
        String res = word.replace("\\", "\\\\");

        for (char c : regular_metasymbols)
            res = res.replace(String.valueOf(c), "\\" + c);
        return res;
    }

    //все запретные символы через пробел.
    public static String all_forbidden_characters_string = "";

    public static boolean isLinuxSystemCommand(String text) {
        for (String command : linux_system_commands) {
            if (text.equalsIgnoreCase(command)) return true;
        }
        return false;
    }

    public static boolean ContainsForbiddenName(String string) {
        char[] chars = string.toCharArray();
        for (char c : chars)
            if (isForbidden(c)) return true;
        return false;
    }

    public static boolean isForbidden(char c) {
        for (char f : forbidden_file_name_characters)
            if (c == f) return true;
        return false;
    }

    public static void init() {
        for (char f : forbidden_file_name_characters)
            all_forbidden_characters_string += f + " ";
    }

    public static String DQuotes(Object o) {
        return "\"" + o.toString() + "\"";
    }

    public static String Quotes(Object o) {
        return "'" + o.toString() + "'";
    }

    public static String Brackets(Object o) {
        return "[" + o.toString() + "]";
    }

    public static String RBrackets(Object o) {
        return "(" + o.toString() + ")";
    }

    public static String MFVar(Object o) {
        return "$(" + o.toString() + ")";
    }

    public static String TBrackets(Object o) {
        return "<" + o.toString() + ">";
    }

    public static String getExtension(File file) {
        String fn = file.getName();
        int di = fn.lastIndexOf(".");
        return (di >= 0) ? fn.substring(di + 1).toLowerCase() : "";
    }

    public static String getNameWithoutExtension(File file) {
        String fn = file.getName();
        return fn.substring(0, fn.lastIndexOf(".") - 1).toLowerCase();
    }

    public static boolean ContainsCyrillic(String string) {
        return string.chars()
                .mapToObj(Character.UnicodeBlock::of)
                .anyMatch(b -> b.equals(Character.UnicodeBlock.CYRILLIC));
    }

    public static void CheckDirectory(File dir) {
        if (!dir.exists()) {
            try {
                FileUtils.forceMkdir(dir);
            } catch (Exception e) {
                Global.Log.PrintException(e);
            }
        }
    }

    public static void CheckAndCleanDirectory(File dir) {
        if (dir.exists()) {
            File[] files = dir.listFiles();
            if (files != null)
                for (File f : files) {
                    try {
                        forceDeleteWithCheck(f);
                    } catch (Exception e) {
                        Global.Log.PrintException(e);
                    }
                }
        } else {
            try {
                FileUtils.forceMkdir(dir);
            } catch (Exception e) {
                Global.Log.PrintException(e);
            }
        }
    }

    public static Object requireNonNullElse(Object value, Object default_value) {
        return (value != null) ? value : default_value;
    }

    //https://javadevblog.com/kak-schitat-fajl-v-string-primer-chteniya-fajla-na-java.html
    //https://habr.com/ru/post/269667/
    public static String ReadAllText(File file) {
        try {
            return new String(Files.readAllBytes(file.toPath()));
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
        return "";
    }

    public static String toU(String path) {
        return path.replace('\\', '/');
    }

    public static String toW(String path) {
        return path.replace('/', '\\');
    }

    public static double getFileSizeMegaBytes(File file) {
        double res = file.length() / (1024 * 1024);
        System.out.println(res);
        return res;
    }

    public static void CleanDirectory(File dir) {
        if (dir.exists() && dir.isDirectory()) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File f : files) {
                    try {
                        forceDeleteWithCheck(f);
                    } catch (Exception e) {
                        Global.Log.PrintException(e);
                    }
                }
            }

        }
    }

    public static String getDateName(String name_) {
        long ticks = new Date().toInstant().getEpochSecond();
        return name_ + "_" + ticks;
    }

    public static void WriteToFile(File f, String text) throws IOException {
        Files.write(
                f.toPath(),
                text.getBytes(),
                StandardOpenOption.CREATE,
                StandardOpenOption.TRUNCATE_EXISTING);
    }

    public static boolean isDigit(String s) {
        try {
            Integer.parseInt(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isEnglishLetter(char c) {
        return (((c >= 'a') && (c <= 'z')) || ((c >= 'A') && (c <= 'Z')));
    }

    public static boolean isRussianLetter(char c) {
        return ((c >= 'а') && (c <= 'я'))
                || ((c >= 'А') && (c <= 'Я'))
                || (c == 'Ё')
                || (c == 'ё');
    }

    public static boolean isSign(char c) {
        switch (c) {
            //арифметика.
            case '+':
            case '-':
            case '*':
            case '/':
            case '<':
            case '>':
            case '&':
            case '=':
            case '%':
            case '^':
                //- обр слеш
            case '\\':
                //препинание
            case ' ':
            case '_':
            case '.':
            case ',':
            case '!':
            case '?':
            case ';':
            case ':':
                //escape последовательности
            case '\t':
            case '\n':
            case '\r':
                //кавычки
            case '\'':
            case '"':
                //- скобки
            case '(':
            case ')':
            case '[':
            case ']':
            case '{':
            case '}':
                //прочее
            case '~':
            case '`':
            case '|':
            case '@':
            case '$':
            case '#':
            case '№':
                return true;
        }
        return false;
    }

    public static char Translit(char c) {
        switch (c) {
            case 'А':
            case 'а':
            case 'Я':
            case 'я':
                return 'A';
            //
            case 'Б':
            case 'б':
                return 'B';
            //-
            case 'В':
            case 'в':
                return 'V';
            //
            case 'Г':
            case 'г':
                return 'G';
            //
            case 'Д':
            case 'д':
                return 'D';
            //
            case 'Е':
            case 'е':
            case 'Ё':
            case 'ё':
            case 'Э':
            case 'э':
                return 'E';
            //
            case 'Ж':
            case 'ж':
                return 'J';
            //
            case 'З':
            case 'з':
                return 'Z';
            //
            case 'И':
            case 'и':
            case 'Й':
            case 'й':
                return 'I';
            //
            case 'К':
            case 'к':
                return 'K';
            //
            case 'Л':
            case 'л':
                return 'L';
            //
            case 'М':
            case 'м':
                return 'M';
            //
            case 'Н':
            case 'н':
                return 'N';
            //
            case 'О':
            case 'о':
                return 'O';
            //
            case 'П':
            case 'п':
                return 'P';
            //
            case 'Р':
            case 'р':
                return 'R';
            //
            case 'С':
            case 'с':
                return 'S';
            case 'Т':
            case 'т':
                return 'T';
            //
            case 'У':
            case 'у':
            case 'Ю':
            case 'ю':
                return 'U';
            case 'Х':
            case 'х':
            case 'Щ':
            case 'щ':
            case 'Ш':
            case 'ш':
                return 'H';
            //
            case 'Ф':
            case 'ф':
                return 'F';
            //
            case 'Ч':
            case 'ч':
            case 'Ц':
            case 'ц':
                return 'C';
            //
            case 'Ы':
            case 'ы':
                return 'Y';
            //
        }
        return ' ';
    }

    public static String ending(boolean flag) {
        return flag ? ")" : ",";
    }

    // http://java-online.ru/blog-archive.xhtml
    public static void getFilesCountR(File dir, Index res) {
        for (File f : dir.listFiles()) {
            res.Inc();
            if (f.isDirectory())
                getFilesCountR(f, res);
        }
    }

    public static int getFilesCount(File dir) {
        Index res = new Index();
        getFilesCountR(dir, res);
        return res.getValue();
    }

    public static String print_date(Date date) {
        String pattern = "dd.MM.yyyy HH:mm:ss";
        DateFormat df = new SimpleDateFormat(pattern);
        return df.format(date);
    }

    public static boolean isBracketsBalanced(String fragment) {
        int cc = 0;
        for (char c : fragment.toCharArray()) {
            if (c == '(')
                cc++;
            if (c == ')')
                cc--;
            if (cc < 0)
                return false;
        }
        return (cc == 0);
    }

    public static File CreateTempResourceFile(String res_name) throws Exception {
        URL u = (Utils.class.getResource("/files/" + res_name));
        InputStream i = u.openStream();
        Path p = Paths.get(Global.TempDirectory.getAbsolutePath(), getDateName(res_name));
        Files.copy(i, p, StandardCopyOption.REPLACE_EXISTING);
        return p.toFile();
    }

    public static void CreateResourceFile(String res_name, File dst) throws Exception {
        URL u = (Utils.class.getResource("/files/" + res_name));
        InputStream i = u.openStream();
        Files.copy(i, dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    //просто дать объект под файл с сгенерированным именем
    public static File CreateTempFile(String name, String text) throws IOException {
        File res = getTempFileName(name);
        WriteToFile(res, text);
        return res;
    }

    public static File CreateTempFile(String name, String ext, String text) throws IOException {
        File res = getTempFileName(name, ext);
        WriteToFile(res, text);
        return res;
    }

    public static File getTempFileName(String name, String ext) {
        return Paths.get(System.getProperty("user.dir"), "Temp", getDateName(name) + "." + ext).toFile();
    }

    public static File getTempFileName(String name) {
        return getTempFileName(name, "");
    }

    public static boolean isAnchestor(File child, File anchestor) {
        return child.getAbsolutePath().startsWith(anchestor.getAbsolutePath() + (Global.isWindows ? "\\" : "/"));
    }

    //при условии что это точно его предок
    public static String getRelativeAddress(File file, File anchestor) {
        return file.getAbsolutePath().substring(anchestor.getAbsolutePath().length() + 1);
    }

    public static String pack(String s_in) {
        return String.join(" ", s_in.split(" "));
    }

    public static String remove(String string, String... to_remove) {
        String res = string;
        for (String c : to_remove)
            res = res.replace(c, "");
        return res;
    }

    //для переименования/добавления новых файлов.
    public static boolean validateFileShortNewName(String name, TextLog Log) {
        boolean res = true;
        if (name.isEmpty()) {
            Log.Writeln_("Имя файла не может быть пустым");
            res = false;
        }
        if (ContainsCyrillic(name)) {
            Log.Writeln_("Имя файла не может содержать кириллицу");
            res = false;
        }
        if (ContainsForbiddenName(name)) {
            Log.Writeln_("Имя файла не может содержать запрещённых символов\n" + all_forbidden_characters_string);
            res = false;
        }
        return res;
    }

    //корень сюда гарантированно не попадает. значит можно запрещать двоеточие.
    //идет по всем уровням файлов
    public static boolean validateProjectFile(File file, TextLog Log) {
        String name = file.getName();
        if (ContainsCyrillic(name) || ContainsForbiddenName(name)) {
            Log.Writeln_(file.getAbsolutePath());
            return false;
        }
        return true;
    }

    //входной параметр всегда папка.
    public static void validateFolder_r(File dir, TextLog Log) {
        validateProjectFile(dir, Log);
        File[] files = dir.listFiles();
        if (files != null)
            for (File file : files) {
                if (file.isDirectory())
                    validateFolder_r(file, Log);
                else validateProjectFile(file, Log);
            }
    }

    public static boolean validateFolder(File dir, TextLog Log) {
        TextLog files_list = new TextLog();
        validateFolder_r(dir, files_list);
        if (!files_list.isEmpty())
            Log.Writeln_("Имена файлов/папок\n\n" + files_list + "\nсодержат запрещённые символы " +
                    all_forbidden_characters_string +
                    "или кириллицу");
        return Log.isEmpty();
    }

    public static ImageIcon getIcon(String path) {
        URL imageUrl = Utils.class.getResource(path);
        if (imageUrl == null) {
            System.out.println("image: " + Brackets(path) + "not found");
            return null;
        }
        return new ImageIcon(imageUrl);
    }

    public static ImageIcon getTabIcon(String path) {
        URL imageUrl = Utils.class.getResource(path);
        if (imageUrl == null) {
            System.out.println("image: " + Brackets(path) + "not found");
            return null;
        }
        ImageIcon icon = new ImageIcon(imageUrl);
        return new ImageIcon(icon.getImage().getScaledInstance(
                Pass_2021.buttonTabDimension.width,
                Pass_2021.buttonTabDimension.height,
                Image.SCALE_DEFAULT));
    }

    public static void CopyToClipboard(String text) {
        Toolkit.getDefaultToolkit()
                .getSystemClipboard()
                .setContents(
                        new StringSelection(text),
                        null
                );
    }

    public static String getFromClipboard() {
        String res = "";
        try {
            res = (String) Toolkit.getDefaultToolkit()
                    .getSystemClipboard().getData(DataFlavor.stringFlavor);
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
        return res;
    }

    public static void delete_with_check(File file) throws Exception {
        int attempts = 0;
        while (attempts < 10) {
            if (file.exists()) file.delete();
            else return;
            if (file.exists()) {
                attempts++;
                System.out.println("файл " + Brackets(file.getAbsolutePath()) + " занят");
                Thread.sleep(2000);
            } else return;
        }
        throw new Exception("Не удалось удалить файл " + Brackets(file.getAbsolutePath()) + " за " + attempts + " попыток");
    }

    public static void GetVertices(float R, float r, float x0, float y0, int n, float phi) {
        boolean inner = false;
        for (int i = 0; i < 2 * n; i++) {
            float rad = inner ? r : R;
            System.out.println("x=" + (x0 + rad * Math.cos(phi + Math.PI * i / n)));
            System.out.println("y=" + (y0 + rad * Math.sin(phi + Math.PI * i / n)));
            System.out.println("--");
            inner = !inner;
        }
    }

    public static String md5Custom(String st) {
        MessageDigest messageDigest = null;
        byte[] digest = new byte[0];
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.reset();
            messageDigest.update(st.getBytes());
            digest = messageDigest.digest();
        } catch (Exception e) {
            // тут можно обработать ошибку
            // возникает она если в передаваемый алгоритм в getInstance(,,,) не существует
            Global.Log.PrintException(e);
        }
        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);
        while (md5Hex.length() < 32) {
            md5Hex = "0" + md5Hex;
        }
        return md5Hex;
    }

    public static void renameSubdirs_r(DefaultMutableTreeNode node, File old_anchestor, File new_anchestor) {
        if (node.getUserObject() instanceof File) {
            File old_file = (File) node.getUserObject();
            String relative_name = old_file.getAbsolutePath().substring(old_anchestor.getAbsolutePath().length() + 1);
            File new_file = Paths.get(new_anchestor.getAbsolutePath(), relative_name).toFile();
            node.setUserObject(new_file);
            for (int i = 0; i < node.getChildCount(); ++i) {
                renameSubdirs_r((DefaultMutableTreeNode) node.getChildAt(i), old_anchestor, new_anchestor);
            }
        }
    }

    //--
    public static void forceDeleteWithCheck(File file) throws Exception {
        int attempts = 0;
        while (attempts < 10) {
            if (file.exists()) {
                try {
                    FileUtils.forceDelete(file);
                } catch (Exception ignore) {
                }
            } else return;
            if (file.exists()) {
                attempts++;
                Global.Log.Print("неудачная попытка удаления: файл " + Brackets(file.getAbsolutePath()) + " занят");
                Thread.sleep(2000);
            } else return;
        }
        throw new Exception("Не удалось удалить файл " + Brackets(file.getAbsolutePath()) + " за " + attempts + " попыток");
    }

    public static byte[] packFile(File src) throws Exception {
        System.out.println("pack begins: " + src.getAbsolutePath());
        byte[] dst = Files.readAllBytes(src.toPath());
        System.out.println("pack done: bytes:" + dst.length);
        return dst;
    }

    public static void unpackFile(byte[] bytes, File dst) throws Exception {
        FileOutputStream os = new FileOutputStream(dst);
        System.out.println(dst.getAbsolutePath());
        System.out.println("unpack begins: bytes:" + bytes.length);
        os.write(bytes);
        os.close();
        System.out.println("unpack done to " + dst.getAbsolutePath());
    }

    public static Socket createClientSocket(InetAddress address, int port, int timeout) throws Exception {
        Socket socket = new Socket();
        socket.setSoTimeout(timeout);
        socket.connect(new InetSocketAddress(address, port), timeout);
        return socket;
    }

    public static void CreateResourceFile(File dst) throws Exception {
        URL u = (Utils.class.getResource("/files/" + dst.getName()));
        InputStream i = u.openStream();
        Files.copy(i, dst.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public static boolean validateEmail(String address, TextLog log) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX.matcher(address);
        if (!matcher.find()) {
            log.Writeln_("введённый адрес электронной почты некорректен.");
            return false;
        }
        String match = address.substring(matcher.start(), matcher.end());
        if (!match.equals(address)) {
            log.Writeln_("введённый адрес электронной почты некорректен.");
            return false;
        }
        return true;
    }

    public static void getFilesByExtensions_r(File dir, Vector<File> res, String... extensions) {
        File[] files = dir.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isFile()) {
                    for (String ext : extensions) {
                        if (getExtension(file).equalsIgnoreCase(ext))
                            res.add(file);
                    }
                }
                if (file.isDirectory())
                    getFilesByExtensions_r(file, res);
            }
        }
    }

    //----->>
    public static void deleteFilesByExtensions(File dir, String... extensions) throws Exception {
        Vector<File> res = new Vector<>();
        Utils.getFilesByExtensions_r(dir, res, extensions);
        for (File src : res)
            Utils.forceDeleteWithCheck(src);
    }

    //----->>
    //--процессы-------------------------------------------------->>>>>
    //<editor-fold desc="создание скрипта">
    public static Process startScript(File scriptDirectory, File targetDirectory, String name, String scriptText, Map<String, String> envs) throws Exception {
        //->
        File scriptFile = Paths.get(scriptDirectory.getAbsolutePath(), name + (Global.isWindows ? ".bat" : "")).toFile();
        FileUtils.write(scriptFile, "cd " + Utils.DQuotes(targetDirectory.getAbsolutePath()) + "\n" + scriptText);
        if (!scriptFile.setExecutable(true)) throw new Exception("Не удалось создать исполняемый файл для скрипта");
        //->>
        ProcessBuilder procBuilder = new ProcessBuilder(scriptFile.getAbsolutePath());
        procBuilder.redirectErrorStream(true);
        if (envs != null) {
            for (String envName : envs.keySet()) {
                procBuilder.environment().put(envName, envs.get(envName));
            }
        }
        return procBuilder.start();
    }

    public static Process startScript(File scriptDirectory, File targetDirectory, String name, String scriptText) throws Exception {
        return startScript(scriptDirectory, targetDirectory, name, scriptText, null);
    }

    //</editor-fold>
    //<editor-fold desc="чтение вывода процесса">
    public static String readLine(Process process) throws Exception {
        InputStream stdout = process.getInputStream();
        InputStreamReader isrStdout = new InputStreamReader(stdout);
        BufferedReader brStdout = new BufferedReader(isrStdout);
        String line = brStdout.readLine();
        System.out.println(line);
        return line;
    }

    public static Vector<String> readAllLines(Process process) throws Exception {
        Vector<String> output = new Vector<>();
        InputStream stdout = process.getInputStream();
        InputStreamReader isrStdout = new InputStreamReader(stdout);
        BufferedReader brStdout = new BufferedReader(isrStdout);
        String line;
        while ((line = brStdout.readLine()) != null) {
            System.out.println(line);
            output.add(line);
        }
        return output;
    }

    public static String readAllOutput(Process process) throws Exception {
        return String.join("\n", readAllLines(process));
    }

    //todo завести таблицу процессов и убивать по закрытии визуализатора если вдруг не закрыты?
    public static void performProcess(String... args) throws Exception {
        ProcessBuilder procBuilder = new ProcessBuilder(args);
        procBuilder.redirectErrorStream(true);
        Process process = procBuilder.start();
        Utils.readAllLines(process);
        int exit_code = process.waitFor();
        System.out.println("завершено с кодом " + exit_code);
        if (exit_code < 0) throw new Exception("Запуск процесса завершился с кодом " + exit_code);
    }
    //</editor-fold>

    public static String extractHeaderName(String line) {
        String tline = line.trim().toLowerCase();
        if (tline.startsWith("include")) {
            String[] data = tline.split("'");
            return data.length > 1 ? data[1] : null;
        }
        return null;
    }

    public static String getRelativeName(File dir, File file) {
        return
                file.getAbsolutePath().startsWith(dir.getAbsolutePath()) ?
                        file.getAbsolutePath().substring(dir.getAbsolutePath().length() + 1).replace('/', '\\') :
                        file.getAbsolutePath()
                ;
    }

    public static String compareTexts(String master, String slave) {

        Vector<String> lines = new Vector<>(Arrays.asList(master.split("\n")));
        Vector<String> slavelines = new Vector<>(Arrays.asList(slave.split("\n")));

        Vector<String> t2 = new Vector<>();
        int old_j = 0;
        int j = 0;
        for (int i = 0; i < lines.size(); ++i) {
            if (UI.Contains(slavelines, lines.get(i), old_j)) {
                for (int k = old_j; k < slavelines.size(); ++k) {
                    j = k;
                    if (lines.get(i).equals(slavelines.get(k))) {
                        j++;
                        t2.add(slavelines.get(k));
                        break;
                    } else
                        t2.add("+ " + slavelines.get(k));
                }
                old_j = j;
            } else {
                //строки гарантированно нет.
                t2.add("- " + lines.get(i));
            }
        }
        //теперь граничное условие. если первый файл кончился а второй нет, его остаток это добавление.
        for (int i = j; i < slavelines.size(); ++i)
            t2.add("+ " + slavelines.get(i));
        return String.join("\n", t2);
    }

    public static int getHalfKernels(){
        int countCores = 1;
        if (Runtime.getRuntime().availableProcessors() > 1)
            countCores = Runtime.getRuntime().availableProcessors()/2;
        return countCores;
    }
    public static int getMaxKernels(){
        return Math.max(2, Runtime.getRuntime().availableProcessors());
    }
}
