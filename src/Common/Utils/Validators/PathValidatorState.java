package Common.Utils.Validators;
public enum PathValidatorState {
    Start,
    Name,
    WrongNameFormat,
    Forbidden,
    SpacesInLastName
}
