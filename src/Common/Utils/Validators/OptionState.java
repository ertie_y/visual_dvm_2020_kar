package Common.Utils.Validators;

public enum OptionState {
    SearchName,
    Name,
    //-
    SearchParameter,
    Parameter,
    //-
    SearchDescription,
    Description
}
