package Common.Utils.Validators;
import Common.Utils.TextLog;
import Common.Utils.Utils;
import org.fife.ui.rsyntaxtextarea.RSyntaxUtilities;
public class PathValidator extends Validator {
    PathValidatorState state;
    StringBuilder name;
    int spaces_count;
    public PathValidator(String string, String string_name_in, TextLog log_in) {
        super(string, string_name_in, log_in);
    }
    @Override
    protected void reset() {
        state = PathValidatorState.Start;
        name = new StringBuilder();
        spaces_count = 0;
    }
    @Override
    protected boolean continueCondition() {
        return (state == PathValidatorState.Name) || state == PathValidatorState.Start;
    }
    @Override
    protected void Body() {
        switch (state) {
            case Start:
                if (RSyntaxUtilities.isLetter(c) || RSyntaxUtilities.isDigit(c) || c == '_') {
                    name.append(c);
                    state = PathValidatorState.Name;
                } else state = PathValidatorState.WrongNameFormat;
                break;
            case Name:
                switch (c) {
                    case '/':
                        reset();
                        break;
                    case ' ':
                        spaces_count++;
                        name.append(c);
                        break;
                    default:
                        if (Utils.isForbidden(c))
                            state = PathValidatorState.Forbidden;
                        else name.append(c);
                        break;
                }
                break;
        }
    }
    @Override
    protected void PerformFinish() {
        switch (state) {
            case WrongNameFormat:
                Log.Writeln(string_name + ": имя файла или каталога в пути имеет неверный формат");
                break;
            case Forbidden:
                Log.Writeln(string_name + ": Составляющие путь имена содержат запрещённые символы \n" + Utils.all_forbidden_characters_string);
                break;
            case Name:
                if (spaces_count > 0)
                    Log.Writeln(string_name + ": Пробелы в окончании пути к файлу запрещены.");
                break;
        }
    }
    @Override
    protected int getStartIndex() {
        return 1;
    }
}
