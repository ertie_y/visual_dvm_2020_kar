package Common.Utils.Validators;
import Common.Utils.TextLog;
public class StatementsChecker {
    public static boolean Check(TextLog Log, boolean drop_on_first, Object ... args){
        if (args.length%2!=0) return false;
        for (int i=0; i<args.length; i+=2){
            if ((boolean) args[i]){
                Log.Writeln((String) args[i+1]);
                if (drop_on_first) return false;
            }
        }
        return true;
    }
}
