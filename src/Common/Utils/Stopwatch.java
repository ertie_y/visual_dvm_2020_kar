package Common.Utils;
public class Stopwatch {
    long st, en;
    double res;
    public void Start() {
        st = System.nanoTime();
    }
    public void Stop() {
        en = System.nanoTime();
        res = en - st;
    }
    public String Print() {
        //https://hr-vector.com/java/formatirovanie-chisel-strok
        return String.format("%,3.2f", res * 0.000001) + " ms";
    }
}
