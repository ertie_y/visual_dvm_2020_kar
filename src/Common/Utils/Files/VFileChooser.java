package Common.Utils.Files;
import Common.Utils.Utils;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.Arrays;
import java.util.Vector;
public class VFileChooser extends VFileChooser_ {
    Vector<String> Extensions = new Vector<>();
    String extensionsLine = "";
    //todo сделать вариант и с запретным списком расширений. то есть добавить флаг.
    public VFileChooser(String title, String... extensions_in) {
        super(title, null);
        Extensions.addAll(Arrays.asList(extensions_in));
        if (Extensions.isEmpty())
            extensionsLine = "*.*";
        else
            for (String ext : Extensions)
                extensionsLine += "*" + (ext.isEmpty() ? "" : ".") + ext + ";";
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                return !Utils.ContainsCyrillic(f.getName())
                        && (f.isDirectory() || acceptExtensions(f));
            }
            @Override
            public String getDescription() {
                return extensionsLine;
            }
        });
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }
    public boolean acceptExtensions(File file) {
        if (Extensions.isEmpty()) return true;
        String file_ext = Utils.getExtension(file);
        for (String ext : Extensions)
            if (ext.equalsIgnoreCase(file_ext)) return true;
        return false;
    }
    public VFileChooser(String title, FileFilter filter_in) {
        super(title, filter_in);
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }
}
