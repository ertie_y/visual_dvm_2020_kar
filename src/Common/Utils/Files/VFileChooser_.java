package Common.Utils.Files;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.io.File;
import java.util.Arrays;
import java.util.Vector;
public class VFileChooser_ {
    protected JFileChooser fileChooser = new JFileChooser(){
        @Override
        protected JDialog createDialog(Component parent) throws HeadlessException {
            JDialog res =  super.createDialog(parent);
            res.setAlwaysOnTop(true);
            return res;
        }
    };
    public void setTitle(String title_in){
        fileChooser.setDialogTitle(title_in);
    }
    public File ShowDialog() {
        fileChooser.setMultiSelectionEnabled(false);
        File result = null;
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            result = fileChooser.getSelectedFile();
        }
        return result;
    }
    public Vector<File> ShowMultiDialog() {
        fileChooser.setMultiSelectionEnabled(true);
        Vector<File> result = new Vector<>();
        if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            result = new Vector<>(Arrays.asList(fileChooser.getSelectedFiles()));
        }
        return result;
    }
    public VFileChooser_(String title, FileFilter filter) {
        fileChooser.setDialogTitle(title);
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.setFileFilter(filter);
    }
    public void SetCurrentDirectory(String dir) {
        fileChooser.setCurrentDirectory(new File(dir));
    }
    public void SetCurrentDirectory(File dir) {
        fileChooser.setCurrentDirectory(dir);
    }
}
