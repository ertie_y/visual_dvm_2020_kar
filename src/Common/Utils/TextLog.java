package Common.Utils;
import Common.Global;
public class TextLog {
    String text = "";
    public void Writeln(String line) {
        text += line + "\n";
    }
    public void Writeln_(String line){
        text += line + "\n";
        Global.Log.Print(line);
    }
    public void Clear() {
        text = "";
    }
    public boolean isEmpty() {
        return text.isEmpty();
    }
    @Override
    public String toString() {
        return text;
    }
}
