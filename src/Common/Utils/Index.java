package Common.Utils;
public class Index {
    int value = 0;
    public int Inc() {
        return value++;
    }
    public int Dec() {
        return value--;
    }
    public void Set(int value_in) {
        value = value_in;
    }
    public int getValue() {
        return value;
    }
    public void Reset() {
        value = 0;
    }
    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
