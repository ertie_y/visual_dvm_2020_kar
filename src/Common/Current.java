package Common;
import Common.Database.iDBObject;
import Common.Passes.UI.PassForm;
import Common.UI.Themes.VisualiserTheme;
import Common.Utils.TextLog;
import GlobalData.Account.Account;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.Machine;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.Scenario.Scenario;
import GlobalData.ScenarioCommand.ScenarioCommand;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.User.User;
import ProjectData.Files.DBProjectFile;
import ProjectData.Project.db_project_info;
import ProjectData.SapforData.Functions.FuncInfo;
import ProjectData.SapforData.Regions.ParallelRegion;
import Repository.BugReport.BugReport;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.Test.Test;

import javax.swing.tree.DefaultMutableTreeNode;
import java.io.File;
import java.util.LinkedHashMap;
public enum Current {
    Undefined,
    Theme,
    FileGraphElement,
    InlineGraphElement,
    InlineGraphElement2,
    IncludeGraphElement,
    Component,
    Project,
    File,
    Root,
    Version,
    BugReport,
    Recipient,
    Account,
    DBArray,
    ProjectArray,
    ParallelRegionInfo,
    ParallelVariant,
    Machine,
    User,
    Compiler,
    Makefile,
    Module,
    RunConfiguration,
    EnvironmentValue,
    CompilationTask,
    RunTask,
    ProjectNode, //узел в дереве проекта. нужен для отображения добавленных файлов
    SelectedDirectory,
    SelectedFile,
    //текущий выбранный удаленный файл
    RemoteFile,
    PassForm, //текущее окно анимации. нужно для сообщений сапфора по сокету.
    RunStsRecord,
    //только для того, чтобы закодировать таблицу.
    Array,
    ParallelRegion,
    Dimensions,
    //----------
    Warnings,
    Errors,
    Notes,
    //-
    Sapfor,
    //-
    Scenario,
    ScenarioCommand,
    //-
    Group,
    DialogCompiler,
    //-
    DVMParameterValue,
    Test,
    Function,
    //-
    TestCompilationTask,
    TestRunTask;
    //---
    public enum Mode {
        Undefined,
        Normal,
        Server
    }
    public static Mode mode;
    public static boolean hasUI() {
        return Current.mode.equals(Current.Mode.Normal);
    }
    public static void setDialogCompiler(Object compiler){
        set(Current.DialogCompiler, (compiler instanceof Compiler) ? compiler:null);
    }
    public static Compiler getDialogCompiler(){
        return (GlobalData.Compiler.Compiler) get(DialogCompiler);
    }
    //-
    //---
    private static final LinkedHashMap<Current, Object> objects = new LinkedHashMap<>();
    public static boolean HasProject() {
        return get(Project) != null;
    }
    public static boolean HasFile() {
        return get(File) != null;
    }
    public static boolean HasAccount() {
        return get(Account) != null;
    }
    public static boolean HasMachine() {
        return get(Machine) != null;
    }
    public static boolean HasUser() {
        return get(User) != null;
    }
    public static boolean HasCompiler() {
        return get(Compiler) != null;
    }
    public static boolean HasRemoteFile() {
        return get(RemoteFile) != null;
    }
    public static boolean HasMakefile() {
        return get(Makefile) != null;
    }
    public static boolean HasRunConfiguration() {
        return get(RunConfiguration) != null;
    }
    public static boolean HasCompilationTask() {
        return get(CompilationTask) != null;
    }
    public static boolean HasRunTask() {
        return get(RunTask) != null;
    }
    public static boolean HasPassForm() {
        return get(PassForm) != null;
    }
    //для быстрого доступа на чтение. слишком много на нем завязано.
    public static db_project_info getProject() {
        return (db_project_info) get(Project);
    }
    public static DBProjectFile getFile() {
        return (DBProjectFile) get(File);
    }
    public static Repository.Component.Component getComponent() {
        return (Repository.Component.Component) get(Component);
    }
    public static Repository.BugReport.BugReport getBugReport() {
        return (BugReport) get(BugReport);
    }
    public static db_project_info getRoot() {
        return (db_project_info) get(Root);
    }
    public static boolean HasRoot() {
        return get(Root) != null;
    }
    public static db_project_info getVersion() {
        return (db_project_info) get(Version);
    }
    public static Account getAccount() {
        return (Account) get(Account);
    }
    public static Machine getMachine() {
        return (Machine) get(Current.Machine);
    }
    public static User getUser() {
        return (User) get(Current.User);
    }
    public static Compiler getCompiler() {
        return (Compiler) get(Current.Compiler);
    }
    public static CompilationTask getCompilationTask() {
        return (CompilationTask) get(Current.CompilationTask);
    }
    public static RunTask getRunTask() {
        return (RunTask) get(Current.RunTask);
    }
    public static RemoteFile getRemoteFile() {
        return (RemoteFile) get(Current.RemoteFile);
    }
    public static Makefile getMakefile() {
        return (Makefile) get(Current.Makefile);
    }
    public static Module getModule() {
        return (Module) get(Current.Module);
    }
    public static RunConfiguration getRunConfiguration() {
        return (RunConfiguration) get(Current.RunConfiguration);
    }
    public static Scenario getScenario() {
        return (Scenario) get(Current.Scenario);
    }
    public static ScenarioCommand getScenarioCommand() {
        return (ScenarioCommand) get(Current.ScenarioCommand);
    }
    public static Repository.Component.Sapfor.Sapfor getSapfor() {
        return (Repository.Component.Sapfor.Sapfor) get(Current.Sapfor);
    }
    public static boolean HasGroup() {
        return get(Current.Group) != null;
    }
    public static TestingSystem.Group.Group getGroup() {
        return (TestingSystem.Group.Group) get(Current.Group);
    }

    public static Test getTest() {
        return (TestingSystem.Test.Test) get(Current.Test);
    }
    public static boolean HasTest() {
        return get(Current.Test) != null;
    }

    public static boolean HasVersion() {
        return get(Current.Version) != null;
    }

    public static TestCompilationTask getTestCompilationTask() {
        return (TestingSystem.Tasks.TestCompilationTask) get(Current.TestCompilationTask);
    }
    public static boolean HasTestCompilationTask() {
        return get(Current.TestCompilationTask) != null;
    }
    public static boolean HasTestRunTask() {
        return get(Current.TestRunTask) != null;
    }

    public static TestRunTask getTestRunTask() {
        return (TestingSystem.Tasks.TestRunTask) get(Current.TestRunTask);
    }
    //-
    public static DefaultMutableTreeNode getProjectNode() {
        return (DefaultMutableTreeNode) get(Current.ProjectNode);
    }
    public static DefaultMutableTreeNode getProjectCurrentParentNode() {
        DefaultMutableTreeNode node = Current.getProjectNode();
        //если в дереве еще никто не выделялся, берем корень.
        if (node == null)
            return Current.getProject().filesTreeRoot;
        return (node.getUserObject() instanceof DBProjectFile) ? (DefaultMutableTreeNode) node.getParent() : node;
    }
    public static File getSelectedDirectory() {
        return (File) get(Current.SelectedDirectory);
    }
    public static DBProjectFile getSelectedFile() {
        return (DBProjectFile) get(Current.SelectedFile);
    }
    //-
    public static boolean HasBugReport() {
        return get(Current.BugReport) != null;
    }
    public static PassForm getPassForm() {
        return (Common.Passes.UI.PassForm) get(Current.PassForm);
    }
    public static VisualiserTheme getTheme() {
        return (VisualiserTheme) get(Current.Theme);
    }
    //--------------------------------------------------------------------------------
    public static ParallelRegion getParallelRegion(){
        return (ParallelRegion) get(Current.ParallelRegion);
    }
    public static boolean HasParallelRegion(){
        return get(Current.ParallelRegion)!=null;
    }

    public static boolean HasFunction() {
        return get(Current.Function) != null;
    }
    public static FuncInfo getFunction() {return (FuncInfo) get(Current.Function);  }
    //--------------------------------------------------------------------------------
    public static boolean Check(TextLog Log, Current... names) {
        for (Current name : names)
            if (get(name) == null)
                Log.Writeln_(name.getDescription() + " не выбран(а)");
        return Log.isEmpty();
    }
    public static void CreateAll() {
        for (Current c : values())
            objects.put(c, null);
    }
    //-----------------------------------------
    public static Object get(Current name) {
        return objects.get(name);
    }
    public static Object set(Current name, Object object) {
        // System.out.println("set current "+name+" = "+object);
        objects.replace(name, object);
        return object;
    }
    //применять только для наследников iDBObject
    public static boolean CheckID(Current name, int id) {
        return (get(name) != null) && (((iDBObject) get(name)).id == id);
    }
    //--------------------------------------------
    public String getDescription() {
        switch (this) {
            case Function:
                return "Функция";
            case TestRunTask:
                return "Задача на запуск теста";
            case TestCompilationTask:
                return "Задача на компиляцию теста";
            case DVMParameterValue:
                return "Параметр DVM системы";
            case DialogCompiler:
                return "Компилятор в диалоговом окне";
            case ParallelRegion:
                return "Область распараллеливания";
            case Group:
                return "Группа тестов";
            case Scenario:
                return "Сценарий";
            case ScenarioCommand:
                return "Команда сценария";
            case ProjectNode:
                return "текущий узел дерева проектов"; //служебка
            case Test:
                return "Тест";
            case Sapfor:
                return "SAPFOR";
            case Theme:
                return "Тема";
            case EnvironmentValue:
                return "Значение переменной окружения";
            case SelectedDirectory:
                return "Папка проекта";
            case SelectedFile:
                return "Файл проекта";
            case RunConfiguration:
                return "Конфигурация запуска";
            case RunTask:
                return "Задача на запуск";
            case CompilationTask:
                return "Задача на компиляцию";
            case Makefile:
                return "Мейкфайл";
            case Module:
                return "Языковой модуль мейкфайла";
            case RemoteFile:
                return "Удалённый файл";
            case Component:
                return "Компонент";
            case Project:
                return"Проект";
            case File:
                return"Файл";
            case Root:
                return "Корень дерева версий";
            case Version:
                return "Версия";
           case BugReport:
                return "Отчёт об ошибке";
            case Recipient:
                return "Адресат";
            case Account:
                return "Аккаунт";
            case Machine:
                return "Машина";
            case User:
                return "Пользователь";
            case Compiler:
                return "Компилятор";
                default:
                return "";
        }
    }
}
