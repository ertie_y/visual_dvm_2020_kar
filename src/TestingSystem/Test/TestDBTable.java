package TestingSystem.Test;

import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.Module.Module;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import ProjectData.LanguageName;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Test.UI.TestFields;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.UI.Interface.FilterWindow;
import Visual_DVM_2021.UI.Main.TestsFilterForm;

import java.util.LinkedHashMap;

import static Common.UI.Tables.TableRenderers.RendererDate;

public class TestDBTable extends DBTable<String, Test> {
    public TestDBTable() {
        super(String.class, Test.class);
    }
    @Override
    public String getDataDescription() {
        return "тест";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            protected void AdditionalInitColumns() {
                columns.get(3).setRenderer(RendererDate);
            }
        };
    }
    @Override
    public Object getFieldAt(Test object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.sender_name;
            case 2:
                return object.dim;
            case 3:
                return object.getDate();
            default:
                return null;
        }
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{"отправитель","размерность" ,"дата создания"};
    }
    @Override
    public Current CurrentName() {
        return Current.Test;
    }
    @Override
    public DBObjectDialog<Test, TestFields> getDialog() {
        return new DBObjectDialog<Test, TestFields>(TestFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }

            @Override
            public void validateFields() {
                if (!Current.getGroup().language.equals(Current.getProject().languageName))
                    Log.Writeln_("В текущую группу могут войти только тесты на языке "+Current.getGroup().language);
                if (TestingSystem.db.tests.containsKey(fields.tfName))
                    Log.Writeln_("Уже существует тест с именем " + Utils.Brackets(fields.tfName));
            }
            @Override
            public void fillFields() {
                fields.tfName.setText(Result.name);
            }
            @Override
            public void ProcessResult() {
                Result.name = fields.tfName.getText();
            }
            @Override
            public void SetEditLimits() {
                fields.tfName.setEditable(false);
            }
        };
    }
    @Override
    public FilterWindow CreateFilterUI() {
        return new TestsFilterForm();
    }

    /*
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(TestCompilationTask.class, new FKBehaviour(FKDataBehaviour.NONE, FKCurrentObjectBehaviuor.PASSIVE));
        return res;
    }
     */
}