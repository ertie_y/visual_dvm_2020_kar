package TestingSystem.Test;

public enum TestType {
    Default,
    Correctness,
    Performance
}
