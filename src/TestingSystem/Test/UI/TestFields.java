package TestingSystem.Test.UI;

import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import ProjectData.LanguageName;
import TestingSystem.Test.TestType;

import javax.swing.*;
import java.awt.*;
public class TestFields implements DialogFields {
    private JPanel content;
    public JTextField tfName;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName=new StyledTextField();
    }
}
