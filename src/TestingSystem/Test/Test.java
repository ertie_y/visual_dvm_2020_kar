package TestingSystem.Test;

import Common.Current;
import Common.Database.DBObject;
import Common.Global;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import com.sun.org.glassfish.gmbal.Description;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;

public class Test extends DBObject {

    @Description("PRIMARY KEY, UNIQUE")
    public String name = "";
    @Description("DEFAULT 1")
    public int dim = 1; //размерность теста. для удобства пусть будет и внешним полем.
    //-
    @Description("DEFAULT ''")
    public String group_name = "";
    //-
    public String sender_name = "";
    public String sender_address = "";
    @Description("DEFAULT 0")
    public long date = 0;
    //-
    @Description("IGNORE")
    public byte [] project_archive_bytes = null;

    public Date getDate() {
        return new Date(date);
    }
    public void CopyFields(Test test_in){
    }
    @Override
    public Object getPK() {
        return name;
    }

    public void init(db_project_info src) {
    }

    public File getArchive() {
        return Paths.get(System.getProperty("user.dir"), "Tests", name + ".zip").toFile();
    }

    public File getServerPath() {
        return Paths.get(System.getProperty("user.dir"), "Tests", name).toFile();
    }

    public File getHomePath() {
        return Paths.get(Global.visualiser.getWorkspace().getAbsolutePath(), name).toFile();
    }
    //--
    public static String filterName="";
    public static String filterSenderName="";
    public static boolean filterMyOnly=false;

    @Override
    public boolean isVisible() {
        return Current.HasGroup()&&(Current.getGroup().name.equals(group_name))
                &&
                name.toUpperCase().contains(filterName.toUpperCase())&&
                sender_name.toUpperCase().contains(filterSenderName.toUpperCase())&&
                (!filterMyOnly || sender_address.equalsIgnoreCase(Current.getAccount().email));
    }
    @Override
    public String getFKName() {
        return "test_name";
    }
    @Override
    public Object getEmptyFK() {
        return "";
    }
}
