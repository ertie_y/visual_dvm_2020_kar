package TestingSystem;

import GlobalData.Tasks.TaskState;
import TestingSystem.Tasks.Supervisor.SupervisorPlot;
import TestingSystem.Tasks.Supervisor.TestCompilationSupervisor;
import TestingSystem.Tasks.Supervisor.TestRunSupervisor;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;

import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Vector;

public class TestingSystem {
    public static TestingDatabase db = new TestingDatabase();
    public static TestCompilationSupervisor compilationSupervisor = new TestCompilationSupervisor();
    public static TestRunSupervisor runSupervisor = new TestRunSupervisor();

    public static void ActivateDB() throws Exception {
        db.Connect();
        db.CreateAllTables();
        db.Synchronize();
    }

    public static void PerformTasks() throws Exception {
        LinkedHashMap<String, Vector<Integer>> compilationTasks = new LinkedHashMap<>();
        int ctcount = 0;
        int rtcount = 0;
        for (TestCompilationTask task : db.testCompilationTasks.Data.values()) {
            if (task.state.equals(TaskState.Waiting)||task.state.equals(TaskState.Running)) {
                if (!compilationTasks.containsKey(task.test_name))
                    compilationTasks.put(task.test_name, new Vector<>());
                Vector<Integer> ids = compilationTasks.get(task.test_name);
                if (!ids.contains(task.id)) {
                    ids.add(task.id);
                    ctcount++;
                }
            }
        }
        //---
        System.out.println(ctcount+" active compilation tasks ");
        //------------------------------------------------------------------->>>
        for (String testName : compilationTasks.keySet()) {
            Vector<Integer> ids = compilationTasks.get(testName);
            ids.sort(Comparator.comparingInt(o->o));
            for (int id: ids){
                TestCompilationTask task = TestingSystem.db.testCompilationTasks.get(id);
                switch (task.state){
                    case Running:
                        compilationSupervisor.Do(task, SupervisorPlot.CheckTask);
                        break;
                    case Waiting:
                        compilationSupervisor.Do(task, SupervisorPlot.StartTask);
                        break;
                }
            }
        }

        LinkedHashMap<String, Vector<Integer>> runTasks = new LinkedHashMap<>();
        for (TestRunTask task : db.testRunTasks.Data.values()) {
            if (task.state.equals(TaskState.Waiting)||task.state.equals(TaskState.Running)) {
                if (!runTasks.containsKey(task.test_name))
                    runTasks.put(task.test_name, new Vector<>());
                Vector<Integer> ids = runTasks.get(task.test_name);
                if (!ids.contains(task.id)) {
                    ids.add(task.id);
                    rtcount++;
                }
            }
        }
        System.out.println(rtcount+" active run tasks ");
        for (String testName : runTasks.keySet()) {
            Vector<Integer> ids = runTasks.get(testName);
            ids.sort(Comparator.comparingInt(o->o));
            for (int id: ids){
                TestRunTask task = TestingSystem.db.testRunTasks.get(id);
                if (task.getTestCompilationTask().state==TaskState.Done) {
                    switch (task.state) {
                        case Running:
                            runSupervisor.Do(task, SupervisorPlot.CheckTask);
                            break;
                        case Waiting:
                            runSupervisor.Do(task, SupervisorPlot.StartTask);
                            break;
                    }
                }
            }
        }
    }
}
