package TestingSystem;

import Common.Database.SQLITE.SQLiteDatabase;
import Common.Global;
import TestingSystem.Group.GroupsDBTable;
import TestingSystem.Tasks.TaskCounterDBTable;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestCompilationTasksDBTable;
import TestingSystem.Tasks.TestRunTasksDBTable;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestDBTable;
import Visual_DVM_2021.Properties.PropertyName;

import java.nio.file.Paths;
import java.util.Vector;

public class TestingDatabase extends SQLiteDatabase {
    //-
    public TestDBTable tests;
    public GroupsDBTable groups;
    public TaskCounterDBTable taskCounter;
    public TestCompilationTasksDBTable testCompilationTasks;
    public TestRunTasksDBTable testRunTasks;
    //-
    public Vector<String> groupsNames =new Vector<>();
    public TestingDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", Global.properties.get(PropertyName.TestsDBName)).toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(groups = new GroupsDBTable());
        addTable(tests = new TestDBTable());
        addTable(taskCounter = new TaskCounterDBTable());
        addTable(testCompilationTasks = new TestCompilationTasksDBTable());
        addTable(testRunTasks = new TestRunTasksDBTable());
    }

    @Override
    public void Init() throws Exception {
    }
    public void CheckGroup(String group_in){
        if (!groupsNames.contains(group_in))
            groupsNames.add(group_in);
    }

}
