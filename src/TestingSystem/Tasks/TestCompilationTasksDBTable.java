package TestingSystem.Tasks;

import Common.Current;
import Common.Database.*;

public class TestCompilationTasksDBTable extends DBTable<Integer,TestCompilationTask> {
    public TestCompilationTasksDBTable() {   super( Integer.class, TestCompilationTask.class);
    }

    @Override
    public String getDataDescription() {
        return "задачи на компиляцию тестов";
    }
    @Override
    public Current CurrentName() {
        return Current.TestCompilationTask;
    }

}
