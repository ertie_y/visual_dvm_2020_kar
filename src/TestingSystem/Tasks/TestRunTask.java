package TestingSystem.Tasks;

import Common.Current;
import Common.Global;
import Common.Utils.StringTemplate;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.Machine;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.TaskState;
import GlobalData.User.User;
import ProjectData.LanguageName;
import ProjectData.Project.db_project_info;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import com.sun.org.glassfish.gmbal.Description;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Vector;

public class TestRunTask extends TestTask{
    //------------------------------------------>>>>>>>
    public int testcompilationtask_id= Utils.Nan;
    public String matrix = "";
    public double CleanTime=0.0;
    @Description("DEFAULT '0'")
    public int progress = 0; //прогресс выполнения в процентах.
    //------------------------------------------>>>>>>>
    //RunTask фактически становится StatisticRecord и имеет всю инфу
    //как о компиляции, так и о запуске теста, причем в текстовом виде.
    //-----------
    public LanguageName language = LanguageName.fortran; //язык линковки тестов.
    public String link_flags = "";
    public String fortran_flags = "";
    public String c_flags = "";
    //-запуск---------------
    public int cube = 1;
    public int min_dim = 1;
    public int max_dim = 1;
    public int r_maxtime = 120;
    public String environments = "";
    public String usr_par = "";
    //-----------
    public int compilation_maxtime = 40;
    public String compilation_output="";
    public String compilation_errors="";
    public TaskState compilation_state = TaskState.Waiting;
    public double compilation_time=0.0;
    //------------
    @Override
    public File getHome() {
        return Global.RunTasksDirectory;
    }
    public TestCompilationTask getTestCompilationTask() {
        return TestingSystem.db.getByPK(TestCompilationTask.class, testcompilationtask_id, Utils.Nan);
    }
    @Override
    public String getFullCommand() {
        return getGroup().getLaunchScriptText(getTestCompilationTask().binary_name, matrix, dvm_drv);
    }
    public TestRunTask(Machine machine, User user, Compiler compiler, Group group, Test test, String dvm_drv_in, String matrix_in) {
        super(machine, user, compiler, group, test);
        //--------------------------
        //инфа о компиляции.
        language = group.language;
        link_flags = group.link_flags;
        fortran_flags = group.fortran_flags;
        compilation_maxtime = group.c_maxtime;
        compilation_output="";
        compilation_errors="";
        compilation_state = TaskState.Waiting;
        //инфа о запуске
        cube = group.cube;
        min_dim = group.max_dim;
        max_dim = group.max_dim;
        r_maxtime = group.r_maxtime;
        environments = group.environments;
        usr_par = group.usr_par;
        //---------
        maxtime = group.r_maxtime;
        dvm_drv=dvm_drv_in;
        matrix = matrix_in;
        state = TaskState.Waiting;
    }
    public TestRunTask(){}

    public void Refresh(TestRunTask task_in) {
        binary_name = task_in.binary_name;
        Time = task_in.Time; //время выполнения.
        StartDate = task_in.StartDate; //дата начала выполнения
        EndDate = task_in.EndDate;//дата окончания выполнения
        output = task_in.output;
        errors = task_in.errors;
        progress = task_in.progress;
        state = task_in.state;
        CleanTime=task_in.CleanTime;
        compilation_output=task_in.compilation_output;
        compilation_errors=task_in.compilation_errors;
        compilation_state =task_in.compilation_state;
        compilation_time=task_in.compilation_time;
    }
}
