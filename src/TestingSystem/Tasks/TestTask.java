package TestingSystem.Tasks;

import Common.Database.DBObject;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachineType;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.Tasks.TaskState;
import GlobalData.User.User;
import ProjectData.Project.db_project_info;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestType;
import TestingSystem.TestingSystem;
import com.sun.org.glassfish.gmbal.Description;

import java.io.File;
import java.nio.file.Paths;
import java.util.Date;

//тут все поля должны быть текстовыми. никаких ссылок по ид. мало ли, группу удалят
public abstract class TestTask extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public int id = Utils.Nan;

    @Override
    public Object getPK() {
        return id;
    }

    //<editor-fold desc="файловые константы">
    public final static String DONE = "DONE";
    public final static String TIMEOUT = "TIMEOUT";
    public final static String out_file = "out.txt";
    public final static String err_file = "err.txt";
    public final static String time_file = "total_time";
    //</editor-fold>
    @Description("DEFAULT ''")
    public String group_name = "";
    //-
    @Description("DEFAULT ''")
    public String test_name = "";
    //-
    @Description("DEFAULT ''")
    public String dvm_drv = "";
    //-
    @Description("DEFAULT ''")
    public String dvm_version = "?";
    @Description("DEFAULT ''")
    public String sapfor_version = "?";
    //-
    @Description("DEFAULT 'Inactive'")
    public TaskState state = TaskState.Inactive;
    //----------------------------------
    @Description("DEFAULT ''")
    public String machine_name = "";
    @Description("DEFAULT ''")
    public String machine_address = "";
    @Description("DEFAULT Server")
    public MachineType machine_type = MachineType.Server;
    @Description("DEFAULT 22")
    public int machine_port = 22;

    @Description("DEFAULT ''")
    public String user_name = "";
    @Description("DEFAULT ''")
    public String user_password = "";
    @Description("DEFAULT ''")
    public String user_workspace = "";
    //-----------------------------------
    @Description("DEFAULT ''")
    public String PID = "";
    //-
    @Description("DEFAULT 40")
    public int maxtime = 40;
    //-----
    @Description("DEFAULT ''")
    public String remote_workspace = "";
    @Description("DEFAULT ''")
    public String binary_name = ""; //исполняемый файл.

    @Description("DEFAULT 'Default'")
    public TestType test_type =TestType.Default;

    public RemoteFile getBinary() {
        return new RemoteFile(getRemoteWorkspace().full_name, binary_name);
    }

    public RemoteFile getRemoteWorkspace() {
        RemoteFile res = new RemoteFile(user_workspace + "/tests", String.valueOf(id), true);
        remote_workspace = res.full_name;
        return res;
    }

    //результаты-------------------------------
    public double Time; //время выполнения.
    public long StartDate = 0; //дата начала выполнения
    public long EndDate = 0;//дата окончания выполнения
    @Description("DEFAULT ''")
    public String output = "";
    @Description("DEFAULT ''")
    public String errors = "";

    //---
    public Group getGroup() {
        return TestingSystem.db.getByPK(Group.class, group_name, "");
    }

    public Test getTest() {
        return TestingSystem.db.getByPK(Test.class, test_name, "");
    }

    //-
    public abstract File getHome();

    public File getLocalWorkspace() {
        return Paths.get(getHome().getAbsolutePath(), String.valueOf(id)).toFile();
    }

    public File getOutputFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), out_file).toFile();
    }

    public File getErrorsFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), err_file).toFile();
    }

    public File getTimeFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), time_file).toFile();
    }

    public abstract String getFullCommand();

    public Date getEndDate() {
        return new Date(EndDate);
    }

    protected String getTextResult(File file) {
        return (file.exists()) ? Utils.ReadAllText(file) : "файл не найден. Задача еще не выполнялась или была завершена некорректно";
    }

    //подразумевается, что выходные потоки задачи видны только при открытом проекте
    public String getOutput() {
        return getTextResult(getOutputFile());
    }

    public String getErrors() {
        return getTextResult(getErrorsFile());
    }

    public void RefreshTime() {
        Time = Double.parseDouble(Utils.ReadAllText(getTimeFile()));
    }

    public void MaximizeTime() {
        Time = maxtime + 1;
    }

    public void UpdateMaxtime(int maxtime_in) {
        if (maxtime != maxtime_in) {
            maxtime = maxtime_in;
            try {
                Global.db.Update(this, "maxtime");
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    }

    public void UpdateState(TaskState state_in) {
        if (state != state_in) {
            state = state_in;
            try {
                TestingSystem.db.Update(this, "state");
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    }

    public boolean isPassive() {
        return (state != TaskState.Queued) && (state != TaskState.Running);
    }

    public boolean isActive() {
        return state == TaskState.Running;
    }


    public TestTask() {
    }

    public TestTask(Machine machine, User user, Compiler compiler, Group group, Test test) {
        group_name = group.name;
        test_name = test.name;
        test_type = group.type;
        //-
        machine_name = machine.name;
        machine_type = machine.type;
        machine_address = machine.address;
        machine_port = machine.port;
        //-
        user_name = user.login;
        user_password = user.password;
        user_workspace = user.workspace;
        //-
        dvm_drv = compiler.call_command;
        //-
    }

    public boolean isComplete() {
        return state == TaskState.Done
                || state == TaskState.DoneWithErrors
                || state == TaskState.AbortedByTimeout
                || state == TaskState.AbortedByUser
                || state == TaskState.Crushed;
    }
}
