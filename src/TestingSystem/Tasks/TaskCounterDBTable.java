package TestingSystem.Tasks;

import Common.Database.iDBTable;
import Common.Utils.Utils;

public class TaskCounterDBTable extends iDBTable<TaskCounter> {
    public int max = Utils.Nan;
    public TaskCounterDBTable() {
        super(TaskCounter.class);
    }
    public int Inc() throws Exception{
        TaskCounter res = new TaskCounter();
        getDb().Insert(res);
        return res.id;
    }
}
