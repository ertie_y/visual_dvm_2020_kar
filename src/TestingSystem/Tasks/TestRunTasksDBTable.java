package TestingSystem.Tasks;

import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;

import static Common.UI.Tables.TableRenderers.*;

public class TestRunTasksDBTable  extends DBTable<Integer, TestRunTask> {
    public TestRunTasksDBTable() {
        super(Integer.class,TestRunTask.class);
    }

    @Override
    public String getDataDescription() {
        return "задачи на запуск тестов";
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(10).setRenderer(RendererStatusEnum);
                columns.get(15).setRenderer(RendererDate);
                columns.get(16).setRenderer(RendererProgress);
                columns.get(17).setRenderer(RendererStatusEnum);
            }
        };
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Группа",
                "Тест",
                "SAPFOR",
                "DVM",

                "Язык",
                "Link",
                "Fortran",
                "С",
                "Компиляция",
                "Время(с)",

                "Матрица",

                "Время (с)",
                "Чистое время (с)",
                "Окончание",
                "Прогресс",
                "Статус"
        };
    }

    @Override
    public Object getFieldAt(TestRunTask object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.group_name;
            case 3:
                return object.test_name;
            case 4:
                return object.sapfor_version;
            case 5:
                return object.dvm_version;
            //---------------
            case 6:
                return object.language;
            case 7:
                return object.link_flags;
            case 8:
                return object.fortran_flags;
            case 9:
                return object.c_flags;
            case 10:
                return object.compilation_state;
            case 11:
                return object.compilation_time;
            //------------------
            case 12:
                return object.matrix;
            case 13:
                return object.Time;
            case 14:
                return object.CleanTime;
            case 15:
                return object.getEndDate();
            case 16:
                return object.progress;
            case 17:
                return object.state;
            default:
                return null;
        }
    }

    @Override
    public Current CurrentName() {
        return Current.TestRunTask;
    }

}
