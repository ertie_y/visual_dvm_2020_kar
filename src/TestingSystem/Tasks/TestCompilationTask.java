package TestingSystem.Tasks;
import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Machine.Machine;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.User.User;
import TestingSystem.Group.Group;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import com.sun.org.glassfish.gmbal.Description;

import java.io.File;
import java.util.Vector;
import java.util.stream.Collectors;

//-
public class TestCompilationTask extends TestTask {
    //-
    @Description("IGNORE")
    public Vector<TestRunTask> runTasks = null; //заполняется только при запуске.
    //-
    @Override
    public File getHome() {
        return Global.CompilationTasksDirectory;
    }
    //-
    @Override
    public String getFullCommand() {
        return "make -j -f Makefile";
    }

    public TestCompilationTask(){

    }

    //todo сделать стат. метод. общий и для конфигов и групп. чтобы не дублировать.
    public static Vector<String> getMatrixes(int dim, int minMatrix, int maxMatrix, boolean cube){
        Vector<Vector<Integer>> res_ = new Vector<>();
        Vector<String> res = new Vector<>();
        if (dim > 0) {
            Vector<String> min_border=new Vector<>();
            Vector<String>  max_border=new Vector<>();

            for (int i=1; i<=dim; ++i){
                min_border.add(String.valueOf(minMatrix));
                max_border.add(String.valueOf(maxMatrix));
            }

            Vector<Integer> from = RunConfiguration.getBounds(String.join(" ",min_border));
            Vector<Integer> to = RunConfiguration.getBounds(String.join(" ",max_border));

            if (from.size() != to.size()){
                System.out.println("Верхняя и нижняя границы матриц конфигурации имеют разные размерности");
                return res;
            }
            if (from.size() != dim){
                System.out.println("Границы матриц не совпадают с размерностью конфигурации");
                return res;
            }
            //1 стадия. заполнение.
            for (int j = from.get(0); j <= to.get(0); ++j) {
                Vector<Integer> m = new Vector<>();
                res_.add(m);
                m.add(j);
            }
            //---
            if (dim > 1)
                RunConfiguration.gen_rec(from, to, res_, 1, dim, cube);

            for (Vector<Integer> m : res_) {

                Vector<String> ms = new Vector<>();
                for (int i : m)
                    ms.add(String.valueOf(i));
                res.add(String.join(" ", ms));
            }
            System.out.println("=======================");
            System.out.println(res.size());
            for (String ms : res) {
                System.out.println(Utils.Brackets(ms));
            }
            System.out.println("=======================");

        } else
            res.add("");
        return res;
    }
    public TestCompilationTask(Machine machine, User user, Compiler compiler, Group group, Test test){
        super(machine, user, compiler, group, test);
        maxtime = group.c_maxtime;
        runTasks = new Vector<>();
        //получить все матрицы от группы и теста.

        Vector<String> matrixes = getMatrixes(test.dim, group.min_dim, group.max_dim, group.isCube());
        System.out.println(matrixes.size());
        for (String matrix: matrixes) {
            System.out.println(matrix);
            runTasks.add(new TestRunTask(machine, user, compiler, group, test, dvm_drv, matrix));
        }
    }
    public Vector<TestRunTask> getRunTasks(){
        return TestingSystem.db.testRunTasks.Data.values().stream().filter(rt -> rt.testcompilationtask_id == id).collect(Collectors.toCollection(Vector::new));
    }
}
