package TestingSystem.Tasks.Supervisor;

import Common.Global;
import Common.Utils.StringTemplate;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.TaskState;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;

import java.util.*;

public class TestRunSupervisor extends TestSupervisor<TestRunTask> {
    TestCompilationTask compilationTask = null;

    @Override
    public String getDescription() {
        return "Запуск тестов";
    }

    @Override
    protected void PrepareTaskRemoteWorkspace() throws Exception {
        compilationTask = task.getTestCompilationTask();
        copy(compilationTask.getBinary(), task.getBinary());
    }
/*
    @Override
    public void AnalyzeOutput(db_project_info project) throws Exception {
        String[] lines_ = output.split("\n");
        Vector<String> lines = new Vector<>(Arrays.asList(lines_));
        for (String line: lines){
            if (RunTask.isCrushed(line)){
                state = TaskState.Crushed;
                return;
            }
        }
        //->>
        //тест на корректность.
        if (!lines.isEmpty() && lines.get(0).toUpperCase().contains("START OF ")) {
            int complete = 0;
            int errors = 0;
            for (int i = 1; i < lines.size(); ++i) {
                String line_ = lines.get(i).toUpperCase();
                if (line_.endsWith("COMPLETE")) {
                    complete++;
                } else if (line_.endsWith("ERROR")) {
                    errors++;
                } else if (line_.contains("END OF")) {
                    if (errors > 0) state = TaskState.DoneWithErrors;
                    else if (complete > 0) state = TaskState.Done;
                    return;
                }
            }
        }
        //тест на производительность.
        StringTemplate stringTemplate = new StringTemplate("Verification =", "");
        for (String line : lines) {
            String param = stringTemplate.check_and_get_param(line);
            if (param != null) {
                switch (param) {
                    case "SUCCESSFUL":
                        state = TaskState.Done;
                        return;
                    case "UNSUCCESSFUL":
                        state = TaskState.DoneWithErrors;
                        return;
                    default:
                        break;
                }
            }
        }
    }
    @Override
    public void AnalyzeErrors(db_project_info project) throws Exception {
        List<String> lines = FileUtils.readLines(getErrorsFile());
        if (lines.stream().anyMatch( RunTask::isCrushed))
            state = TaskState.Crushed;
    }
* */

    @Override
    protected void AnalyseTaskResults() throws Exception {
        List<String> output_lines = Arrays.asList(task.output.split("\n"));
        List<String> errors_lines = Arrays.asList(task.errors.split("\n"));
        //---
        if (output_lines.stream().anyMatch(RunTask::isCrushedLine) ||
            errors_lines.stream().anyMatch(RunTask::isCrushedLine)) {
            task.state = TaskState.Crushed;
            return;
        }

        /*
        switch (task.test_type){
            case Correctness:
                break;
            case Performance:
                break;
            default:
                task.progress=100;
                task.state = TaskState.Done;
                break;
        }

         */
        task.state = TaskState.Done;
        System.out.println(task.id + " progress = " + task.progress);
        StringTemplate template = new StringTemplate("Time in seconds =", "");
        String p = template.check_and_get_param(task.output);
        try {
            if (p != null) task.CleanTime = Double.parseDouble(p);
        } catch (Exception ex) {
            Global.Log.PrintException(ex);
        }
    }
}
