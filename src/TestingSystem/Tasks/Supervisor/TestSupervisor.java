package TestingSystem.Tasks.Supervisor;

import Common.Utils.Utils;
import GlobalData.Machine.Machine;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.Tasks.Task;
import GlobalData.Tasks.TaskState;
import GlobalData.User.User;
import ProjectData.Project.db_project_info;
import TestingSystem.Group.Group;
import TestingSystem.Tasks.TestTask;
import TestingSystem.Test.Test;
import TestingSystem.TestingSystem;
import Visual_DVM_2021.Passes.SSH.ConnectionPass;
import org.apache.commons.io.FileUtils;

import java.util.Date;

public abstract class TestSupervisor<T extends TestTask> extends ConnectionPass<T> {
    //здесь не нужно разделять супервизор и проход, ибо выполнение всегда удаленное.
    public T task; //задача
    public SupervisorPlot plot;
    protected Group group;
    protected Test test;
    protected db_project_info project; //тест. проект к которому относится задача
    protected RemoteFile remoteWorkspace = null; //Раб пространство текущей зедачи

    //------------------------------------------------------------------------------------
    protected RemoteFile getBinary() {
        return new RemoteFile(remoteWorkspace.full_name, "0");
    }

    protected RemoteFile getRemoteTime() {
        return new RemoteFile(remoteWorkspace.full_name, Task.time_file);
    }

    protected RemoteFile getRemoteOutput() {
        return new RemoteFile(remoteWorkspace.full_name, Task.out_file);
    }

    protected RemoteFile getRemoteErrors() {
        return new RemoteFile(remoteWorkspace.full_name, Task.err_file);
    }

    public RemoteFile getTestsHome() {
        return new RemoteFile(user.workspace, tests, true);
    }

    @Override
    protected boolean canStart(Object... args) throws Exception {
        //---
        task = (T) args[0];
        plot = (SupervisorPlot) args[1];
        //---
        machine = new Machine(task.machine_name, task.machine_address, task.machine_port, task.machine_type);
        user = new User(task.user_name, task.user_password);
        user.workspace = task.user_workspace;
        group = TestingSystem.db.groups.get(task.group_name);
        test = TestingSystem.db.tests.get(task.test_name);
        project = new db_project_info(test.getServerPath());
        //загрузка всей инфы о тесте.
        project.Open();
        project.Close();
        return true;
    }

    @Override
    protected void ServerAction() throws Exception {
        tryMKDir(getTestsHome());
        remoteWorkspace = task.getRemoteWorkspace();
        System.out.print(task.getClass() + " " + task.id);
        switch (plot) {
            case StartTask:
                System.out.println(" starts...");
                //локальная папка задачи на сервере
                Utils.CheckAndCleanDirectory(task.getLocalWorkspace());
                //удаленная папка задачи на целевой машине
                tryMKDir(remoteWorkspace);
                PrepareTaskRemoteWorkspace();
                //запуск задачи.
                StartTask();
                break;
            case CheckTask:
                System.out.println(" checking...");
                CheckTask();
                break;
            default:
                System.out.println(" ?");
                break;
        }
        TestingSystem.db.Update(task);
        System.out.println("состояние = " + task.state);
    }

    protected void PrepareTaskRemoteWorkspace() throws Exception {
    }

    protected void StartTask() throws Exception {
        task.PID = ShellCommand(
                "cd " + Utils.DQuotes(remoteWorkspace.full_name),
                String.join(" ", Utils.DQuotes(getStarter()), Utils.DQuotes(getLauncher()), String.valueOf(task.maxtime), task.getFullCommand())
        );
        task.StartDate = new Date().getTime();
        task.state = TaskState.Running;
    }

    protected void AnalyseTaskResults() throws Exception {
    }

    protected void CheckTask() throws Exception {
        if (Exists(remoteWorkspace.full_name, Task.DONE)) {
            task.EndDate = new Date().getTime();
            task.state = TaskState.Finished;
            tryGetSingleFile(getRemoteOutput(), task.getOutputFile(), 10240);
            tryGetSingleFile(getRemoteErrors(), task.getErrorsFile(), 10240);
            if (tryGetSingleFile(getRemoteTime(), task.getTimeFile(), 0))
                task.RefreshTime();
            task.output = FileUtils.readFileToString(task.getOutputFile());
            task.errors = FileUtils.readFileToString(task.getErrorsFile());
            AnalyseTaskResults();
        } else if (Exists(remoteWorkspace.full_name, Task.TIMEOUT)) {
            task.EndDate = new Date().getTime();
            task.state = TaskState.AbortedByTimeout;
        }
        //----------
        if (task.state == TaskState.AbortingByUser) {
            ShellCommand("kill -2 " + task.PID);
            task.EndDate = new Date().getTime();
            task.state = TaskState.AbortedByUser;
        }
    }
}
