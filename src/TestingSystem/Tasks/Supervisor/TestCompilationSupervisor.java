package TestingSystem.Tasks.Supervisor;

import Common.Utils.Utils;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.Tasks.TaskState;
import TestingSystem.Tasks.TestCompilationTask;
import TestingSystem.Tasks.TestRunTask;
import TestingSystem.TestingSystem;

import java.io.File;
import java.util.Vector;

public class TestCompilationSupervisor extends TestSupervisor<TestCompilationTask> {
    @Override
    public String getDescription() {
        return "Компиляция тестов";
    }

    @Override
    protected void PrepareTaskRemoteWorkspace() throws Exception {
        SynchronizeSubDirsR(project, project.Home, remoteWorkspace, false);
        tryRM(getBinary());
        if (!task.binary_name.isEmpty()) {
            RemoteFile old_binary = new RemoteFile(remoteWorkspace.full_name, task.binary_name);
            tryRM(old_binary);
        }
        //отправить мейкфайл.
        String makefileText = group.GenerateMakefile(project, task.dvm_drv);
        File localMF = Utils.CreateTempFile("makefile", makefileText);
        RemoteFile remoteMF = new RemoteFile(remoteWorkspace.full_name, "Makefile");
        putSingleFile(localMF, remoteMF);
        //очистить все что связано с gcov
        //файлы gcda, gcno, gcov
        deleteFilesByExtensions(remoteWorkspace, "gcda", "gcno", "gcov");
    }

    @Override
    protected void CheckTask() throws Exception {
        super.CheckTask();
        Vector<TestRunTask> runTasks = task.getRunTasks();
        for (TestRunTask runTask : runTasks) {
            System.out.print(runTask.id);
            runTask.compilation_state = task.state;
            TestingSystem.db.Update(runTask, "compilation_state");
            if (task.isComplete()) {
                runTask.compilation_output = task.output;
                runTask.compilation_errors = task.errors;
                runTask.compilation_time=task.Time;
                TestingSystem.db.Update(runTask, "compilation_output");
                TestingSystem.db.Update(runTask, "compilation_errors");
                TestingSystem.db.Update(runTask, "compilation_time");
            }
        }
    }

    @Override
    protected void AnalyseTaskResults() throws Exception {
        if (Exists(remoteWorkspace.full_name, getBinary().name)) {
            RemoteFile renamed_binary = new RemoteFile(remoteWorkspace.full_name, Utils.getDateName("spf_" + getBinary().name));
            sftpChannel.rename(getBinary().full_name, renamed_binary.full_name);
            task.binary_name = renamed_binary.name;
            task.state = TaskState.Done;
        } else task.state = TaskState.DoneWithErrors;
    }
}
