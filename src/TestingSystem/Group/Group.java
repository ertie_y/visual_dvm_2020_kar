package TestingSystem.Group;

import Common.Database.DBObject;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Module.Module;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;
import ProjectData.Project.db_project_info;
import TestingSystem.Test.TestType;
import com.sun.org.glassfish.gmbal.Description;

import java.util.LinkedHashMap;
import java.util.Vector;

public class Group extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String name;  //имя группы.
    @Description("DEFAULT ''")
    public String description = "";
    @Description("DEFAULT 'Default'")
    public TestType type = TestType.Default;
    //--линковка--------------
    @Description("DEFAULT 'fortran'")
    public LanguageName language = LanguageName.fortran; //язык линковки тестов.
    @Description("DEFAULT ''")
    public String link_flags = "";
    @Description("DEFAULT ''")
    public String fortran_flags = "";
    @Description("DEFAULT ''")
    public String c_flags = "";
    @Description("DEFAULT 40")
    public int c_maxtime = 40;
    //-запуск---------------
    @Description("DEFAULT 1")
    public int cube = 1;
    @Description("DEFAULT 1")
    public int min_dim = 1;
    @Description("DEFAULT 1")
    public int max_dim = 1;
    @Description("DEFAULT 120")
    public int r_maxtime = 120;
    @Description("DEFAULT ''")
    public String environments = "";
    @Description("DEFAULT ''")
    public String usr_par = "";

    //----------------------
    //аргументы командной строки индивидуальные у каждого теста. или же принимает что их нет.
    //------------------------------------------------------------------------------------------------
    @Override
    public Object getPK() {
        return name;
    }

    @Override
    public String getFKName() {
        return "group_name";
    }

    public boolean isCube() {
        return cube == 1;
    }

    //-
    //только для двм.
    public String getStyleOptions(DBProjectFile program) {
        if (program.languageName == LanguageName.fortran) {
            switch (program.style) {
                case fixed:
                case extended:
                    return "-FI";
                case free:
                    return "-f90";
                case none:
                    break;
            }
        }
        return "";
    }
    public String getLanguageCompileCommand(LanguageName language){
        switch (language){
            case fortran:
                return "f";
            case c:
                return "c";
        }
        return "";
    }
    public String getLanguageLinkCommand(LanguageName language){
        switch (language){
            case fortran:
                return "flink";
            case c:
                return "clink";
        }
        return "";
    }
    public String getLanguageFlags(LanguageName language){
        switch (language){
            case fortran:
                return fortran_flags;
            case c:
                return c_flags;
        }
        return "";
    }

    public void generateForLanguage(
            String dvm_drv,
            LinkedHashMap<LanguageName, Vector<DBProjectFile>> programs,
            LanguageName language,
            Vector<String> titles,
            Vector<String> objects,
            Vector<String> bodies) {
        //определить а активен ли модуль.
        //выбран ли он. есть ли у него компилятор.  есть ли для него программы.
        if (!programs.get(language).isEmpty()) {
            //---------------------------------------------------------
            String LANG_ = language.toString().toUpperCase() + "_";
            Vector<String> module_objects = new Vector<>();
            String module_body = "";
            int i = 1;
            for (DBProjectFile program : programs.get(language)) {
                //--
                program.last_assembly_name = language + "_" + i + ".o";
                String object = Utils.DQuotes(program.last_assembly_name);
                module_objects.add(object);
                module_body +=
                        object + ":\n" +
                                "\t" +
                                String.join(" ",
                                        Utils.MFVar(LANG_ + "COMMAND"),
                                        Utils.MFVar(LANG_ + "FLAGS"),
                                        getStyleOptions(program),
                                        "-c",
                                        program.getQSourceName(),
                                        "-o",
                                        object + "\n\n"
                                );
                ++i;
            }

            titles.add(String.join("\n",
                    LANG_ + "COMMAND=" + Utils.DQuotes(dvm_drv) + " " +
                            getLanguageCompileCommand(language),
                    LANG_ + "FLAGS=" + getLanguageFlags(language),
                    LANG_ + "OBJECTS=" + String.join(" ", module_objects),
                    ""
            ));
            objects.add(Utils.MFVar(LANG_ + "OBJECTS"));
            bodies.add(module_body);
        }
    }

    //-
    public String GenerateMakefile(db_project_info project, String dvm_drv) {
        //----->>
        LinkedHashMap<LanguageName, Vector<DBProjectFile>> programs = project.getPrograms();
        Vector<String> titles = new Vector<>();
        Vector<String> objects = new Vector<>();
        Vector<String> bodies = new Vector<>();
        String binary = Utils.DQuotes("0");
        //----->>
        //программы на фортране.
        generateForLanguage(dvm_drv, programs, LanguageName.fortran, titles, objects, bodies);
        //программы на Си
        generateForLanguage(dvm_drv, programs, LanguageName.c, titles, objects, bodies);
        //----->>
        return String.join("\n",
                "LINK_COMMAND=" + Utils.DQuotes(dvm_drv) + " " +
                        getLanguageLinkCommand(language),
                "LINK_FLAGS=" + link_flags + "\n",
                String.join("\n", titles),
                "all: " + binary,
                binary + " : " + String.join(" ", objects),
                "\t" + Utils.MFVar("LINK_COMMAND") + " " + Utils.MFVar("LINK_FLAGS") + " " + String.join(" ", objects) + " -o " + binary,
                String.join(" ", bodies));
    }

    public String getLaunchScriptText(String binary_name, String task_matrix, String dvm_drv) {
        String res = "";
        res += Utils.DQuotes(dvm_drv)+ " run";
        if (!task_matrix.isEmpty())
            res += " " + task_matrix+" ";
        res += Utils.DQuotes("./" + binary_name);
        return res;
    }
}
