package TestingSystem.Group.UI;

import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import ProjectData.LanguageName;
import TestingSystem.Test.TestType;

import javax.swing.*;
import java.awt.*;

public class GroupFields implements DialogFields {
    public JPanel content;
    public JTextField tfName;
    public JTextField tfDescription;
    public JComboBox<TestType> cbType;
    public JComboBox<LanguageName> cbLanguage;
    public JTextField tfLinkFlags;
    public JTextField tfFortranFlags;
    public JTextField tfCFlags;
    public JCheckBox cbCube;
    public JSpinner sCompilationMaxtime;
    public JSpinner sMinDim;
    public JSpinner sMaxDim;
    public JSpinner sRunMaxtime;
    public JScrollPane envScroll;
    public JTextArea taEnvironment;
    public JScrollPane parScroll;
    public JTextArea taPar;

    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName = new StyledTextField();
        tfDescription = new StyledTextField();
        tfLinkFlags= new StyledTextField();
        tfFortranFlags= new StyledTextField();
        tfCFlags=new StyledTextField();
        //-
        cbType = new JComboBox<>();
        cbType.addItem(TestType.Performance);
        cbType.addItem(TestType.Correctness);
        //-
        cbLanguage = new JComboBox<>();
        cbLanguage.addItem(LanguageName.fortran);
        cbLanguage.addItem(LanguageName.c);
        //-
    }
}
