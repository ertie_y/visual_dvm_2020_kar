package TestingSystem.Group;

import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import ProjectData.LanguageName;
import TestingSystem.Group.UI.GroupFields;
import TestingSystem.Test.Test;
import TestingSystem.Test.TestType;
import TestingSystem.TestingSystem;

import java.util.LinkedHashMap;

//-
public class GroupsDBTable extends DBTable<String, Group> {
    public GroupsDBTable() {
        super(String.class, Group.class);
    }

    @Override
    public String getDataDescription() {
        return "группа тестов";
    }

    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(Test.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "описание",
                "тип",
                "язык",
                "сборка",
                "Fortran",
                "C",
                "c_maxtime",
                //-
                "куб",
                "min",
                "max",
                "r_maxtime",
                "окружение",
                "usr.par"
        };
    }

    @Override
    public Object getFieldAt(Group object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.description;
            case 2:
                return object.type;
            case 3:
                return object.language;
            case 4:
                return object.link_flags;
            case 5:
                return object.fortran_flags;
            case 6:
                return object.c_flags;
            case 7:
                return object.c_maxtime;
            case 8:
                return object.cube;
            case 9:
                return object.min_dim;
            case 10:
                return object.max_dim;
            case 11:
                return object.r_maxtime;
            case 12:
                return object.environments;
            case 13:
                return object.usr_par;
            default:
                return null;
        }
    }

    @Override
    public Current CurrentName() {
        return Current.Group;
    }

    @Override
    public DBObjectDialog<Group, GroupFields> getDialog() {
        return new DBObjectDialog<Group, GroupFields>(GroupFields.class) {
            @Override
            public int getDefaultHeight() {
                return 500;
            }

            @Override
            public int getDefaultWidth() {
                return 820;
            }

            @Override
            public void validateFields() {
                if (!edit && TestingSystem.db.groups.Data.containsKey(fields.tfName.getText()))
                    Log.Writeln("Уже существует группа с именем " + Utils.Brackets(fields.tfName.getText()));
            }

            @Override
            public void fillFields() {
                fields.tfName.setText(Result.name);
                fields.tfDescription.setText(Result.description);
                UI.TrySelect(fields.cbType, Result.type);
                //-
                UI.TrySelect(fields.cbLanguage, Result.language);
                fields.tfLinkFlags.setText(Result.link_flags);
                fields.tfFortranFlags.setText(Result.fortran_flags);
                fields.tfCFlags.setText(Result.c_flags);
                fields.sCompilationMaxtime.setValue(Result.c_maxtime);
                //-
                fields.sMinDim.setValue(Result.min_dim);
                fields.sMaxDim.setValue(Result.max_dim);
                fields.taEnvironment.setText(Result.environments);
                fields.taPar.setText(Result.usr_par);
                fields.cbCube.setSelected(Result.isCube());
                fields.sRunMaxtime.setValue(Result.r_maxtime);
            }

            @Override
            public void ProcessResult() {
                Result.name = fields.tfName.getText();
                Result.description = fields.tfDescription.getText();
                Result.type = (TestType) fields.cbType.getSelectedItem();
                //-
                Result.language = (LanguageName) fields.cbLanguage.getSelectedItem();
                Result.link_flags = fields.tfLinkFlags.getText();
                Result.fortran_flags = fields.tfFortranFlags.getText();
                Result.c_flags = fields.tfCFlags.getText();
                Result.c_maxtime = (int) fields.sCompilationMaxtime.getValue();
                //-
                Result.min_dim = (int) fields.sMinDim.getValue();
                Result.max_dim = (int) fields.sMaxDim.getValue();
                Result.environments = fields.taEnvironment.getText();
                Result.usr_par = fields.taPar.getText();
                Result.cube = fields.cbCube.isSelected() ? 1 : 0;
                Result.r_maxtime = (int) fields.sRunMaxtime.getValue();
            }

            @Override
            public void SetEditLimits() {
                fields.tfName.setEditable(false);
            }
        };
    }

}
