package GlobalData.Recipient;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class DBRecipient extends DBObject {
    @Description("PRIMARY KEY,UNIQUE")
    public String Email = "";
    public String Name = "";
    public DBRecipient(String name_in, String email_in) {
        Name = name_in;
        Email = email_in;
    }
    public DBRecipient() {
    }
    @Override
    public Object getPK() {
        return Email;
    }
}
