package GlobalData.Recipient.UI;
import Common.Global;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.Recipient.DBRecipient;
public class DBRecipientForm extends DBObjectDialog<DBRecipient, DBRecipientFields> {
    public DBRecipientForm() {
        super(DBRecipientFields.class);
    }
    @Override
    public int getDefaultHeight() {
        return 150;
    }
    @Override
    public int getDefaultWidth() {
        return 450;
    }
    @Override
    public void validateFields() {
        if (fields.tfName.getText().isEmpty())
            Log.Writeln("Имя пользователя не может быть пустым");
        Utils.validateEmail(fields.tfAddress.getText(), Log);
        if (fields.tfAddress.getText().isEmpty())
            Log.Writeln_("Адрес электронной почты не может быть пустым");
        // не очень красиво. но придется потому что у адресатов адрес это первичный ключ.
        if (!title_text.equals("Регистрация") && (fields.tfAddress.isEditable() && Global.db.recipients.Data.containsKey(fields.tfAddress.getText()))) {
            Log.Writeln_("Адрес электронной почты " + Utils.Brackets(fields.tfAddress.getText()) + " уже есть в списке.");
        }
    }
    @Override
    public void fillFields() {
        fields.tfName.setText(Result.Name);
        fields.tfAddress.setText(Result.Email);
    }
    @Override
    public void SetEditLimits() {
        fields.tfAddress.setEditable(false);
    }
    @Override
    public void ProcessResult() {
        Result.Name = fields.tfName.getText();
        Result.Email = fields.tfAddress.getText();
    }
}
