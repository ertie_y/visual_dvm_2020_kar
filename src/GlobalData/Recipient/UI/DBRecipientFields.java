package GlobalData.Recipient.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class DBRecipientFields implements DialogFields {
    public JPanel content;
    public JTextField tfName;
    public JTextField tfAddress;
    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName=new StyledTextField();
        tfAddress=new StyledTextField();
    }
}
