package GlobalData.Recipient;
import Common.Current;
import Common.Database.DBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Recipient.UI.DBRecipientFields;
import GlobalData.Recipient.UI.DBRecipientForm;
public class RecipientsDBTable extends DBTable<String, DBRecipient> {
    public RecipientsDBTable() {
        super(String.class, DBRecipient.class);
    }
    @Override
    public String getDataDescription() {
        return "адресат";
    }
    @Override
    public DBObjectDialog<DBRecipient, DBRecipientFields> getDialog() {
        return new DBRecipientForm();
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                if (Current.getBugReport() == null) columns.get(1).setVisible(false);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Имя"};
    }
    @Override
    public Object getFieldAt(DBRecipient object, int columnIndex) {
        return object.Name;
    }
    @Override
    public Current CurrentName() {
        return Current.Recipient;
    }
}
