package GlobalData.Makefile;
import Common.Current;
import Common.Database.*;
import Common.Global;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Module.Module;
import GlobalData.Module.UI.ModuleAnchestorFields;
import GlobalData.Module.UI.ModuleAnchestorForm;
import GlobalData.Tasks.CompilationTask.CompilationTask;

import java.util.LinkedHashMap;
public class MakefilesDBTable extends iDBTable<Makefile> {
    public MakefilesDBTable() {
        super(Makefile.class);
    }
    @Override
    public String getDataDescription() {
        return "мейкфайл";
    }
    @Override
    public DBObjectDialog<Makefile, ModuleAnchestorFields> getDialog() {
        return new ModuleAnchestorForm<>();
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(Module.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(CompilationTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Линковщик",
                "Команда",
                "Флаги"
        };
    }
    @Override
    public Object getFieldAt(Makefile object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.getCompilerDescription();
            case 2:
                return object.command;
            case 3:
                return object.flags;
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.Makefile;
    }
}
