package GlobalData.Module;
import Common.Database.iDBObject;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
public class ModuleAnchestor extends iDBObject {
    //--------------------------------------------------------------------------------------
    public int machine_id = Utils.Nan;
    public int compiler_id = Utils.Nan;
    public String command = ""; //дополнительная команда компилятору. между вызовом и флагами.
    public String flags = ""; //последовательность флагов
    //---------------------------------------------------------------------------------------
    //для таблиц
    public String getCompilerDescription() {
        Compiler compiler;
        return ((compiler = getCompiler()) == null) ? "" : compiler.getDescription();
    }
    public Compiler getCompiler() {
        return Global.db.getById(Compiler.class, compiler_id);
    }
    public String getDescription() {
        String res = "";
        if (getCompiler() != null) {
            res += Utils.Brackets(getCompiler().getDescription());
            if (!command.isEmpty())
                res += " " + Utils.Brackets(command);
            if (!flags.isEmpty())
                res += " " + Utils.Brackets(flags);
        }
        return res;
    }
    public String getStatCommand() {
        Compiler compiler = getCompiler();
        return ((compiler != null) ? (compiler.call_command) : "");
    }
}
