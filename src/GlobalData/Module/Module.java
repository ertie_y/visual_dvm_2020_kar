package GlobalData.Module;
import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Makefile.Makefile;
import ProjectData.LanguageName;
public class Module extends ModuleAnchestor {
    public int makefile_id = Utils.Nan;
    public LanguageName language = LanguageName.n;
    public int on = 1; //учитывать ли модуль при сборке. указание пользователя. если файлы отсутствуют - игнорится
    public Module() {
    }
    public Module(LanguageName language_in, Makefile makefile) {
        language = language_in;
        if (makefile!=null) {
            makefile_id = makefile.id;
            machine_id = makefile.machine_id;
        }
    }

    @Override
    public boolean isVisible() {
        return Current.HasMakefile() && (makefile_id == Current.getMakefile().id);
    }
    @Override
    public boolean isSelected() {
        return on > 0;
    }
    @Override
    public void Select(boolean flag) {
        on = flag ? 1 : 0;
        try {
            Global.db.Update(this);
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
}
