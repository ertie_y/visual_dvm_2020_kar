package GlobalData.Module;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.Module.UI.ModuleAnchestorForm;
public class ModulesDBTable extends iDBTable<Module> {
    public ModulesDBTable() {
        super(Module.class);
    }
    @Override
    public String getDataDescription() {
        return "языковой модуль";
    }
    @Override
    public DBObjectDialog<Module, ? extends DialogFields> getDialog() {
        return new ModuleAnchestorForm<>();
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
        };
    }
    @Override
    public Object getFieldAt(Module object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.language.getDescription();
            case 3:
                return object.getCompilerDescription();
            case 4:
                return object.command;
            case 5:
                return object.flags;
            default:
                return null;
        }
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Язык",
                "Компилятор",
                "Команда",
                "Флаги"
        };
    }
    @Override
    public Current CurrentName() {
        return Current.Module;
    }
}
