package GlobalData.Module.UI;

import Common.Current;
import Common.Passes.Pass;
import Common.Passes.PassCode;
import Common.UI.ComboBox.StyledTextComboBox;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.Compiler.Compiler;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.Module.ModuleAnchestor;
import ProjectData.LanguageName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;

public class ModuleAnchestorFields implements DialogFields {
    public JPanel content;
    public JComboBox<Compiler> cbCompilers;
    public JComboBox<String> cbCommands;
    public JComboBox<String> cbFlags;
    private JButton bHelp;
    private JButton BPickOptions;

    //считаем что машина есть.
    public ModuleAnchestorFields() {
        Current.setDialogCompiler(null);
        //-
        LinkedHashMap<Integer, Compiler> compilers = Current.getMachine().getCompilers();
        compilers.values().forEach(compiler -> cbCompilers.addItem(compiler));
        bHelp.addActionListener(e -> {
            if (cbCompilers.getSelectedItem() != null) {
                Pass_2021.passes.get(PassCode_2021.ShowCompilerHelp).Do(true);
            } else UI.Info("Компилятор не выбран");
        });
        BPickOptions.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Pass_2021<String> pass = Pass_2021.passes.get(PassCode_2021.PickCompilerOptions);
                if (pass.Do()){
                        UI.TrySelect(cbFlags, pass.target);
                }
            }
        });
        cbCompilers.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Current.setDialogCompiler(cbCompilers.getSelectedItem());
            }
        });
    }

    public void setListeners(ModuleAnchestor target) {
        boolean linker = (target instanceof Makefile);
        LanguageName languageName = linker ?
                (Current.HasProject() ? Current.getProject().languageName : LanguageName.n) :
                ((Module) target).language;
        cbCompilers.addActionListener(e -> {
            if (cbCompilers.getSelectedItem() instanceof Compiler) {
                Compiler compiler = ((Compiler) cbCompilers.getSelectedItem());
                UI.TrySelect(cbCommands,
                        linker ? compiler.getSpecialLinkCommand(languageName) : compiler.getSpecialCompilationCommand(languageName));
            }
        });
    }

    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        cbFlags = new StyledTextComboBox();
    }
}
