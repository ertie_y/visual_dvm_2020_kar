package GlobalData.Module.UI;
import Common.Current;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.Module.ModuleAnchestor;
import ProjectData.LanguageName;
public class ModuleAnchestorForm<T extends ModuleAnchestor> extends DBObjectDialog<T, ModuleAnchestorFields> {
    public static String[] banned_flags = new String[]{
            "-c", "-o",
            "-ffree-form", "-ffixed-line-length-132", "-ffixed-form",
            "-FI", "-f90",
            "-fixed", "-free"
    };
    String command;
    String flags;
    public ModuleAnchestorForm() {
        super(ModuleAnchestorFields.class);
    }
    @Override
    public int getDefaultHeight() {
        return 250;
    }
    @Override
    public void validateFields() {
        Compiler compiler = (Compiler) fields.cbCompilers.getSelectedItem();
        command = (String) fields.cbCommands.getSelectedItem();
        if (command == null) {
            Log.Writeln("команда компиляции не выбрана");
        } else {
            if ((compiler != null) && compiler.type.equals(CompilerType.dvm)) {
                //проверим команду.
                if (Result instanceof Makefile) {
                    //могут быть только flink/clink
                    if (!command.equals("flink") && !command.equals("clink")) {
                        Log.Writeln("При линковке DVM системой допустимы\n" +
                                "только команды flink или clink");
                    }
                } else if (Result instanceof Module) {
                    //могут быть только f/c
                    LanguageName languageName = ((Module) Result).language;
                    switch (languageName) {
                        case fortran:
                        case c:
                            if (!command.equals(languageName.getDVMCompile()))
                                Log.Writeln("компиляция " + languageName.getDescription() +
                                        " программ DVM системой осуществляется только командой " +
                                        languageName.getDVMCompile()
                                );
                            break;
                        default:
                            Log.Writeln("язык " + languageName + " не поддерживается DVM системой");
                            break;
                    }
                }
            } else {
                if (!command.isEmpty())
                    Log.Writeln("Для всех компиляторов, кроме DVM системы, команда компиляции/линковки должна быть пуста.");
            }
        }
        flags = (String) fields.cbFlags.getSelectedItem();
        if (flags == null) Log.Writeln("флаги компиляции не выбраны");
        else {
            //проверка на служебные флаги
            String[] data = flags.split(" ");
            for (String flag : data) {
                if (!flag.isEmpty()) {
                    /*
                    if (!flag.startsWith("-")) {
                        Log.Writeln("неверный формат флага " + Utils.Brackets(flag));
                    } else {
                     */
                        for (String banned_flag : banned_flags) {
                            if (flag.equalsIgnoreCase(banned_flag)) {
                                Log.Writeln("флаги:\n" +
                                        String.join(",", banned_flags) +
                                        "\nзарезервированы системой.");
                                return;
                            }
                       // }
                    }
                }
            }
        }
        //проврка команды
    }
    @Override
    public void fillFields() {
        UI.TrySelect(fields.cbFlags, Result.flags);
        UI.TrySelect(fields.cbCommands, Result.command);
        UI.TrySelect(fields.cbCompilers, Result.getCompiler());
        //--------------------------------------------
        fields.setListeners(Result);
    }
    @Override
    public void ProcessResult() {
        Result.machine_id = Current.getMachine().id;
        Compiler compiler = (Compiler) fields.cbCompilers.getSelectedItem();
        Result.compiler_id = (compiler != null) ? compiler.id : Utils.Nan;
        Result.command = command;
        Result.flags = flags;
    }
}
