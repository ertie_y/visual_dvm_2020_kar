package GlobalData.Settings;
public enum SettingName {
    Undefined,
    ProjectsSearchDirectory,
    LastVersionPath,
    EditorFontSize,
    ConfirmPassesStart,
    ShowPassesDone,
    FocusPassesResult,
    //-
    AutoUpdateSearch, //совместимость.
    AutoBugReportsLoad,//совместимость.
    AutoSubscribeTest,//совместимость.
    //-
    AutoSynchronizeRecipients,
    FastAccessPassesCount,
    LastOpenedProjectsCount,
    Workspace,
    RegisterOn,
    SpacesOn,
    EmptyLinesOn,
    FortranWrapsOn,
    ExtensionsOn,
    DarkThemeOn,
    ShowFullArraysDeclarations,
    ShowFullTabsNames,
    LocalMakePathWindows,
    SmallScreen,
    Precompilation,
    GCOVLimit,
    Kernels,
    //<editor-fold desc="Настройки сапфора. должны быть подряд!">
    STATIC_SHADOW_ANALYSIS,
    STATIC_PRIVATE_ANALYSIS,
    FREE_FORM,
    KEEP_DVM_DIRECTIVES,
    KEEP_SPF_DIRECTIVES,
    PARALLELIZE_FREE_LOOPS,
    MAX_SHADOW_WIDTH,
    OUTPUT_UPPER,
    TRANSLATE_MESSAGES,
    KEEP_LOOPS_CLOSE_NESTING,
    KEEP_GCOV,
    ANALYSIS_OPTIONS,
    DEBUG_PRINT_ON,
    MPI_PROGRAM,
    IGNORE_IO_SAPFOR,
    KEEP_SPF_DIRECTIVES_AMONG_TRANSFORMATIONS,
    PARSE_FOR_INLINE,
    EMPTY_OPTION;
    public static SettingName getByDescription(String desc) {
        SettingName[] all = SettingName.values();
        for (SettingName settingName : all)
            if (desc.equals(settingName.getDescription())) return settingName;
        return Undefined;
    }
    public String getDescription() {
        switch (this) {
            case Kernels:
                return "Число ядер при компиляции";
            case GCOVLimit:
                return "Нижний порог отображения GCOV";
            case Precompilation:
                return "Предварительная компиляция";
            case SmallScreen:
                return "Маленький экран";
            case LocalMakePathWindows:
                return "Путь к make.exe";
            case ShowFullTabsNames:
                return "Показывать полные имена вкладок";
            case ShowFullArraysDeclarations:
                return "Показывать развернутый список объявлений массивов";
            case ExtensionsOn:
                return "Учитывать расширения файлов";
            case DarkThemeOn:
                return "Тёмная тема редактора кода";
            case FortranWrapsOn:
                return "Учитывать переносы Fortran";
            case EmptyLinesOn:
                return "Учитывать пустые строки";
            case SpacesOn:
                return "Учитывать пробелы и табуляции";
            case RegisterOn:
                return "Учитывать регистр";
            case Workspace:
                return "Рабочее пространство визуализатора";
            case ConfirmPassesStart:
                return "Запрашивать подтверждение выполнения проходов";
            case ShowPassesDone:
                return "Сообщать об успешном выполнении проходов";
            case AutoBugReportsLoad:
                return "Автозагрузка журнала отчётов об ошибках";
            case STATIC_SHADOW_ANALYSIS:
                return "Оптимизация теневых обменов";
            case STATIC_PRIVATE_ANALYSIS:
                return "Статический анализ приватностей";
            case FREE_FORM:
                return "Свободный выходной стиль";
            case KEEP_DVM_DIRECTIVES:
                return "Учитывать DVM директивы";
            case KEEP_SPF_DIRECTIVES:
                return "Сохранять SPF директивы при построении параллельных вариантов";
            case PARALLELIZE_FREE_LOOPS:
                return "Улучшенное распараллеливание";
            case MAX_SHADOW_WIDTH:
                return "Максимальный размер теневых граней";
            case OUTPUT_UPPER:
                return "Верхний выходной регистр";
            case TRANSLATE_MESSAGES:
                return "Сообщения на русском языке";
            case KEEP_LOOPS_CLOSE_NESTING:
                return "Учитывать тесную вложенность циклов";
            case KEEP_GCOV:
                return "Учитывать GCOV";
            case ANALYSIS_OPTIONS:
                return "Опции анализа";
            case DEBUG_PRINT_ON:
                return "Включить отладочную печать";
            case MPI_PROGRAM:
                return "MPI программа";
            case IGNORE_IO_SAPFOR:
                return "Игнорировать ввод/вывод";
            case KEEP_SPF_DIRECTIVES_AMONG_TRANSFORMATIONS:
                return "Сохранять SPF директивы при преобразованиях";
            case PARSE_FOR_INLINE:
                return "Синтаксический анализ для подстановки";
            case FocusPassesResult:
                return "Переходить на результирующую вкладку проходов по их завершении";
            case AutoSubscribeTest:
                return "Автоматическая проверка подписки";
            case AutoSynchronizeRecipients:
                return "Автоматическое извлечение адресатов";
            case FastAccessPassesCount:
                return "Число проходов на панели быстрого доступа";
            case LastOpenedProjectsCount:
                return "Число отображаемых последних открытых проектов";
        }
        return "";
    }
}
//</editor-fold>