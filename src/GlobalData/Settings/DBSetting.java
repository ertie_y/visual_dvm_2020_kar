package GlobalData.Settings;
import Common.Database.DBObject;
import Common.UI.Menus.StableMenuItem;
import Common.Utils.Utils;
import Repository.Component.ComponentType;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import com.sun.org.glassfish.gmbal.Description;

import javax.swing.*;
import java.awt.event.ActionEvent;
public class DBSetting extends DBObject {
    @Description("PRIMARY KEY,UNIQUE")
    public SettingName Name;
    public String Value;
    public ComponentType Owner;
    @Description("IGNORE")
    public boolean Visible = true;
    @Description("IGNORE")
    public SettingType settingType = SettingType.Undefined;
    public JMenuItem menu_item = null;
    public JMenuItem getMenuItem() {
        if (menu_item == null) {
            menu_item = new StableMenuItem(Name.getDescription());
            menu_item.addActionListener(new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Pass_2021.passes.get(PassCode_2021.UpdateSetting).Do(Name);
                }
            });
            Mark();
        }
        return menu_item;
    }
    public DBSetting() {
    }
    public DBSetting(SettingName Name_,
                     Object Value_,
                     SettingType type_in,
                     ComponentType owner_in,
                     boolean Visible_in) {
        Name = Name_;
        Value = Value_.toString();
        settingType = type_in;
        Owner = owner_in;
        Visible = Visible_in;
    }
    public DBSetting(SettingName Name_,
                     Object Value_,
                     SettingType type_in,
                     ComponentType owner_in
    ) {
        this(Name_, Value_, type_in, owner_in, true);
    }
    /*
     * булевского типа не надо. только 0 или 1
     */
    //заводить разные типы настроек не надо.
    //иначе придется ковыряться с типами и таблицами в бд
    public void Mark() {
        switch (settingType) {
            case SapforFlag:
                menu_item.setIcon(Utils.getIcon(toBoolean() ? "/icons/Pick.png" : "/icons/NotPick.png"));
                break;
            case PercentField:
                menu_item.setText(Name.getDescription() + " : " + this + "%");
                break;
            case StringField:
                menu_item.setText(Name.getDescription() + " : " + Utils.Quotes(toString()));
                break;
            case IntField:
                menu_item.setText(Name.getDescription() + " : " + this);
                break;
        }
    }
    public int toInt32() {
        return Integer.parseInt(Value);
    }
    public boolean toBoolean() {
        return toInt32() != 0;
    }
    @Override
    public String toString() {
        return Value;
    }
    @Override
    public Object getPK() {
        return Name;
    }
}