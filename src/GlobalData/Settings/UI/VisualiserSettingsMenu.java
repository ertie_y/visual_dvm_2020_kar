package GlobalData.Settings.UI;
import Common.Global;
import Common.UI.Menus.StyledPopupMenu;
import GlobalData.Settings.DBSetting;
import GlobalData.Settings.SettingName;
import Repository.Component.ComponentType;
public class VisualiserSettingsMenu extends StyledPopupMenu {
    public VisualiserSettingsMenu() {
        Global.db.settings.getSettingsByOwner(ComponentType.Visualiser).stream().filter(s -> s.Visible).map(DBSetting::getMenuItem).forEach(this::add);


    }
}