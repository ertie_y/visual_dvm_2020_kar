package GlobalData.Settings.UI;
import Common.Global;
import Common.UI.Menus.StyledPopupMenu;
import GlobalData.Settings.DBSetting;
import Repository.Component.ComponentType;
public class ComparsionMenu extends StyledPopupMenu {
    public ComparsionMenu() {
        Global.db.settings.getSettingsByOwner(ComponentType.ComparsionOptions).stream().filter(s -> s.Visible).map(DBSetting::getMenuItem).forEach(this::add);
    }
}
