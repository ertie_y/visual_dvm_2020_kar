package GlobalData.Settings.UI;
import Common.Global;
import Common.UI.Menus.SettingsSubmenu;
import Common.UI.Menus.StyledPopupMenu;
import GlobalData.Settings.SettingName;
public class SapforSettingsMenu extends StyledPopupMenu {
    public SapforSettingsMenu() {
        super();
        add(new SettingsSubmenu("Анализ", null,
                SettingName.Precompilation,
                SettingName.STATIC_SHADOW_ANALYSIS,
                SettingName.STATIC_PRIVATE_ANALYSIS,
                SettingName.KEEP_DVM_DIRECTIVES,
                SettingName.IGNORE_IO_SAPFOR,
                SettingName.MPI_PROGRAM,
                SettingName.ANALYSIS_OPTIONS,
                SettingName.PARALLELIZE_FREE_LOOPS
        ));
        addSeparator();
        add(new SettingsSubmenu("Построение системы интервалов", null,
                SettingName.KEEP_LOOPS_CLOSE_NESTING,
                SettingName.KEEP_GCOV
        ));
        addSeparator();
        add(new SettingsSubmenu("Построение версий", null,
                SettingName.FREE_FORM,
                SettingName.KEEP_SPF_DIRECTIVES,
                SettingName.KEEP_SPF_DIRECTIVES_AMONG_TRANSFORMATIONS,
                SettingName.OUTPUT_UPPER,
                SettingName.MAX_SHADOW_WIDTH
        ));
        addSeparator();
        add(Global.db.settings.get(SettingName.TRANSLATE_MESSAGES).getMenuItem());
        add(Global.db.settings.get(SettingName.DEBUG_PRINT_ON).getMenuItem());
        add(Global.db.settings.get(SettingName.GCOVLimit).getMenuItem());
    }
}