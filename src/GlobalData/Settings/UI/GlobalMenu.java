package GlobalData.Settings.UI;
public enum GlobalMenu {
    Analyses,
    Transformations,
    Debug,
    Cleaning,
    SapforSettings,
    VisualiserSettings,
    ComparsionSettings,
    LastProjects,
    //-
    ProjectLanguage,
    //-
    FileLanguage,
    FileType,
    FileStyle,
    //-
    ConfigurationEnvironments,
    Attachments
}
