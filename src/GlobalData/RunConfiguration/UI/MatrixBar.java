package GlobalData.RunConfiguration.UI;

import Common.UI.Tables.StyledCellLabel;
import Common.UI.UI;
import ProjectData.SapforData.Regions.UI.DimensionSpinner;
import javafx.util.Pair;

import javax.swing.*;
import java.util.LinkedHashMap;
import java.util.Vector;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MatrixBar extends JToolBar {
    public Vector<JSpinner> dimensions = new Vector<>();

    public MatrixBar(String bounds) {
        this.setFloatable(false);
        if (!bounds.isEmpty()) {
            String[] sDims = bounds.split(" ");
            for (String sDim : sDims) {
                JSpinner S = new MatrixDimensionSpinner(Integer.parseInt(sDim));
                dimensions.add(S);
                add(S);
            }
        }
    }

    public void ShowTillDim(int dim) {
        if (dimensions.size()<dim){
            int delta = dim-dimensions.size();
           // UI.Info("needs additions "+delta);
            for (int i=0; i<delta; ++i) {
                JSpinner S = new MatrixDimensionSpinner(1);
                dimensions.add(S);
                add(S);
            }
        }
       for (int i=0; i<dimensions.size(); ++i)
           dimensions.get(i).setVisible((i + 1) <= dim);
       this.revalidate();
    }

    public String pack(int dim) {
        return String.join(" ",IntStream.range(0, dim).mapToObj(i -> dimensions.get(i).getValue().toString()).collect(Collectors.toCollection(Vector::new)));
    }
}
