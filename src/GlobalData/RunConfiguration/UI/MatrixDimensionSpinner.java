package GlobalData.RunConfiguration.UI;

import javax.swing.*;
import java.awt.*;

public class MatrixDimensionSpinner extends JSpinner {
    public MatrixDimensionSpinner(int value_in) {
        java.awt.Dimension p = new Dimension(40, 26);
        setMinimumSize(p);
        setMaximumSize(p);
        setPreferredSize(p);
        setModel(new SpinnerNumberModel(
                value_in,
                1, 16, 1));
    }
}