package GlobalData.RunConfiguration;

import Common.Current;
import Common.Database.*;
import Common.Global;
import Common.Passes.Pass;
import Common.Passes.PassCode;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.Compiler.CompilerType;
import GlobalData.DVMParameter.DVMParameter;
import GlobalData.EnvironmentValue.EnvironmentValue;
import GlobalData.RunConfiguration.UI.MatrixBar;
import GlobalData.RunConfiguration.UI.RunConfigurationFields;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;
import org.fife.ui.rsyntaxtextarea.RSyntaxUtilities;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.LinkedHashMap;

public class RunConfigurationsDBTable extends iDBTable<RunConfiguration> {
    public RunConfigurationsDBTable() {
        super(RunConfiguration.class);
    }

    @Override
    public String getDataDescription() {
        return "конфигурация запуска";
    }

    @Override
    public DBObjectDialog<RunConfiguration, RunConfigurationFields> getDialog() {
        return new DBObjectDialog<RunConfiguration, RunConfigurationFields>(RunConfigurationFields.class) {
            @Override
            public void fillFields() {
                for (Compiler compiler : Global.db.compilers.Data.values()) {
                    if (compiler.isVisible() && compiler.type.equals(CompilerType.dvm))
                        fields.cbLauncherCall.addItem(compiler);
                }
                UI.TrySelect_s(fields.cbLauncherCall, Result.LauncherCall);
                UI.TrySelect(fields.cbLaunchOptions, Result.LauncherOptions);
                fields.tfArgs.setText(Result.args);
                fields.cbLauncherCall.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (fields.cbLauncherCall.getSelectedItem() instanceof Compiler) {
                            UI.TrySelect(fields.cbLaunchOptions, "run");
                        }
                    }
                });
                fields.cbCube.setSelected(Result.isCube());
                //------------------------------------------->>>
                fields.sMaxDim.setModel(new SpinnerNumberModel(
                        Result.dim,
                        0, RunConfiguration.maxProc, 1));
                fields.minMatrixPanel.add(fields.minMatrixBar = new MatrixBar(Result.minMatrix));
                fields.maxMatrixPanel.add(fields.maxMatrixBar = new MatrixBar(Result.maxMatrix));
                //------------------------------------------->>>
                if (!edit)
                    fields.sMaxDim.setValue(Current.getProject().maxdim);
            }

            @Override
            public void ProcessResult() {
                Result.machine_id = Current.getMachine().id;
                Result.LauncherCall = fields.cbLauncherCall.getSelectedItem().toString();
                Result.LauncherOptions = (String) fields.cbLaunchOptions.getSelectedItem();
                if (fields.cbLauncherCall.getSelectedItem() instanceof Compiler) {
                    Result.compiler_id = ((Compiler) (fields.cbLauncherCall.getSelectedItem())).id;
                } else Result.compiler_id = Utils.Nan;
                //-
                Result.dim = (int) fields.sMaxDim.getValue();
                Result.minMatrix = fields.minMatrixBar.pack(Result.dim);
                Result.maxMatrix = fields.maxMatrixBar.pack(Result.dim);
                //-
                Result.args = fields.tfArgs.getText();
                Result.cube = fields.cbCube.isSelected() ? 1 : 0;
            }

            @Override
            public void validateFields() {
                String launcher_call = fields.cbLauncherCall.getSelectedItem().toString();
                String launcher_options = (String) fields.cbLaunchOptions.getSelectedItem();
                if (launcher_call.isEmpty() && !launcher_options.isEmpty())
                    Log.Writeln_("Непустые опции запуска допускаются только для DVM системы или MPI");
                if (fields.cbLauncherCall.getSelectedItem() instanceof Compiler) {
                    Compiler compiler = (Compiler) (fields.cbLauncherCall.getSelectedItem());
                    switch (compiler.type) {
                        case dvm:
                        case mpi:
                            int dim_ = (int) fields.sMaxDim.getValue();
                            Result.validateMatrixes(
                                    fields.minMatrixBar.pack(dim_),
                                    fields.maxMatrixBar.pack(dim_),
                                    dim_,
                                    fields.cbCube.isSelected(),
                                    Log
                            );
                            break;
                    }

                }
            }
        };
    }

    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(RunTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        res.put(EnvironmentValue.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(DVMParameter.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Команда",
                "Опции",
                "Разм.",
                "Куб",
                "Min",
                "Max",
                "Аргументы"
        };
    }

    @Override
    public Object getFieldAt(RunConfiguration object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.LauncherCall;
            case 2:
                return object.LauncherOptions;
            case 3:
                return object.dim;
            case 4:
                return object.printCube();
            case 5:
                return object.minMatrix;
            case 6:
                return object.maxMatrix;
            case 7:
                return object.args;
            default:
                return null;
        }
    }

    @Override
    public Current CurrentName() {
        return Current.RunConfiguration;
    }

    public void Patch() throws Exception {
        for (RunConfiguration c : Data.values()) {
            c.Patch();
            getDb().Update(c);
        }
    }
}
