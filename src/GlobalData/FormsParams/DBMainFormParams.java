package GlobalData.FormsParams;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;
public class DBMainFormParams extends DBObject {
    //todo вместо этого кошмара сделать объект DBSC для сплит контейнера,
    // и включить туда все палки в проекте, а не только в главном окне.
    @Description("PRIMARY KEY,UNIQUE, NOT NULL")
    public int id;
    public int SC1 = -1;
    public int SC2 = -1;
    public int SC3 = -1;
    public int SC4 = -1;
    public int SC5 = -1;
    public int SC6 = -1;
    public int SC7 = -1;
    public int SC8 = -1;
    public int SC9 = -1;
    public int SC10 = -1;
    public int SC11 = -1;
    public DBMainFormParams() {
    }
    public DBMainFormParams(int SC1_, int SC2_, int SC3_, int SC4_, int SC5_, int SC6_, int SC7_, int SC8_, int SC9_, int SC10_, int SC11_) {
        id = 0;
        SC1 = SC1_;
        SC2 = SC2_;
        SC3 = SC3_;
        SC4 = SC4_;
        SC5 = SC5_;
        SC6 = SC6_;
        SC7 = SC7_;
        SC8 = SC8_;
        SC9 = SC9_;
        SC10 = SC10_;
        SC11 = SC11_;
    }
    public void Validate() {
        if (SC1 == 0) SC1 = -1;
        if (SC2 == 0) SC2 = -1;
        if (SC3 == 0) SC3 = -1;
        if (SC4 == 0) SC4 = -1;
        if (SC5 == 0) SC5 = -1;
        if (SC6 == 0) SC6 = -1;
        if (SC7 == 0) SC7 = -1;
        if (SC8 == 0) SC8 = -1;
        if (SC9 == 0) SC9 = -1;
        if (SC10 == 0) SC10 = -1;
        if (SC11 == 0) SC11 = -1;
    }
    @Override
    public Object getPK() {
        return id;
    }
}
