package GlobalData.FormsParams;
import Common.Database.DBObject;
import Common.UI.Windows.FormType;
import com.sun.org.glassfish.gmbal.Description;

import java.awt.*;
public class DBForm extends DBObject {
    @Description("PRIMARY KEY,UNIQUE, NOT NULL")
    public FormType type = FormType.Undefined;
    public int X = 0;
    public int Y = 0;
    public int Width = 0;
    public int Height = 0;
    public DBForm(FormType type_, Window window) {
        type = type_;
        Init(window);
    }
    public DBForm() {
    }
    public void Init(Window window) {
        X = window.getX();
        Y = window.getY();
        Width = window.getWidth();
        Height = window.getHeight();
    }
    public void Apply(Window window) {
        window.setSize(Width, Height);
        window.setLocation(X, Y);
    }
    @Override
    public Object getPK() {
        return type;
    }
}
