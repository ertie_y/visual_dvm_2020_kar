package GlobalData.FormsParams;
import Common.Database.DBTable;
import Common.UI.Windows.FormType;
public class FormsDBTable extends DBTable<FormType, DBForm> {
    public FormsDBTable() {
        super(FormType.class, DBForm.class);
    }
    @Override
    public String getDataDescription() {
        return "параметры окны";
    }
}
