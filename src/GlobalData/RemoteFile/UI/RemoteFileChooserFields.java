package GlobalData.RemoteFile.UI;
import Common.UI.Trees.TreeForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class RemoteFileChooserFields implements DialogFields {
    public JPanel MainPanel;
    public JLabel lCurrentFolder;
    public JLabel lCurrentFile;
    public TreeForm treeForm;
    private JPanel TreePanel;
    private JButton bBack;
    private JButton bHome;
    public RemoteFileChooserFields() {
        bBack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UI.getRemoteFileChooser().goUp();
            }
        });
        bHome.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                UI.getRemoteFileChooser().goHome();
            }
        });
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        TreePanel = (treeForm = new TreeForm(RemoteFilesTree.class)).getContent();
    }
    @Override
    public Component getContent() {
        return MainPanel;
    }
}
