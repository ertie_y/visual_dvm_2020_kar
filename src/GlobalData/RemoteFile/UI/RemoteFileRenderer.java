package GlobalData.RemoteFile.UI;
import Common.Current;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.Trees.StyledTreeCellRenderer;
import GlobalData.RemoteFile.RemoteFile;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
public class RemoteFileRenderer extends StyledTreeCellRenderer {
    public java.awt.Component getTreeCellRendererComponent(
            JTree tree, Object value,
            boolean selected, boolean expanded,
            boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        Object o = ((DefaultMutableTreeNode) value).getUserObject();
        if (o instanceof RemoteFile) {
            RemoteFile file = (RemoteFile) o;
            setText(file.name);
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
            if (file.isDirectory())
                setIcon(new ImageIcon(getClass().getResource("/icons/Folder.png")));
        }
        setForeground(tree.getForeground());
        return this;
    }
}
