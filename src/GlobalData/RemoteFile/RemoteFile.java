package GlobalData.RemoteFile;
public class RemoteFile {
    public String name;
    public String full_name;
    public String parent;
    public boolean isDirectory;
    public long updateTime;
    public RemoteFile(String path, boolean isDirectory_in) {
        full_name = path;
        isDirectory = isDirectory_in;
        String[] data = full_name.split("/");
        name = (data.length > 0) ? data[data.length - 1] : "";
        int i = full_name.lastIndexOf('/');
        parent = (i > 0) ? full_name.substring(0, i) : "/";
    }
    public RemoteFile(String parent_in, String name_in, boolean isDirectory_in) {
        parent = parent_in;
        full_name = parent + (parent.endsWith("/") ? "" : "/") + name_in;
        name = name_in;
        isDirectory = isDirectory_in;
    }
    public RemoteFile(String parent_in, String name_in) {
        this(parent_in, name_in, false);
    }
    public static long convertUpdateTime(int mtime) {
        return (long) mtime * 1000L;
    }
    public boolean isDirectory() {
        return isDirectory;
    }
}


