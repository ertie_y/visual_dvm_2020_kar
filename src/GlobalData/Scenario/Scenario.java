package GlobalData.Scenario;
import Common.Database.iDBObject;
import Common.Global;
import GlobalData.Compiler.Compiler;
import GlobalData.ScenarioCommand.ScenarioCommand;
import com.sun.org.glassfish.gmbal.Description;

import java.util.LinkedHashMap;
import java.util.Vector;
public class Scenario extends iDBObject {
    @Description("DEFAULT ''")
    public String description="";
    public LinkedHashMap<Integer, ScenarioCommand> getCommands() {
        return Global.db.getMapByFKi(this, ScenarioCommand.class);
    }
}
