package GlobalData.Scenario;
import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Scenario.UI.ScenarioFields;
import GlobalData.ScenarioCommand.ScenarioCommand;

import java.util.LinkedHashMap;
public class ScenarioDBTable extends iDBTable<Scenario> {
    public ScenarioDBTable() {
        super(Scenario.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Описание"
        };
    }
    @Override
    public Object getFieldAt(Scenario object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.description;
            default:
                return null;
        }
    }
    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(ScenarioCommand.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }
    @Override
    public Current CurrentName() {
        return Current.Scenario;
    }
    //-
    @Override
    public DBObjectDialog<Scenario, ScenarioFields> getDialog() {
        return new DBObjectDialog<Scenario, ScenarioFields>(ScenarioFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public void fillFields() {
                fields.tfDescription.setText(Result.description);
            }
            @Override
            public void ProcessResult() {
                Result.description = fields.tfDescription.getText();
            }
        };
    }
}
