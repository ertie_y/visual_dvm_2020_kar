package GlobalData.CompilerEnvironment;

import Common.Database.DBObject;
import Common.Utils.TextLog;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Vector;

public class CompilerEnvironment extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String name = "";
    public String value = "";
    public EnvironmentValueType type = EnvironmentValueType.String;
    public Vector<String> valueVariants = new Vector<>();
    public Vector<String> description = new Vector<>();

    @Override
    public Object getPK() {
        return name;
    }

    public void CheckDefaults() {
        //определить тип. в dvm переменных он может быть указан в начале 1 строки.
        if (!description.isEmpty()) {
            String line = description.get(0);
            if (line.startsWith("Boolean value.")) {
                type = EnvironmentValueType.Boolean;
                valueVariants.add("0");
                valueVariants.add("1");
            } else if (line.startsWith("Integer value.")) {
                type = EnvironmentValueType.Integer;
            }else if (line.startsWith("Real number.")) {
                type = EnvironmentValueType.Real;
            }
            //-
            if (line.endsWith("Accepted values:")){
                // все что дальше - варианты и их описания после запятой.
                for (int i=1; i< description.size(); ++i){
                    String[] data = description.get(i).split(",");
                    if (data.length>1)
                        valueVariants.add(data[0]);
                }
            }
        }
    }
    public void validate(TextLog log, String new_value) {
        if (new_value.isEmpty()) {
            log.Writeln_("Переменная не предполагает пустого значения");
        } else {
            if (!valueVariants.isEmpty()) {
                if (!valueVariants.contains(new_value))
                    log.Writeln_("Переменная : недопустимое значение. Выберите доступный вариант.");
            }
            switch (type) {
                case Boolean:
                    if (!valueVariants.contains(new_value))
                        log.Writeln_("Переменная допускает только 0 или 1 в качестве значений");
                    break;
                case Integer:
                    try {
                        Integer.parseInt(new_value);
                    } catch (NumberFormatException ex) {
                        log.Writeln_("Переменная допускает только целочисленные значения");
                    }
                    break;
                case Real:
                    try {
                        Double.parseDouble(new_value);
                    } catch (NumberFormatException ex) {
                        log.Writeln_("Переменная допускает только числа с плавающей запятой в качестве значения");
                    }
                    break;
                case String:
                    if (new_value.contains("\""))
                        log.Writeln_("Переменная не может содержать двойные кавычки в значении");
                    break;
            }
        }
    }
}
