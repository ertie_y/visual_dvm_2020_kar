package GlobalData.CompilerEnvironment;

import Common.Database.DataSet;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableEditors;
import Common.UI.Tables.TableRenderers;

import static Common.UI.Tables.TableRenderers.RendererMultiline;

public class CompilerEnvironmentsSet extends DataSet<String, CompilerEnvironment> {
    public CompilerEnvironmentsSet() {
        super(String.class, CompilerEnvironment.class);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
               "Значение", "Описание"
        };
    }
    @Override
    public Object getFieldAt(CompilerEnvironment object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.value;
            case 3:
                return object.description;
            default:
                return null;
        }
    }


    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(2).setRenderer(TableRenderers.RendererCompilerEnvironmentValue);
                columns.get(2).setEditor(TableEditors.EditorCompilerEnvironmentValue);
                //-
                columns.get(3).setRenderer(RendererMultiline);
            }
        };
    }
}
