package GlobalData.CompilerEnvironment.UI;

import Common.Current;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;

public class CompilerEnvironmentsFields implements DialogFields {
    private JPanel content;

    @Override
    public Component getContent() {
        return content;
    }
    public CompilerEnvironmentsFields(){
        Current.getDialogCompiler().environments.mountUI(content);
        Current.getDialogCompiler().environments.ShowUI();
    }
}
