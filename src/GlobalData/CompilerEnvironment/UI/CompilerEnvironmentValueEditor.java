package GlobalData.CompilerEnvironment.UI;

import Common.Current;
import Common.UI.Tables.DBObjectEditor;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.ComboTextDialog;
import Common.Utils.TextLog;
import GlobalData.CompilerEnvironment.CompilerEnvironment;

import java.util.Objects;

public class CompilerEnvironmentValueEditor extends DBObjectEditor<CompilerEnvironment> {
    @Override
    public void Action() {

        setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
        setText(value.value.isEmpty() ? "не задано" : value.value);
        //-
        ComboTextDialog dialog = new ComboTextDialog() {
            @Override
            public void validateFields() {
                super.validateFields();
                if (fields.getSelectedItem() != null)
                    value.validate(Log, fields.getSelectedItem().toString());
            }
        };
        dialog.fields.setEditable(value.valueVariants.isEmpty());
        if (!value.value.isEmpty())
            UI.TrySelect(dialog.fields, value.value);
        if (dialog.ShowDialog("Изменить значение опции " + value.name,
                value.valueVariants
        )) {
            value.value = dialog.Result;
            setText(value.value.isEmpty() ? "не задано" : value.value);
        }
    }

    @Override
    public Object getCellEditorValue() {
        return value.value;
    }
}
