package GlobalData.CompilerEnvironment.UI;

import Common.Current;
import Common.UI.Tables.DBObjectRenderer;
import Common.UI.Themes.VisualiserFonts;
import GlobalData.CompilerEnvironment.CompilerEnvironment;

public class CompilerEnvironmentValueRenderer extends DBObjectRenderer {
    @Override
    public void Display() {
        if (value != null) {
            CompilerEnvironment environment = (CompilerEnvironment) value;
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
            setText(environment.value.isEmpty() ? "не задано" : environment.value);
        }
    }
}
