package GlobalData.CompilerEnvironment;

public enum EnvironmentValueType {
    String,
    Boolean,
    Integer,
    Real
}
