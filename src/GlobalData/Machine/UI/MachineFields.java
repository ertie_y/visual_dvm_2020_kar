package GlobalData.Machine.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.Machine.MachineType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MachineFields implements DialogFields {
    public JTextField tfName;
    public JTextField tfAddress;
    public JSpinner sPort;
    public JPanel content;
    public JComboBox<MachineType> cbMachineType;
    private static final String localName  = "этот компьютер";
    public MachineFields() {
        sPort.setModel(new SpinnerNumberModel(22, 1, 65535, 1));
        cbMachineType.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if ((cbMachineType.getSelectedItem()!=null)&&(cbMachineType.getSelectedItem().equals(MachineType.Local))){
                    tfName.setText(localName);
                    tfAddress.setText(localName);
                }
            }
        });
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfName=new StyledTextField();
        tfAddress=new StyledTextField();
        //-
        cbMachineType = new JComboBox<>();
        cbMachineType.addItem(MachineType.Server);
        cbMachineType.addItem(MachineType.MVS_cluster);
        cbMachineType.addItem(MachineType.Local);
    }
    @Override
    public Component getContent() {
        return content;
    }
}
