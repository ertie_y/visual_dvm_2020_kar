package GlobalData.Machine;
import Common.Database.iDBObject;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Compiler.Compiler;
import GlobalData.User.User;
import Visual_DVM_2021.Passes.SSH.ConnectionPass;

import java.util.LinkedHashMap;
public class Machine extends iDBObject {
    public MachineType type = MachineType.Server;
    public String name = "";
    public String address = "";
    public int port = 22;
    public Machine(String name_in, String address_in, int port_in, MachineType type_in) {
        name = name_in;
        address = address_in;
        port = port_in;
        type = type_in;
    }
    public Machine() {
    }
    public String getURL() {
        return type.equals(MachineType.Local) ? name : (address + ":" + port);
    }
    public String getFullDescription() {
        return this.equals(ConnectionPass.rep_machine) ? "Репозиторий визуализатора" : "Машина по адресу " + Utils.Brackets(getURL());
    }
    public LinkedHashMap<Integer, Compiler> getCompilers() {
        return Global.db.getMapByFKi(this, Compiler.class);
    }
    public LinkedHashMap<Integer, User> getUsers() {
        return Global.db.getMapByFKi(this, User.class);
    }

    @Override
    public String getDialogName() {
        return super.getDialogName() + ":" + getURL();
    }
/* инициализация компиляторов по умолчанию. добавить в отдельный проход.
    //todo если машина локальная и винда, их не добавлять.
        Visualiser.db.Insert(new Compiler(
                (Machine)Target,
                "gfortran",
    CompilerType.gnu,
            "gfortran",
            "--version",
            "--help"
            ));
        Visualiser.db.Insert(new Compiler(
                (Machine)Target,
                "gcc",
    CompilerType.gnu,
            "gcc",
            "--version",
            "--help"
            ));
        Visualiser.db.Insert(new Compiler(
                (Machine)Target,
                "g++",
    CompilerType.gnu,
            "g++",
            "--version",
            "--help"
            ));
     */
}
