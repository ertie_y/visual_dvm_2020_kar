package GlobalData.Machine;
public enum MachineType {
    Undefined,
    Server,
    MVS_cluster,
    Local;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "?";
            case Server:
                return "одиночный сервер";
            case MVS_cluster:
                return "MVS кластер";
            case Local:
                return "локальная";
        }
        return "";
    }
}
