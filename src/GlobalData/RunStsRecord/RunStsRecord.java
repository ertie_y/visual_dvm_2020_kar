package GlobalData.RunStsRecord;

import Common.Current;
import Common.Database.iDBObject;
import Common.Global;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.Tasks.RunTask.RunTask;
import ProjectData.LanguageName;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Vector;

public class RunStsRecord extends iDBObject {
    //---------------------------------
    public static String filter_path = "";
    public static boolean only_current_project = false;
    public static boolean only_dvm_sts = false;
    public static String filter_machine = "";
    public static String filter_matrix = "";
    public static int filter_proc = 0;
    public static Vector<String> filter_link_flags = new Vector();
    public static Vector<String> filter_fortran_flags = new Vector();
    public static Vector<String> filter_c_flags = new Vector();
    public static Vector<String> filter_cpp_flags = new Vector();
    public static Vector<String> filter_environments = new Vector();
    //---------------------------------
    public String machineURL = "";
    //---------------------------------------------------
    public String projectName = "";
    public String projectPath = "";
    public String projectDescription = "";
    //---------------------------------------------------
    public String linker = "";
    public String linkerDescription = "";
    public String linkFlags = "";
    //-------------------
    public String fortranCompiler = "";
    public String fortranCompilerDescription = "";
    public String fortranFlags = "";
    //-------------------
    public String cCompiler = "";
    public String cCompilerDescription = "";
    public String cFlags = "";
    //------------------
    public String cppCompiler = "";
    public String cppCompilerDescription = "";
    public String cppFlags = "";
    //---------------------------------------------------
    public String runCommand = "";
    public String runCommandDescription = "";
    public String runOptions = "";
    public String matrix = "";
    public String environments = "";
    public String args = "";
    //---------------------------------------------------
    // public String dvmStsText = "";
    //---------------------------------------------------
    public long StartDate = 0;
    public long EndDate = 0;
    public double Time = 0;
    public double CleanTime = 0;

    //---------------------------------------------------
    public RunStsRecord() {
    }

    public RunStsRecord(RunTask runTask) {
        //если задача успешно выполнена. все данные можно смело брать с Current объектов
        // благодаря SelectProjectData
        machineURL = runTask.getMachine().getURL();
        //-------------------------------------------------------
        projectName = Current.getProject().name;
        projectPath = Current.getProject().Home.getAbsolutePath();
        projectDescription = Current.getProject().description;
        //-------------------------------------------------------
        Makefile makefile = Current.getProject().getMakefile();
        LinkedHashMap<LanguageName, Module> modules = makefile.getModules();
        //-----------------------
        linker = makefile.getStatCommand();
        linkerDescription = makefile.getCompilerDescription();
        linkFlags = makefile.flags;
        //
        fortranCompiler = modules.get(LanguageName.fortran).getStatCommand();
        fortranCompilerDescription = modules.get(LanguageName.fortran).getCompilerDescription();
        fortranFlags = modules.get(LanguageName.fortran).flags;
        //
        cCompiler = modules.get(LanguageName.c).getStatCommand();
        cCompilerDescription = modules.get(LanguageName.c).getCompilerDescription();
        cFlags = modules.get(LanguageName.c).flags;
        //
        cppCompiler = modules.get(LanguageName.cpp).getStatCommand();
        cppCompilerDescription = modules.get(LanguageName.cpp).getCompilerDescription();
        cppFlags = modules.get(LanguageName.cpp).flags;
        //------------------------
        RunConfiguration runConfiguration = Current.getProject().getRunConfiguration();
        runCommand = runConfiguration.LauncherCall;
        if (runCommand.contains("\\")) {
            runCommandDescription =
                    runCommand.substring(runCommand.lastIndexOf('\\')).replace("\\", "");
        } else if (runCommand.contains("/")) {
            runCommandDescription =
                    runCommand.substring(runCommand.lastIndexOf('/')).replace("/", "");
        } else runCommandDescription = runCommand;
        runOptions = runConfiguration.LauncherOptions;
        matrix = runTask.matrix;
        args = runConfiguration.args;
        environments = String.join("\n", runConfiguration.getEnvList());
        StartDate = runTask.StartDate;
        EndDate = runTask.EndDate;
        Time = runTask.Time;
        CleanTime = runTask.CleanTime;
    }

    public File getSts() {
        return Paths.get(Global.StsDirectory.getAbsolutePath(), id + "_sts.gz+").toFile();
    }

    public void SaveSts(RunTask runTask) throws IOException {

        Files.copy(runTask.getLocalStsFile().toPath(), getSts().toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    public Date getEndDate() {
        return new Date(EndDate);
    }

    public Vector<String> getEnvs() {
        return new Vector<>(Arrays.asList(environments.split("\n")));
    }

    @Override
    public boolean isVisible() {
        return
                pathFilter() &&
                        dvmStsFilter() &&
                        machineFilter() &&
                        flagsFilter() &&
                        matrixFilter() &&
                        procFilter();
    }

    public boolean dvmStsFilter() {
        return getSts().exists() || !only_dvm_sts;
    }

    public boolean matrixFilter() {
        return matrix.startsWith(filter_matrix);
    }

    public boolean procFilter() {
        if (filter_proc <= 0) return true;
        int pn = 1;
        String[] data = matrix.split(" ");
        for (String d : data) {
            if (!d.isEmpty()) {
                int dim = 1;
                try {
                    dim = Integer.parseInt(d);
                } catch (Exception ex) {
                    Global.Log.PrintException(ex);
                }
                pn *= dim;
            }
        }
        return pn == filter_proc;
    }

    public boolean pathFilter() {
        return only_current_project ?
                (projectPath.equals(Current.getProject().Home.getAbsolutePath())) :
                (projectPath.startsWith(filter_path));
    }

    public boolean machineFilter() {
        return machineURL.startsWith(filter_machine);
    }

    public boolean flagsFilter() {
        return
                (filter_link_flags.isEmpty() || getLinkFlags().containsAll(filter_link_flags)) &&
                        (filter_fortran_flags.isEmpty() || getFortranFlags().containsAll(filter_fortran_flags)) &&
                        (filter_c_flags.isEmpty() || getCFlags().containsAll(filter_c_flags)) &&
                        (filter_cpp_flags.isEmpty() || getCppFlags().containsAll(filter_cpp_flags)) &&
                        (filter_environments.isEmpty() || getEnvs().containsAll(filter_environments));
    }

    public Vector<String> getProjectDescription() {
        return new Vector<>(Arrays.asList(projectDescription.split(";")));
    }

    public Vector<String> getLinkFlags() {
        return new Vector<>(Arrays.asList(linkFlags.split(" ")));
    }

    public Vector<String> getFortranFlags() {
        return new Vector<>(Arrays.asList(fortranFlags.split(" ")));
    }

    public Vector<String> getCFlags() {
        return new Vector<>(Arrays.asList(cFlags.split(" ")));
    }

    public Vector<String> getCppFlags() {
        return new Vector<>(Arrays.asList(cppFlags.split(" ")));
    }
}
