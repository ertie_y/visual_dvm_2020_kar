package GlobalData.RunStsRecord;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;

import static Common.UI.Tables.TableRenderers.RendererDate;
import static Common.UI.Tables.TableRenderers.RendererMultiline;
public class RunStsRecordsDBTable extends iDBTable<RunStsRecord> {
    public RunStsRecordsDBTable() {
        super(RunStsRecord.class);
    }
    @Override
    public String getDataDescription() {
        return "статистика запуска задачи";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(3).setRenderer(RendererMultiline);
                columns.get(17).setRenderer(RendererMultiline);
                columns.get(20).setRenderer(RendererDate);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "проект",
                "описание",
                "машина",
                //
                "сборка",
                "",
                //
                "Fortran",
                "",
                //
                "С",
                "",
                //
                "С++",
                "",
                //
                "запуск",
                "",
                "матрица",
                "аргументы",
                "окружение",
                //
                "время",
                "чистое время",
                "дата",
        };
    }
    @Override
    public Object getFieldAt(RunStsRecord object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.projectPath;
            case 3:
                return object.getProjectDescription();
            case 4:
                return object.machineURL;
            case 5:
                return object.linkerDescription;
            case 6:
                return object.linkFlags;
            case 7:
                return object.fortranCompilerDescription;
            case 8:
                return object.fortranFlags;
            case 9:
                return object.cCompilerDescription;
            case 10:
                return object.cFlags;
            case 11:
                return object.cppCompilerDescription;
            case 12:
                return object.cppFlags;
            case 13:
                return object.runCommandDescription;
            case 14:
                return object.runOptions;
            case 15:
                return object.matrix;
            case 16:
                return object.args;
            case 17:
                return object.getEnvs();
            case 18:
                return object.Time;
            case 19:
                return object.CleanTime;
            case 20:
                return object.getEndDate();
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.RunStsRecord;
    }
}
