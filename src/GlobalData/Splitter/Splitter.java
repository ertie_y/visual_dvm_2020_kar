package GlobalData.Splitter;
import Common.Database.DBObject;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;

import javax.swing.*;
public class Splitter extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String name = "";
    @Description("DEFAULT -1")
    public int position = Utils.Nan;
    @Override
    public Object getPK() {
        return name;
    }
    public Splitter(){

    }
    public Splitter(JSplitPane splitPane){
        name=splitPane.getName();
        position=splitPane.getDividerLocation();
    }
}
