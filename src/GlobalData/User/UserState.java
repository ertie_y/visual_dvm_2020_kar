package GlobalData.User;

import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;
import javafx.util.Pair;

import java.awt.*;

public enum UserState implements StatusEnum {
    initial, //на пользователя еще не заходили.
    ready,   //рудимент.
    ready_to_work;//пользователь готов к работе,все установлено.

    @Override
    public Font getFont() {
        return this == UserState.ready_to_work ? Current.getTheme().Fonts.get(VisualiserFonts.GoodState) : StatusEnum.super.getFont();
    }

    @Override
    public String getDescription() {
        switch (this) {
            case initial:
            case ready:
                return "не проинициализирован";
            case ready_to_work:
                return "готов к работе";
            default:
                return StatusEnum.super.getDescription();
        }
    }

}
