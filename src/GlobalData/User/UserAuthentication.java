package GlobalData.User;
public enum UserAuthentication {
    password,
    key;
    public String getDescription() {
        switch (this) {
            case password:
                return "по паролю";
            case key:
                return "по ключу";
        }
        return "";
    }
}
