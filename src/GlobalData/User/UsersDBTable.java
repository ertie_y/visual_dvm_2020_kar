package GlobalData.User;

import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableRenderers;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import GlobalData.Machine.MachineType;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.User.UI.UserFields;

import java.util.LinkedHashMap;

public class UsersDBTable extends iDBTable<User> {
    public UsersDBTable() {
        super(User.class);
    }

    @Override
    public String getDataDescription() {
        return "пользователь";
    }

    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(CompilationTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        res.put(RunTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.PASSIVE));
        return res;
    }

    @Override
    public DBObjectDialog<User, UserFields> getDialog() {
        return new DBObjectDialog<User, UserFields>(UserFields.class) {
            @Override
            public void SetEditLimits() {
                fields.tfLogin.setEditable(false);
            }

            @Override
            public void fillFields() {
                if (Current.getMachine().type.equals(MachineType.Local)) {
                    Result.login = "этот пользователь";
                    fields.tfLogin.setEditable(false);
                }
                fields.tfLogin.setText(Result.login);
                fields.tfPassword.setText(Result.password);
                fields.tfWorkspace.setText(Result.workspace);
            }

            @Override
            public void validateFields() {
                if (fields.tfLogin.getText().isEmpty())
                    Log.Writeln_("Логин пользователя не может быть пустым");
            }

            @Override
            public void ProcessResult() {
                Result.machine_id = Current.getMachine().id;
                Result.login = fields.tfLogin.getText();
                Result.authentication = UserAuthentication.password;
                Result.password = new String(fields.tfPassword.getPassword());
            }

            @Override
            public int getDefaultHeight() {
                return 250;
            }
        };
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this){
            @Override
            protected void AdditionalInitColumns() {
                columns.get(2).setRenderer(TableRenderers.RendererStatusEnum);
            }
        };
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{"Логин", "Статус"};
    }

    @Override
    public Object getFieldAt(User object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.login;
            case 2:
                return object.state;
            default:
                return null;
        }
    }

    @Override
    public Current CurrentName() {
        return Current.User;
    }
}
