package GlobalData.User;

import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import GlobalData.Machine.Machine;
import Visual_DVM_2021.Passes.SSH.ConnectionPass;
import javafx.util.Pair;

import java.awt.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class User extends iDBObject {
    public String login;
    public String password = "";
    public int machine_id = Utils.Nan;
    public UserAuthentication authentication = UserAuthentication.password;
    public String workspace = ""; //рабочая папка визуализатора пользователя на машине. полный путь.
    public UserState state = UserState.initial;

    public User(Machine machine_in, String login_in, String password_in) {
        machine_id = machine_in.id;
        login = login_in;
        password = password_in;
    }
    public User( String login_in, String password_in) {
        login = login_in;
        password = password_in;
    }


    public User() {
    }

    @Override
    public boolean isVisible() {
        return Current.HasMachine() && Current.getMachine().id == machine_id;
    }

    @Override
    public String getDialogName() {
        return login;
    }

    public String getRemoteProjectsPath() {
        return workspace + "/projects";
    }

    //-ЛОКАЛЬНЫЙ СЛУЧАЙ-----------------------------------------------------------------------
    public File getLocalWorkspace() {
        return new File(workspace);
    }

    public File getLocalProjectsDir() {
        return Paths.get(workspace, "projects").toFile();
    }

    public File getLocalModulesDir() {
        return Paths.get(workspace, "modules").toFile();
    }

    public File getHeaderCodeFile() {
        return Paths.get(workspace, "modules", ConnectionPass.Process_r_header).toFile();
    }

    public File getStarterCodeFile() {
        return Paths.get(workspace, "modules", ConnectionPass.starter_code).toFile();
    }

    public File getStarterFile() {
        return Paths.get(workspace, "modules", ConnectionPass.starter).toFile();
    }

    public File getLauncherCodeFile() {
        return Paths.get(workspace, "modules", ConnectionPass.launcher_code).toFile();
    }

    public File getLauncherFile() {
        return Paths.get(workspace, "modules", ConnectionPass.launcher).toFile();
    }
}
