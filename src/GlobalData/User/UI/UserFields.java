package GlobalData.User.UI;
import Common.UI.Menus.TextEditorMenu;
import Common.UI.TextField.StyledPasswordField;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;
import Common.Utils.Utils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class UserFields implements DialogFields {
    public JPanel content;
    public JTextField tfLogin;
    public JTabbedPane AuthenticationTabs;
    public JPanel PasswordTab;
    public JPasswordField tfPassword;
    public JTextField tfWorkspace;
    char mask;
    char unmask = (char) 0;
    boolean password_visible = false;
    private JButton bPasswordVisibility;
    public UserFields() {
        mask = tfPassword.getEchoChar();
        bPasswordVisibility.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                password_visible = !password_visible;
                tfPassword.setEchoChar(password_visible ? unmask : mask);
                bPasswordVisibility.setIcon(Utils.getIcon("/icons/"+(password_visible ? "Show" : "Hide") + "Password.png"));
            }
        });
    }
    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfLogin=new StyledTextField();
        tfPassword=new StyledPasswordField();
        tfPassword.setComponentPopupMenu(new TextEditorMenu(tfPassword));
        tfWorkspace = new StyledTextField();
    }
}
