package GlobalData.CompilerOption;

import Common.Database.DataSet;
import Common.UI.DataSetControlForm;
import Common.UI.Tables.TableEditors;
import Common.UI.Tables.TableRenderers;

import static Common.UI.Tables.TableRenderers.*;

public class CompilerOptionsSet extends DataSet<String, CompilerOption> {
    public CompilerOptionsSet() {
        super(String.class, CompilerOption.class);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Параметр","Значение", "Описание"
        };
    }
    @Override
    public Object getFieldAt(CompilerOption object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.parameterName+object.parameterSeparator;
            case 3:
                return object.parameterValue;
            case 4:
                return object.description;
            default:
                return null;
        }
    }


    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(2).setRenderer(TableRenderers.RendererCompilerOptionParameterName);
                columns.get(3).setRenderer(TableRenderers.RendererCompilerOptionParameterValue);
                columns.get(3).setEditor(TableEditors.EditorCompilerOptionParameterValue);
                columns.get(4).setRenderer(RendererMultiline);
            }
        };
    }
}
