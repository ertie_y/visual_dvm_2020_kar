package GlobalData.CompilerOption.UI;

import Common.Current;
import Common.UI.Tables.DBObjectRenderer;
import Common.UI.Themes.VisualiserFonts;
import GlobalData.CompilerOption.CompilerOption;

import javax.swing.*;

public class CompilerOptionParameterValueRenderer extends DBObjectRenderer {

    @Override
    public void Display() {
        if (value != null) {
            CompilerOption option = (CompilerOption) value;
            if (option.hasParameter()) {
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
                setText(option.parameterValue.isEmpty()?"не задано":option.parameterValue);

            } else {
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.UnknownState));
                setText("нет");
            }
        }
    }
}
