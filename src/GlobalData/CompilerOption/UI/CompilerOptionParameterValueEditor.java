package GlobalData.CompilerOption.UI;

import Common.Current;
import Common.UI.Tables.DBObjectEditor;
import Common.UI.Themes.VisualiserFonts;
import Common.UI.UI;
import Common.UI.Windows.Dialog.Text.ComboTextDialog;
import GlobalData.CompilerOption.CompilerOption;

import javax.swing.*;

public class CompilerOptionParameterValueEditor extends DBObjectEditor<CompilerOption> {

    @Override
    public void Action() {

        if (value.hasParameter()) {
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.Hyperlink));
            //-
            setText(value.parameterValue.isEmpty() ? "не задано" : value.parameterValue);
            //-
            ComboTextDialog dialog = new ComboTextDialog();
            if (!value.parameterValue.isEmpty())
                UI.TrySelect(dialog.fields, value.parameterValue);
            dialog.fields.setEditable(value.parameterVariants.isEmpty());
            if (dialog.ShowDialog("Изменить значение опции " + value.name,
                    value.parameterVariants)) {
                value.parameterValue = dialog.Result;
                setText(value.parameterValue.isEmpty() ? "не задано" : value.parameterValue);
            }
        } else {
            setFont(Current.getTheme().Fonts.get(VisualiserFonts.UnknownState));
            setText("нет");
        }
    }

    @Override
    public Object getCellEditorValue() {
        return value.parameterValue;
    }
}
