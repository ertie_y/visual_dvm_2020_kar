package GlobalData.CompilerOption.UI;

import Common.Current;
import Common.UI.Tables.DBObjectRenderer;
import Common.UI.Themes.VisualiserFonts;
import GlobalData.CompilerOption.CompilerOption;

public class CompilerOptionParameterNameRenderer extends DBObjectRenderer {
    @Override
    public void Display() {
        if (value != null) {
            CompilerOption option = (CompilerOption) value;
            if (option.hasParameter()) {
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.TreePlain));
                setText(option.parameterName);

            } else {
                setFont(Current.getTheme().Fonts.get(VisualiserFonts.UnknownState));
                setText("нет");
            }
        }
    }
}
