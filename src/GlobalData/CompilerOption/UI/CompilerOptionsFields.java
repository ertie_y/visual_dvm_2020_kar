package GlobalData.CompilerOption.UI;

import Common.Current;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;

public class CompilerOptionsFields implements DialogFields {
    private JPanel content;

    @Override
    public Component getContent() {
        return content;
    }
    public CompilerOptionsFields(){
        Current.getDialogCompiler().options.mountUI(content);
        Current.getDialogCompiler().options.ShowUI();
    }
}
