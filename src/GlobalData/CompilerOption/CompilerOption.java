package GlobalData.CompilerOption;

import Common.Database.DBObject;
import Common.UI.UI;
import Common.Utils.Utils;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Arrays;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CompilerOption extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String name = "";
    public String parameterSeparator = "";
    public String parameterName = "";
    public String parameterValue = "";
    public Vector<String> parameterVariants = new Vector<>();
    public Vector<String> description = new Vector<>();
    //--------------------------------------
    public boolean hasParameter() {
        return !parameterName.isEmpty();
    }

    @Override
    public Object getPK() {
        return name;
    }

    @Override
    public String toString() {
        return name + (hasParameter()? (parameterSeparator +
                (parameterValue.contains(" ")? Utils.DQuotes(parameterValue):parameterValue)):"");
    }

    public void CheckParameterVariants() {
        for (String line : description)
            if (CheckLine(line)) break;
    }

    public boolean CheckLine(String line) {
        if (hasParameter()) {
            Pattern DVM_PARAM_VALUES_REGEX = Pattern.compile(Utils.TBrackets(parameterName)+ "\\s*=\\s*"+ "\\w+(\\|\\w+)+",Pattern.CASE_INSENSITIVE);
            Matcher matcher = DVM_PARAM_VALUES_REGEX.matcher(line);
            if (matcher.find()){
                String s = line.substring(matcher.start(), matcher.end());
                String packed = s.substring(s.lastIndexOf('=')+1).trim();
                parameterVariants = new Vector<>(Arrays.asList(packed.split("\\|")));
                return true;
            }
        }
        return false;
    }
}
