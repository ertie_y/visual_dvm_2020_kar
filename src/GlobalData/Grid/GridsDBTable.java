package GlobalData.Grid;

import Common.Current;
import Common.Database.DBTable;

public class GridsDBTable extends DBTable<Current, Grid> {
    public GridsDBTable() {
        super(Current.class, Grid.class);
    }
}
