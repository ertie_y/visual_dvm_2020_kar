package GlobalData.Grid;

import Common.Current;
import Common.Database.DBObject;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Arrays;
import java.util.Vector;
import java.util.stream.Collectors;

public class Grid extends DBObject {
    @Description("PRIMARY KEY, UNIQUE") //имя таблицы
    public Current name = Current.Undefined;
    @Description("DEFAULT ''")
    public String sizes="";  //ширины столбцов запакованные через |. вводить объекты ради них нецелесообразно.

    @Override
    public Object getPK() {
        return name;
    }
    public Vector<Integer> unpack(){
        return Arrays.stream(sizes.split("\\|")).map(Integer::parseInt).collect(Collectors.toCollection(Vector::new));

    }
    public Grid(){}
    public Grid(Current name_in){
        name = name_in;
        sizes="";
    }
}
