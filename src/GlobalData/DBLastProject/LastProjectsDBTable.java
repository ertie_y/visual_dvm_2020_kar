package GlobalData.DBLastProject;
import Common.Database.DBTable;

import java.util.Comparator;
import java.util.Vector;
class SortByDate implements Comparator<DBLastProject> {
    public int compare(DBLastProject p1, DBLastProject p2) {
        return (int) (p2.lastOpened - p1.lastOpened);
    }
}
public class LastProjectsDBTable extends DBTable<String, DBLastProject> {
    public LastProjectsDBTable() {
        super(String.class, DBLastProject.class);
    }
    public Vector<DBLastProject> getOrdered() {
        return getOrderedRecords(new SortByDate());
    }
    @Override
    public String getDataDescription() {
        return "путь к проекту";
    }
}
