package GlobalData.DBLastProject.UI;
import Common.Global;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import GlobalData.DBLastProject.DBLastProject;
import GlobalData.Settings.SettingName;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.File;
import java.util.Vector;
public class LastProjectsMenu extends StyledPopupMenu {
    @Override
    public void CheckElementsVisibility() {
        this.removeAll();
        Vector<DBLastProject> projects = Global.db.lastProjects.getOrdered();
        int k = 1;
        for (DBLastProject p : projects) {
            if (new File(p.HomePath).exists()) {
                StyledMenuItem i = new StyledMenuItem(p.HomePath);
                i.addActionListener(new AbstractAction() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        Pass_2021.passes.get(PassCode_2021.OpenCurrentProject).Do(new File(p.HomePath));
                    }
                });
                add(i);
                ++k;
                if (k > (Global.db.settings.get(SettingName.LastOpenedProjectsCount).toInt32())) break;
            }
        }
        addSeparator();
        add(Pass_2021.passes.get(PassCode_2021.DropLastProjects).getMenuItem());
        super.CheckElementsVisibility();
    }
}