package GlobalData.DBLastProject;
import Common.Database.DBObject;
import Common.Utils.Utils;
import ProjectData.Project.db_project_info;
import com.sun.org.glassfish.gmbal.Description;

import java.util.Date;
public class DBLastProject extends DBObject {
    @Description("PRIMARY KEY, UNIQUE")
    public String HomePath = "";
    public long lastOpened = 0;
    //-NEW--------------------------------
    @Description("DEFAULT -1")
    public int makefile_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int run_configuration_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int machine_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int user_id = Utils.Nan;
    //-------------------------------------
    @Description("DEFAULT -1")
    public int compilation_task_id = Utils.Nan;
    @Description("DEFAULT -1")
    public int run_task_id = Utils.Nan;
    //-------------------------------------
    //-------------------------------------
    public DBLastProject() {
    }
    public DBLastProject(db_project_info project) {
        HomePath = project.Home.getAbsolutePath();
        RefreshOpenTime();
    }
    public void RefreshOpenTime() {
        lastOpened = new Date().toInstant().getEpochSecond();
    }
    @Override
    public Object getPK() {
        return HomePath;
    }
}