package GlobalData;
import Common.Current;
import Common.Database.SQLITE.SQLiteDatabase;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Account.Account;
import GlobalData.Account.AccountsDBTable;
import GlobalData.Compiler.CompilersDBTable;
import GlobalData.DBLastProject.DBLastProject;
import GlobalData.DBLastProject.LastProjectsDBTable;
import GlobalData.DVMParameter.DVMParameterDBTable;
import GlobalData.EnvironmentValue.EnvironmentValuesDBTable;
import GlobalData.FormsParams.FormsDBTable;
import GlobalData.FormsParams.MainFormParamsDBTable;
import GlobalData.Grid.GridsDBTable;
import GlobalData.Machine.Machine;
import GlobalData.Machine.MachinesDBTable;
import GlobalData.Makefile.Makefile;
import GlobalData.Makefile.MakefilesDBTable;
import GlobalData.Module.ModulesDBTable;
import GlobalData.Scenario.ScenarioDBTable;
import GlobalData.ScenarioCommand.ScenarioCommandsDBTable;
import Visual_DVM_2021.PassStats.PassStatsDBTable;
import GlobalData.Recipient.RecipientsDBTable;
import GlobalData.RunConfiguration.RunConfiguration;
import GlobalData.RunConfiguration.RunConfigurationsDBTable;
import GlobalData.RunStsRecord.RunStsRecordsDBTable;
import GlobalData.Settings.SettingsDBTable;
import GlobalData.Splitter.SplittersDBTable;
import GlobalData.Tasks.CompilationTask.CompilationTasksDBTable;
import GlobalData.Tasks.RunTask.RunTasksDBTable;
import GlobalData.User.User;
import GlobalData.User.UsersDBTable;
import Visual_DVM_2021.Properties.PropertyName;

import java.nio.file.Paths;
public class GlobalDatabase extends SQLiteDatabase {
    //---------СЕАНС----------------------------------------------
    public MachinesDBTable machines;
    public UsersDBTable users;
    public CompilersDBTable compilers;
    public MakefilesDBTable makefiles;
    public ModulesDBTable modules;
    public CompilationTasksDBTable compilationTasks;
    public RunTasksDBTable runTasks;
    public RunConfigurationsDBTable runConfigurations;
    public EnvironmentValuesDBTable environmentValues;
    public DVMParameterDBTable dvmParameters;
    public RunStsRecordsDBTable runStsRecords;
    //----- ДАННЫЕ ВИЗУАЛИЗАТОРА----------------------------------
    public FormsDBTable forms;
    public MainFormParamsDBTable mainFormParams;
    public SettingsDBTable settings;
    public LastProjectsDBTable lastProjects;
    public AccountsDBTable accounts;
    public RecipientsDBTable recipients;
    public PassStatsDBTable passStats;
    public SplittersDBTable splitters;
    public ScenarioDBTable scenarios;
    public ScenarioCommandsDBTable scenarioCommands;
    public GridsDBTable grids;
    //-
    public GlobalDatabase() {
        super(Paths.get(System.getProperty("user.dir"), "Data", Global.properties.get(PropertyName.GlobalDBName)).toFile());
    }
    @Override
    protected void initAllTables() throws Exception {
        addTable(machines = new MachinesDBTable());
        addTable(users = new UsersDBTable());
        addTable(compilers = new CompilersDBTable());
        addTable(makefiles = new MakefilesDBTable());
        addTable(modules = new ModulesDBTable());
        addTable(compilationTasks = new CompilationTasksDBTable());
        addTable(runTasks = new RunTasksDBTable());
        addTable(runConfigurations = new RunConfigurationsDBTable());
        addTable(environmentValues = new EnvironmentValuesDBTable());
        addTable(runStsRecords = new RunStsRecordsDBTable());
        addTable(forms = new FormsDBTable());
        addTable(settings = new SettingsDBTable());
        addTable(mainFormParams = new MainFormParamsDBTable());
        addTable(lastProjects = new LastProjectsDBTable());
        addTable(accounts = new AccountsDBTable());
        addTable(recipients = new RecipientsDBTable());
        addTable(passStats = new PassStatsDBTable());
        addTable(splitters = new SplittersDBTable());
        addTable(scenarios = new ScenarioDBTable());
        addTable(scenarioCommands = new ScenarioCommandsDBTable());
        addTable(dvmParameters = new DVMParameterDBTable());
        addTable(grids = new GridsDBTable());
    }
    //---------------------------------------------------------------------------------
    @Override
    public void Init() throws Exception {
        Current.set(Current.Account,
                accounts.Data.isEmpty() ? Insert(new Account()) :
                        accounts.getFirstRecord()
        );
        //настройки компонент
        settings.AddAll();
        runConfigurations.Patch();
    }
    //<editor-fold desc="каскадные обновления в связи с удалением">
    public void DropProjectsMachine(Machine machine) throws Exception {
        for (DBLastProject project : lastProjects.Data.values()) {
            if (project.machine_id == machine.id) {
                //-------------------------------
                project.machine_id = Utils.Nan;
                project.user_id = Utils.Nan;
                project.makefile_id = Utils.Nan;
                project.run_configuration_id = Utils.Nan;
                project.compilation_task_id = Utils.Nan;
                project.run_task_id = Utils.Nan;
                //--------------------------------
                Update(project);
            }
        }
    }
    public void DropProjectsUser(User user) throws Exception {
        for (DBLastProject project : lastProjects.Data.values()) {
            if (project.user_id == user.id) {
                //-------------------------------
                project.user_id = Utils.Nan;
                project.compilation_task_id = Utils.Nan;
                project.run_task_id = Utils.Nan;
                //--------------------------------
                Update(project);
            }
        }
    }
    public void DropProjectsMakefile(Makefile makefile) throws Exception {
        for (DBLastProject project : lastProjects.Data.values()) {
            if (project.makefile_id == makefile.id) {
                //-------------------------------
                project.makefile_id = Utils.Nan;
                project.compilation_task_id = Utils.Nan;
                project.run_task_id = Utils.Nan; //раз сборка уничтожена значит остальное смысла не имеет
                //--------------------------------
                Update(project);
            }
        }
    }
    public void DropProjectsRunConfiguration(RunConfiguration configuration) throws Exception {
        for (DBLastProject project : lastProjects.Data.values()) {
            if (project.run_configuration_id == configuration.id) {
                //-------------------------------
                project.run_configuration_id = Utils.Nan;
                project.run_task_id = Utils.Nan;
                //--------------------------------
                Update(project);
            }
        }
    }
    //</editor-fold>
}
