package GlobalData.Compiler.UI;

import Common.Current;
import Common.Passes.Pass;
import Common.Passes.PassCode;
import Common.UI.TextField.StyledTextField;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DialogFields;
import Common.Utils.Files.VDirectoryChooser;
import GlobalData.Compiler.CompilerType;
import GlobalData.Machine.MachineType;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.io.File;
import java.util.Objects;

public class CompilerFields implements DialogFields {
    public JPanel content;
    public JTextField tfHome;
    public JButton bBrowse;
    public JTextField tfDescription;
    public JTextField tfCallCommand;
    public JTextField tfVersionCommand;
    public JTextField tfHelpCommand;
    public JComboBox<CompilerType> cbCompilerType;
    public boolean events_on = false;
    VDirectoryChooser directoryChooser = new VDirectoryChooser("Выбор домашней папки dvm системы");

    public CompilerFields() {
        bBrowse.addActionListener(e -> {

            CompilerType type = (CompilerType) cbCompilerType.getSelectedItem();
            if (type == CompilerType.dvm) {
                String dst = null;
                if (Current.getMachine().type.equals(MachineType.Local)) {
                    File file = directoryChooser.ShowDialog();
                    if (file != null)
                        dst = file.getAbsolutePath();
                } else {
                    if (Pass_2021.passes.get(PassCode_2021.SelectRemoteFile).Do(true))
                        dst = Current.getRemoteFile().full_name;
                }
                if (dst != null)
                    tfHome.setText(dst);
            } else
                UI.Info("Назначение домашней папки поддерживается только для dvm системы.");


        });
        tfHome.getDocument().addDocumentListener(new DocumentListener() {
                                                     @Override
                                                     public void insertUpdate(DocumentEvent e) {
                                                         TryRestoreCallCommand();
                                                     }

                                                     @Override
                                                     public void removeUpdate(DocumentEvent e) {
                                                         TryRestoreCallCommand();
                                                     }

                                                     @Override
                                                     public void changedUpdate(DocumentEvent e) {
                                                     }
                                                 }
        );
        cbCompilerType.addActionListener(e -> {
            if (Objects.requireNonNull(cbCompilerType.getSelectedItem()) == CompilerType.dvm) {
                tfVersionCommand.setText("ver");
                tfHelpCommand.setText("help");
            } else {
                tfVersionCommand.setText("--version");
                tfHelpCommand.setText("--help");
            }
        });
    }

    public void TryRestoreCallCommand() {
        if (events_on && (cbCompilerType.getSelectedItem() != null) && cbCompilerType.getSelectedItem().equals(CompilerType.dvm))
            tfCallCommand.setText(tfHome.getText() +
                    (tfHome.getText().endsWith("/") ? "" : "/") +
                    "bin/dvm_drv");
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfDescription = new StyledTextField();
        tfHome = new StyledTextField();
        tfCallCommand = new StyledTextField();
        tfVersionCommand = new StyledTextField();
        tfHelpCommand = new StyledTextField();


        cbCompilerType = new JComboBox<>();
        cbCompilerType.addItem(CompilerType.dvm);
        cbCompilerType.addItem(CompilerType.intel);
        cbCompilerType.addItem(CompilerType.gnu);
    }

    @Override
    public Component getContent() {
        return content;
    }
}
