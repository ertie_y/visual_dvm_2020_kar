package GlobalData.Compiler;

import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import Common.Utils.Validators.DVMHelpParser;
import GlobalData.CompilerEnvironment.CompilerEnvironmentsSet;
import GlobalData.CompilerOption.CompilerOption;
import GlobalData.CompilerOption.CompilerOptionsSet;
import GlobalData.Machine.Machine;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;
import com.sun.org.glassfish.gmbal.Description;

import java.util.LinkedHashMap;

public class Compiler extends iDBObject {
    public int machine_id = -1;
    public String description = "";
    public CompilerType type = CompilerType.dvm;
    public String home_path = ""; //домашняя папка. нужна для двм компиляторов, в теории и для остальных может сгодиться, например для установки окружения ifort под виндой
    public String call_command = ""; //непосредственно вызов компилятора.
    public String version_command = "";//команда запроса версии компилятора.
    public String help_command = "";// команда запроса help
    //-
    @Description("IGNORE")
    public boolean helpLoaded = false;
    @Description("IGNORE")
    public String helpText = "";
    public CompilerOptionsSet options = new CompilerOptionsSet();
    public CompilerEnvironmentsSet environments = new CompilerEnvironmentsSet();

    public void ResetHelp() {
        helpLoaded = false;
        helpText = "";
        options.clear();
        environments.clear();
    }

    public void ParseHelp() {
        switch (type) {
            case dvm:
                DVMHelpParser.ReadOptions(this);
                break;
            default:
                break;
        }
    }

    //-
    public Compiler() {
    }

    public Compiler(Machine machine,
                    String description_in,
                    CompilerType type_in,
                    String call_command_in,
                    String version_command_in,
                    String help_command_in
    ) {
        machine_id = machine.id;
        description = description_in;
        type = type_in;
        call_command = call_command_in;
        version_command = version_command_in;
        help_command = help_command_in;
    }

    @Override
    public boolean isVisible() {
        return Current.HasMachine() && Current.getMachine().id == machine_id;
    }

    //todo понять как извлекать версию чтобы выдавать нормальную инфу.
    @Override
    public String toString() {
        return call_command;
    }

    public String getStyleOptions(DBProjectFile program) {
        String res = "";
        switch (type) {
            case gnu:
                switch (program.languageName) {
                    case fortran:
                        switch (program.style) {
                            case free:
                                res = "-ffree-form";
                                break;
                            case extended:
                                res = "-ffixed-line-length-132";
                                break;
                            case fixed:
                                res = "-ffixed-form";
                                break;
                            case none:
                                break;
                        }
                        break;
                }
                break;
            case dvm:
                switch (program.languageName) {
                    case fortran:
                        switch (program.style) {
                            case fixed:
                            case extended:
                                res = "-FI";
                                break;
                            case free:
                                res = "-f90";
                                break;
                            case none:
                                break;
                        }
                        break;
                }
                break;
            case intel:
                switch (program.languageName) {
                    case fortran:
                        switch (program.style) {
                            case fixed:
                            case extended:
                                res = "-fixed";
                                break;
                            case free:
                                res = "-free";
                                break;
                            case none:
                                break;
                        }
                        break;
                }
                break;
        }
        return res;
    }

    //todo как то извлекать/заполнять версии компилятора?
    public String getDescription() {
        return description;
    }

    public String getSpecialLinkCommand(LanguageName languageName) {
        switch (type) {
            case dvm:
                switch (languageName) {
                    case fortran:
                        return "flink";
                    case c:
                        return "clink";
                }
                break;
        }
        return "";
    }

    public String getSpecialCompilationCommand(LanguageName languageName) {
        switch (type) {
            case dvm:
                switch (languageName) {
                    case fortran:
                        return "f";
                    case c:
                        return "c";
                }
                break;
        }
        return "";
    }

    public String getHelpCommand() {
        return Utils.DQuotes(call_command) + " " + help_command;
    }
}
