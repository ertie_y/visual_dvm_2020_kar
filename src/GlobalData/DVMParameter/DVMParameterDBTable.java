package GlobalData.DVMParameter;

import Common.Current;
import Common.Database.iDBTable;
import Common.Global;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.Utils.Utils;
import GlobalData.DVMParameter.UI.DVMParameterFields;

public class DVMParameterDBTable extends iDBTable<DVMParameter> {
    public DVMParameterDBTable() {
        super(DVMParameter.class);
    }
    @Override
    public String getDataDescription() {
        return "параметр DVM системы";
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{"имя", "значение"};
    }
    @Override
    public Object getFieldAt(DVMParameter object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.name;
            case 2:
                return object.value;
        }
        return null;
    }
    @Override
    public Current CurrentName() {
        return Current.DVMParameterValue;
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public DBObjectDialog<DVMParameter, DVMParameterFields> getDialog() {
        return new DBObjectDialog<DVMParameter, DVMParameterFields>(DVMParameterFields.class) {
            @Override
            public void fillFields() {
               UI.TrySelect(fields.cbName, Result.name);
               fields.tfValue.setText(Result.value);
            }
            //https://javarush.ru/groups/posts/regulyarnye-vyrazheniya-v-java
            @Override
            public void validateFields() {
                String name = (String) fields.cbName.getSelectedItem();
                String value = fields.tfValue.getText();
                if (name.isEmpty())
                    Log.Writeln("Имя параметра DVM системы не может быть пустым.");
                /*
                if (!name.matches("\\w*")) {
                    Log.Writeln("Имя переменной окружения может содержать только латинские буквы, цифры и подчёркивания");
                }
                 */
                if (Utils.isLinuxSystemCommand(name))
                    Log.Writeln(Utils.DQuotes(name) + " является системной командой Linux,\nи не может быть задано в качестве имени переменной окружения.");

               /*
                if (value.contains("\"")) {
                    Log.Writeln("Значение переменной окружения не может содержать двойные кавычки");
                }
                */
                for (DVMParameter par : Global.db.dvmParameters.Data.values()) {
                    if (par.isVisible() && (Result.id != par.id) && (par.name.equals(name))) {
                        Log.Writeln("В конфигурации запуска уже задан параметр DVM системы  с именем " + Utils.Brackets(name));
                        break;
                    }
                }
            }
            @Override
            public void ProcessResult() {
                Result.machine_id = Current.getMachine().id;
                Result.run_configuration_id = Current.getRunConfiguration().id;
                Result.name = (String) fields.cbName.getSelectedItem();
                Result.value = fields.tfValue.getText();
            }
            @Override
            public int getDefaultHeight() {
                return 200;
            }
        };
    }
}
