package GlobalData.DVMParameter;

import GlobalData.EnvironmentValue.EnvironmentValue;

public class DVMParameter extends EnvironmentValue {
    @Override
    public String toString() {
        return name + "=" + value + ";";
    }
}
