package GlobalData.Tasks;

import Common.Database.iDBObject;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Machine.Machine;
import GlobalData.User.User;
import ProjectData.Project.db_project_info;
import com.sun.org.glassfish.gmbal.Description;
import javafx.util.Pair;

import javax.rmi.CORBA.Util;
import java.awt.*;
import java.io.File;
import java.nio.file.Paths;
import java.util.Date;

public abstract class Task extends iDBObject {
    //<editor-fold desc="файловые константы">
    public final static String DONE = "DONE";
    public final static String TIMEOUT = "TIMEOUT";
    public final static String out_file = "out.txt";
    public final static String err_file = "err.txt";
    public final static String time_file = "total_time";
    //</editor-fold>
    public TaskState state = TaskState.Inactive;
    //----------------------------------
    public int machine_id = Utils.Nan;
    public int user_id = Utils.Nan;
    //-----------------------------------
    public String PID = "";
    public String project_path;// путь к проекту.
    public String project_description; // краткое описание(чтобы не лезть в бд целевого проекта). только для таблицы.
    public int maxtime = 40;
    //результаты-------------------------------
    public double Time; //время выполнения.
    public long StartDate = 0; //дата начала выполнения
    public long EndDate = 0;//дата окончания выполнения

    public boolean belongsToProject(db_project_info project) {
        return this.project_path.equalsIgnoreCase(project.Home.getAbsolutePath());
    }

    public void DropResults() throws Exception {
        Utils.forceDeleteWithCheck(getOutputFile());
        Utils.forceDeleteWithCheck(getErrorsFile());
        //-
        StartDate = 0;
        EndDate = 0;
        Time = 0;
        state = TaskState.Inactive;
        Global.db.Update(this, "StartDate");
        Global.db.Update(this, "EndDate");
        Global.db.Update(this, "Time");
        Global.db.Update(this, "state");
    }

    //</editor-fold>
    //<editor-fold desc="локальные файлы">
    public abstract File getHome();

    public File getLocalWorkspace() {
        return Paths.get(getHome().getAbsolutePath(), String.valueOf(id)).toFile();
    }

    public File getOutputFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), out_file).toFile();
    }

    public File getErrorsFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), err_file).toFile();
    }

    public File getTimeFile() {
        return Paths.get(getLocalWorkspace().getAbsolutePath(), time_file).toFile();
    }

    public abstract String getFullCommand();

    public Date getEndDate() {
        return new Date(EndDate);
    }

    public Machine getMachine() {
        return Global.db.getById(Machine.class, machine_id);
    }

    public User getUser() {
        return Global.db.getById(User.class, user_id);
    }

    protected String getTextResult(File file) {
        return (file.exists()) ? Utils.ReadAllText(file) : "файл не найден. Задача еще не выполнялась или была завершена некорректно";
    }

    //подразумевается, что выходные потоки задачи видны только при открытом проекте
    public String getOutput() {
        return getTextResult(getOutputFile());
    }

    public String getErrors() {
        return getTextResult(getErrorsFile());
    }

    public void RefreshTime() {
        Time = Double.parseDouble(Utils.ReadAllText(getTimeFile()));
    }

    public void MaximizeTime() {
        Time = maxtime + 1;
    }

    public void UpdateMaxtime(int maxtime_in) {
        if (maxtime != maxtime_in) {
            maxtime = maxtime_in;
            try {
                Global.db.Update(this, "maxtime");
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    }

    public void UpdateState(TaskState state_in) {
        if (state != state_in) {
            state = state_in;
            try {
                Global.db.Update(this, "state");
            } catch (Exception ex) {
                Global.Log.PrintException(ex);
            }
        }
    }

    public void Reset() {
        PID = "";
        StartDate = 0;
        EndDate = 0;
        state = TaskState.Inactive;
    }

    //---------------------------------
    @Description("IGNORE")
    public int progressStep = Utils.Nan;
    @Description("IGNORE")
    public int progressAll = Utils.Nan;

    public void setProgress(int progressStep_in, int progressAll_in) {
        progressStep = progressStep_in;
        progressAll = progressAll_in;
    }

    public void dropProgress() {
        progressStep = Utils.Nan;
        progressAll = Utils.Nan;
    }

    public boolean hasProgress() {
        return (progressStep != Utils.Nan) && (progressAll != Utils.Nan);
    }

    //---------------------------------
    public void AnalyzeResultsTexts(db_project_info project) throws Exception {
        AnalyzeOutFile(project);
        AnalyzeErrorsFile(project);
    }

    public void AnalyzeOutFile(db_project_info project) throws Exception {
    }

    public void AnalyzeErrorsFile(db_project_info project) throws Exception {
    }

    public boolean isPassive() {
        return (state != TaskState.Queued) && (state != TaskState.Running);
    }
}
