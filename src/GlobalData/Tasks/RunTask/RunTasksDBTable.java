package GlobalData.Tasks.RunTask;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.Menus.DataTableMenu;
import Common.UI.UI;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import static Common.UI.Tables.TableRenderers.*;

public class RunTasksDBTable extends iDBTable<RunTask> {
    public RunTasksDBTable() {
        super(RunTask.class);
    }
    @Override
    public String getDataDescription() {
        return "задача на запуск";
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {

            @Override
            public boolean hasCheckBox() {
                return true;
            }
            @Override
            protected void AdditionalInitColumns() {
                columns.get(6).setRenderer(RendererDate);
                columns.get(7).setRenderer(RendererStatusEnum);
            }
            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                UI.getNewMainWindow().getTestingWindow().ShowCurrentRunTask();
            }

            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                UI.getNewMainWindow().getTestingWindow().ShowNoCurrentRunTask();
            }

            @Override
            public void CreateControl() {
                super.CreateControl();
                DataTableMenu dataTableMenu = new DataTableMenu(control, dataSource, true);
                dataTableMenu.add(Pass_2021.passes.get(PassCode_2021.DeleteSelectedRunTasks).getMenuItem());
                control.setComponentPopupMenu(dataTableMenu);
            }
        };
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Матрица",
                "Лимит(с)",
                "Время(с)",
                "Чистое время",
                "Дата",
                "Статус"};
    }
    @Override
    public Object getFieldAt(RunTask object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.matrix;
            case 3:
                return object.maxtime;
            case 4:
                return object.Time;
            case 5:
                return object.CleanTime;
            case 6:
                return object.getEndDate();
            case 7:
                return object.state;
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.RunTask;
    }
}
