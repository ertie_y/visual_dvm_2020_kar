package GlobalData.Tasks.RunTask.UI;

import Common.Current;
import Common.Utils.Utils;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.UI.Main.TaskComparsionForm;

public class RunTaskComparsionForm extends TaskComparsionForm<RunTask> {
    @Override
    protected Current getDefaultTaskType() {
        return Current.RunTask;
    }

    public RunTaskComparsionForm(TaskComparsionForm<RunTask> slave_in) {
        super(slave_in);
    }

    @Override
    public void setIcons() {
        outputTabs.setIconAt(0, Utils.getIcon("/icons/RunOutput.png"));
        outputTabs.setIconAt(1, Utils.getIcon("/icons/RunErrors.png"));
    }

    @Override
    public void showTask(RunTask task_in) {
        super.showTask(task_in);
        stsBody.setText(task.getLocalStsText());
        mMatrix.setText(task.matrix);
        mCleanTime.setText(String.valueOf(task.CleanTime));
    }

    @Override
    public void Clear() {
        super.Clear();
        stsBody.setText("");
        mMatrix.setText("?");
        mCleanTime.setText("?");
    }
}
