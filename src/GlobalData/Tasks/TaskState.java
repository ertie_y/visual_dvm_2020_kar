package GlobalData.Tasks;

import Common.Current;
import Common.UI.StatusEnum;
import Common.UI.Themes.VisualiserFonts;
import javafx.util.Pair;

import java.awt.*;

public enum TaskState implements StatusEnum {
    Inactive,
    Running,
    Finished,
    AbortedByTimeout,
    Done,
    DoneWithErrors,
    AbortingByUser,
    AbortedByUser,
    //для кластеров
    Queued,
    NoSuchTask,
    FailedToQueue,
    //-
    Crushed,
    //-
    Waiting;

    public String getDescription() {
        switch (this) {
            case FailedToQueue:
                return "не удалось поставить в очередь";
            case NoSuchTask:
                return "не существует";
            case Queued:
                return "в очереди";
            case AbortingByUser:
                return "прерывание...";
            case AbortedByUser:
                return "прервана";
            case Inactive:
                return "неактивно";
            case Running:
                return "выполняется";
            case Finished:
                return "завершено";
            case AbortedByTimeout:
                return "превышен лимит";
            case Done:
                return "успешно";
            case DoneWithErrors:
                return "с ошибками";
            case Crushed:
                return "падение";
            case Waiting:
                return "ожидание";
            default:
                return StatusEnum.super.getDescription();
        }

    }
    @Override
    public Font getFont() {
        switch (this) {
            case FailedToQueue:
            case NoSuchTask:
            case AbortedByUser:
            case AbortedByTimeout:
            case DoneWithErrors:
                return Current.getTheme().Fonts.get(VisualiserFonts.BadState);
            case Queued:
            case Running:
                return Current.getTheme().Fonts.get(VisualiserFonts.ProgressState);
            case Done:
                return Current.getTheme().Fonts.get(VisualiserFonts.GoodState);
            case Crushed:
                return Current.getTheme().Fonts.get(VisualiserFonts.Fatal);
            default:
                return StatusEnum.super.getFont();
        }
    }
}
