package GlobalData.Tasks.CompilationTask;

import Common.Current;
import Common.Database.*;
import Common.UI.DataSetControlForm;
import Common.UI.Menus.DataTableMenu;
import Common.UI.Menus.TableMenu;
import Common.UI.UI;
import GlobalData.Tasks.RunTask.RunTask;
import Visual_DVM_2021.Passes.PassCode_2021;
import Visual_DVM_2021.Passes.Pass_2021;

import java.util.LinkedHashMap;

import static Common.UI.Tables.TableRenderers.*;

public class CompilationTasksDBTable extends iDBTable<CompilationTask> {
    public CompilationTasksDBTable() {
        super(CompilationTask.class);
    }

    @Override
    public String getDataDescription() {
        return "задача на компиляцию";
    }

    @Override
    public LinkedHashMap<Class<? extends DBObject>, FKBehaviour> getFKDependencies() {
        LinkedHashMap<Class<? extends DBObject>, FKBehaviour> res = new LinkedHashMap<>();
        res.put(RunTask.class, new FKBehaviour(FKDataBehaviour.DELETE, FKCurrentObjectBehaviuor.ACTIVE));
        return res;
    }

    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this) {
            @Override
            public boolean hasCheckBox() {
                return true;
            }

            @Override
            protected void AdditionalInitColumns() {

                columns.get(12).setRenderer(RendererDate);
                columns.get(13).setRenderer(RendererStatusEnum);
            }

            @Override
            public void ShowCurrentObject() throws Exception {
                super.ShowCurrentObject();
                UI.getNewMainWindow().getTestingWindow().ShowCurrentCompilationTask();
            }

            @Override
            public void ShowNoCurrentObject() throws Exception {
                super.ShowNoCurrentObject();
                UI.getNewMainWindow().getTestingWindow().ShowNoCurrentCompilationTask();
            }

            @Override
            public void CreateControl() {
                super.CreateControl();
                DataTableMenu dataTableMenu = new DataTableMenu(control, dataSource, true);
                dataTableMenu.add(Pass_2021.passes.get(PassCode_2021.DeleteSelectedCompilationTasks).getMenuItem());
                control.setComponentPopupMenu(dataTableMenu);
            }
        };
    }

    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "сборка",
                "",
                //
                "Fortran",
                "",
                //
                "С",
                "",
                //
                "С++",
                "",
                "Лимит(c)",
                "Время(c)",
                "Дата",
                "Статус"
        };
    }

    @Override
    public Object getFieldAt(CompilationTask object, int columnIndex) {
        switch (columnIndex) {
            case 2:
                return object.linkerDescription;
            case 3:
                return object.linkFlags;
            case 4:
                return object.fortranCompilerDescription;
            case 5:
                return object.fortranFlags;
            case 6:
                return object.cCompilerDescription;
            case 7:
                return object.cFlags;
            case 8:
                return object.cppCompilerDescription;
            case 9:
                return object.cppFlags;
            case 10:
                return object.maxtime;
            case 11:
                return object.Time;
            case 12:
                return object.getEndDate();
            case 13:
                return object.state;
            default:
                return null;
        }
    }

    @Override
    public Current CurrentName() {
        return Current.CompilationTask;
    }


}
