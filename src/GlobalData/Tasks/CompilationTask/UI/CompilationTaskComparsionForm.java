package GlobalData.Tasks.CompilationTask.UI;

import Common.Current;
import Common.Utils.Utils;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import Visual_DVM_2021.UI.Main.TaskComparsionForm;

public class CompilationTaskComparsionForm extends TaskComparsionForm<CompilationTask> {
    @Override
    protected Current getDefaultTaskType() {
        return Current.CompilationTask;
    }

    public CompilationTaskComparsionForm(TaskComparsionForm<CompilationTask> slave_in) {
        super(slave_in);
        outputTabs.remove(2);
        mCleanTime.setVisible(false);
        mMatrix.setVisible(false);
    }

    @Override
    public void setIcons() {
        outputTabs.setIconAt(0, Utils.getIcon("/icons/CompilationOutput.png"));
        outputTabs.setIconAt(1, Utils.getIcon("/icons/CompilationErrors.png"));
    }
}
