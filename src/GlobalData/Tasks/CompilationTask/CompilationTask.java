package GlobalData.Tasks.CompilationTask;

import Common.Current;
import Common.Global;
import Common.Utils.Utils;
import GlobalData.Makefile.Makefile;
import GlobalData.Module.Module;
import GlobalData.Settings.SettingName;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.Task;
import GlobalData.Tasks.TaskState;
import ProjectData.Files.DBProjectFile;
import ProjectData.LanguageName;
import ProjectData.Project.db_project_info;
import com.sun.org.glassfish.gmbal.Description;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.LinkedHashMap;

public class CompilationTask extends Task {
    public int makefile_id = Utils.Nan;
    public String binary_name = ""; //исполняемый файл.
    //---------------------------------------------------
    @Description("DEFAULT ''")
    public String linkerDescription = "";
    @Description("DEFAULT ''")
    public String linkFlags = "";
    //для отображения.
    //-------------------
    @Description("DEFAULT ''")
    public String fortranCompilerDescription = "";
    @Description("DEFAULT ''")
    public String fortranFlags = "";
    //-------------------
    @Description("DEFAULT ''")
    public String cCompilerDescription = "";
    @Description("DEFAULT ''")
    public String cFlags = "";
    //------------------
    @Description("DEFAULT ''")
    public String cppCompilerDescription = "";
    @Description("DEFAULT ''")
    public String cppFlags = "";

    //---------------------------------------------------
    public void UpdateSummary(int maxtime_in) throws Exception{
        maxtime = maxtime_in;
        Makefile makefile = getMakefile();
        LinkedHashMap<LanguageName, Module> modules = makefile.getModules();
        linkerDescription = makefile.getCompilerDescription();
        linkFlags = makefile.flags;
        for (Module module : modules.values()) {
            switch (module.language) {
                case fortran:
                    fortranCompilerDescription = module.getCompilerDescription();
                    fortranFlags = module.flags;
                    break;
                case c:
                    cCompilerDescription = module.getCompilerDescription();
                    cFlags = module.flags;
                    break;
                case cpp:
                    cppCompilerDescription = module.getCompilerDescription();
                    cppFlags = module.flags;
                    break;
            }
        }
        Global.db.Update(this);
    }
    //---------------------------------------------------
    @Override
    public File getHome() {
        return Global.CompilationTasksDirectory;
    }

    @Override
    public boolean isVisible() {
        return
                Current.CheckID(Current.Machine, machine_id) &&
                        Current.CheckID(Current.User, user_id) &&
                        Current.HasProject() &&
                        belongsToProject(Current.getProject());

    }

    public Makefile getMakefile() {
        return Global.db.getById(Makefile.class, makefile_id);
    }

    @Override
    public String getFullCommand() {
        return "make -j -f Makefile";
    }

    public LinkedHashMap<Integer, RunTask> getRunTasks(){
        LinkedHashMap<Integer, RunTask> res  = new LinkedHashMap<>();
        //так не получится. не правильно назвал ключевое поле. F...
       // return Global.db.getMapByFKi(this, RunTask.class);
        for (RunTask runTask: Global.db.runTasks.Data.values()){
            if (runTask.compilation_task_id==id)
                res.put(runTask.id, runTask);
        }
        return res;
    }

    @Override
    public void AnalyzeResultsTexts(db_project_info project) throws Exception {
        String output = getOutput();
        String errors = getErrors();

        for (DBProjectFile file: project.db.files.Data.values()){
            if (!file.last_assembly_name.isEmpty()){
                String replacement =   file.last_assembly_name+" "+Utils.RBrackets(file.name);
                output = output.replace(file.last_assembly_name, replacement);
                errors = errors.replace(file.last_assembly_name, replacement);
            }
        }
        FileUtils.writeStringToFile(getOutputFile(), output);
        FileUtils.writeStringToFile(getErrorsFile(), errors);
    }
}
