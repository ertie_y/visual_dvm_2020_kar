package GlobalData.Tasks.Passes;
import Common.Current;
import Common.Global;
import Common.Passes.Pass;
import GlobalData.Tasks.Supervisor.TaskSupervisor;
import Visual_DVM_2021.Passes.Pass_2021;
import Visual_DVM_2021.Passes.ProcessPass;

public abstract class TaskLocalPass<S extends TaskSupervisor> extends ProcessPass {
    public S supervisor; //инициализация идет в конструкторе потомка.
    public TaskLocalPass(Class<S> s_class) {
        try {
            supervisor = s_class.newInstance();
        } catch (Exception e) {
            Global.Log.PrintException(e);
        }
    }
    @Override
    protected boolean needsAnimation() {
        return true;
    }
    @Override
    protected void body() throws Exception {
        supervisor.PerformTask();
    }
    @Override
    protected void performFinish() throws Exception {
        supervisor.UpdateTask();
    }
    @Override
    public void Interrupt() throws Exception {
        Current.getProject().CreateInterruptFile();
    }
}
