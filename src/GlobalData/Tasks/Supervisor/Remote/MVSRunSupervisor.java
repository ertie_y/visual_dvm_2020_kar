package GlobalData.Tasks.Supervisor.Remote;
import Common.Current;
import Common.Utils.Utils;
import GlobalData.RemoteFile.RemoteFile;
import GlobalData.Tasks.QueueSystem.MVS;
import GlobalData.Tasks.TaskState;
public class MVSRunSupervisor extends ServerRunSupervisor {
    MVS mvs = new MVS();
    int mvs_time;
    @Override
    protected RemoteFile getRemoteOutput() {
        return new RemoteFile(getRemoteProject().full_name + "/" + task.PID, "output");
    }
    @Override
    protected RemoteFile getRemoteErrors() {
        return new RemoteFile(getRemoteProject().full_name + "/" + task.PID, "errors");
    }
    @Override
    protected String getLaunchScriptText() {
        return task.getFullCommand();
    }
    @Override
    protected int getTaskCheckPeriod() {
        return 5;
    }
    @Override
    public int getMaxtime() {
        return mvs_time * 60;
    }
    @Override
    protected void StartTask() throws Exception {
        String env = String.join(" ", project.getRunConfiguration().getEnvList());
        mvs_time = (task.maxtime / 60); //в минутах
        if (task.maxtime % 60 > 0) mvs_time += 1;
        String res = "maxtime=" + Utils.DQuotes(mvs_time) + " ./run";
        if (!env.isEmpty())
            res = env + " " + res;
        mvs.enqueueTask(pass.ShellCommand(
                "cd " + Utils.DQuotes(getRemoteProject().full_name),
                res), task);
    }
    @Override
    protected boolean isTaskActive() {
        return super.isTaskActive() || task.state.equals(TaskState.Queued);
    }
    @Override
    protected void CheckTask() throws Exception {
        mvs.checkTask(pass.ShellCommand(mvs.getCheckTaskCommand(task)), task);
    }
    @Override
    protected void AbortTask() throws Exception {
        pass.ShellCommand(mvs.getCancelTaskCommand(task));
    }
    @Override
    protected void CoupDeGraceTask() throws Exception {
        //AbortTask(); практика показала что не всегда. например если задача в очереди, а лимит превышен, не сдохнет.
        //хотя, возможно стоит различать лимит вообще, и время пребывания в очереди? ну пока так.
    }
    @Override
    protected void CalculatePerformanceTime() throws Exception {
        task.Time = performanceTime;
    }
}
