package GlobalData.Tasks.Supervisor.Local.Linux;

import Common.Utils.Utils;
import GlobalData.Tasks.RunTask.RunTask;
import GlobalData.Tasks.TaskState;
import ProjectData.Project.db_project_info;

import java.util.Map;

public class LinuxLocalRunSupervisor extends LinuxLocalTaskSupervisor<RunTask> {
    @Override
    protected void PrepareWorkspace() throws Exception {
        super.PrepareWorkspace();
        PrepareRunTaskWorkspace(task);
    }
    @Override
    protected void AchieveResults() throws Exception {
        super.AchieveResults();
        AchieveRunTaskResults(task);
    }

    @Override
    protected Map<String, String> getEnvs() {
        return project.getRunConfiguration().getEnvMap();
    }
    @Override
    protected String getScriptText() {
        return "ulimit -s unlimited\n" + super.getScriptText();
    }


    @Override
    protected void ValidateTaskResults() throws Exception {
        task.AnalyzeResultsTexts(project);
    }

    void kill_mpi() throws Exception {
        Runtime.getRuntime().exec("killall -SIGKILL " + Utils.DQuotes(task.getCompilationTask().binary_name));
    }

    @Override
    protected void AbortTask() throws Exception {
        super.AbortTask(); //убить группу.
        kill_mpi();
    }

    @Override
    protected void CoupDeGraceTask() throws Exception {
        kill_mpi();
    }

}
