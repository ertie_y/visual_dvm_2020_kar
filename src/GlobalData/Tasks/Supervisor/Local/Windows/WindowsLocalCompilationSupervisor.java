package GlobalData.Tasks.Supervisor.Local.Windows;

import Common.Global;
import Common.UI.UI;
import Common.Utils.Utils;
import GlobalData.Makefile.Makefile;
import GlobalData.Settings.SettingName;
import GlobalData.Tasks.CompilationTask.CompilationTask;
import GlobalData.Tasks.TaskState;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class WindowsLocalCompilationSupervisor extends WindowsLocalTaskSupervisor<CompilationTask> {
    @Override
    protected String getKillCommand() {
        return "taskkill /FI \"IMAGENAME eq make.exe\" /F /T" ;
    }

    @Override
    protected String getScriptText() {
        return Utils.DQuotes(Global.db.settings.get(SettingName.LocalMakePathWindows).Value)+" 1>out.txt 2>err.txt";
    }

    //скорее всего как то выделить подготовку к компиляции как метод предка.
    @Override
    protected void PrepareWorkspace() throws Exception {
        //возможно тут сделать отличие. не удалять все, а тоже сверять по дате и именам..
        super.PrepareWorkspace();
        Utils.CheckAndCleanDirectory(getProjectCopy());
        //скопировать проект в папку. без бд.
        project.Clone(getProjectCopy(), false);
        //-удалить старый бинарник если есть? бессмысленно если папка и так полностью пересоздается при каждой компиляции.
        //по идее надо исправить на синхронизацию.
        //if (!task.binary_name.isEmpty()) {
        //    File old_binary = Paths.get(getProjectCopy().getAbsolutePath(), task.binary_name).toFile();
        //    FileUtils.forceDelete(old_binary);
        // }
        //создать мейкфайл.
        Makefile makefile = task.getMakefile();
        File makefile_text = Paths.get(getProjectCopy().getAbsolutePath(), "Makefile").toFile();
        FileUtils.write(makefile_text, makefile.Generate(project));
    }

    //как и валидация.
    @Override
    protected void ValidateTaskResults() throws Exception {
        if (getBinary().exists()) {
            File renamed_binary = Paths.get(getProjectCopy().getAbsolutePath(),
                    Utils.getDateName("spf")+".exe").toFile();
            task.binary_name = renamed_binary.getName();
            Files.move(getBinary().toPath(), renamed_binary.toPath(), StandardCopyOption.REPLACE_EXISTING);
            task.state = TaskState.Done;
        } else task.state = TaskState.DoneWithErrors;
        task.AnalyzeResultsTexts(project);
    }
}
