package GlobalData.Tasks.QueueSystem;
import Common.Utils.Utils;
import GlobalData.Tasks.Task;
import GlobalData.Tasks.TaskState;
public class QueueSystem {
    public String check_command;
    public String cancel_command;
    public String refuse;
    QueueCommand[] commands;
    public void checkTask(String Output, Task task) {
        for (QueueCommand command : commands)
            if (command.check(Output)) {
                task.state = command.state;
                return;
            }
    }
    public void enqueueTask(String Output, Task task) {
        if (Output.equalsIgnoreCase(refuse))
            task.state = TaskState.FailedToQueue;
        else
            for (QueueCommand command : commands) {
                String c_res = command.check_and_get_param(Output);
                if (c_res != null) {
                    task.PID = c_res.replace("\"", "").replace("'", "");
                    task.state = command.state;
                    return;
                }
            }
    }
    public String getCheckTaskCommand(Task task) {
        return check_command + " " + Utils.DQuotes(task.PID);
    }
    public String getCancelTaskCommand(Task task) {
        return cancel_command + " " + Utils.DQuotes(task.PID);
    }
}
