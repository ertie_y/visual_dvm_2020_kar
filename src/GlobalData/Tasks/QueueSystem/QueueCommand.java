package GlobalData.Tasks.QueueSystem;
import Common.Utils.StringTemplate;
import GlobalData.Tasks.TaskState;
public class QueueCommand extends StringTemplate {
    public TaskState state; //состояние задачи в случае успешного разбора этой команды.
    public QueueCommand(TaskState state_in, String p, String s) {
        super(p, s);
        state = state_in;
    }
}
