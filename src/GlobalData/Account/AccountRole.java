package GlobalData.Account;
public enum AccountRole {
    Undefined,
    User,
    Admin;
    public String getDescription() {
        switch (this) {
            case Undefined:
                return "не зарегистрирован";
            case User:
                return "Пользователь";
            case Admin:
                return "Администратор";
            default:
                break;
        }
        return "";
    }
}
