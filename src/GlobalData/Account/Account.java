package GlobalData.Account;

import Common.Database.DBObject;
import Common.UI.UI;
import Common.Utils.TextLog;
import com.sun.org.glassfish.gmbal.Description;
public class Account extends DBObject {
    @Description("PRIMARY KEY,NOT NULL")
    public int id = 0;
    public String name = "?";
    public String email = "?";
    public AccountRole role = AccountRole.Undefined; //роль незареганного пользователя
    @Description("IGNORE")
    public AccountSubscribeState subscription = AccountSubscribeState.Undefined;
    public boolean CheckRegistered(TextLog Log) {
        if (role.equals(AccountRole.Undefined)) {
            Log.Writeln("Пользователь не зарегистрирован");
            UI.getNewMainWindow().FocusCallback();
            return false;
        }
        return true;
    }
    public boolean CheckAuthorship(String address_in, TextLog log){
        if (!isAdmin() && (!email.equals(address_in))) {
            log.Writeln_("Вы не являетесь автором или администратором");
            UI.getNewMainWindow().FocusCallback();
            return false;
        }
        return true;
    }
    public boolean CheckAccessRights(String address_in, TextLog log) {
        return (CheckRegistered(log)&&CheckAuthorship(address_in, log));
    }
    public boolean isAdmin() {
        return role.equals(AccountRole.Admin);
    }
    @Override
    public Object getPK() {
        return id;
    }

}
