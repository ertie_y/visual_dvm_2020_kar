package GlobalData.Account;
import Common.Database.DBTable;
public class AccountsDBTable extends DBTable<Integer, Account> {
    public AccountsDBTable() {
        super(Integer.class, Account.class);
    }
    @Override
    public String getDataDescription() {
        return "аккаунт";
    }
}
