package GlobalData.EnvironmentValue.UI;
import Common.Current;
import Common.Global;
import Common.UI.Menus.StyledMenuItem;
import Common.UI.Menus.StyledPopupMenu;
import GlobalData.EnvironmentValue.EnvironmentValue;
public class EnvironmentValuesMenu extends StyledPopupMenu {
    @Override
    public void CheckElementsVisibility() {
        removeAll();
        for (EnvironmentValue env : Global.db.environmentValues.Data.values()) {
            if (env.run_configuration_id == Current.getProject().getRunConfiguration().id)
                add(new StyledMenuItem(env.toString()));
        }
        super.CheckElementsVisibility();
    }
}
