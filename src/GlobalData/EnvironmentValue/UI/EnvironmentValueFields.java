package GlobalData.EnvironmentValue.UI;
import Common.UI.TextField.StyledTextField;
import Common.UI.Windows.Dialog.DialogFields;

import javax.swing.*;
import java.awt.*;
public class EnvironmentValueFields implements DialogFields {
    public JPanel content;
    public JComboBox cbName;
    public JTextField tfValue;
    public EnvironmentValueFields() {
        cbName.addItem("");
    }
    @Override
    public Component getContent() {
        return content;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
        tfValue=new StyledTextField();
    }
}
