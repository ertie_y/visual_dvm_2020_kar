package GlobalData.EnvironmentValue;
import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
public class EnvironmentValue extends iDBObject {
    public String name = "";
    public String value = "";
    public int machine_id = Utils.Nan; //для удаления машин
    public int run_configuration_id = Utils.Nan;
    @Override
    public boolean isVisible() {
        return Current.HasRunConfiguration() && (run_configuration_id == Current.getRunConfiguration().id);
    }
    @Override
    public String toString() {
        return name + "=" + Utils.DQuotes(value);
    }
}
