package GlobalData.ScenarioCommand;
import Common.Current;
import Common.Database.iDBObject;
import Common.Utils.Utils;
import Visual_DVM_2021.Passes.PassCode_2021;
import com.sun.org.glassfish.gmbal.Description;
public class ScenarioCommand extends iDBObject {
    public int scenario_id = Utils.Nan;
    @Description("DEFAULT 'SPF_ParseFilesWithOrder'")
    public PassCode_2021 passCode=PassCode_2021.SPF_ParseFilesWithOrder;
    public String args = "";

    @Override
    public boolean isVisible() {
        return (Current.getScenario()!=null)&&(Current.getScenario().id==scenario_id);
    }
}
