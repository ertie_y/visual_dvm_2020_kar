package GlobalData.ScenarioCommand.UI;
import Common.Current;
import Common.UI.Tables.StyledCellLabel;
import Common.UI.Windows.Dialog.DialogFields;
import Repository.Component.Sapfor.Sapfor;
import Visual_DVM_2021.Passes.PassCode_2021;

import javax.swing.*;
import java.awt.*;
public class ScenarioCommandFields implements DialogFields {
    private JPanel content;
    public JComboBox<PassCode_2021> cbPassCode;
    @Override
    public Component getContent() {
        return content;
    }
    private void createUIComponents() {
        // TODO: place custom component creation code here
        cbPassCode = new JComboBox<>();
        cbPassCode.setRenderer(new ListCellRenderer<PassCode_2021>() {
            @Override
            public Component getListCellRendererComponent(JList<? extends PassCode_2021> list, PassCode_2021 value, int index, boolean isSelected, boolean cellHasFocus) {
                JLabel res = new StyledCellLabel();
                res.setText(value.getDescription());
                res.setBackground(isSelected ?
                        Current.getTheme().selection_background : Current.getTheme().background
                );
                return res;
            }
        });
        //-
        for (PassCode_2021 code : Sapfor.getAnalysesCodes())
            cbPassCode.addItem(code);
        for (PassCode_2021 code: Sapfor.getAllTransformationsCodes())
            cbPassCode.addItem(code);
    }
}
