package GlobalData.ScenarioCommand;
import Common.Current;
import Common.Database.iDBTable;
import Common.UI.DataSetControlForm;
import Common.UI.UI;
import Common.UI.Windows.Dialog.DBObjectDialog;
import Common.UI.Windows.Dialog.DialogFields;
import GlobalData.ScenarioCommand.UI.ScenarioCommandFields;
import Visual_DVM_2021.Passes.PassCode_2021;
public class ScenarioCommandsDBTable extends iDBTable<ScenarioCommand> {
    public ScenarioCommandsDBTable() {
        super(ScenarioCommand.class);
    }
    @Override
    protected DataSetControlForm createUI() {
        return new DataSetControlForm(this);
    }
    @Override
    public String[] getUIColumnNames() {
        return new String[]{
                "Проход",
                "Аргументы"
        };
    }
    @Override
    public Object getFieldAt(ScenarioCommand object, int columnIndex) {
        switch (columnIndex) {
            case 1:
                return object.passCode.getDescription();
            case 2:
                return object.args;
            default:
                return null;
        }
    }
    @Override
    public Current CurrentName() {
        return Current.ScenarioCommand;
    }
    //-
    @Override
    public DBObjectDialog<ScenarioCommand, ScenarioCommandFields> getDialog() {
        return new DBObjectDialog<ScenarioCommand, ScenarioCommandFields>(ScenarioCommandFields.class) {
            @Override
            public int getDefaultHeight() {
                return 250;
            }
            @Override
            public void fillFields() {
                UI.TrySelect(fields.cbPassCode, Result.passCode);
            }
            @Override
            public void ProcessResult() {
                Result.passCode = (PassCode_2021) fields.cbPassCode.getSelectedItem();
                Result.scenario_id = Current.getScenario().id;
            }
            @Override
            public void validateFields() {

            }
        };
    }
}
