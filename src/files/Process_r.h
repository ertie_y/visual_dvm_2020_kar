#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "fcntl.h"

class Process_r {

public :

	int pid;
	
		
	Process_r();   // конструктор по умолчанию
	
	void Start(); // создание процесса
	void Stop();
	
	
};

Process_r::Process_r(){

	pid = -1;

}



void Process_r::Start(){


	pid = fork();
	
	//потомок
	if (pid ==0 ){
				
		//printf("I am child. my pid is %d, my parent has pid %d\n", getpid(), getppid());
		
	
		int out = open("out.txt",O_RDWR|O_CREAT,0777); //S_IRWXU|S_IRWXG|S_IRWXO);
		int err = open("err.txt",O_RDWR|O_CREAT,0777); //S_IRWXU|S_IRWXG|S_IRWXO);
		
        dup2(out,1);		
		close(out);				
		//-----------------------------								
		dup2(err,2);		
		close(err);			
	
		
	}
	
	//предок
	if (pid>0) {
	
	  //  printf("I am parent. my pid is %d, my child has pid %d\n", getpid(),pid);
								
	}
	
	if (pid <0)
		throw("Can not start console");

}



