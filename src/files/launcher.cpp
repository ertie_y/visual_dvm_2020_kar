#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/wait.h>
#include <time.h>
//--------------------------------------------------------------------------------------------
#include "Process_r.h"



Process_r task;  //задача



void hdl_abort_by_user(int sig)
{	    
  // killpg(0, SIGINT);	   
  // killpg(0, SIGINT);	
   
   killpg(0, SIGKILL);	
   exit(0);	
}

void set_handlers(){
		
	struct sigaction act;
	
	memset(&act, 0, sizeof(act)); //обнуление полей структуры
	act.sa_handler = hdl_abort_by_user; //установка обработчика в поле стурктуры
	
	sigset_t   set;  //маска сигналов, которые буудт блокироваться во время выполнения обработчика
	sigemptyset(&set);         //предваритальеное обнуление всех сигналов                                                     
	sigaddset(&set, SIGINT); 	
	act.sa_mask = set;
	
	sigaction(SIGINT, &act, 0); //установка обработчика
	
	//---------------------------------------------------------------------
		
}


//argv[0] - имя программы



//argv[0] - время
//argv[1] путь к драйверу dvm
//argv[2] f/c(компиляция) или run(запуск)	
//argv[3] имя программы	
//argv[4] .. argv [argc-1] либо флаги компиляции, либо измерения решетки по одному 


int main(int argc, char ** argv){
	
	int i;
	
	time_t time1;
	time_t time2;

//	for (i=0; i<argc; ++i){
//		printf(("argv["+String_::ToString(i)+"] = '%s'\n").string,argv[i]);
		
//	}
	
//	printf("\n");
	char ** args = new char* [argc] ;
		
	int maxtime = atoi(argv[0]);		
//	printf("\nmaxtime = %d\n",maxtime);
	
//	printf("dvm_drv = %'s'\n",argv[1]);
	
		
	for ( i=1; i<argc; ++i)
		args [i-1] = argv[i];
		
	args[argc-1] = NULL;
/*	
	for (i=0; i<argc; ++i){
	   printf("args %d = '%s'\n",i,args[i]);
	   }*/	
	
#if 1
	int pid = getpid();
	
	task.Start();	
		
	//потомок	- задача	
	if (task.pid ==0 )		
		execvp (argv[1],args);	

		
	time1 = time((time_t *)0);

	
	int s;
		
	double total_time = 0.;


	
	int timer_pid = fork();
		
	if (timer_pid ==0){	
	     
		sleep(maxtime);
		
		FILE * killed = fopen("TIMEOUT","w");	
        fprintf(killed,"task was killed by timer\n");
        fclose(killed);
		
		kill(pid,SIGINT);	  
       
        exit(0);	
		   		
	}
	
	set_handlers();
	
	waitpid(task.pid,&s,WUNTRACED);
	kill(timer_pid,SIGKILL);
	

	time2 = time((time_t *)0);

	total_time = difftime(time2, time1);
	

	FILE * tt = fopen("total_time","w");	
	fprintf(tt,"%.5lf",total_time);
	fclose(tt);

	FILE * done  = fopen("DONE","w");	
	fprintf(done,"task finished correctly");
	fclose(done);		
	

	
#endif
		   
	return 0;
}


